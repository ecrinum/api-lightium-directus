# Explorateur des données Lightium pour ÉcrituresNumériques.ca (2016-2023)

Ce projet permet d'explorer la base de données SQLITE de l'ancien site ÉcrituresNumériques.ca, qui fonctionnait sur Lightium (2016-2023).
Il permet également de récupérer des collections au format JSON, pour importation éventuelle dans un autre système.

Ce projet est construit avec [SvelteKit](https://kit.svelte.dev).

## Présentation sommaire des points d'accès

La page d'accueil `/` documente la plupart des points d'accès.

- exemple: `/items.json` liste des items en json (pratique).
- exemple: `/items/200.fr.json` pour l’item avec l’ID `200` en format json/
- exemple: `/categories.json` liste des catégories en json.
- exemple: `/items/200.fr.md` pour l’item avec l’ID `200`, en format markdown.

## Importation dans Directus

L'objectif est de migrer les données dans la base SQLITE vers un autre système.
Nous utiliserons Directus (projet séparé).

Les mécanismes de migration sont écrites dans des points d'accès qu'on peut déclencher avec des requêtes `POST`.
Chacune de ces requêtes doit être pourvu d'un un `body` (`Content-Type: application/json`) rempli comme suit, au format JSON :

```
{
  "email": "user@email.com",
  "password: "passw"
}
```

Voici les étapes à suivre dans l'ordre pour effectuer l'importation dans Directus.

### Étape 0. Préparer la base de données dans Directus
Créer une installation Directus (voir le [projet séparé](https://gitlab.huma-num.fr/ecrinum/plateforme-donnees-crcen)).
On peut utiliser le fichier `schema.yml` (_snapshot_) pour reproduire le schéma de la base de données.

Il faudra aussi créer manuellement les langues (français, `fr`, et anglais, `en`).

### Étape 1. Migration des entités abstraites (axes de recherche, concepts clés, etc.)
Cette étape doit être faite en premier pour pouvoir lier les autres entités (événements, projets) par la suite.

#### Étape 1.1. Importer les axes de recherche

```
POST /sous-categories/import-axes
```

#### Étape 1.2. Importer les concepts clés

```
POST /sous-categories/import-concepts
```

#### Étape 1.3. Importer les champs de recherche

```
POST /sous-categories/import-champs-de-recherche
```

#### Étape 1.4. Importer les objets de recherche

```
POST /sous-categories/import-objets-de-recherche
```

### Étape 2. Migration des personnes
Dans Lightium, les personnes constituaient une « sous-catégorie ».
Nous allons récupérer chacune de ces sous-catégories pour en faire des entités à part entière.

#### 2.1 Importer les personnes

```
POST /equipe/import
```

### Étape 3. Migration des événements legacy
Dans Lightium, les événements « legacy » se trouvent dans une table à part.
Il s'agit d'une collection qui a été utilisée plus tôt dans la vie du site, mais qui a été abandonnée en cours de route au profit des `items` (gérés à l'étape suivante). 

#### Étape 3.1 Importer les événements legacy

```
POST /evenements-legacy/import
```

### Étape 4. Migration des items
Les items comprennent les événements (distincts des « événements legacy »), les projets et les publications.
Les publications sont entreposées et modifiées dans Zotero ; il n'est pas nécessaire pour le moment de les transférer intégralement.
Néanmoins, des relations avec des entités (concepts, objets de recherche, etc.) ont été faites dans Lightium et peuvent être retracées avec le champ `zoterokey`.

#### Étape 4.1 Importer les événements

```
POST /evenements/import
```

#### Étape 4.2 Importer les projets

```
POST /projets/import
```

#### Étape 4.2 Importer les publications

```
POST /publications/import
```

### Étape 5. Création des relations
Dans Lightium, les items étaient liés à des sous-catégories de manière indifférenciées (axes de recherche, concepts clés, personnes, etc.).
Dans Directus, nous avons mis en place des champs qui différencient les différentes entités et relations (un axe de recherche est ontologiquement différent d'une personne, d'où des modèles de données différentes dans la base de données).
Il s'agira d'établir les relations en recoupant les sous-catégories appropriées (ex. ne pas tenter de lier une personne dans le champ des concepts clés, et vice-versa).

#### 5.1 Lier les événements

```
POST /evenements/relations
```

#### 5.2 Lier des événements legacy

Les événenements legacy étant dans une table à part dans Lightium, ils n'ont pas de relations avec les sous-catégories.

#### 5.3 Lier les projets

```
POST /projets/relations
```

#### 5.4 Lier les publications

```
POST /publications/relations
```

---

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

Before creating a production version of your app, install an [adapter](https://kit.svelte.dev/docs#adapters) for your target environment. Then:

```bash
npm run build
```

> You can preview the built app with `npm run preview`, regardless of whether you installed an adapter. This should _not_ be used to serve your app in production.
