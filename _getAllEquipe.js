import http from 'http';

let itemsJson = [];

getIndex();

function getIndex() {
  const httpOptions = {
    hostname: 'localhost',
    port: 3000,
    path: `/equipe.json`
  };

  const req = http.get(httpOptions, async res => {
      console.log(`GET /equipe.json ${res.statusCode}`);
      const chunks = [];

      res.on('data', d => {
        chunks.push(d)
      });

      res.on('end', () => {
        const rawData = Buffer.concat(chunks).toString();
        itemsJson = JSON.parse(rawData);

        // Here we go…
        getEachEquipe()
      });
    });
}

function getEachEquipe() {
  console.info('START getEachEquipe');
  let counter = 0;
  _getItem(); // start with zero

  function _getItem() {
    // prevent undefined items
    if (itemsJson[counter]) {
      let file = `${itemsJson[counter].id_subcat}.${itemsJson[counter].lang.toLowerCase()}.md`;
      http.get({
        host: 'localhost',
        port: 3000,
        path: `/equipe/${file}`
      }, res => {
        if (res.error) {
          console.error(`ERROR -- GET /equipe/${file} - ${res.statusCode}`)
        } else {
          console.log(`GET /equipe/${file}- ${res.statusCode}`);
        }
        counter++
        _getItem(); // finished, on to the next one
      });
    }
    console.info('END getEachEquipe');
  }
}
