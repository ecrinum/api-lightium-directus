import http from 'http';

let itemsJson = [];

getIndex();


function getIndex() {
  const httpOptions = {
    hostname: 'localhost',
    port: 3000,
    path: `/items.json`
  };

  const req = http.get(httpOptions, async res => {
      console.log(`GET /items.json ${res.statusCode}`);
      const chunks = [];

      res.on('data', d => {
        chunks.push(d)
      });

      res.on('end', () => {
        const rawData = Buffer.concat(chunks).toString();
        itemsJson = JSON.parse(rawData);

        // Here we go…
        getEachItem()
      });
    });
}

function getEachItem() {
  console.info('START getEachItem');
  let counter = 0;
  _getItem(); // start with zero

  function _getItem() {
    // prevent undefined items
    if (itemsJson[counter]) {
      let file = `${itemsJson[counter].id_item}.${itemsJson[counter].lang.toLowerCase()}.md`;
      http.get({
        host: 'localhost',
        port: 3000,
        path: `/items/${file}`
      }, res => {
        if (res.error) {
          console.error(`ERROR -- GET /items/${file} - ${res.statusCode}`)
        } else {
          console.log(`GET /items/${file}- ${res.statusCode}`);
        }
        counter++
        _getItem(); // finished, on to the next one
      });
    }
    console.info('END getEachItem');
  }
}
