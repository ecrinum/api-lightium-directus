---
name: Projets
short: ""
cleanstring: Projets
lang: FR
caption: ""
description: La CRC est impliquée dans une série de projets théoriques, éditoriaux et techniques. Certains de nos projets sont financés par le FRQSC ou le CRSH.
image: https://upload.wikimedia.org/wikipedia/commons/5/58/Mercator_World_Map.jpg
categories: null

---

