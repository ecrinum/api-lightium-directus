---
name: Publications
short: ""
cleanstring: Publications
lang: EN
caption: ""
description: "The Canada Research Chair in digital textualities advocates for open access. All its publications are available on this site. You can also access the publications of the members of the Chair on <a href=\"https://papyrus.bib.umontreal.ca/xmlui/handle/1866/12963\">Papyrus</a>, the institutional repository of Université de Montréal, or on <a href=\"https://www.zotero.org/groups/critures_numriques/items\">Zotero</a>.\r

  </h2><br/>\r

  <table width=\"100%\">\r

  \r

  <tr>\r

  \r

  <td>\r

  <a href=\"http://ecrituresnumeriques.ca/fr/Axes\">Par axes de recherche</a>\r

  </td>\r

  <td>\r

  <a href=\"http://ecrituresnumeriques.ca/fr/Champs-de-recherche\">Par champs de recherche</a>\r

  </td>\r

  <td>\r

  <a href=\"http://ecrituresnumeriques.ca/fr/Objets-de-recherche\">Par objets de recherche</a>\r

  </td>\r

  <td>\r

  <a href=\"http://ecrituresnumeriques.ca/fr/Concepts-cles\">Par concepts clés</a>\r

  </td>\r

  <td>\r

  <a href=\"http://ecrituresnumeriques.ca/fr/Equipe\">Par auteur</a>\r

  </td></tr>\r

  <table>\r

  <h2>"
image: http://i.imgur.com/fHFWgju.png
categories: null

---

