---
name: Publications
short: ""
cleanstring: Publications
lang: FR
caption: ""
description: "La Chaire de recherche du Canada sur les écritures numériques milite pour l'accès libre. Toutes ses publications sont disponibles en accès libre sur ce site. Vous pouvez aussi accéder à l'ensemble des publications des membres de la Chaire sur le <a href=\"https://papyrus.bib.umontreal.ca/xmlui/handle/1866/12963\">dépôt institutionnel de l'Université de Montréal</a> ou sur <a href=\"https://www.zotero.org/groups/critures_numriques/items\">Zotero</a>.\r

  \r

  </h2><br/>\r

  <table width=\"100%\">\r

  \r

  <tr>\r

  \r

  <td>\r

  <a href=\"http://ecrituresnumeriques.ca/fr/Axes\">Par axes de recherche</a>\r

  </td>\r

  <td>\r

  <a href=\"http://ecrituresnumeriques.ca/fr/Champs-de-recherche\">Par champs de recherche</a>\r

  </td>\r

  <td>\r

  <a href=\"http://ecrituresnumeriques.ca/fr/Objets-de-recherche\">Par objets de recherche</a>\r

  </td>\r

  <td>\r

  <a href=\"http://ecrituresnumeriques.ca/fr/Concepts-cles\">Par concepts clés</a>\r

  </td>\r

  <td>\r

  <a href=\"http://ecrituresnumeriques.ca/fr/Equipe\">Par auteur</a>\r

  </td></tr>\r

  </table>\r

  <h2>"
image: ""
categories: null

---

