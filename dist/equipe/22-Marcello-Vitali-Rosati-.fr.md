---
name: "Marcello Vitali-Rosati "
short: "Titulaire de la Chaire\r

  <br>Professeur agrégé"
cleanstring: Marcello-Vitali-Rosati-
lang: FR
caption: ""
description: "Marcello Vitali-Rosati est professeur agrégé au département des littératures de langue française de l'Université de Montréal et titulaire de la Chaire de recherche du Canada sur les écritures  numériques.\r

  \r

  Il développe une réflexion philosophique sur les enjeux des technologies numériques: le concept de virtuel, l'identité numérique, les notions d’auteur et d'autorité, les formes de production, légitimation et circulation du savoir à l'époque du web, et la théorie de l'éditorialisation - dont il est l'un des contributeurs les plus actifs.\r

  \r

  Il est l'auteur de nombreux articles et monographies et exerce également une activité d'éditeur en tant que directeur de la revue Sens public et co-directeur de la collection \"Parcours Numériques\" aux Presses de l'Université de Montréal. \r

  \r

  En tant que titulaire de la Chaire sur les écritures numériques, il dirige plusieurs projets en  humanités numériques, particulièrement dans le domaine de l'édition savante. C'est dans ce cadre que sont notamment développés des plateformes d'édition de revues et de monographies enrichies, un logiciel d'édition d'articles scientifiques ainsi qu'une plateforme d'édition collaborative du Codex Palatinus 23."
image: http://vitalirosati.net/chaire/img/mvr.jpg
categories: null

---

