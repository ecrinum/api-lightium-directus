---
name: Marcello Vitali-Rosati
short: "Chairholder\r

  <br>Associate Professor"
cleanstring: Marcello-Vitali-Rosati
lang: EN
caption: ""
description: "Marcello Vitali-Rosati is Associate Professor in the Department of French\r

  Literature at the University of Montreal and holds the Canada Research\r

  Chair on Digital Textualities. He studied Philosophy and Literature and holds\r

  a Ph.D. from the University of Pisa (Italy) and the University of Paris IV La\r

  Sorbonne. Previous publications include several articles and four books: <i>Riflessione e trascendenza: Itinerari a partire da Lévinas</i> (ETS, 2003), <i>Corps et\r

  virtuel: Itinéraires à partir de Merleau-Ponty</i> (Harmattan, 2009), <i>S'orienter\r

  dans le virtuel</i> (Hermann, 2012), and <i>Égarements: Amour, mort et identités\r

  numériques</i> (Hermann, 2014). \r

  <br>\r

  <br>He has taught at the Foreign University of Perugia (Italy) and at art and technology schools in Paris.<br>\r

  <br>\r

  His research focuses on developing a philosophical re\x1dflection on the changes brought by digital technologies - the idea of digital identity, the concept\r

  of the author in the era of the Web, the forms of production, publication and\r

  diffusion of online content, and the notion of “editorialization”.</h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:marcello.vitali.rosati@umontreal.ca\">marcello.vitali.rosati@umontreal.ca</a>"
image: http://vitalirosati.net/chaire/img/mvr.jpg
categories: null

---

