---
name: Emmanuel Château-Dutier
short: Assistant Teacher
cleanstring: Emmanuel-Chateau-Dutier
lang: EN
caption: ""
description: "Emmanuel Château-Dutier is an architectural historian and digital humanist. His research looks at the administration of public architecture in France during the 19th century, the profession of the architect and relationships between the mastery of work and the mastery of artwork, the architecture of zoological gardens, and architectural publishing. His work focuses on, among other subjects, museology and the history of digital art.</h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:emmanuel.chateau.dutier@umontreal.ca\">emmanuel.chateau.dutier@umontreal.ca</a>"
image: http://vitalirosati.net/chaire/img/ecd.jpg
categories: null

---

