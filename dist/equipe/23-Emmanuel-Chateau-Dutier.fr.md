---
name: Emmanuel Château-Dutier
short: Professeur adjoint
cleanstring: Emmanuel-Chateau-Dutier
lang: FR
caption: ""
description: "Emmanuel Château-Dutier est historien de l’architecture et digital humanist. Ses recherches portent sur l’administration de l’architecture publique en France au XIXe siècle, la profession d’architecte et les relations entre la maîtrise d’ouvrage et la maîtrise d’œuvre, l’architecture des jardins zoologiques, l’édition et le livre d’architecture. Ses travaux concernent par ailleurs la muséologie et l’histoire de l’art numérique.</h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:emmanuel.chateau.dutier@umontreal.ca\">emmanuel.chateau.dutier@umontreal.ca</a>"
image: http://vitalirosati.net/chaire/img/ecd.jpg
categories: null

---

