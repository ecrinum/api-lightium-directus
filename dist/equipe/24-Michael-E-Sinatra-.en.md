---
name: "Michael E. Sinatra "
short: Professor
cleanstring: Michael-E-Sinatra-
lang: EN
caption: ""
description: "I am Professor of English at the University of Montreal. Trained as a Romanticist at Oxford and a specialist of Leigh Hunt, I have been involved in electronic publications and digital humanities for twenty years. I am the founding editor of the SSHRC-funded electronic peer-reviewed journal, <i>Romanticism on the Net</i> (founded in 1996 in Oxford and hosted on the Érudit platform since 2002), which expanded into the Victorian period in 2007 and changed its name to <i>Romanticism and Victorianism on the Net</i> (RaVoN). With Marcello Vitali-Rosati, I launched an innovative publication series entitled “Parcours numériques” in the spring of 2014, which includes our volume <i>Manuel des pratiques de l’édition numérique</i>. I am also the team leader of the “Groupe de recherche sur les éditions critiques en contexte numérique“.</h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:michael.eberle.sinatra@umontreal.ca\">michael.eberle.sinatra@umontreal.ca</a>"
image: http://vitalirosati.net/chaire/img/mes.jpg
categories: null

---

