---
name: Michael E. Sinatra
short: Professeur titulaire
cleanstring: Michael-E-Sinatra
lang: FR
caption: ""
description: "Micheal E. Sinatra est professeur titulaire d'anglais au Département de littératures et de langues du monde de l'Université de Montréal. Il est aussi le directeur du groupe de recherche GREN et le co-éditeur général de la série Parcours numériques.\r

  </h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:michael.eberle.sinatra@umontreal.ca\">michael.eberle.sinatra@umontreal.ca</a>"
image: http://vitalirosati.net/chaire/img/mes.jpg
categories: null

---

