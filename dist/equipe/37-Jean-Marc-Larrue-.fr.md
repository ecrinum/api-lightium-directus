---
name: "Jean-Marc Larrue "
short: Professeur agrégé
cleanstring: Jean-Marc-Larrue-
lang: FR
caption: ""
description: "Après une maîtrise (Université McGill) et un doctorat (Université de Montréal) sur le théâtre à Montréal à la Belle Époque, il a poursuivi ses recherches sur les périodes subséquentes, en particulier la postmodernité et la période contemporaine. L’avènement de l’électricité (fin XIXe siècle) et des technologies numériques (fin du XXe) ayant profondément transformé les pratiques théâtrales, il consacre l’essentiel de ses recherches, depuis une dizaine d’années, aux questions d’intermédialité, de performativité et de théâtralité.</h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:jean-marc.larrue@umontreal.ca\">jean-marc.larrue@umontreal.ca</a>"
image: http://vitalirosati.net/chaire/img/jml.jpg
categories: null

---

