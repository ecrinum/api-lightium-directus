---
name: Jean-Marc Larrue
short: Associate Professor
cleanstring: Jean-Marc-Larrue
lang: EN
caption: ""
description: "Following a Master’s degree (McGill University) and a Ph.D. (University of Montreal) on Montreal theatre during the Belle Époque, Jean-Marc pursued research on subsequent periods, particularly postmodernity and the contemporary period. The advent of electricity (end of the 19th century) and of digital technology (end of the 20th century) having deeply transformed theatrical practices, as such, he has dedicated the core of his research over the last dozen years to questions of intermediality, performativity and theatricality.</h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:jean-marc.larrue@umontreal.ca\">jean-marc.larrue@umontreal.ca</a>"
image: http://vitalirosati.net/chaire/img/jml.jpg
categories: null

---

