---
name: Marie-Christine Corbeil
short: Administrative coordinator<br>Master's Student
cleanstring: Marie-Christine-Corbeil
lang: EN
caption: ""
description: "Marie-Christine Corbeil is a Master’s student in French Literature and Language at the University of Montreal. Her research currently addresses cookbooks from the early 1960s. She is particularly interested in narrative spaces that are found within these texts and the way they question the author’s role. She also participates in the research team’s work for the project “La presse montréalaise  de l’entre-deux-guerres, lieu de transformation de la vie culturelle et de l’espace public,” a SSHRC-funded project led by Micheline Cambron.</h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:marie-christine.corbeil@umontreal.ca\">marie-christine.corbeil@umontreal.ca\r

  </a>"
image: http://vitalirosati.net/chaire/img/mcc.jpg
categories: null

---

