---
name: Marie-Christine Corbeil
short: Coordinatrice administrative <br>Étudiante à la maîtrise
cleanstring: Marie-Christine-Corbeil
lang: FR
caption: ""
description: "Marie-Christine Corbeil est étudiante à la maîtrise en littératures de langue française à l’Université de Montréal. Ses recherches portent présentement sur les livres de recettes du début des années 1960. Elle s’intéresse plus particulièrement aux espaces de récits qu’on y trouve et à la façon dont ces textes interrogent la fonction d’auteur. Elle participe, par ailleurs, aux travaux de l’équipe de recherche du projet La presse montréalaise de l’entre-deux-guerres, lieu de transformation de la vie culturelle et de l’espace public (CRSH) dirigé par Micheline Cambron.\r

  </h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:marie-christine.corbeil@umontreal.ca\">marie-christine.corbeil@umontreal.ca\r

  </a>"
image: http://vitalirosati.net/chaire/img/mcc.jpg
categories: null

---

