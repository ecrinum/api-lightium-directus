---
name: "Servanne Monjour "
short: Alumni<br />Postdoctorante
cleanstring: Servanne-Monjour-
lang: FR
caption: ""
description: "Servanne Monjour est postdoctorante au département des littératures de langue française de l’Université de Montréal, où elle travaille au sein de la Chaire de recherche du Canada sur les écritures numériques (titulaire Marcello Vitali-Rosati). Ses travaux portent sur les nouvelles mythologies de l’image à l’ère du numérique. Depuis 2014, elle est coordonnatrice de la revue numérique Sens public.</h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:servanne.monjour@umontreal.ca\">servanne.monjour@umontreal.ca\r

  </a>"
image: http://vitalirosati.net/chaire/img/sm.jpg
categories: null

---

