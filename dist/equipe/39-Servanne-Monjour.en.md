---
name: Servanne Monjour
short: Alumni<br />Postdoctoral fellow
cleanstring: Servanne-Monjour
lang: EN
caption: ""
description: "Servanne Monjour is a postdoctoral fellow in the Department of French Literature and Language at the University of Montreal where she works within the Canada Research Chair on Digital Textualities (held by Marcello Vitali-Rosati). Her work looks at new mythologies of the image in the digital era. Since 2014, she coordinates the digital journal <i>Sens Public<i>.</h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:servanne.monjour@umontreal.ca\">servanne.monjour@umontreal.ca\r

  </a>"
image: http://vitalirosati.net/chaire/img/sm.jpg
categories: null

---

