---
name: Élisabeth Routhier
short: Ph.D. holder
cleanstring: Elisabeth-Routhier
lang: EN
caption: ""
description: "Élisabeth Routhier holds a Ph.D. in Comparative Literature from University of Montreal. Her researches lie at the confluence of literature, cinema and media studies, thus informing an intermedial stance through which she addresses different modalities of remediation. Following her dissertation on the relation between disappearance and remediation in the work of Georges Perec, Patrick Modiano and Christopher Nolan, she intends to take on postdoctoral researches oriented towards the mobilopgraphy of GoPro cameras. As a member of the Canada Research Chair on Digital Textualities, Élisabeth Routhier more specifically published papers reflecting on the influence of a digital culture over practices of reading and modalities of academic research. She is also a member of the Lab-Doc (Laboratoire de recherche sur les pratiques audiovisuelles documentaires) and a researcher within the international research project “Deceptive Arts: Machines, Magic, Media”.</h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:elisabethrouthier@hotmail.com\">elisabethrouthier@hotmail.com</a>"
image: https://i.imgur.com/daCSXRA.jpg
categories: null

---

