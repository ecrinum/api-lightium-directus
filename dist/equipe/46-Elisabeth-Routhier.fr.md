---
name: Élisabeth Routhier
short: Membre Docteure
cleanstring: Elisabeth-Routhier
lang: FR
caption: ""
description: "Élisabeth Routhier est titulaire d’un doctorat en littérature comparée de l’Université de Montréal. Ses recherches, situées au confluent de la littérature, du cinéma et des <i>media studies</i>, informent une posture résolument intermédiale à partir de laquelle elle étudie diverses modalités de la remédiation. Sa thèse de doctorat porte plus spécifiquement sur les rapports entre disparition et remédiation chez Georges Perec, Patrick Modiano et Christopher Nolan. Dans le cadre des activités de la Chaire de recherche du Canada sur les écritures numériques, elle a également publié des articles portant sur l’influence de la culture numérique sur les modalités de la lecture et de la recherche. Elle est membre du Lab-Doc et chercheure au sein du projet de recherche international « Les Arts Trompeurs. Machines, Magie, Médias », lequel l’amène à interroger les rapports entre magie, mobilographie et pratiques numériques. \r

  </h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:elisabethrouthier@hotmail.com\">elisabethrouthier@hotmail.com</a>"
image: https://i.imgur.com/daCSXRA.jpg
categories: null

---

