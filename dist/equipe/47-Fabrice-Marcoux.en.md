---
name: Fabrice Marcoux
short: "Master's Student "
cleanstring: Fabrice-Marcoux
lang: EN
caption: ""
description: "Fabrice Marcoux is a Master’s student in the Department of French Literature and Language at the University of Montreal, under the direction of Marcello Rosati-Vitali. He holds a specialized Bachelor’s degree in Philosophy and a certificate in Digital Information Management from EBSI (École de bibliothéconomie et des sciences de l’information). His thesis examined elements of digital culture in the “décentrements” collection at Publie.net. The issue of the transmission of knowledge and culture is at the heart of his concerns. He authored “Le livre et le format ePub”, chapter 11 of the textbook <i>Pratiques de l’édition numérique</i>, a collective text published by the Montreal University Press (PUM) under the direction of Marcello Vitali-Rosati and Michael E. Sinatra in March 2014.\r

  </h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:homegnolia@gmail.com\">homegnolia@gmail.com\r

  </a>"
image: http://vitalirosati.net/chaire/img/fm.jpg
categories: null

---

