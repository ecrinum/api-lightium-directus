---
name: Fabrice Marcoux
short: Étudiant à la Maîtrise
cleanstring: Fabrice-Marcoux
lang: FR
caption: ""
description: "Après des études en communications, un baccalauréat spécialisé en philosophie et des recherches sur les théories du jugement critique (esthétique), Fabrice a effectué un certificat en gestion de l’information numérique à l’EBSI et mis en pratique ces enseignements en tant que webmestre et responsable des communications pour différents organismes.\r

  Il a été assistant pour les séminaires ‟Écritures numériques et éditorialisation” (2013-2015). Il participe aux travaux du groupe de recherche sur les théories de la littérature numérique (ThéoLiNum) et de la Chaire de recherche du Canada sur les écritures numériques, et s’intéresse plus particulièrement à la transformation des pratiques éditoriales en relation avec l’évolution des plateformes de publication. \r

  Ayant complété la rédaction de son mémoire qui met en dialogue l’humanisme numérique de Doueihi et l’écologie des media de McLuhan, il est actuellement réalisateur de livres numériques et consultant en édition numérique à son compte chez Fabrix Livres. La question de la transmission des connaissances et des cultures se situe au cœur de ses préoccupations. Il est l’auteur de ‟ Le livrel et le format ePub ”, chapitre du manuel <i>Pratiques de l’édition numérique</i>, sous la direction de Marcello Vitali-Rosati et Michael E. Sinatra.\r

  </h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:homegnolia@gmail.com\">homegnolia@gmail.com\r

  </a>"
image: http://vitalirosati.net/chaire/img/fm.jpg
categories: null

---

