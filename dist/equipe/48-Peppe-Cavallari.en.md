---
name: Peppe Cavallari
short: PhD Holder
cleanstring: Peppe-Cavallari
lang: EN
caption: ""
description: "Peppe Cavallari holds a doctorate in philosophy and information and communication sciences from the Compiègne University of Technology and comparative literature (intermedial studies option) at the Université de Montréal.\r

  Since 2012 he has been a lecturer in Philosophy and Digital Culture at Hétic, while, as a lecturer, teaches Digital Culture at SciencesPo, History and Prospects for Cultural Industries and Digital Media at UTC, Document Engineering and Web writing at Paris Descartes University. He has also taught at Paris Nanterre\r

  University (Digital Culture), at Paris 13 University (History of the Media) and at the University of Rouen (Collective Identities). He is scientific director of A glass behind the screen, the digital culture seminar of Hétic school (in partnership with the Chair and the CRIHN). "
image: http://vitalirosati.net/chaire/img/pc.jpg
categories: null

---

