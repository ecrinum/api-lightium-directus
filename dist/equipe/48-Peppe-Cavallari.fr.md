---
name: Peppe Cavallari
short: Docteur
cleanstring: Peppe-Cavallari
lang: FR
caption: ""
description: "Peppe Cavallari est docteur en philosophie et en sciences de l'information e de la communication à l'Université de Technologie de Compiègne et en littérature comparée (option études intermédiales) à l'Université de Montréal.\r

  Depuis 2012 il est enseignant titulaire du cours en Philosophie et culture numérique à Hétic, tandis que, en tant que chargé de cours, enseigne Culture numérique à SciencesPo, Histoire et prospectives des industries culturelles et des médias numériques à l'UTC, Ingénierie documentaire et rédaction web à l'Université Paris Descartes. Il a enseigné aussi à l'Université Paris Nanterre (Culture numérique), à l'Université Paris13 (Histoire des moyens de communication) et à l'Université de Rouen (Identités collectives). Il est responsable scientifique de Un verre derrière l'écran, le séminaire de culture numérique de l'école Hétic (en partenariat avec la Chaire et le CRIHN). "
image: http://vitalirosati.net/chaire/img/pc.jpg
categories: null

---

