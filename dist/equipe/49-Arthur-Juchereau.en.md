---
name: Arthur Juchereau
short: "Developer "
cleanstring: Arthur-Juchereau
lang: EN
caption: ""
description: "Arthur Juchereau holds a D.E.S.S. in Arts, Creation and Technology, as well as a Master’s degree in Information Sciences (Network Architecture and Internet Development). For many years, he has worked in the field of personalized internet platform development.</h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:arthur.juchereau@gmail.com\">arthur.juchereau@gmail.com\r

  </a>"
image: http://vitalirosati.net/chaire/img/aj.jpg
categories: null

---

