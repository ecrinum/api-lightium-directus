---
name: Arthur Juchereau
short: "Développeur  "
cleanstring: Arthur-Juchereau
lang: FR
caption: ""
description: "Arthur Juchereau détient un D.E.S.S. en Arts, Création et Technologies ainsi qu'une maîtrise en Informatique (Architecture réseau et Développement Internet). Il travaille depuis de nombreuses années dans le domaine du développement de plateformes internet personnalisées.</h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:arthur.juchereau@gmail.com\">arthur.juchereau@gmail.com\r

  </a>"
image: http://vitalirosati.net/chaire/img/aj.jpg
categories: null

---

