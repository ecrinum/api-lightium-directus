---
name: Jean-François Thériault
short: PhD candidate
cleanstring: Jean-Francois-Theriault
lang: EN
caption: ""
description: "Jean-Francois is currently writing a Master’s thesis, at the University of Montreal, on the reactualization of theories of reading in the digital age. His work is funded by the SSRHC and FRQSC. He is interested in contemporary Quebecois literature, its modes of reception, and questions of popular culture. He is a Research Assistant for various projects that focus on contemporary literature and digital culture. </h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:jfrancois.theriault@gmail.com\">jfrancois.theriault@gmail.com\r

  </a>"
image: http://vitalirosati.net/chaire/img/jft.jpg
categories: null

---

