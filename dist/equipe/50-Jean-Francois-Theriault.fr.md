---
name: Jean-François Thériault
short: Candidat au doctorat
cleanstring: Jean-Francois-Theriault
lang: FR
caption: ""
description: "Jean-François Thériault rédige actuellement, à l’Université de Montréal, un mémoire de maitrise qui touche la réactualisation des théories de la lecture à l’ère du numérique. Ses travaux sont financés par le Conseil de recherches en sciences humaines du Canada et par le Fond de recherche du Québec – Société et culture. Il s’intéresse à la littérature québécoise contemporaine, à ses modes de réception et aux questions de culture populaire. Il est assistant de recherche pour divers projets qui touchent la littérature contemporaine et la culture numérique. </h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:jfrancois.theriault@gmail.com\">jfrancois.theriault@gmail.com\r

  </a>"
image: http://vitalirosati.net/chaire/img/jft.jpg
categories: null

---

