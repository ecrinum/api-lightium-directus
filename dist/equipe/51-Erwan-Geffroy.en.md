---
name: Erwan Geffroy
short: PhD Candidate
cleanstring: Erwan-Geffroy
lang: EN
caption: ""
description: "Erwan Geffroy is a PhD candidate at the University of Montreal (Faculty of Arts and Sciences, Department of World Literatures and Languages, under the direction of Jean-Marc Larrue) and the University of Rennes 2 (Doctoral School Arts, Letters, Languages, Research group History and Art Criticism, under the direction of Pierre-Henry Frangne). In his thesis, he is examining the notion of intermediality in contemporary artistic practices, primarily in the fields of the plastic and performing arts. He is also a student member of the Centre de recherches intermediales sur les arts, les lettres et les techniques (CRIalt), Research Assistant  for the Centre de recherche interuniversitaire sur la littérature et la culture québécoise (CRILCQ) and Coordinator of the Laboratoire sur les récits du soi mobile. He obtained a Diplôme National Supérieure d’Expression Plastique from the École Supérieure d’Art de Bretagne (France) in 2011.  His artistic practice, strongly tied to his theoretical research, examines concepts of abstraction and interpretation. In this, he focuses primarily on installation and performance.  </h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:erwan.geffroy@gmail.com\">erwan.geffroy@gmail.com\r

  </a>"
image: http://vitalirosati.net/chaire/img/eg.jpg
categories: null

---

