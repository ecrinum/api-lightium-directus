---
name: Erwan Geffroy
short: Candidat au doctorat
cleanstring: Erwan-Geffroy
lang: FR
caption: ""
description: "Erwan Geffroy est doctorant en cotutelle entre l’Université de Montréal (Faculté des Arts et des Sciences, département de Littératures et de langues du monde, sous la direction de Jean-Marc Larrue) et l’Université Rennes 2 (École doctorale Arts, lettres, langues, unité de recherche Histoire et critique des arts, sous la direction de Pierre-Henry Frangne). Il étudie, pour sa thèse, la notion d’intermédialité dans les pratiques artistiques contemporaines, principalement dans les champs des arts plastiques et des arts vivants.\r

  \r

  <br>Il est également membre étudiant du Centre de recherches intermédiales sur les arts, les lettres et les techniques (CRIalt), auxiliaire de recherche pour le Centre de recherche interuniversitaire sur la littérature et la culture québécoises (CRILCQ) et coordonnateur du Laboratoire sur les récits du soi mobile.\r

  \r

  <br>Il est enfin gradué d’un Diplôme National Supérieure d’Expression Plastique obtenu à l’École Européenne Supérieure d’Art de Bretagne (France) en 2011. Sa pratique artistique, fortement liée à ses recherches théoriques, porte sur les notions d’abstraction, de déchiffrage et d’interprétation. Pour cela, il recourt principalement à l’installation et la performance. </h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:erwan.geffroy@gmail.com\">erwan.geffroy@gmail.com\r

  </a>"
image: http://vitalirosati.net/chaire/img/eg.jpg
categories: null

---

