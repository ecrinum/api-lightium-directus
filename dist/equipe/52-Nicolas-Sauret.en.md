---
name: Nicolas Sauret
short: PhD Candidate
cleanstring: Nicolas-Sauret
lang: EN
caption: ""
description: "<p>Nicolas is a PhD candidate at University of Montreal and at the University Paris X. His research focuses on the concept of “editorialization” and the impact of digital notational apparatuses on the production and dissemination of knowledge.</p>\r

  <p>His PhD research intersects with his work at the <em>Institut de recherche et d'innovation du Centre Pompidou</em> (IRI) as a research engineer and project manager (2009-2016). Since 2011, at IRI and now at the Canada Research Chair on Digital Textualities, he co-organises the international seminar series <em>Écritures numériques et éditorialisation</em>, in collaboration with the journal <em>Sens Public</em> and the DICEN-IDF lab.</p>\r

  <p>He worked as a documentary filmmaker and video producer in Laos and Hong Kong for six years, before co-founding the production company <em>Inflammable Productions</em> in Paris, which is dedicated to new media and new forms of storytelling. His work as a director has been shown in several exhibitions and festivals.</p>\r

  \r

  <h2 style=\"text-align:right;\"><a href=\"http://nicolassauret.net\">nicolassauret.net</h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:nicolas.sauret@umontreal.ca\">nicolas.sauret@umontreal.ca</h2>\r

  </a>"
image: http://vitalirosati.net/chaire/img/ns.jpg
categories: null

---

