---
name: Nicolas Sauret
short: Candidat au doctorat
cleanstring: Nicolas-Sauret
lang: FR
caption: ""
description: "<p><i>Nicolas Sauret</i> est actuellement doctorant à l'Université de Montréal et l'Université Paris Ouest et travaille sur la question de l'éditorialisation et de l'impact des dispositifs numériques d'écriture et de lecture dans la construction et la circulation des savoirs.</p>\r

  <p>Sa thèse s'inscrit dans la continuité de son travail à l'Institut de recherche et d'innovation du Centre Pompidou (IRI) comme ingénieur de recherche et chef de projet (2009-2016). </p>\r

  <p>Au sein de l'IRI puis au sein de la chaire Écritures numériques de l'Université de Montréal, il co-organise le séminaire « Écritures numériques et éditorialisation » depuis 2011, en partenariat avec la revue Sens Public et le laboratoire Dicen-IDF.</p>\r

  \r

  <p>Il a travaillé comme vidéaste et réalisateur de documentaire en Asie pendant six années, au Laos puis à Hong Kong, avant de créer à Paris avec deux associés la société de production Inflammable Productions, spécialisée dans les nouveaux médias et les nouvelles formes de narration. Son travail de réalisation a été diffusé dans nombre d’expositions et de festivals. </p>\r

  \r

  <h2 style=\"text-align:right;\"><a href=\"http://nicolassauret.net\">nicolassauret.net</h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:nicolas.sauret@umontreal.ca\">nicolas.sauret@umontreal.ca</h2>\r

  </a>"
image: http://vitalirosati.net/chaire/img/ns.jpg
categories: null

---

