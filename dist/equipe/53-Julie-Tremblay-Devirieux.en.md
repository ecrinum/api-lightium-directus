---
name: Julie Tremblay-Devirieux
short: PhD Candidate
cleanstring: Julie-Tremblay-Devirieux
lang: EN
caption: ""
description: "PhD candidate in Comparative Literature at the University of Montreal, Julie is preparing a thesis on the narrator’s presence in the writing of Élise Turcotte, Clarice Lispector, and Pierre Guyotat. Her research lies at the intersection of literature and philosophy. Continuing from the subject of her Master’s thesis, her research is interested in identity and bodily abjection in contemporary writing by women.</h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:julie.tremblay-devirieux@umontreal.ca\">julie.tremblay-devirieux@umontreal.ca\r

  </a>"
image: http://vitalirosati.net/chaire/img/jtd.jpg
categories: null

---

