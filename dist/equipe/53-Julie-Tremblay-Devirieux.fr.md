---
name: Julie Tremblay-Devirieux
short: Candidate au doctorat
cleanstring: Julie-Tremblay-Devirieux
lang: FR
caption: ""
description: "Doctorante en littérature comparée à l’Université de Montréal, elle prépare une thèse sur la présence au monde du narrateur dans les récits d’Élise Turcotte, de Clarice Lispector et de Pierre Guyotat. Ses recherches s’inscrivent au confluent de la littérature et de la philosophie. Elle s’est intéressée, dans le cadre de son mémoire de maîtrise, à l’abjection du corps et de l’identité dans les écrits de femmes contemporains.</h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:julie.tremblay-devirieux@umontreal.ca\">julie.tremblay-devirieux@umontreal.ca\r

  </a>"
image: http://vitalirosati.net/chaire/img/jtd.jpg
categories: null

---

