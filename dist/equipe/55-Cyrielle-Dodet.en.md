---
name: Cyrielle Dodet
short: Guest researcher
cleanstring: Cyrielle-Dodet
lang: EN
caption: ""
description: "Cyrielle Dodet is a PhD candidate in Comparative Literature and Theatre Studies. She is completing her dissertation at the University of Montreal and the University Sorbonne nouvelle – Paris III. Titled <i>Entre theatre et poesie: devenir intermedial du poème et dispositif théâtral au tournant des XXe et XXIe siècles</i>, her dissertation considers, through an intermedial methodology, the relationships between theatre and poetry. By analyzing a representative corpus composed of contemporary stagings and textual creations, she shows how the active presence of poetry affects theatre, and, as such, she defines each through their interactions. More widely, her research addresses the intermedial dynamics between literature and the stage. A graduate of the ÉNS in Lyon, with a degree in Modern Letters, she is a temporary member of the teaching and research team at the Institut d’Études Théâtrales and at the Department of Film and Audiovisuals at Paris III. She has participated in conferences in France and Canada. She has contributed to several collective texts, to the Quebecois journal <i>JEU</i>, to <i>L’Annuaire théâtral</i>, to <i>MOSAIC<i></i>, and to the online journal <i>The Postcolonialist</i>. She is a member of the Centre de recherche interuniversitaire sur la littérature et la culture québécoise (CRILCQ) and of the Research Group on Poetics and Modern and Contemporary Drama at the IRET at Paris III.\r

  </h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:cyrielle.dodet@hotmail.fr\">cyrielle.dodet@hotmail.fr\r

  </a>"
image: http://vitalirosati.net/chaire/img/cd.jpg
categories: null

---

