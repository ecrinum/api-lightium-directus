---
name: Cyrielle Dodet
short: Chercheure invitée
cleanstring: Cyrielle-Dodet
lang: FR
caption: ""
description: "Cyrielle Dodet est docteure en Littérature comparée et en Études théâtrales et a réalisé sa thèse en cotutelle entre l’Université de Montréal et l'Université Sorbonne nouvelle - Paris III. Intitulée Entre théâtre et poésie : devenir intermédial du poème et dispositif théâtral au tournant des XXe et XXIe siècles, sa thèse envisage selon une méthodologie intermédiale les relations entre théâtre et poésie. En analysant un corpus témoin composé de créations textuelles et scéniques contemporaines, elle montre comment la présence active de la poésie travaille le théâtre, et partant, elle les précise tous deux par leurs interactions. Plus largement, ses recherches portent sur les dynamiques intermédiales entre littérature et scène. Ancienne élève de l’ÉNS de Lyon, agrégée de Lettres modernes, elle est attachée temporaire d'enseignement et de recherche à l’Institut d’Études Théâtrales et au département de Cinéma et d’Audiovisuel de Paris III. Elle a participé à différents colloques en France et au Canada, elle a contribué à plusieurs ouvrages collectifs, à la revue québécoise JEU, à L’Annuaire théâtral, à MOSAIC et à la revue en ligne The Postcolonialist. Elle est membre du Centre de recherche interuniversitaire sur la littérature et la culture québécoises (CRILCQ) et du groupe de recherche sur la poétique du drame moderne et contemporain de l'IRET à Paris III.\r

  </h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:cyrielle.dodet@hotmail.fr\">cyrielle.dodet@hotmail.fr\r

  </a>"
image: http://vitalirosati.net/chaire/img/cd.jpg
categories: null

---

