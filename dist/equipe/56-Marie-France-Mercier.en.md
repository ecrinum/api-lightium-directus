---
name: Marie-France Mercier
short: Master's student
cleanstring: Marie-France-Mercier
lang: EN
caption: ""
description: "A master’s student in French Literature and Language at the University of Montreal, she is writing a thesis on online communities of writers that produce discourse on literature and the arts. She is also interested in the editorial practices of these communities. Her research interests are essentially focused on digital literature, new critical spaces, and the genre of the essay. </h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:marie-france.mercier@outlook.com\">marie-france.mercier@outlook.com\r

  </a>"
image: http://vitalirosati.net/chaire/img/mfm.jpg
categories: null

---

