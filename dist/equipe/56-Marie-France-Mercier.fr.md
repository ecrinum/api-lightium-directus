---
name: Marie-France Mercier
short: Étudiante à la maîtrise
cleanstring: Marie-France-Mercier
lang: FR
caption: ""
description: "Étudiante à la maîtrise en littératures de langue française à l’Université de Montréal, elle écrit un mémoire sur des communautés d’écrivains en ligne produisant des discours sur la littérature et sur les arts. Elle s’intéresse également aux pratiques éditoriales de ces communautés. Ses intérêts de recherche se rapportent essentiellement à la littérature numérique, aux nouveaux espaces critiques, ainsi qu’au genre de l’essai. </h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:marie-france.mercier@outlook.com\">marie-france.mercier@outlook.com\r

  </a>"
image: http://vitalirosati.net/chaire/img/mfm.jpg
categories: null

---

