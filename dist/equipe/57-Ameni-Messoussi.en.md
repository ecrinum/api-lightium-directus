---
name: Ameni Messoussi
short: Guest researcher
cleanstring: Ameni-Messoussi
lang: EN
caption: ""
description: "Ameni Messoussi is a doctoral candidate at the Faculty of Letters and Humanities of Manouba – Tunisia. She is preparing a thesis in the field of English Studies entitled <i>Transposition numérique de l’édition savant: vers une édition critique électronique de Jane Austen</i>. A graduate of the University Paris XIII – Sorbonne Cité and of the University of Cracovia, she was a guest researcher at the Canada Research Chair on Digital Textualities.</h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:ameni_messoussi@hotmail.fr\">ameni_messoussi@hotmail.fr\r

  </a>"
image: http://vitalirosati.net/chaire/wp-content/uploads/2016/05/Ameni.jpg
categories: null

---

