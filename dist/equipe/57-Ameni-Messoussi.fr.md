---
name: Ameni Messoussi
short: Chercheure invitée
cleanstring: Ameni-Messoussi
lang: FR
caption: ""
description: "Ameni Messoussi est étudiante au doctorat, inscrite à la Faculté des Lettres et des Humanités de la Manouba - Tunisie. Elle prépare sa thèse en études anglaises, intitulée : « Transposition numérique de l'édition savante : vers une édition critique électronique de Jane Austen ». Ancienne étudiante de l’Université Paris XIII – Sorbonne Cité et de l’Université de Cracovie, elle est actuellement chercheure invitée au sein de l’équipe de la Chaire de recherche du Canada sur les écritures numériques.</h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:ameni_messoussi@hotmail.fr\">ameni_messoussi@hotmail.fr\r

  </a>"
image: http://vitalirosati.net/chaire/wp-content/uploads/2016/05/Ameni.jpg
categories: null

---

