---
name: Enrico Agostini Marchese
short: PhD Candidate
cleanstring: Enrico-Agostini-Marchese
lang: EN
caption: ""
description: "Enrico Agostini Marchese is a doctoral candidate in French Literature and Language at the University of Montreal. He obtained his Master’s in Philosophy at the University of Florence, with an intermedial thesis on the philosophical and aesthetic implications of the project <i>Atlas de Photos, Collagen und Skizzen</i> by the German painter Gerhard Richter. He has long been interested in the confluence of contemporary philosophy and aesthetics.</h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:enrico.agostini88@gmail.com\">enrico.agostini88@gmail.com\r

  </a>"
image: https://i.imgur.com/hva7MWJ.jpg
categories: null

---

