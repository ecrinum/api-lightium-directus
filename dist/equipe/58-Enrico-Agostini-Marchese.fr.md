---
name: Enrico Agostini Marchese
short: Candidat au doctorat
cleanstring: Enrico-Agostini-Marchese
lang: FR
caption: ""
description: "Enrico Agostini Marchese est candidat au doctorat en littérature de langue française à l'Université de Montréal. Il a obtenu sa maîtrise en philosophie de l'Université de Florence, avec une thèse à caractère intermédial sur les enjeux philosophiques et esthétiques du projet <i>Atlas der Photos, Collagen un Skizzen</i> du peintre allemand Gerhard Richter. Il s'intéresse depuis toujours au confluent de l'esthétique et de la philosophie contemporaine.</h2>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:enrico.agostini88@gmail.com\">enrico.agostini88@gmail.com\r

  </a>"
image: https://i.imgur.com/hva7MWJ.jpg
categories: null

---

