---
name: Laurent de Maisonneuve
short: Master's student
cleanstring: Laurent-de-Maisonneuve
lang: EN
caption: ""
description: "Laurent de Maisonneuve is a Master's student in French Literature at the University of Montreal. His research, is funded by SSHRC and FRQSC, focuses on the analysis of paratextual theory in the digital space. He is also interested in issues pertaining to intermediality, cultural studies, and contemporary Quebecois literature.\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:laurent.demaisonneuve@gmail.com\">laurent.demaisonneuve@gmail.com</a>"
image: https://i.imgur.com/7Py60UB.png
categories: null

---

