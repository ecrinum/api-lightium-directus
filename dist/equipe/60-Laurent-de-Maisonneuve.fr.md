---
name: Laurent de Maisonneuve
short: Étudiant à la Maîtrise
cleanstring: Laurent-de-Maisonneuve
lang: FR
caption: ""
description: "Laurent de Maisonneuve est étudiant à la maîtrise en littératures de langue française à l’Université de Montréal. Ses travaux, financés par le Conseil de recherches en sciences humaines du Canada (CRSH) et par le Fonds de recherche du Québec – Société et culture (FRQSC), portent sur la réactualisation de la théorie paratextuelle dans l’espace numérique. Il s’intéresse à temps partiel aux questions d’intermédialité, aux cultural studies et à la littérature québécoise contemporaine.\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:laurent.demaisonneuve@gmail.com\">laurent.demaisonneuve@gmail.com</a>"
image: https://i.imgur.com/7Py60UB.png
categories: null

---

