---
name: Karine Bissonnette
short: PhD Candidate
cleanstring: Karine-Bissonnette
lang: EN
caption: ""
description: "Karine Bissonnette is a PhD candidate in French Literature at the University of Montreal. In the framework of her research, funded by SSHRC, she studies the poetics of enumeration and its related forms in French autobiographical writing in the digital era, specifically in the works of François Bon, Éric Chevillard, and Philippe Didion. Since her Master's studies, which focused on the imbrication of writing and photography in <i>Les années</i> by Annie Ernaux, she has been interested in the challenges of documentation and transmission, as well as trace and memory.\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:kbissonnette1@gmail.com\">kbissonnette1@gmail.com</a>"
image: https://i.imgur.com/7Xbp9RH.jpg
categories: null

---

