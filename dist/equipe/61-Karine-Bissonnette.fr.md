---
name: Karine Bissonnette
short: Candidate au doctorat
cleanstring: Karine-Bissonnette
lang: FR
caption: ""
description: "Karine Bissonnette est candidate au doctorat en littératures de langue française à l'Université de Montréal. Dans le cadre de ses recherches doctorales, financées par le CRSH, elle se penche sur la poétique de l'énumération et de ses formes apparentées dans l'autobiographique français à l'ère du numérique, principalement chez François Bon, Éric Chevillard et Philippe Didion. Depuis ses études de maîtrise, lesquelles portaient sur l'imbrication de l'écriture et de la photographie dans <i>Les années</i> d'Annie Ernaux, elle s'intéresse à l'enjeu de la documentation et de la transmission à autrui ainsi qu'aux questions  reliées à la trace et à la mémoire.\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:kbissonnette1@gmail.com\">kbissonnette1@gmail.com</a>"
image: https://i.imgur.com/7Xbp9RH.jpg
categories: null

---

