---
name: Cédric Kayser
short: PhD candidate
cleanstring: Cedric-Kayser
lang: EN
caption: ""
description: "Cédric Kayser is a PhD candidate in French Literature at the University of Montreal. After having studied the subject of crisis in the works of Huysmans, he obtained his Master's in Philosophy at the University Paris IV with a thesis on the phenomenological basis of <i>In Search of Lost Time</i>. After he studied philosophy in Paris, he completed the Master in Modern and Contemporary European Philosophy in Luxembourg, with a thesis on the reception of Schelling's <i>Naturphilosophie</i> in the pre-reflexive phenomenology in Merleau-Ponty. Aside from his studies, he is pursuing a musical career as a composer for theatre, films, and museum exhibitions.\r

  <br>\r

  <br>\r

  His PhD project continues from his previous research, which analyses the relations between the body's limits and the way technical devices affect subject seeing."
image: http://i.imgur.com/TjcDJOjm.jpg
categories: null

---

