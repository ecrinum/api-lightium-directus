---
name: Cédric Kayser
short: Candidat au doctorat
cleanstring: Cedric-Kayser
lang: FR
caption: ""
description: "Cédric Kayser est candidat au doctorat en littérature de langue française à l’Université de Montréal. Après avoir travaillé sur la crise du sujet dans l’œuvre de Huysmans, il a obtenu sa maîtrise en Philologie à Paris-IV, avec un mémoire portant sur les bases phénoménologiques dans <i>La Recherche du Temps Perdu</i>. Il s’est ensuite tourné vers des études de Philosophie à Paris, avant de terminer le programme « Master in Modern and Contemporary European Philosophy» à l’Université de Luxembourg avec un travail de Maîtrise consacré à la réception de la <i>Naturphilosophie</i> de Schelling dans la phénoménologie pré-réflexive de Merleau-Ponty. À côté de ces études, il poursuit aussi une carrière de musicien et de compositeur pour des pièces de théâtre, des projets de films et des expositions muséales.\r

  <br>\r

  <br>\r

  Son projet de doctorat s’inscrit dans la continuité de ces travaux antérieurs et analyse les rapports entre les limites du corps et la façon dont les dispositifs techniques affectent le « voir » du sujet.\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:cedrickayser1@gmail.com\">cedrickayser1@gmail.com</a>"
image: http://i.imgur.com/TjcDJOjm.jpg
categories: null

---

