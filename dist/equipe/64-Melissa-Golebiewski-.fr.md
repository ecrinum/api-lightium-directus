---
name: "Mélissa Golebiewski "
short: Candidate au doctorat
cleanstring: Melissa-Golebiewski-
lang: FR
caption: ""
description: "Mélissa Golebiewski est actuellement doctorante en études théâtrales. Sa thèse, réalisée en cotutelle entre l’<i>École Normale Supérieure</i> de Lyon et l’<i>Université de Montréal</i> (sous la direction de Jean-Loup Rivière et de Jean-Marc Larrue), porte sur les relations entre vidéo et dramaturgie dans les mises en scène contemporaines. Elle y travaille à une étude croisée, à la fois dramaturgique et esthétique, d’un corpus de spectacles utilisant des dispositifs de captation ou de déclenchement d’image vidéographique en direct.\r

  <br>\r

  Elle est également membre étudiante du <i>Centre de recherche interuniversitaire sur la littérature et la culture québécoises</i> (CRILCQ) et du <i>Centre d’Etudes et de Recherches Comparées sur la Création</i> (CERCC).\r

  <br>\r

  <br>\r

  Doctorante mais également praticienne, elle s’intéresse de près à ce qu’on dit comme à ce qu’on voit au théâtre aujourd’hui, au live, aux relations entre récit et technologies numériques, aux processus de création et aux outils conceptuels du champ des études intermédiales.\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:melissa.golebieswki@gmail.com\">melissa.golebiewski@gmail.com</a>"
image: https://i.imgur.com/uWkrk8e.jpg
categories: null

---

