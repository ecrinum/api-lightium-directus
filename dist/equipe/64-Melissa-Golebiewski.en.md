---
name: Mélissa Golebiewski
short: PhD Candidate
cleanstring: Melissa-Golebiewski
lang: EN
caption: ""
description: "Mélissa Golebiewski is currently a PhD candidate in theatre studies, co-supervised by Jean-Marc Larrue at the University of Montreal and Jean-Loup Rivière at the École Normale Supérieure de Lyon. Her research explores the complex relationship between video images and dramaturgy (staging as well as playwriting) in contemporary theatre, through a corpus of recent productions using live video feed, both in Europe and Canada. \r

  \r

  She’s a member of the <i>Centre de recherche interuniversitaire sur la littérature et la culture québécoises</i> (CRILCQ) and the <i>Centre d’Etudes et de Recherches Comparées sur la Création</i> (CERCC).\r

  <br>\r

  <br>\r

  As a researcher and practitioner, Mélissa is interested in what you can see, hear, or feel in contemporary theatre, in the links between narration and technology, in creative processes, and in the conceptual framework of intermediality.\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:melissa.golebiewski@gmail.com\">melissa.golebieswki@gmail.com</a>"
image: https://i.imgur.com/uWkrk8e.jpg
categories: null

---

