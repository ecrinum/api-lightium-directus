---
name: Philippe Pouchet
short: MBA Student
cleanstring: Philippe-Pouchet
lang: EN
caption: ""
description: "Philippe Pouchet is a graduate student in French Literature at University of Montreal. \r

  <br>\r

  His research focuses on the ambiguity between the real and the imaginary, truth and fiction with regard to conspiracy theories, fake news, and the notion of identity in digital literature."
image: http://i.imgur.com/wWycLQr.jpg
categories: null

---

