---
name: Philippe Pouchet
short: Étudiant à la maîtrise
cleanstring: Philippe-Pouchet
lang: FR
caption: ""
description: "Philippe Pouchet est étudiant de 2e cycle en recherche/création au Département de littératures de langue française à l'Université de Montréal. \r

  <br>\r

  Ses recherchent portent sur les frontières entre réel, imaginaire, vérité et fiction, relativement aux récits de conspiration, au phénomène des 'fake news' et à la notion d'identité dans la littérature numérique.\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:pouchetphil@gmail.com\">pouchetphil@gmail.com</a>"
image: http://i.imgur.com/wWycLQr.jpg
categories: null

---

