---
name: Thomas Carrier-Lafleur
short: Banting Postdoctoral fellow
cleanstring: Thomas-Carrier-Lafleur
lang: EN
caption: ""
description: "Thomas Carrier-Lafleur is a Banting postdoctoral fellow and a lecturer at the University of Montreal. He is the author of <i>L’œil cinématographique de Proust</i> (Classiques Garnier), <i>Une Philosophie du « temps à l’état pur ». L’Autofiction chez Proust et Jutra</i> (Vrin/Presses of Université Laval). He is a member of the editorial boards of <i>Nouvelles Vues</i> and <i>Sens Public</i>, and is also a member of GRAFICS, Figura, Les Arts Trompeurs: Machines, Magie, Médias (Deceptive Arts: Machines, Magic, Media), the Canada Research Chair in Cinema and Media Studies, the Canada Research Chair on Digital Textualities, and of the international research partnership TECHNÈS. His researcj focuses on the work of Marcel Proust, the 19th and 20th century French novel, the history of media from a literary point of view, the myths of modern literature, spatial production in the digital age, and Quebecois cinema.\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:thomas.carrier-lafleur@umontreal.ca\">thomas.carrier-lafleur@umontreal.ca</a>"
image: http://i.imgur.com/9G2ckOT.png
categories: null

---

