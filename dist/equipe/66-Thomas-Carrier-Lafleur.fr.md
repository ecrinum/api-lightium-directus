---
name: Thomas Carrier-Lafleur
short: Postdoctorant Banting
cleanstring: Thomas-Carrier-Lafleur
lang: FR
caption: ""
description: "Thomas Carrier-Lafleur est stagiaire postdoctoral Banting et chargé de cours à l’Université de Montréal. Il est notamment l’auteur de <i>L’œil cinématographique de Proust</i> (Classiques Garnier), <i>Une Philosophie du « temps à l’état pur ». L’Autofiction chez Proust et Jutra</i> (Vrin/Presses de l’Université Laval), et fait partie des comités de rédaction de <i>Nouvelles Vues</i> et <i>Sens Public</i> et est également membre du GRAFICS, de Figura, du regroupement Les Arts trompeurs, du CRIHN, de la Chaire de recherche du Canada en Études cinématographiques et médiatiques, de la Chaire de recherche du Canada en Écritures numériques et du partenariat international TECHNÈS. Ses recherches portent sur l’histoire littéraire des médias, les mythes de la littérature moderne, la production spatiale à l’ère du numérique et le cinéma québécois.\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:thomas.carrier-lafleur@umontreal.ca\">thomas.carrier-lafleur@umontreal.ca</a>"
image: http://i.imgur.com/9G2ckOT.png
categories: null

---

