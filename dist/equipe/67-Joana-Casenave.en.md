---
name: Joana Casenave
short: Postdoctoral Fellow
cleanstring: Joana-Casenave
lang: EN
caption: ""
description: "<p>A graduate from the École nationale des chartes (Paris) and from the École de Bibliothéconomie et des Sciences de l’Information (EBSI) at the University of Montreal, Joana Casenave  defended her PhD in 2016 (cotenured at EBSI / Université Paris-Est Créteil), in Information Sciences and French Literature, on the subject of : « La transposition numérique de l’édition critique : éléments pour une édition de l’IsopetI-Avionnet » (under the direction of Professor Jeanne-Marie Boivin at UPEC and Professor Yves Marcoux at EBSI). Her research focuses on the history and epistemology of the digital scholarly edition, its impact on the Digital Humanities and on contemporary publishing practices.</p>\r

  \r

  <p>In 2017-2018, she holds a postdoctoral fellowship at the University of Montreal, with the Canada Research Chair on Digital Textualities, under the co-direction of Professors Marcello Vitali-Rosati and Michael E. Sinatra.</p>"
image: https://i.imgur.com/c8tO5iv.jpg
categories: null

---

