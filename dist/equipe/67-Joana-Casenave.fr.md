---
name: Joana Casenave
short: Postdoctorante
cleanstring: Joana-Casenave
lang: FR
caption: ""
description: "<p>Diplômée de l’École nationale des chartes (Paris) et de l’École de Bibliothéconomie et des Sciences de l’Information (EBSI) de l’Université de Montréal, Joana Casenave a soutenu en 2016 un doctorat (cotutelle EBSI / Université Paris-Est Créteil), en Sciences de l’information et en Langue et littérature françaises, sur le sujet suivant : « La transposition numérique de l’édition critique : éléments pour une édition de l’IsopetI-Avionnet » (sous la direction du Professeur Jeanne-Marie Boivin à l’UPEC et du Professeur Yves Marcoux à l’EBSI). Ses recherches portent sur l’histoire et l’épistémologie de l’édition critique numérique, sur sa place dans les Humanités numériques et sur les pratiques éditoriales contemporaines.</p>\r

  \r

  <p>Elle effectue en 2017-2018 son postdoctorat à l’Université de Montréal, à la Chaire de recherche du Canada sur les écritures numériques, sous la direction conjointe des Professeurs Marcello Vitali-Rosati et Michael E. Sinatra.</p>"
image: https://i.imgur.com/c8tO5iv.jpg
categories: null

---

