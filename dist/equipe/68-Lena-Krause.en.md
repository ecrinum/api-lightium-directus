---
name: Lena Krause
short: Master's student
cleanstring: Lena-Krause
lang: EN
caption: ""
description: Lena Krause is a Master's student in Art History at the University of Montreal. Her research concerns digital art history and the use of geography in the humanities. This points to the creation of a digital atlas about French public architecture (1795 - 1849). She is also interested in the mediation and appropriation opportunities presented by the digital dimension. As such, she is currently leading a collaborative project that develops a mobile application inviting the discovery of Montreal's public art
image: https://i.imgur.com/NXSedI9.jpg
categories: null

---

