---
name: Lena Krause
short: Étudiante à la maîtrise
cleanstring: Lena-Krause
lang: FR
caption: ""
description: Lena Krause est étudiante à la maîtrise en histoire de l'art à l'Université de Montréal. Ses recherches s'articulent autour de l'histoire de l'art numérique et de l'usage de la géographie en sciences humaines. Plus précisément, il s'agit de créer un atlas numérique de l'architecture publique en France (1795  - 1840). Elle s'intéresse également aux opportunités de médiation et d'appropriations offertes par le numérique. Sous son initiative est né un projet collaboratif de développement d'une application mobile invitant à découvrir l'art public de la Ville de Montréal
image: https://i.imgur.com/NXSedI9.jpg
categories: null

---

