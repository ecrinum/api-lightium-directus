---
name: Daniel Pinheiro
short: Resident artist
cleanstring: Daniel-Pinheiro
lang: EN
caption: ""
description: "<p>Born in Venezuela. Currently lives and works from Portugal. Graduated in Theater also developed his skills in the fields of Video Editing, Media, Performance Art and Networked Performance. As an independent artist and researcher his work bounces between the experimentation with video and how it transforms the perception of the self through different techniques, by investigating the tension between human and machine through embodied practice on collaborative projects either in remote or physically present. Since 2011 his work has greatly focused on the Internet as a medium and video as format to mediate communication and emulate presence. Although video itself and the editing process are essential tools to operate and produce video works that fall on categories such as video and queer art, it also serves an essential purpose that is to have the “camera” as a spectator of actions. </p>\r

  <p>This Artist Residence was made possible with the support of \"<a href=\"http://www.fundacaogda.pt/pt\">Fundação GDA - Gabinete dos Direitos dos Artistas</a>\" from Portugal.</p>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:danielpinheiro.geral@gmail.com\">danielpinheiro.geral@gmail.com</h2></a>\r

  <h2 style=\"text-align:right;\"><a href=\"http://daniel-pinheiro.tumblr.com/\">http://daniel-pinheiro.tumblr.com/</h2>"
image: https://i.imgur.com/Yakt2gz.jpg
categories: null

---

