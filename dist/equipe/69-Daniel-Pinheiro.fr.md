---
name: Daniel Pinheiro
short: Artiste en résidence
cleanstring: Daniel-Pinheiro
lang: FR
caption: ""
description: "<p>Né au Venezuela, il vit et travaille actuellement en Portugal. Diplômé en Théâtre, il s'est intéressé au domaine de l'édition vidéo, des médias, de l'art performative et la performance en réseau. En tant qu'artiste indépendant et chercher, son travail se situe entre l'expérimentation vidéo et la manière dont celle-ci modifie la perception de soi à travers différentes techniques, tout en réfléchissant aux tensions entre humain et machine dans les pratiques incarnées en projets collaboratifs, à la fois à distance et en coprésence. Depuis 2011, son travail a porté majoritairement sur Internet en tant que média et sur la vidéo en tant que forme pour médiatiser la communication et émuler la présence. Bien que la vidéo elle-même et le processus d'édition soient des outils essentiels pour opérer et produire des œuvres vidéo qui tombent dans des catégories telles que la vidéo et l'art queer, elles ont aussi un but essentiel: avoir la caméra comme spectateur d'actions.</p>\r

  <p>Cette résidence d'artiste a été rendue possible avec le support de la \"<a href=\"http://www.fundacaogda.pt/pt\">Fundação GDA - Gabinete dos Direitos dos Artistas</a>\", Portugal.</p>\r

  <h2 style=\"text-align:right;\"><a href=\"mailto:danielpinheiro.geral@gmail.com\">danielpinheiro.geral@gmail.com</h2></a>\r

  <h2 style=\"text-align:right;\"><a href=\"http://daniel-pinheiro.tumblr.com/\">http://daniel-pinheiro.tumblr.com/</h2>\r\n"
image: https://i.imgur.com/Yakt2gz.jpg
categories: null

---

