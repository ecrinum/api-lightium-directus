---
name: Margot Mellet
short: PhD student<br />Scientfic coordinator
cleanstring: Margot-Mellet
lang: EN
caption: ""
description: Margot Mellet is a Classic literature graduate and a Modern literature one. Her research focuses on the transhistorical <i>topoi</i> that could be found in ancient and modern corpus, and on digital scholarly edition.
image: https://i.imgur.com/88XHpSk.jpg
categories: null

---

