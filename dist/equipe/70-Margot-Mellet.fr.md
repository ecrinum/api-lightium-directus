---
name: Margot Mellet
short: "Étudiante au doctorat<br />\r

  Coordonnatrice scientifique"
cleanstring: Margot-Mellet
lang: FR
caption: ""
description: Margot Mellet est diplômée d’une Maîtrise en Lettres Classiques et d’une Maîtrise en Lettres modernes. S’intéressant aux liens transhistoriques pouvant être pensés entre les corpus littéraires antiques et modernes et l’édition savante numérique.
image: https://i.imgur.com/88XHpSk.jpg
categories: null

---

