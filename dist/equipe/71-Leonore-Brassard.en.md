---
name: Léonore Brassard
short: Ph.D. candidate
cleanstring: Leonore-Brassard
lang: EN
caption: ""
description: "<p>Léonore Brassard is a PhD candidate in the Comparative Literature Department of the University of Montreal. Her work focuses on the sexual exchange in modern and contemporary literature, inter alia, on the economy of gifting and reciprocity in the prostitution’s literary representation. <br />She also specializes in feminist and gender studies: her master’s degree thesis being a comparative analysis of <em>Les Chiennes savantes</em> (1996) from Virginie Despentes and <em>L’Ève future</em> (1886) from Villiers de l’Isle-Adam about the technical and mechanical performance of gender.</p>\r\n"
image: https://i.imgur.com/SlUqLuc.jpg
categories: null

---

