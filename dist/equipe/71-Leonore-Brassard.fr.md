---
name: Léonore Brassard
short: Candidate au doctorat
cleanstring: Leonore-Brassard
lang: FR
caption: ""
description: "<p>Léonore Brassard est étudiante de doctorat dans le programme des Littératures et langues du monde de l’Université de Montréal. Elle travaille sur la représentation de l’échange sexuel dans les littératures moderne et contemporaine, notamment sur l’économie du don et du contre-don dans la mise en texte de la prostitution. <br />Elle s’intéresse parallèlement aux champs des études féministes et du genre : sa recherche de maîtrise posait la question de la performance technique et mécanique du genre, de façon comparative entre <em>Les Chiennes savantes</em> (1996), de Virginie Despentes et <em>L’Ève future</em> (1886) de Villiers de l’Isle-Adam.</p>"
image: https://i.imgur.com/SlUqLuc.jpg
categories: null

---

