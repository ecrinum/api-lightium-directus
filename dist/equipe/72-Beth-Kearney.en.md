---
name: Beth Kearney
short: MBA Student
cleanstring: Beth-Kearney
lang: EN
caption: ""
description: <p>With a background in English Literature and Art History, Beth is currently a Master's student in the Department of French Literature at the University of Montreal. She devotes her master's thesis to the influence of literary saphism and the Gothic novel in the work of the surrealist, Valentine Penrose. She is also interested in intermedial studies, but more particularly in the relationship between text and image. In 2020, she hopes to undertake a PhD on the presence of visual arts in the writings and collaborations of Swiss author S. Corinna Bille.</p>
image: https://i.imgur.com/93IKOrw.jpg
categories: null

---

