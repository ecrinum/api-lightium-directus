---
name: Beth Kearney
short: Étudiante à la maîtrise
cleanstring: Beth-Kearney
lang: FR
caption: ""
description: <p>Avec une formation en littératures de langue anglaise et en histoire de l’art, Beth est actuellement une étudiante à la maîtrise dans le département des littératures de langue française à l’Université de Montréal. Elle consacre son mémoire de maîtrise à l’influence du saphisme littéraire et du roman gothique dans l’œuvre de la surréaliste, Valentine Penrose. Elle s’intéresse également aux études intermédiales, mais plus particulièrement aux rapports entre texte et image. Elle souhaite entreprendre, en 2020, un doctorat portant sur la présence des arts visuels dans les écrits et les collaborations de l’auteure suisse, S. Corinna Bille. </p>
image: https://i.imgur.com/93IKOrw.jpg
categories: null

---

