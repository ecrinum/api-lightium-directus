---
title: La fin de l’autorité ? Pour une philosophie politique du web
short: "Enssib - École Nationale Supérieure\r

  des Sciences de l'Information et des Bibliothèques\r

  <br>17-21, Boulevard du 11 Novembre 1918\r

  <br>Villeurbanne\r

  <br>France\r

  <br>17h-19h, Mardi, 18 octobre 2016\r

  </h3>"
cleanstring: La-fin-de-lautorite--Pour-une-philosophie-politique-du-web
lang: FR
caption: ""
image: http://www.enssib.fr/sites/www/files/logo-enssib_0.png
url: ""
urlTitle: ""
categories: null

---
    
Conférence de _Marcello Vitali-Rosati_, professeur agrégé de littérature et culture numérique au département des littératures de langue française de l'_Université de Montréal_.  
  
Le modèle d'autorité que nous avons connu jusqu'à aujourd'hui est profondément lié à une certaine conception de ce qu'est l'auteur : celle qui s'est développée dans le domaine de l'édition imprimée à partir du XVIIIe siècle. L'émergence de ce modèle correspond à l'émergence des États Nations : le type d'autorité des institutions étatiques est profondément lié à l'autorité de l'écrivain-auteur. Les changements déterminés par le numérique nous laissent l'impression que ce modèle n'est plus valable. On affirme souvent qu'Internet est un lieu d'émancipation de l'autorité. "Tout le monde" peut parler, il n'y a plus de pouvoirs forts, il n'y a plus d'autorité, la collectivité a le dernier mot, il n'y a plus d'auteurs. Mais une analyse plus attentive nous montre que cela est loin d'être vrai. Internet n'est pas un lieu déstructuré et non réglé, il est tout simplement caractérisé par des structures différentes. Dans le cadre de ces structures émergent de nouveaux dispositifs de production de l'autorité. Identifier et étudier ces dispositifs est essentiel pour comprendre les enjeux politiques de l'environnement numérique. Cette intervention propose une approche pour analyser ces dispositifs.  
  
Pour écouter l'enregistrement de la conférence : <http://www.enssib.fr/bibliotheque-numerique/ecouter/66991-la-fin-de-l-autorite-pour-une-philosophie-politique-du-web> 
