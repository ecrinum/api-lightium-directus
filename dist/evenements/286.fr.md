---
title: Théorie de l’éditorialisation
short: "Enssib - École Nationale Supérieure\r

  des Sciences de l'Information et des Bibliothèques\r

  <br>17-21, Boulevard du 11 Novembre 1918\r

  <br>Villeurbanne\r

  <br>France\r

  <br>14h-18h, Jeudi, 20 octobre 2016\r

  </h3>"
cleanstring: Theorie-de-leditorialisation
lang: FR
caption: ""
image: http://www.enssib.fr/sites/www/files/logo-enssib_0.png
url: ""
urlTitle: ""
categories: null

---
    
Marcello Vitali-Rosati présentera les résultats de huit ans de travail sur le concept d’éditorialisation, réalisé dans le cadre du séminaire international _Écritures numériques et éditorialisation_ qu'il co-organise avec Nicolas Sauret depuis 2008\.   
Il définit l’éditorialisation comme l’ensemble des dynamiques qui produisent l’espace numérique. Ces dynamiques peuvent être comprises comme les interactions d’actions individuelles et collectives avec un environnement numérique. À partir de cette définition, il analysera les caractéristiques et les spécificités de l'éditorialisation en soulignant en particulier les différences entre l’éditorialisation et la curation de contenu.  
  
Pour écouter l'enregistrement de la conférence : <http://www.enssib.fr/bibliotheque-numerique/index-des-auteurs?selecAuteur=Vitali-Rosati%2C%20Marcello#haut>
