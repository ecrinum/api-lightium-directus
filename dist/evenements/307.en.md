---
title: "Round Table: Post-truth and Authority"
short: "<h2>University of Montréal\r

  <br>Pavillon Lionel-Groulx, Local C-3061\r

  <br>Wednesday, 8th February, 4.30pm</h2>"
cleanstring: Round-Table-Post-truth-and-Authority
lang: EN
caption: ""
image: http://g8fip1kplyr33r3krz5b97d1.wpengine.netdna-cdn.com/wp-content/uploads/2016/12/GettyImages-628655648-714x489.jpg
url: ""
urlTitle: ""
categories: null

---
    
![](http://g8fip1kplyr33r3krz5b97d1.wpengine.netdna-cdn.com/wp-content/uploads/2016/12/GettyImages-628655648-714x489.jpg)   
  
According to the _Oxford dictionary_ the word of the year 2016 is _Post-truth_, defined as an adjective «relating to or denoting circumstances in which objective facts are less influential in shaping public opinion than appeals to emotion and personal belief.»  
The _Brexit_ and the Donald Trump election to the presidency of the U.S. have been the occasions were the terme _Post-truth_ was mostly used, especially referring to the circulation of fake news on social networks. This has been possible because of the lack of a final authority confirming the truth of these news.  
Is this lack a structural component of the Web? Is it true that there is no authority in the Web? What is authority on the Web? What happens to truth in the social networks age? This round table aim for reflecting upon these matters in order to better understand our time, and maybe answer to these questions.  
  
Participants : **Adriano Fabris** (University of Pise, Italy), **Pierre Lévy** (University of Ottawa, Canada), **Stéphane Vial** (University of Nîmes, France), **Maude Bonenfant** (University of Québec in Montréal), and **Marcello Vitali-Rosati** (University of Montréal). Moderator : **Servanne Monjour** (University of Montréal).  
**Round Table in french and english**.  
  
The round table recording is available on the Archive.org [Chair account](https://archive.org/details/postverite-autorite).
