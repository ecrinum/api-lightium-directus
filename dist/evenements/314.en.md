---
title: Conference proceedings' publication of <br><i>Internet est un cheval de Troie</i>
short: ""
cleanstring: Conference-proceedings-publication-of-briInternet-est-un-cheval-de-Troiei
lang: EN
caption: ""
image: http://vincent.mabillot.net/wp-content/uploads/InternetChevalDeTroie.png
url: ""
urlTitle: ""
categories: null

---
    
![](http://vincent.mabillot.net/wp-content/uploads/InternetChevalDeTroie.png)
  
  
I have the great pleasure to announce that [Fabula](https://www.fabula.org/#) just published, yesterday evening, the conference proceedings of _Internet est un cheval de Troie_, which you agreed to participe. The conference happened in Lyon, in March 2016\.   
  
**Élisabeth Routhier** and **Jean-François Thériault**, doctoral candidates at University of Montreal, talks about the [Auctorial Performance and the Literary System around _Pourquoi Bologne_ from Alain Farah](https://www.fabula.org/colloques/document4167.php).  
**Marcello Vitali-Rosati**, holder of the **Chair** and associate professor at University of Montreal, concludes the publication by article [_Paper Literature and Digital Literature, An Opposition ?_](https://www.fabula.org/colloques/document4191.php)   
  
To access the whole publication, it's [here](https://www.fabula.org/colloques/index.php?id=4120).  
  
This conference was supported by the **Research Commission** and the **Letters and Civilisations' Faculty of University of Jean Moulin-Lyon 3**.
