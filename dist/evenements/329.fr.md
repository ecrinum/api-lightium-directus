---
title: Cinquième séance séminaire Écritures Numériques et Éditorialisation - L'archive éditorialisée
short: "<h2>Université de Montréal\r

  <br>Jeudi 6 avril 2017, 11h30-13h30\r

  <br>Pavillon Lionel-Groulx, salle C-3061\r

  <br>Séance enregistrée\r

  </h2>"
cleanstring: Cinquieme-seance-seminaire-Ecritures-Numeriques-et-Editorialisation---Larchive-editorialisee
lang: FR
caption: ""
image: http://seminaire.sens-public.org/local/cache-vignettes/L230xH147/arton66-bfdfa.png
url: ""
urlTitle: ""
categories: null

---
    
![](http://seminaire.sens-public.org/local/cache-vignettes/L230xH147/arton66-bfdfa.png)   
  
La numérisation a conféré aux archives de nouvelles propriétés qui en bouleversent tant la nature que la mission institutionnelle. L’éditorialisation de l’archive garantit son accès et ses appropriations à travers des dispositifs dont la conception relève d’un design de la connaissance, tel que _Jeffrey Schnapp_ l’introduit. Cette séance sera l’occasion d’identifier les évolutions et les tendances de l’archive éditorialisée, d’en considérer les pratiques professionnelles, et de voir en retour ce que ces pratiques nous apprennent sur l’éditorialisation.**[Voir la séance en direct.](https://www.youtube.com/edit?o=U&video%5Fid=zuwktd5utK4 "La séance en direct")**   
  
Intervenants :  
  
Montréal : **Jeffrey Schapp**, _Harvard University_, fondateur et directeur du _metaLAB_ de Harvard and co-directeur du _Berkman Klein Center for Internet and Society_.  
  
Paris : **Antoine Courtin**, ingénieur de recherche à l’_Institut national d’histoire de l’art (INHA)_, responsable de la _Cellule d’ingénierie documentaire_ du département des études et de la recherche.  
  
La Chaire de recherche utilise comme système d'archivage de ses médias [Internet Archive](https://archive.org). L’IA met gratuitement ses collections à la disposition des chercheurs, historiens et universitaires. Le robot d'indexation utilisé par l'IA est un logiciel libre, ainsi que son logiciel de numérisation de livres.  
  
Si ne visualisez pas le lecteur vidéo, l'enregistrement de la conférence est disponible sur le compte Archive.org de la [Chaire de recherche](https://archive.org/details/LarchiveEditorialiseDeLaRessourceOriginaleALappropriation).
