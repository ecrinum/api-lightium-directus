---
title: Conférence de Jeffrey Schnapp
short: "<h2>Université de Montréal\r

  <br>Pavillon Lionel-Groulx, Salle C-3061\r

  <br>Jeudi 6 avril 2017, 17h45-19h\r

  <br>Séance enregistrée</h2>"
cleanstring: Conference-de-Jeffrey-Schnapp
lang: FR
caption: ""
image: https://upload.wikimedia.org/wikipedia/commons/8/8d/Jeffrey2.jpg
url: ""
urlTitle: ""
categories: null

---
    
![](https://upload.wikimedia.org/wikipedia/commons/8/8d/Jeffrey2.jpg)

  
« L'expression _Knowledge Design_ décrit la situation dans les arts contemporains et les sciences sociales qui m'engagent le plus en «humaniste numérique»: le fait que la forme que la connaissance suppose ne peut plus être considérée comme une donnée. Dans mon exposé, je vais dresser une cartographie globale de cette situation et souligner quelques nœuds-clés: la re-médiation de l'impression, le portrait des données, la réduction de la fracture analogique / numérique et la refonte des espaces de connaissances des salles de classe aux musées. »  
  
**Jeffrey Schnapp** est le fondateur / directeur de faculté de _metaLAB_ à Harvard et co-directeur du _Berkman Center for Internet and Society_. À **Harvard**, il est professeur de littératures romanes et de littérature comparée et il fait partie de la faculté d'enseignement du département d'architecture de la Graduate School of Design de Harvard. En juin 2015, il a occupé le poste de directeur général et cofondateur de _Piaggio Fast Forward_, dédié au développement de solutions innovantes aux défis du transport dans le monde contemporain.  
  
Pour avoir une meilleure idée de ce qu'est le _Knowledge Design_, vous pouvez consulter le [site de la Bibliothèque du Trinity College de Dublin](http://www.tcd.ie/library/news/future-library/jeffrey-schnapp-knowledge-design/), où M. Schnapp en a fait une présentation exhaustive.  
  
Si vous souhaitez en apprendre plus sur le travail et les champs d'intérêts de Jeffrey Schnapp, cet [article](https://digital.hbs.edu/events/knowledge-design/) paru sur le site de l'Université Harvard saura satisfaire votre curiosité.   
  
La Chaire de recherche utilise comme système d'archivage de ses médias [Internet Archive](https://archive.org). L’IA met gratuitement ses collections à la disposition des chercheurs, historiens et universitaires. Le robot d'indexation utilisé par l'IA est un logiciel libre, ainsi que son logiciel de numérisation de livres.  
  
Si ne visualisez pas le lecteur vidéo, l'enregistrement de la conférence est disponible sur le compte Archive.org de la [Chaire de recherche](https://archive.org/download/JeffreySchnappsLectureKnowledgeDesign).
