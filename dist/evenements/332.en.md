---
title: Sixth session of the seminar Digital Textualities and Editorialization - Plaforms and Content Production
short: "<h2>University of Montréal\r

  <br> Building Lionel-Groulx, Local C-2059\r

  <br>Tuesday 20th April 2017, 11:30am-1:30pm\r

  </h2>"
cleanstring: Sixth-session-of-the-seminar-Digital-Textualities-and-Editorialization---Plaforms-and-Content-Production
lang: EN
caption: ""
image: http://www.crihn.org/files/sites/33/2017/04/arton67-24974.png
url: ""
urlTitle: ""
categories: null

---
    
### Between Creativity and Control

![](http://www.crihn.org/files/sites/33/2017/04/arton67-24974.png) 
