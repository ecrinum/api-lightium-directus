---
title: Conference of Jill Didur
short: "<h2>University of Montréal\r

  <br>Friday 21th April 2017, 11am-12:30pm\r

  <br>3150 Jean-Brillant Street, Room C-8111 \r

  </h2>"
cleanstring: Conference-of-Jill-Didur
lang: EN
caption: ""
image: http://www.crihn.org/files/sites/33/2015/07/didurjill.jpg
url: ""
urlTitle: ""
categories: null

---
    
![](http://www.crihn.org/files/sites/33/2015/07/didurjill.jpg)   
  
**Jill Didur** will present a talk entitled « **Beyond Anti-Conquest: Unearthing the Botanical Archive with Locative Media** »:  
  
"This presentation considers the productive role locative media can play in unearthing the complexities of the colonial archive associated with botanic gardens. Through a discussion of the history and design of alpine gardens, the critical affordances of locative media apps, and the pedagogical structure and goals of my experimental mobile app, the Alpine Garden MisGuide / le Jardin alpin autrement, I consider how mobile media platforms can be used to challenge the nostalgia for colonialism that often unconsciously inflects the way visitors are encouraged to experience botanic gardens."   
  
Dr. Jill Didur is Professor and former Chair of the Department of English at **Concordia University**, Montreal. She is the co-editor of _Global Ecologies and the Environmental Humanities: Postcolonial Approaches_ (Routledge 2015), the author of _Unsettling Partition: Literature, Gender, Memory_ (UTP 2006), and the creator of the [Alpine Garden MisGuide / Jardin alpine autremont](https://itunes.apple.com/ca/app/alpine-garden-misguide/id991874716?mt=8) (iTunes Apple Store 2015).
