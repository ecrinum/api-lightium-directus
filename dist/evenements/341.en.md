---
title: "Seventh session of the seminar Digital Textualities and Editorialization - Space Editorialization "
short: "<h2>University of Montréal\r

  <br> Building Lionel-Groulx, Local C-2059\r

  <br>Tuesday 1<sup>st</sup> April 2017, 11:30am-1:30pm\r

  </h2>"
cleanstring: Seventh-session-of-the-seminar-Digital-Textualities-and-Editorialization---Space-Editorialization-
lang: EN
caption: ""
image: http://seminaire.sens-public.org/local/cache-vignettes/L230xH147/arton68-b5e27.png
url: ""
urlTitle: ""
categories: null

---
    
### De l'écriture à l'architecture

![](http://seminaire.sens-public.org/local/cache-vignettes/L230xH147/arton68-b5e27.png)   
