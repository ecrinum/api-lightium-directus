---
title: "Conférence NECS : Sensibility and the Senses"
short: "<h2>Université Sorbonne Nouvelle - Paris 3\r

  <br>Jeudi 29 juin - Samedi 1<sup>er</sup> juillet 2017</h2>"
cleanstring: Conference-NECS--Sensibility-and-the-Senses
lang: FR
caption: ""
image: https://scontent-yyz1-1.xx.fbcdn.net/v/t1.0-9/17991117_681250928742014_6720412891535972124_n.png?oh=64025d0e73314177d85e75b29cdea518&oe=59D72035
url: ""
urlTitle: ""
categories: null

---
    
![](https://scontent-yyz1-1.xx.fbcdn.net/v/t1.0-9/17991117_681250928742014_6720412891535972124_n.png?oh=64025d0e73314177d85e75b29cdea518&oe=59D72035)   
  
### Média, Corps, Pratiques

La question de la relation entre les medias, les corps et les sens traverse toute l'histoire des théories des médias. Depuis leur première apparition, les médias techniques tels que la télégraphie, la photographie, le gramophone, le film, la machine à écrire, le téléphone, la radio, puis la télévision, l'ordinateur, l'internet, ainsi qu'une grande variété de techniques culturelles pour l'enregistrement, le traitement et la transmission des informations ont été analysées en tenant compte de leurs relations avec le corps humain et ses organes sensoriels. Des concepts tels que « projection d'organe », « prothèse », « innervation », « extension » et « interface » ont été utilisés pour décrire le contact et l'interaction entre les organismes humains et les appareils techniques avec leurs différents degrés d'hybridation, qui à leur tour ont généré toute une série de visions utopiques et dystopiques d'une condition future « post-humaine ». Et bien que la notion même de médium soit strictement liée au problème de la perception sensorielle (puisqu'il trouve l'une de ses origines dans la traduction latine d'un terme grec, _métaxy_, utilisé par Aristote afin d'indiquer les entités intermédiaires matérielles qui rendent la perception possible), le corps lui-même (avec son visage expressif, sa peau sensible et ses gestes et mouvements significatifs) a souvent été considéré comme une sorte de médium primaire, un point de référence crucial pour comprendre la nature même de la médiation.

 Les transformations actuelles dans notre paysage médiatique soulèvent une fois de plus la question de la corrélation entre l'histoire de la technologie et l'histoire du sensorium humain et nous invitent à reconsidérer les différentes relations possibles entre les médias - au sens le plus large du terme - et le sens des sens, des affects et des émotions. Le cinéma, avec les diverses transformations historiques de son dispositif spatial, a fourni pendant des décennies et continue de fournir un domaine particulièrement important pour l'interprétation de la dynamique culturelle impliquée dans la représentation et la réception des identités corporelles et pour l'analyse de l'expérience esthétique, incarnée du spectateur. On peut en dire autant pour les autres médias visuels, audiovisuels et sonores, qui ont essayé de faire passer à travers les grains, les textures et les fréquences de leurs représentations les matérialités différentes, dynamiques des corps et des sensations.

 Aujourd'hui, les nouvelles formes de vie bio-techniques produites par des médias numériques omniprésents et par toute une gamme de pratiques artistiques et non artistiques nous confrontent à des questions théoriques sans précédent, qui peuvent être abordées en combinant des perspectives à la fois archéologiques et tournées vers l'avenir. Nous avons besoin de cadres théoriques appropriés pour comprendre les phénomènes tels que les fonctions sensorielles et cognitives réalisées par les écrans en réseau contemporains, le retour de l'imagerie 3D stéréoscopique, les développements récents dans les domaines de la réalité virtuelle et augmentée, la présence croissante des dispositifs intelligents de détection dans notre environnement de vie, les agences des médias élémentaires et les questions de médiation, ainsi que nos interactions quotidiennes avec les technologies numériques dont les processus et les résultats de calcul sont situés au-dessous ou au-delà des seuils de perception humaine.

Comprendre les nouvelles conditions de la sensibilité humaine et non humaine dans un environnement multimédia en réseau est l'un des principaux défis des études cinématographiques et médiatiques contemporaines.

 La conférence NECS 2017 - qui aura lieu pour la première fois en France, dans un contexte culturel qui a apporté une contribution très importante au développement et à l'institutionnalisation des études cinématographiques et médiatiques - essaiera de relever ce défi en abordant la question cruciale de la relation entre les médias, les corps et les sens à travers les différentes perspectives de recherche poursuivies par les membres de notre communauté.

 Le titulaire de la Chaire de recherche sur les écritures numériques **Marcello Vitali-Rosati** fera une intervention intitulée « The Fantasy of the Perfect Model » au cours de la seconde journée de la Conférence entre 11h et 12h45 dans le cadre du panel _Virtual Reality and Cinema: Environments, Experiences, Narrations_. 
