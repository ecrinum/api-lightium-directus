---
title: The Publishing Sphere
short: <h2>Jeudi 13 juin 2017 (10h30 - 22h) et vendredi 14 juin 2017 (9h45 - 22h)</h2>
cleanstring: The-Publishing-Sphere
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories: null

---
    
### Les écosystèmes de la littérature contemporaine

  
Le domaine de l'écriture n'a jamais été aussi vaste, tout comme l'idée de publication n'a jamais été aussi plurielle. Il n'y a pas un jour qui passe sans qu’un grand pourcentage de l'humanité publie un ou plusieurs textes: sur un blog, un réseau de médias sociaux ou ailleurs. Un programme de deux jours d'édition performative en direct célébrera cette sphère émergente avec des discussions et des conversations, des lectures, des performances sonores, des interventions et plus encore. Radio Brouhaha, lancée lors de l'événement, présentera en _livestream_ \[voicerepublic.com\] la soirée, capturant des sons d'ambiance et d'infrastructure.

L'idée traditionnelle de l'auteur solitaire en contact direct avec son éditeur, et parlant par contumace à un public anonyme est obsolète. Au cours des dernières années, une abondance de pratiques littéraires - spectacles, lectures publiques, travail sonore et visuel et nouveaux espaces publics - ont émergé, formant une « sphère d’édition » artistiquement et politiquement dynamique. S'il est vrai que l'imaginaire de la littérature moderne est constitutif de la fantaisie d'une « bonne » sphère démocratique de publication, alors nous devons trouver quelles sortes de sociétés émergent de la sphère d'édition auxquelles nous sommes confrontés aujourd'hui.

À _The Publishing Sphere_, les érudits, les écrivains, les artistes et les représentants d'initiatives étudieront les différents lieux des littératures contemporaines, entre une sphère abstraite et un espace matériel. Ils explorent ce qui constitue une œuvre littéraire au-delà de la matérialité du livre, exposent d'autres formes d'édition en dehors des textes et enquêtent sur les agents et les joueurs qui habitent le domaine. En alternant entre les tables rondes, les éléments miniatures performatifs, les ateliers et les lectures littéraires, ils examineront leurs pré-réflexions, leurs notes de recherche, leurs documents d'image, leurs références littéraires, afin de créer une sphère de publication qui leur sera propre.

Le titulaire de la Chaire de recherche sur les écritures numériques, **Marcello Vitali-Rosati**, prendra place à une table ronde avec Miriam Rasch entre 16h40 et 17h20, le 13 juin, et offrira une présentation intitulée "Editorialization: Writing and Producing Space".  
  
Pour écouter l'intervention de Marcello Vitali-Rosati, cliquez [ici](https://voicerepublic.com/talks/the-publishing-sphere-2nd-session-protocols).

The Publishing Sphere est soutenu par **Haus der Kulturen der Welt**, l’**Université Paris Lumières**, l'**Institut Universitaire de France** et la **Fondation Alexander von Humboldt**, par l'intermédiaire de la chaire d'Irene Albers à Peter Szondi-Institut für Allgemeine und Vergleichende Literaturwissenschaft de Freie Universität Berlin.
