---
title: Colloque international  « Le temps à l'épreuve des œuvres numériques, les œuvres numériques à l'épreuve du temps »
short: "<h2>Université Jean Monnet, Saint-Etienne, France \r

  <br>Mardi 3 et mercredi 4 octobre 2017</h2>"
cleanstring: Colloque-international---Le-temps-a-lepreuve-des-uvres-numeriques-les-uvres-numeriques-a-lepreuve-du-temps-
lang: FR
caption: ""
image: https://www.univ-st-etienne.fr/_contents/ametys-internal%253Asites/ujm/ametys-internal%253Acontents/cierec-journee-d-etude-face-au-flux-4-3/_metadata/illustration/image/Capture%2520d%25C2%25B9%25C3%25A9cran%25202017-09-09%2520%25C3%25A0%252009.57.22.png?objectId=defaultWebContent%3A%2F%2F79903aae-26bd-40af-b361-29dc1adb9c00
url: ""
urlTitle: ""
categories: null

---
    
![](https://www.univ-st-etienne.fr/_contents/ametys-internal%253Asites/ujm/ametys-internal%253Acontents/cierec-journee-d-etude-face-au-flux-4-3/_metadata/illustration/image/Capture%2520d%25C2%25B9%25C3%25A9cran%25202017-09-09%2520%25C3%25A0%252009.57.22.png?objectId=defaultWebContent%3A%2F%2F79903aae-26bd-40af-b361-29dc1adb9c00)
  
  
**Marcello Vitali-Rosati**, titulaire de la **Chaire de recherche sur les écritures numériques**, aura le plaisir de faire partie du comité scientifique du colloque international « Le temps à l'épreuve des œuvres numériques, les œuvres numériques à l'épreuve du temps », organisé par le **Centre Interdisciplinaire d’Études et de Recherche sur l’Expression Contemporaine** (CIEREC) de l'Université Jean Monnet.  
  
Ce colloque, sous la responsabilité d'**Anolga Rodionoff** et coordonné par **Lorella Abenavoli**, s'inscrit dans la lignée des travaux du CIEREC, qui privilégie l’étude transversale de grandes questions concernant la littérature et les arts d’aujourd’hui en réunissant des enseignants-chercheurs d’Esthétique et Sciences de l’art, d’Arts plastiques, de Design, d’Arts numériques, de Littérature, de Linguistique, de Musicologie.   
  
Programmation:  
  
MARDI 3 OCTOBRE 2017   
MAISON DE L’UNIVERSITÉ - SALLE DES SPECTACLES   
  
9.30 Introduction - Anolga Rodionoff (Université Jean Monnet)   
Modératrice : Carole Nosella   
10.00 Jean Dubois (Université du Québec à Montréal) _La conservation allographique et autographique des arts interactifs : Le cas des œuvres à écran tactile de Jean Dubois (1996-2006)_   
10.40 Valérie Perrin (Espace multimédia Gantner) _La conservation des œuvres d'art numérique à travers l'expérience de la collection de l'Espace multimédia Gantner_   
PAUSE   
11.20 Clarisse Bardiot (Université de Valenciennes) _Préserver les arts de la scène au temps du numérique_   
12.00 Lorella Abenavoli (Université Jean Monnet & Université du Québec à Montréal) _So(g)nifier les silencieux tremblements du temps_   
DÉJEUNER   
Modérateur : Vincent Ciciliato   
14.30 Cécile Le Prado (Cnam, Enjmin, Cologne Game Lab) _Œuvres ouvertes et disparition_   
15.10 Laurent Pottier (Université Jean Monnet) _La préservation des œuvres musicales électroniques temps réel_   
PAUSE   
16.00 Atau Tanaka (Université de Londres) _Le corps comme instrument de musique_   
  
MERCREDI 4 OCTOBRE 2017  
SITE DENIS PAPIN - SALLE 223   
  
Modérateur : David-Olivier Lartigaud   
9.30 Alain Depocas (Conseil des Arts et des Lettres du Québec) _DOCAM : Documentation et conservation du patrimoine des arts médiatiques_   
10.10 Cécile Dazord (Centre de Recherche et de Restauration des Musées de France ) _Conserver, restaurer à l’ère de l’obsolescence technologique_   
PAUSE  
11.00 Anolga Rodionoff (Université Jean Monnet) _Le temps dans l’art contemporain_   
11.40 Rodolphe Olcèse (Université Jean Monnet) _Temps contractés : surrection de l’archive dans le présent de l’œuvre_   
12.20 André Eric Létourneau (Université du Québec à Montréal) _Le Musée Standard : les archives-créations télématiques de collectif “La société de conservation du présent”_   
CLÔTURE DU COLLOQUE ET DÉJEUNER   
  
Pour plus de détails, consultez le [site de l'Université Jean Monnet](https://www.univ-st-etienne.fr/fr/cierec/agenda/actualites-2017/cierec-colloque-international-le-temps-a-l-epreuve-des-oeuvres-numeriques-les-oeuvres-numeriques-a-l-epreuve-du-temps.html).
