---
title: Participation au Podcast Rendez-vous
short: <h2>Samedi 16 septembre 2017</h2>
cleanstring: Participation-au-Podcast-Rendez-vous
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories: null

---
    
Hier, notre collègue **Peppe Cavallari**, doctorant en Technologie et Sciences de l’homme (UTC, Compiègne; UdeM, Montréal), était l'invité d'**Anne Vidal** dans le podcast _Rendez-vous_ dont le thème était « Cultures numériques : où est le corps ? ».  
  
Vous pouvez écouter son intervention en suivant le lien vers le compte [Archive](https://archive.org/details/culture-numerique-corps) de la chaire.
