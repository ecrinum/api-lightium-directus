---
title: Appel de textes pour <i>Études cinématographiques</i> n°75, 2018
short: ""
cleanstring: Appel-de-textes-pour-iEtudes-cinematographiquesi-nsevenfive-twozerooneeight
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories: null

---
    
## « Cinéma et journalisme » Dossier sous la direction de **Richard Bégin**, **Thomas Carrier-Lafleur** et **Mélodie Simard-Houde**

« Le journalisme, vois-tu, c’est la religion des sociétés modernes, et il y a progrès. »   
 — Honoré de Balzac, _La Peau de chagrin_ 

« Que le Cinéma ait, pour une part importante, contribué, depuis le début du XXe siècle, à l’évolution des idées et des mœurs, c’est indiscutable et c’est dans sa nature même. Il lui suffisait d’être lui-même et d’exercer son activité dans les voies et par les moyens qui lui sont naturels ». C’est par ses lignes que s’ouvre l’ouvrage de René Jeanne et Charles Ford, _Le Cinéma et la Presse (1895-1960)_[\[1\]](#note1). Est ensuite développée la transition paradigmatique, maintes fois glosée, qui va du « cinématographe » Lumière au « cinéma » de Méliès, donnant ainsi du grain à moudre au lieu commun qui veut que le dispositif cinématographique soit porteur d’une double identité originaire. Fenêtre ouverte sur le monde ou prestidigitation totale, le cinéma, selon le mot du magicien de Montreuil, serait ni plus ni moins que « le monde à portée de la main », ce qui incite nos deux auteurs à proposer l’analogie suivante, que leur ouvrage tentera ensuite de poursuivre : « Le monde réel sous tous ses aspects, ce qui est exactement le rôle, la mission de la Presse[\[2\]](#note2) ». Or, en dépit du caractère plutôt péremptoire et ontologiquement flou de telles affirmations, il est vrai que le Cinéma et la Presse – ainsi unis par deux capitales qui les propulsent dans le monde des idées – semblent en effet, et _depuis toujours_, posséder un grand nombre d’affinités électives, ce qui les amène à investir symboliquement le même espace dans notre sphère médiatique, nos sociétés et notre imaginaire. Comme le soulignait le personnage balzacien de La Peau de chagrin (1831), quelques années avant ce moment que Marie-Ève Thérenty et Alain Vaillant ont nommé « l’an un de l’ère médiatique[\[3\]](#note3) » – soit le lancement du quotidien _La Presse_ par Émile de Girardin, en 1836 –, _le journalisme est inséparable d’une certaine image du monde moderne_. Autre vecteur d’imaginaire, dont l’institutionnalisation succède de quelques décennies à celle de la grande presse, il en sera de même pour le cinématographe, qui deviendra rapidement l’art de masse et le moyen d’expression des foules, poursuivant ainsi le processus benjaminien de _la dégradation de l’« aura » par l’« exposition » du réel_, tel que déjà amorcé par le journal. Aussi, au même titre que, depuis la division archétypale entre _bas_ et _haut_ de page, le journal a été investi comme un ensemble multiforme – accueillant nouvelles, entrefilets, reportages, feuilletons, publicités, illustrations et photographies –, le média cinématographique repose également sur l’_hybridité de la chronique et du romanesque_. Avec la propagation des actualités filmées au début du XXe siècle, le cinéma concurrencera d’ailleurs le journalisme sur son propre terrain, de même qu’il montrera très tôt à l’œuvre dans ses fictions des personnages de journalistes pratiquant leur métier. L’indiscernabilité entre les deux médias deviendra encore plus grande alors que le journalisme commencera à _investir d’autres espaces_, dont en premier lieu l’écran télévisuel, qui lui-même se positionne en tant que concurrent immédiat de l’institution filmique. Maintenant fait de sons et d’images en mouvement, le journalisme télévisuel – et, bientôt, le journalisme numérique – s’appropriera rapidement le _langage du cinéma_. En revanche, cette _remédiation_[\[4\]](#note4) du cinéma par le journal contribuera largement à l’émergence de _nouveaux genres cinématographiques_, tels le cinéma-vérité et le cinéma direct, venant ainsi boucler une nouvelle fois la boucle de cette _redéfinition continuelle des médias_, dont les alliances et les rivalités entre cinéma et journalisme offrent un exemple privilégié.  

En s’intéressant aussi bien aux questions d’ordre philosophique qu’aux problèmes plus matériels, c’est précisément cette _porosité des frontières propres à l’environnement médiatique moderne et contemporain_ que souhaite étudier le dossier « Cinéma et journalisme ». À l’ère d’une redéfinition générale de l’identité des médias traditionnels qu’est celle de nos _hypermédias_, une telle réflexion semble même de plus en plus nécessaire. Plutôt que de considérer le cinéma et le journalisme comme des entités closes qui s’offriraient d’un coup à l’entendement, les auteurs sont au contraire invités à mettre à profit les _crises identitaires_ et les _naissances multiples_[\[5\]](#note5) constitutives de l’évolution des deux médias. De l’anticipation d’une vision cinématographique du monde à même l’espace déjà multimédiatique de la grande presse jusqu’aux représentations cinématographiques et télévisuelles de l’activité journalistique contemporaine, les articles recherchés peuvent aussi bien porter sur des réflexions historiques et théoriques plus générales que sur des études de cas. Interdisciplinaire, le dossier souhaite accueillir des chercheurs aux horizons multiples et complémentaires (études cinématographiques, études littéraires, théorie des médias, philosophie, etc.).   
Sans restriction et sans exclusivité, les propositions pourront entre autres être axées sur l’un ou l’autre des sujets suivants : 
* La représentation des journalistes et de la presse dans les fictions cinématographiques et télévisuelles.
* Les « newspaper movies » et l’âge d’or du cinéma classique.
* L’institutionnalisation du cinéma par la presse.
* L’horizon hypermédiatique de la page de journal.
* La postérité transmédiatique du reportage et du grand reportage.
* Le feuilleton comme forme hybride.
* La dimension journalistique de la pratique documentaire.
* La dialectique fiction/documentaire au sein des deux médias.
* L’impact des nouvelles technologies sur la pratique, la réception et l’imaginaire des deux médias.
* Les rapports entre cinéma et journalisme à même la fiction littéraire.
* La double représentation cinématographique et journalistique d’un événement.
* L’évolution de l’écran de cinéma et de la page de journal.
* Le journalisme citoyen et autres pratiques sociales transmédiatiques.

Accompagnées d’une courte bibliographie ainsi que d’une notice biobibliographique (150 mots), les propositions d’articles, d’environ 500 mots, devront être envoyées à Thomas Carrier-Lafleur ([thomas.carrier-lafleur@umontreal.ca](mailto:thomas.carrier-lafleur@umontreal.ca)) avant le **1er décembre 2017**. D’une longueur maximale de 30 000 signes, les articles seront ensuite à remettre pour le **1er juin 2018**.   
  
  
\[1\] René Jeanne et Charles Ford, _Le Cinéma et la Presse (1895-1960)_, Paris, Armand Colin, 1961, p. 5.  
\[2\] _Ibid_, p. 6\.   
\[3\] Marie-Ève Thérenty et Alain Vaillant, _1836 : L’An I de l’ère médiatique. Analyse littéraire et historique de_ La Presse _de Gidardin_, Paris, Nouveau Monde, 2001.  
\[4\] Jay David Bolter et Richard Grusin, _Remediation: Understanding New Media_, Cambridge (Mass.), MIT Press, 1999\.   
\[5\] Voir André Gaudreault et Philippe Marion, « Un média naît toujours deux fois... », _Sociétés & Représentations_, no 9, 2000, p. 21-36\. 
