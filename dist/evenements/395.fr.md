---
title: Conférence de Jason Boyd
short: "<h2>Université de Montréal\r

  <br>Pavillon Lionel-Groulx, Salle C-8111\r

  <br>Vendredi 10 novembre, 11h</h2>\r\n"
cleanstring: Conference-de-Jason-Boyd
lang: FR
caption: ""
image: https://0.academia-photos.com/820458/284792/336813/s200_jason.boyd.jpg
url: ""
urlTitle: ""
categories: null

---
    
### « People, Talking : Using TEI to Analyze Information Transmission in a Biographical Corpus »

  
![](https://0.academia-photos.com/820458/284792/336813/s200_jason.boyd.jpg)

  
La Text Encoding Initiative (TEI) Guidelines contient un ensemble d'éléments pour construire des entrées structurées sémantiquement sur des individus. Le cas discuté ici, le Texting Wilde Project (TWP), qui comprend un corpus de textes biographiques anciens (avant 1945) concernant Oscar Wilde, ajoute un nombre de composants additionnels au simple modèle factuel du « cette chose à propos de cette personne est dite dans cette source ». Le corpus biographique TWP, tout comme ceux concernant d'autres corpus du XIXe siècle et même avant, inclue les sources originaires de la plupart des informations constituant le Oscar Wilde que nous connaissons aujourd'hui (principalement, comme réitéré et réinterprété dans nombreux textes biographiques contemporains).

Jason Boys est professeur agrégé du département de Langue Anglaise de Ryerson University et membre de faculté de la Maîtrise en Médias Numérique, faisant partie du programme conjoint des universités Ryerson et York « Communication and Culture ». Il est codirecteur du Centre des Humanités Numérique de Ryerson et vice-directeur du Digital Humanities Summer Institute. Il a travaillé sur le développement de méthodes d'études de larges corpus biographiques assistées par l'ordinateur dans son « Texting Wilde Project », financé par le CRSH, projet qui étudie les premiers textes biographiques concernant Oscar Wilde.

  
Voici l'intervention integrale de Jason Boyd :  
  
