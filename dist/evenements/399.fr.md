---
title: Ontologie du numérique
short: <h2>Dossier Sens Public</h2>
cleanstring: Ontologie-du-numerique
lang: FR
caption: ""
image: http://sens-public.org/IMG/arton1282.jpg?1513282518
url: ""
urlTitle: ""
categories: null

---
    
![](http://sens-public.org/IMG/arton1282.jpg?1513282518)
  
  
Dirigé par Servanne Monjour, Matteo Treleani et Marcello Vitali-Rosati, ce dossier Sens Public est consacré au rapport entre imaginaire, réel et mimesis à l'ère du numérique.

  
> « Ce dossier se conçoit comme un champ d’exploration des problématiques ontologiques du numérique, dans une perspective résolument interdisciplinaire, accueillant tout autant la philosophie, l’esthétique, les études littéraires, la sémiologie, la sociologie ou les sciences de l’information et de la communication. Des arts numériques à la littérature hypermédiatique, en passant par les webdocumentaires et les jeux vidéo, de nombreux domaines permettent en effet d’étudier ces dichotomies apparemment périlleuses entre représentation et réalité, réel et imaginaire, fiction et documentaire… »

  
Avec des articles de Marta Boni Lund, Pierre Lévy, Enrico Agostini-Marchese, Filip Dukanic, Renée Bourassa, Suzanne Paquet, Eric de Thoisy, Ariane Maugery, Lucie Roy, Miruna Craciunescu, Sophie Beauparlant, Lia Kurts-Wöste, Adriano Fabris et Nicolas Sauret.

Pour accéder au dossier cliquez [ici](http://sens-public.org/article1282.html).
