---
title: A glass behind the screen
short: "<h2>January, 17th - June, 6th.\r

  <br>\r

  7 pm, NUMA - Paris\r

  <br>\r

  All sessions will be live-broadcasted</h2>"
cleanstring: A-glass-behind-the-screen
lang: EN
caption: ""
image: http://uvde.fr/wp-content/uploads/2014/12/uvde_plateau_media.png
url: ""
urlTitle: ""
categories: null

---
    
![](http://uvde.fr/wp-content/uploads/2014/12/uvde_plateau_media.png)
  
  
[_Un verre derrière l’écran_](http://uvde.fr) is a digital culture seminar aiming to investigate, from a social and human sciences point of view, the new technologies' issues in contemporary society and culture. It is an opportunity to debate on digital-oriented theories, methods and practices. The purpose is to critically exchange and discuss on these themes in order to let the public opinion be more aware of the digital culture.  
  
These meetings want themselves to be a bridge between human sciences and digital players. The meetings are free and open to everybody and they gather together academics coming form professional and media domains. Every session is live broadcasted. _Un verre derrière l'écran_ is as well present on [**Facebook**](https://www.facebook.com/unverrederrierelecran/) and [**Twitter**](https://twitter.com/uvde%5F).  
  
  
Sessions schedule:  
  
January 17th - **Camille Alloing et Julien Pierre** : [**« DON’T WORRY, TWEET HAPPY EMOJI, LIKES ET TRAVAIL AFFECTIF »**](http://uvde.fr/seance/dont-worry-tweet-happy-emoji-likes-travail-affectif/)   
  
January 31st - **Anthony Ferretti** :[ « **DES ENJEUX EN DESIGN. VRAP : COMMENT FAIRE VENIR UN OBJET AVEC SON ENVIRONNEMENT ?** »](http://uvde.fr/seance/enjeux-design-vrap-faire-venir-objet-environnement/) ;  
  
March 14th - **Thomas Thibault** :[ « **DU DESIGN EN JEUX. COMMENT FAIRE VENIR LE JEU ET QUE NOUS INSPIRE-T-IL ?** »](http://uvde.fr/seance/design-jeux-faire-venir-jeu-inspire-t/) ;  
  
March 28th - **Christian Licoppe** : [ « **MOBILITÉ DIGITALE ET ESPACES PUBLICS URBAINS** »](http://uvde.fr/seance/mobilite-digitale-espaces-publics-urbains/) ;  
  
May 6th -   **« TÉMOINS DE PRÉSENCE ET CONSTAT D’ACTIVITÉ : LES DÉCALAGES DU TEMPS (IR)RÉEL » ;** 
  
**June 6th - Virginie Julliard :[ «   COMMENT GARDER LES DÉFUNTS VIVANT ? »](http://uvde.fr/seance/garder-defunts-vivant/).** 
  
  
**Seminar's organizer : [Hétic](http://www.hetic.net) and [Numa](https://paris.numa.co), with the partnership of [Canada Research Chair on Digital textualities](http://ecrituresnumeriques.ca/en/), the [CRIHN - Intercollegiate Research Center on Digital Humanities](http://www.crihn.org) and the [University of Montréal](http://www.umontreal.ca)**
