---
title: Marcello Vitali-Rosati symposium at Rouen University
short: "<h2>15th March 2018\r

  <br />10 am\r

  <br />University of Rouen</h2>"
cleanstring: Marcello-Vitali-Rosati-symposium-at-Rouen-University
lang: EN
caption: ""
image: ""
url: ""
urlTitle: ""
categories: null

---
    
Within his staying as Visiting Professor at the University of Rouen, Marcello Vitali-Rosati will held the symposium "Une API pour l'Anthologie grecque: repenser le codex Palatinus à l'époque du numérique".

He will show and discuss the researches on the [Greek Anthology](http://ecrituresnumeriques.ca/en/2016/1/19/Greek-Anthology) and on the [platform](https://anthologia.ecrituresnumeriques.ca/home) developed by the Chair for collaborative translation and edition of the manuscript.
