---
title: Séminaire de Marcello Vitali-Rosati à l'Université de Rouen
short: "<h2>15 mars 2018\r

  <br />10h\r

  <br />Université de Rouen</h2>"
cleanstring: Seminaire-de-Marcello-Vitali-Rosati-a-lUniversite-de-Rouen
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories: null

---
    
Dans le cadre de son séjour à l'Université de Rouen en tant que professeur invité, Marcello Vitali-Rosati donnera le séminaire "Une API pour l'Anthologie grecque: repenser le codex Palatinus à l'époque du numérique".

Il présentera les recherches sur l'[Anthologie Palatine](http://ecrituresnumeriques.ca/fr/2016/1/19/Anthologie-Palatine) et sur la [plateforme](https://anthologia.ecrituresnumeriques.ca/home) développée par la Chaire pour la traduction et l'édition collaborative du manuscrit.
