---
title: Séances de travail des membres du CRIHN 2017-2018 - Stéfan Sinclair
short: "<h2>Université de Montréal\r

  <br>Salle C-2059\r

  <br>Vendredi, 23 février, 13h</h2>"
cleanstring: Seances-de-travail-des-membres-du-CRIHN-twozerooneseven-twozerooneeight---Stefan-Sinclair
lang: FR
caption: ""
image: http://www.crihn.org/files/sites/33/2015/07/sinclair-225x225.png
url: ""
urlTitle: ""
categories: null

---
    
![](http://www.crihn.org/files/sites/33/2015/07/sinclair-225x225.png)
  
  
Notre deuxième séance de travail des membres du CRIHN pour 2017-2018 sera consacrée au projet de Stéfan Sinclair : « DreamScape: A Peek into the Development of a New Spatial Humanities Tool ».

Stéfan Sinclair est professeur agrégé en Humanités numériques à McGill University et Directeur du Centre pour les Humanités numériques de McGill. Son domaine de recherche principal est le design, le développement, l'utilisation et la théorie des outils pour les humanités numériques, notamment pour l'analyse et la visualisation de textes
  
  
