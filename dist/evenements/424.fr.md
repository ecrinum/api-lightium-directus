---
title: "Parution des actes du colloque Reprendre À tout prendre "
short: <h2>Avec une contribution de Thomas Carrier-Lafleur</h2>
cleanstring: Parution-des-actes-du-colloque-Reprendre-A-tout-prendre-
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories: null

---
    
Suite au colloque « Reprendre _À tout prendre_ », qui a eu lieu le 12 novembre 2015, consacré au film _À tout prendre_ de Claude Jutra, la publication des actes du colloque, dirigée par Pierre Jutras, vient proposer au public un retour sur la journée d'étude.

Parmi les contributions de ce dossier thématique — disponible sur le [site internet de la Cinémathèque québécoise](http://collections.cinematheque.qc.ca/dossiers/a-tout-prendre/colloque-reprendre-a-tout-prendre/) —, Thomas Carrier-Lafleur nous livre une réflexion intitulée « L’œuvre fait chair. _À tout prendre_, _Jeux de dames_ et les dispositifs de soi », consultable gratuitement [en ligne](http://collections.cinematheque.qc.ca/dossiers/a-tout-prendre/colloque-reprendre-a-tout-prendre/loeuvre-fait-chair-a-tout-prendre-jeux-de-dames-et-les-dispositifs-de-soi-par-thomas-carrier-lafleur-universite-de-montreal-graficsfigura/).
