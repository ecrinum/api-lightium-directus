---
title: L’invention littéraire des médias
short: <h2>Sens Public Special Issue</h2>
cleanstring: Linvention-litteraire-des-medias
lang: EN
caption: ""
image: http://www.sens-public.org/IMG/arton1305.jpg
url: ""
urlTitle: ""
categories: null

---
    
![](http://www.sens-public.org/IMG/arton1305.jpg)

Would media exist without Literature ? Could we coin the terms “television”, “photography”, “cinema” ou “digital” had these devices not been built, institutionalized—sometimes deconstructed in the collective mind ? As the digital world is reinventing itself, the current issue emphasizes the role of literature in the construction of our media. At the same time, the hybridation of our digital contemporaneity encourages us to rethink the capacity of the media to reinvent themselves reciprocally, renewing the order of the speech and the function of Literature. Because of its capacity to testify the heterogeneity of the media universe, Literature offers a fertile ground—where everything is yet to be done—to take on such a research.

Issue edited by Thomas Carrier-Lafleur, André Gaudreaul, Servanne Monjour, and Marcello Vitali-Rosati
