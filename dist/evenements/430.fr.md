---
title: Prix Jean-Claude Guédon
short: "<h2>Date limite pour les candidatures : 1er septembre 2018</h2>"
cleanstring: Prix-Jean-Claude-Guedon
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories: null

---
    
Le _Centre de recherche interuniversitaire sur les humanités numériques_ est très fier d’annoncer la création d’un prix qui sera remis pour la première fois lors du colloque international bilingue soulignant le cinquième anniversaire du centre en octobre 2018 à Montréal.

D’une valeur de 1,000$ et nommé en l’honneur d’un des pionniers mondiaux de l’accès libre, le prix Jean-Claude Guédon sera remis annuellement à l’auteur-e du meilleur article consacré à des questions liées aux publications savantes en ligne et/ou à l’accès libre. Écrit en français ou en anglais, l’article doit être soumis par un étudiant ou un chercheur en début de carrière (c’est-à-dire ayant obtenu son doctorat depuis moins de dix ans) et avoir été publié entre le 1er janvier 2017 et le 1er août 2018\. Le texte doit être disponible en ligne, en accès libre (dans une revue en accès libre ou un dépôt institutionnel, par exemple).

Veuillez envoyer par courriel un bref CV et le lien vers votre article avant le 1erseptembre 2018, à l'adresse mail du CRIHN : [crihunum@gmail.com](mailto:crihunum@gmail.com)
