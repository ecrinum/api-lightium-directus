---
title: "Colloque CRIHN 2018 : Repenser les humanités numériques"
short: "<h2>25-27 octobre 2018\r

  <br />Carrefour des Arts et des Sciences\r

  <br />Université de Montréal"
cleanstring: Colloque-CRIHN-twozerooneeight--Repenser-les-humanites-numeriques
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories: null

---
    
Durant la semaine mondiale de l’accès libre, le CRIHN fêtera ses cinq ans avec un colloque international bilingue du jeudi 25 octobre au samedi 27 octobre 2018.

Le colloque aura lieu au Carrefour des Arts et des Sciences à l’Université de Montréal.

Le programme du colloque est maintenant disponible sur le site du [CRIHN](http://crihn.openum.ca/colloque-2018/programme/).

Les [inscriptions en ligne](https://ecommerce.dexero.com/shopping/umontreal/fas/event/colloque%5Fcrihn%5F2018%5Frepenser%5Fles%5Fhumanites%5Fnumeriques%5Fthinking%5Fthe%5Fdigital%5Fhumanities%5Fanew/Detail.view) sont maintenant ouvertes.
