---
title: "Conference de Junia Barreto : Scenophilie : le cinéma de François Ozon"
short: "<h2>Lundi 4 juin\r

  <br />14h, Salle C-8111\r

  <br />Université de Montréal"
cleanstring: Conference-de-Junia-Barreto--Scenophilie-le-cinema-de-Francois-Ozon
lang: FR
caption: ""
image: http://bonemagazine.com/cache/slider/uploads/6e0af99fd75d4260346937a581ce9e3b612.jpg
url: ""
urlTitle: ""
categories: null

---
    
![](http://bonemagazine.com/cache/slider/uploads/6e0af99fd75d4260346937a581ce9e3b612.jpg)

En-deçà des différents formats numériques qui retiennent notre attention, les écrans sont d’abord des images. Images qui sont au centre de l’écriture des cultures contemporaines, fortement centrées sur l’audiovisuel. L’expérience des cinéastes est unique pour aborder cette question. Le cinéma s’est développé en interrogeant toujours plus intensément les types de représentation du monde et des rapports qu’il établit avec les spectateurs. À l’heure actuelle, il s’empare des technologies de l’ère numérique en tant qu’industrie, mais puise aux sources culturelles les plus diverses en tant qu’il est un art modifiant l’écriture du monde.

Héritier de la Nouvelle Vague, le cinéaste François Ozon observe les incertitudes de notre époque, nous permettant établir une réflexion sur le statut des images pour penser la société. Chacune de ses créations exalte le bonheur de mettre en scène et de présenter des attitudes et des existences singulières qui interrogent nos certitudes. Ozon a une prédilection pour les différentes scènes issues des arts - la littérature, le théâtre, la musique, celle du cinéma lui-même - et pour l’idée de mise en scène et de performance. Ses fictions sont marquées par le mélange de genres et il y produit une critique matérialiste de la vie numérique à travers une sorte de sampling artistique. 

De là notre thèse considérant le mouvement de l’écriture ozonienne comme marqué par ce que nous nommons « scénophilie », c’est-à-dire, un état constant de productions de narration. Ce terme indique également que l’écran où se projette le film accueille les traces de processus divers et complexes (ce qui engagerait une intermédialité). La mise en scène, quel qu’en soit le support, devenue une dimension permanente des rapports humains, nous permet d’explorer l’hypothèse de l’existence d’une “scénographie généralisée” - la scénophilie, devenue un mode d’intervention dominant la création contemporaine et dont l’œuvre de François Ozon nous semble paradigmatique.
  
  
