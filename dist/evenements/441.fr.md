---
title: "Conférence de Marcello Vitali-Rosati et Pierre Lévy : Écrire, connaitre et faire de la recherche à l'ère numérique"
short: "<h2>Mercredi 26 septembre\r

  <br />19h, Desmarais 12102\r

  <br />Université d'Ottawa</h2>"
cleanstring: Conference-de-Marcello-Vitali-Rosati-et-Pierre-Levy--Ecrire-connaitre-et-faire-de-la-recherche-a-lere-numerique
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories: null

---
    
Pour célébrer le programme de doctorat en communication à l'Université d'Ottawa, cet événement annuel réunit des chercheurs en communication renommés pour partager leurs points de vue sur les événements actuels. Cette année, le département de communication accueillera Pierre Lévy, professeur à l’Université d’Ottawa et ancien titulaire de la Chaire de recherche du Canada en intelligence collective et Marcello Vitali-Rosati, professeur agrégé au département des littératures de langue française de l'Université de Montréal et titulaire de la Chaire de recherche du Canada sur les écritures numériques. Ils discuteront les changements dans le monde numérique et l’impact de ceux-ci sur la recherche en communication
  
  
