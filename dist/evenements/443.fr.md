---
title: Mythologies postphotographiques de Servanne Monjour
short: <h2>Parution du livre de Servanne Monjour chez PUM - Parcours numériques</h2>
cleanstring: Mythologies-postphotographiques-de-Servanne-Monjour
lang: FR
caption: ""
image: http://parcoursnumeriques-pum.ca/IMG/jpg/pn-mythologie_postphoto-ep-9_96_mm-profil_gauvin-hr_web.jpg
url: ""
urlTitle: ""
categories: null

---
    
![](http://parcoursnumeriques-pum.ca/IMG/jpg/pn-mythologie_postphoto-ep-9_96_mm-profil_gauvin-hr_web.jpg)
  
  
Désormais, nous sommes tous photographes. Les appareils intégrés à nos téléphones nous permettent de capter, de visualiser, de modifier et de partager nos photos sur les réseaux sociaux en moins d’une minute. Omniprésente sur nos écrans, la photographie est devenue une nouvelle forme de langage. Alors que les clichés s’accumulent par centaines sur nos disques durs, où l’on finit pas les oublier, certaines voix s’élèvent pour se demander si, dans sa transition de l’argentique vers le numérique, la photographie n’aurait pas perdu ce qui la rendait justement photographique. Pour comprendre ces mutations fascinantes — et un peu inquiétantes — de la culture visuelle, l’auteure analyse les pratiques photographiques contemporaines, à la fois amateur et artistiques, ainsi que les discours, surtout littéraires, consacrés à l’image. Elle décortique ainsi les nouvelles mythologies de l’image pour mieux les recadrer dans une histoire générale de la photographie, avec autant de brio et d’érudition.  
[Servanne Monjour, _Mythologies postphotographiques. L'invention littéraire de l'image numérique_.](http://parcoursnumeriques-pum.ca/mythologiespostphotographiques)
