---
title: "Participation de Marcello VItali-Rosati au Webinaire : Culture numérique et ville inclusive"
short: "<h2>Uninorte-Acre\r

  <br />Rio Branco, Brésil\r

  <br />21 novembre 2018</h2>"
cleanstring: Participation-de-Marcello-VItali-Rosati-au-Webinaire--Culture-numerique-et-ville-inclusive
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories: null

---
    
## Éditorialisation: comprendre la réalité numérique

L’environnement numérique semble réaliser de façon définitive le projet encyclopédique: la totalité du réel est décrite, définie, expliquée. La relation entre le monde et la connaissance est tellement étroite qu’il n’est plus possible de faire la différence entre les deux. Nous manipulons le monde via les informations numériques. Le savoir devient opérationnel: nous faisons des choses, déplaçons des objets, organisons l’architecture des espaces en manipulant les informations numériques.Cette dynamique de structuration de l’espace est ce que j’appelle “éditorialisation”. Sa caractéristique fondamentale, et ce qui la distingue de l’édition et de la curation des contenus, est qu’elle ne vise pas tellement à structurer des informations à propos du monde; elle vise à structurer le monde lui-même: on éditorialise les choses et non les informations sur les choses. Quelles sont les implications de ce changement de paradigme?

**Programme du Webinaire « Culture numérique et ville inclusive »**

**Centre Universitaire Uninorte-Acre, Rio Branco, Brésil 21/11/ 2018**

* 08:00 **⇒** Discours d’ouverture, Professeure Solange Chalub, Coordinatrice du département des formations de troisième cycle (Pós-graduação) à Uninorte-Acre (5 min);
* 08:05 **⇒** Discours de l’Institution, Madame Indira Maria Kitamura, Directrice de marché à Uninorte-Acre (5 min);
* 08:10 **⇒** Discours du Maire de la ville de Rio Branco, Madame Socorro Neri (5 min);
* 08:15 ⇒ Introduction au Webinaire, Dr. Hadi Saba Ayon, enseignant chercheur, CDHET, France, Uninorte-Acre, Brésil (5 min);
* 08:20 **⇒** 1ère présentation, Professeure Dorien Kartikawangi, École de communication à l’Université Catholique à Jakarta en Indonésie (20 min);
* 08:40 — Débat (5 min);
* 08:45 **⇒** 2ème présentation, Professeure Béatrice Galinon-Mélénec, Université Le Havre Normandie et Dr. Cyril Desjeux, Observatoire National des Aides Humaines à Paris (20 min);
* 09:05 — Débat (5 min);
* 09:10 **⇒** 3ème présentation, Armony Altinier, Koena pour l’accessibilité numérique au service de l’inclusion des personnes handicapées, Paris (20 min);
* 09:30 Débat (5 min);
* 09:35 **⇒** 4ème présentation, Dr. Agnès d’Arripe, Université Catholique de Lille (20 min);
* 09:55 — Débat (5 min);
* 10:00 **⇒** 5ème présentation, Professeur Marcello Vitali-Rosati, Université de Montréal au Canada (20 min);
* 10:20 — Débat (5 min);
* 10:25 **⇒** 6ème présentation, Jean-Pierre Robin, Réseau International sur le Processus de production du handicap, Québec-Canada (20 min);
* 10:45 — Débat (5 min);
* 10:50 **⇒** 7ème présentation, Maria Fernanda Arentsen, Université de Saint-Boniface, Manitoba, Canada (20 min);
* 11:10 — Débat (5 min);
* 11 :15 **⇒** 8ème présentation, Professeure Lucia de Anna, Université Foro Italico à Rome, Italie (20 min );
* 11 :35 — Débat (5 min);
* 11 :40 **⇒** 9ème présentation, Professeure Salete Maria Chalub Bandeira, Université Fédérale de l’Acre au Brésil (15 min);
* 11:55 — Débat (5 min);
* 12:00 **⇒** 10ème présentation, Armando Borges, spécialiste en éducation spécialisée et inclusive, Faculté SINAL à Rio branco-Acre au Brésil (15 min);
* 12:15 — Débat (5 min);
* 12:20 **⇒** 11ème présentation, Professeure Simone Maria Chalub Bandeira Bezerra, Université Fédérale de l’Acre au Brésil (15 min);
* 12:35 — Débat (5 min);
* 12:40 **⇒** Discours de clôture, Janeo da Silva Nascimento et Vander Magalhães Nicacio, enseignants à Uninorte-Acre au Brésil.
* 13:00 — Fin.
