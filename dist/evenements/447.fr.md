---
title: Séminaire Écritures numériques et éditorialisation 2018-2019
short: "<h2>Université de Montréal\r

  <br />15 novembre 2018 - 9 mai 2019</h2>"
cleanstring: Seminaire-Ecritures-numeriques-et-editorialisation-twozerooneeight-twozeroonenine
lang: FR
caption: ""
image: https://wpchaire.ecrituresnumeriques.ca/wp-content/uploads/2018/11/AfficheSeminaireEdito18-19.png
url: ""
urlTitle: ""
categories: null

---
    
![](https://wpchaire.ecrituresnumeriques.ca/wp-content/uploads/2018/11/AfficheSeminaireEdito18-19.png)

Le séminaire _Écritures numériques et éditorialisation_ reprend son cycle annuel sur la thématique **« Architectures et connaissances »**.  
Cette année, l’organisation des séances a été distribuée à plusieurs institutions, invitées à prendre en charge successivement une séance et un intervenant. Le format des séances évolue en conséquence avec une intervention unique, suivie d’une discussion longue.  
En faisant un pas de côté par rapport au champ numérique, il s’agira cette année d’explorer les liens qu’entretiennent structures et autorité, infrastructures et pouvoir, et comment s’y négocient encore des espaces, des pratiques et des communautés. Le paradigme architectural de production d’un espace à vivre et à habiter collectivement pourra en retour nous éclairer sur la nature des espaces numériques, façonnés autant par des écritures structurelles que des écritures relevant de l’ethos (Merzeau, 2016).  
Nous aurons le plaisir d’accueillir **Emmanuel Château-Dutier** pour **la séance d’ouverture le 15 novembre prochain** à l’Université de Montréal (salle C-2059, pavillon Lionel Groulx), de 11h30 à 13h30.  
**Calendrier** :  
* **15 novembre 2018** – séance #1 à l’Université de Montréal – avec Emmanuel Château-Dutier — [enregistrement](https://www.youtube.com/watch?v=H4CLxGNrEh8).
* **13 décembre 2018**, horaire spécial : **10h30/16h30** – séance #2 organisée par Sylvia Fredriksson à La Myne — Avec Antoine Burret — [enregistrement](https://www.youtube.com/watch?v=gQc5yNVK7js).
* **17 janvier 2019** – séance #3 organisée par Marta Severo et Evelyne Broudoux au laboratoire Dicen-IDF (CNAM) — avec Dario Rodighiero — [vidéo en direct](https://www.youtube.com/watch?v=rMaUXVhHY8k).
* **14 février 2019** – séance #4 organisée par Michaël Nardone à la Concordia University
* **21 mars 2019** – séance #5 organisée par Benoit Epron à la Haute École Spécialisée de Suisse Occidentale
* **11 avril 2019** – séance #6 organisée par Servanne Monjour à la McGill University
* **09 mai 2019** – séance #7 organisée par Matteo Treleani et Joana Casenave au laboratoire GERiiCO (Université Lille-3)
