---
title: "Francesco Di Teodoro : architecture entre fonction structurale et perception"
short: "<h2>Séance spéciale séminaire Éditorialisation et écritures numériques\r

  <br />Mardi 12 mars\r

  <br />Université de Montréal, Salle C-8132, 11h30\r

  </h2>"
cleanstring: Francesco-Di-Teodoro--architecture-entre-fonction-structurale-et-perception
lang: FR
caption: ""
image: https://i.imgur.com/oqfzkLS.png
url: ""
urlTitle: ""
categories: null

---
    
![](https://i.imgur.com/oqfzkLS.png)

La quatrième séance du séminaire "Éditorialisation et écritures numériques", dirigée par Marcello Vitali-Rosati et Nicolas Sauret à l'Université de Montréal, accueillera **Francesco di Teodoro** qui parlera d'« Architecture, entre fonction structurale et perception » 

Pour initier la discussion, nous aborderons les thèmes de la perception et de la communication/traduction en architecture : les systèmes et les ordres architectoniques, voûtés et à charpente indépendante, la dichotomie "en soi" entre aspect et résistance, vu et caché, vrai et faux, vrai faux, faux vrai. On discutera des méthodes de représentation architectonique : comment montrer le même objet entre immédiateté, recomposition mentale, conventions. Finalement, on parlera de la traduction des images (plus précisément celles du _De architectura_ de Vitruve), dans la vulgarisation et dans les langues nationales, tenant compte du contexte et de la nécessité de communiquer de façon claire, compréhensible selon le milieu culturel et identitaire : l'Ancien comme ancien local et l'Ancien comme nouveau local.

Francesco Paolo di Teodoro est professeur en histoire de l'architecture au Polytechnique de Turin. Diplômé à l'université de Florence, il a enseigné à l'École de spécialisation en histoire de l'art du Moyen Âge et moderne de l'université La Sapienza de Rome. Il a également été professeur invité à l'École Pratique des Hautes Études de Paris. Ses intérêts de recherche portent sur l'histoire de l'art et de l'architecture modernes (Brunelleschi, Leon Battista Alberti, Piero della Francesca, Donato Bramante, Leonardo, Raffaello, Ammannati), de la tradition vitruvienne et de l'édition critique des textes d'intérêts historico-artistique. À côté de ses recherches universitaires, il est engagé dans l'activité de commissaire d'expositions, notamment de l'exposition sur Léonard de Vinci (Musei Reali) et celle sur Raphaël (Rome, Scuderie de Quirinale, Mantova, Palazzo Ducale).
