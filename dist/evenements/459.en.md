---
title: DH Showcase 2019
short: "<h2>February, 1st, 2019\r

  <br />9am-12pm, UQAM\r

  <br />Local D-R200, pavillon Athanase-David</h2>"
cleanstring: DH-Showcase-twozeroonenine
lang: EN
caption: ""
image: https://www.crihn.org/files/sites/33/2018/12/Poster-CRIHN-Vitrine-HN-2019.jpg
url: ""
urlTitle: ""
categories: null

---
    
![](https://www.crihn.org/files/sites/33/2018/12/Poster-CRIHN-Vitrine-HN-2019.jpg)
  
  
For the 7th edition of the CRIHN Vitrine HN / DH Showcase, the organizers invite proposals (in English or French) for demonstration of digital humanities projects and tools. These projects can be, for instance, databases, text mining and data visualization tools, electronic publications, or mobile applications. Two sessions with 5 or 6 demonstrations are planned. Each presenter will first have 3 minutes to present an overview of their project to the whole group, before fielding questions about their projects as audience members circulate among the projects displayed on presenters¹ individual laptops. DH showcase gives both participants and the public an overview of the most recent digital humanities research, and creates stimulating discussions around tools, good practices, and research trajectories. Coffee and pastries will be served.

Organized by Carolina Ferrer (Université du Québec à Montréal) and Jean-François Vallée (Collège de Maisonneuve), the event will take place on Friday February 1, 2019, between 9am and noon in room D-R200, pavillon Athanase-David, UQAM (1430, rue Saint-Denis, Montréal; métro Berri-UQAM). The full text of the accepted proposals will be available on the [CRIHN](https://www.crihn.org/) website when the program is unveiled on January 21st.
