---
title: Parution d'Il s'est écarté de Thomas Carrier-Lafleur et David Bélanger
short: ""
cleanstring: Parution-dIl-sest-ecarte-de-Thomas-Carrier-Lafleur-et-David-Belanger
lang: FR
caption: ""
image: http://www.groupenotabene.com/sites/notabene2.aegir.nt2.uqam.ca/files/styles/image_couverture_publication_liste/public/nb-grise-ilsestecarte_c1.jpg?itok=bLmQfVj_
url: ""
urlTitle: ""
categories: null

---
    
![](http://www.groupenotabene.com/sites/notabene2.aegir.nt2.uqam.ca/files/styles/image_couverture_publication_liste/public/nb-grise-ilsestecarte_c1.jpg?itok=bLmQfVj_)

L’enquête menée dans ces pages porte sur la mort de François Paradis, le héros du célèbre et pourtant méconnu roman de Louis Hémon, Maria Chapdelaine. Il s’agit pour les auteurs d’éclairer une affaire noyée dans l’ombre de l’idéologique, du mythe, du naturel : et si François Paradis ne s’était pas écarté dans la forêt un soir de tempête, mais qu’on l’avait froidement assassiné ? Or, sans pour autant se priver du plaisir de révéler le coupable d’un tel crime, cette enquête a surtout pour ambition de redonner le choix à Maria, d’expliquer que l’absence de choix n’est jamais qu’une fiction machinée en coulisse. Le geste interprétatif des auteurs suit cette éthique : refuser l’immuable texte, désirer le chantier sans fin, l’intranquillité.

Voici les infos pour le lancement du livre : <https://www.facebook.com/events/404162803680853/>.
