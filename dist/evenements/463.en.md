---
title: Marcello Vitali-Rosati's keynote speech for the Humanités numériques 2019 symposium
short: "<h2>20th March 2019\r

  <br />Maison des sciences de l'homme de Bretagne\r

  <br />Salle du Conseil\r

  <br />2 avenue Gaston Berger, Rennes\r

  </h2>"
cleanstring: Marcello-Vitali-Rosatis-keynote-speech-for-the-Humanites-numeriques-twozeroonenine-symposium
lang: EN
caption: ""
image: https://i.ytimg.com/vi/kPQ5a8XNWjA/maxresdefault.jpg
url: ""
urlTitle: ""
categories: null

---
    
![](https://i.ytimg.com/vi/kPQ5a8XNWjA/maxresdefault.jpg)

within the frame of the _Humanités numériques 2019_ symposium, organized by the Maison des sciences de l'homme de Bretagne that is dedicated to the editorialization concept as developed by Marcello Vitali-Rosati, and his workrs at the CRC on digital textualities, Marcello Vitali-Rosati will give a talk on _Penser l'éditorialisation : pour une théorie générale de l'espace numérique_.

For further information, check out the symposium [webpage](https://www.mshb.fr/agenda%5Fmshb/seminaire-humanites-numeriques-2019-br-reception-et-transmission/5677/).
