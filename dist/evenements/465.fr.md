---
title: "Un Verre Derrière l'Écran - Christian Licoppe : Géo-localisation et espace public : qui sont les inconnus pseudonymes ?"
short: "<h2>Espace 71<br />\r

  18h18<br />\r

  28 mars 2019</h2>"
cleanstring: Un-Verre-Derriere-lEcran---Christian-Licoppe--Geo-localisation-et-espace-public--qui-sont-les-inconnus-pseudonymes-
lang: FR
caption: ""
image: https://scontent.fyhu1-1.fna.fbcdn.net/v/t1.0-9/54728517_1041171742739861_8944869637074452480_o.jpg?_nc_cat=111&_nc_ht=scontent.fyhu1-1.fna&oh=d8262fa29c7fd34bed03584802365a47&oe=5D0F5570
url: ""
urlTitle: ""
categories: null

---
    
![](https://scontent.fyhu1-1.fna.fbcdn.net/v/t1.0-9/54728517_1041171742739861_8944869637074452480_o.jpg?_nc_cat=111&_nc_ht=scontent.fyhu1-1.fna&oh=d8262fa29c7fd34bed03584802365a47&oe=5D0F5570)

La ville contemporaine en tant qu’espace public a été caractérisée comme un lieu où se côtoient et se rencontrent des étrangers, selon des modalités caractéristiques, telles que l’inattention civile de Goffman. Lorsque les citadins sont dotés de smartphones et d’applications géolocalisées (jeux, réseaux sociaux, « dating »), on montrera qu’ils ne se côtoient plus en tant que parfaits inconnus (qui ne sauraient rien des uns des autres en dehors de ce qui est disponible visuellement lors de la rencontre, ni non plus comme des connaissances. Cet exposé vise à cerner les modalités de ces rencontres mobiles et « équipées », et à montrer en quoi elles sont constitutives du type particulier d’espace public que peut proposer la ville connectée ou « augmentée ».
