---
title: International Seminar Publishing Sphere
short: "<h2>Montreal\r

  <br />23rd-25th May 2019\r

  </h2>"
cleanstring: International-Seminar-Publishing-Sphere
lang: EN
caption: ""
image: http://publishingsphere.ecrituresnumeriques.ca/wp-content/uploads/2019/05/afficheSansTypo-11th-copy.jpg
url: ""
urlTitle: ""
categories: null

---
    
In May 2019, the second edition of Publishing Sphere will take over the public sphere of Montreal. In the continuity with the first edition in Berlin (<https://www.hkw.de/en/programm/projekte/2017/internationaler%5Fliteraturpreis%5F2017/publishing%5Fsphere%5F1/publishing%5Fsphere.php>),we’ll continue the project of experimenting writing, publishing and performances forms capable of opening new spaces of expression, exchange and creation as well as of renovating the modalities of public spaces building.

In order to do so, we call editors, writers, artists, designers, commoneers, archivists and participants from various communities to invent alternative ways to take over the Publishing Sphere by proposing devices, apparati, texts, supports, protocols, architectures and action that renew the idea of publishing itself , till redefine its sense and political impact.

As atelier, performance, co-writing, public speaking, booksprint, forum, wiki, fish-bowl or pow-wow, we hope for propositions exploring inscription, writing and publishing as dynamics opening new spaces. Publishing Sphere will try to put in motion a dialogue between different communities, with an editorial gesture (curation) meeting reflection and creation
