---
title: Offre de stage à la CRC sur les écritures numériques
short: "Date limite : 18 septembre 2019\r

  <br />Date de début : mai 2020"
cleanstring: Offre-de-stage-a-la-CRC-sur-les-ecritures-numeriques
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories: null

---
    
La Chaire de recherche du Canada sur les écritures numériques, dirigée par M. Marcello Vitali-Rosati à l’Université de Montréal, est présentement à la recherche de 6 stagiaires intéressés à travailler, dès le mois de **MAI 2020**, sur deux projets en humanités numériques : le premier porte sur l'[édition numérique collaborative de l'Anthologie Palatine](http://ecrituresnumeriques.ca/fr/2016/1/19/Anthologie-Palatine), le second sur [les enjeux théoriques et pratiques de la transition numérique des revues savantes](http://ecrituresnumeriques.ca/fr/2018/3/22/Revue-twozero). Les étudiant.e.s sélectionné.e.s recevront une bourse de recherche pour un montant de 6 000 CAD couvrant les frais de déplacement et d’hébergement ainsi que les dépenses directes liées à la recherche.

Les candidat.e.s intéressé.e.s par ces postes peuvent présenter leur candidatures via la plateforme du MITACS (OBNL consacré au développement et au soutien à la recherche et à l’innovation), à l’adresse <https://globalink.mitacs.ca/#/student/application/welcome>. Ils et elles pourront trouver les projets disponibles — description du projet, encadrement, prérequis et toute autre information — dans l’onglet « Projects » en indiquant comme nom du professeur celui de M. Marcello Vitali-Rosati.

Plus d’informations sur la CRC sur les écritures numériques, son équipe et ses travaux, veuillez consulter son site institutionnel à l’adresse : <http://ecrituresnumeriques.ca/fr/>. Pour toute question, vous pouvez contacter l’équipe par courriel : <mailto:crc.ecrituresnumeriques@gmail.com>.
