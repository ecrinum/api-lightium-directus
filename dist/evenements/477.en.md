---
title: Pierre Lévy's seminar on IEML
short: "<h2>University of Montreal\r

  <br />CRIHN, local C-8132\r

  <div class=\"dateHeure\">1pm, 2nd October-4th December 2019</div>"
cleanstring: Pierre-Levys-seminar-on-IEML
lang: EN
caption: ""
image: https://pbs.twimg.com/profile_images/771800361384472577/lWB41YJH_400x400.jpg
url: ""
urlTitle: ""
categories: null

---
    
IEML est une langue artificielle conçue pour être simultanément manipulable de manière optimale par les ordinateurs et capable d'exprimer les nuances sémantiques et pragmatiques des langues naturelles. Ce métalangage peut notamment servir à la gestion des connaissances et à l'adressage sémantique des données numériques.

Les séances seront diffusées en direct à l'adresse : <https://meet.jit.si/ieml>.
