---
title: The status of the Author in Digital Era (FRQSC)
short: Questions on the status of the author in digital era constitute a major social issue that concerns communities of writers and readers. Since authors are the authority, how can we rethink the process of validation and legitimization of literary contents published online today?
cleanstring: The-status-of-the-Author-in-Digital-Era-FRQSC
lang: EN
caption: ""
image: http://vitalirosati.net/slides/img/valery.jpeg
url: ""
urlTitle: ""
categories:
  - 2
  - 6
  - 9
  - 11
  - 14
  - 18
  - 22
  - 25
  - 33
  - 35
  - 38
  - 39

---
    
![](http://www.wumingfoundation.com/images/wu-ming-official-portrait.jpg) 

In 1993, Mark Rose analyzed the invention of the modern concept of the author, and demonstrated that the latter had no absolute or timeless value. Instead, he pointed out that this concept developed in the 18th century due to concrete needs, particularly economic ones, concerning the then-emerging paper edition. He concluded by saying that the "author" function (and in particular the copyright system) was still too important in our culture to be abandoned. But with the birth and the diffusion of the Web, new models of content production and circulation plead in favour for a reevaluation of the concept of the author. Controversies on the role of the author as producer of original content have multiplied. (We remember in 2010 the controversy triggered by the publication of _The Map and the Territory_ by Michel Houellebecq, where he had copied entire passages from Wikipedia). Collective production systems torpedo the notion of the author as a singular creative entity. (In Italy, a collective of writers has been publishing novels for fifteen years under the pseudonym Wu-Ming, "anonymous" in Chinese). Finally, the author as a persona is the subject of a playful _mise en scène_ on the Web. (In Quebec, notably, we'll mention the case of Victoria Welby, Pharaon Parka, and Les Fourchettes, who expose themselves on social media).

It's crucial to analyze these new models in order to understand how new technologies affect the concept of the author.

**Publications**:  
* Marcello Vitali-Rosati, « Auteur ou acteur du Web? », _Implications philosophiques_, 10 juillet 2012, [en ligne](https://papyrus.bib.umontreal.ca/xmlui/handle/1866/12980)
* Marcello Vitali-Rosati, « Digital Paratext : Editorialization and the Very Death of the Author », dans Nadine Desrochers et Daniel Apollon (dir.), _Examining Paratextual Theory and its Applications in Digital Culture, Information Science Reference_, 2014, p. 110-127, [en ligne](https://papyrus.bib.umontreal.ca/xmlui/handle/1866/11392)
* Marcello Vitali-Rosati, « The Writer is the Architect. Editorialization and the Production of Digital Space », _Sens Public_, 15 décembre 2017, [en ligne](http://sens-public.org/article1288.html)
* Servanne Monjour, « L'écrivain de profil(s)... Facebook. Réflexion autour d'une photographie de profil de Victoria Welby », dans David Martens, Jean-Pierre Montier et Anne Reverseau (dir.), _L'écrivain vu par la photographie_, Rennes, Presses universitaires de Rennes, 2017, [ en ligne](https://papyrus.bib.umontreal.ca/xmlui/handle/1866/18302)
* Servanne Monjour, « Dibutade 2.0 : la "femme-auteur" à l’ère du numérique », _Sens Public_, 24 septembre 2015, [en ligne](http://www.sens-public.org/article1164.html)
* Ariane Mayer et Nicolas Sauret, « L’autorité dans Anarchy. Les constructions de l’autorité et de l’auctorialité dans un dispositif de production littéraire collaborative : le cas de l’expérience transmédia Anarchy.fr », _Quaderni_, no 91, 2016, [ en ligne](https://papyrus.bib.umontreal.ca/xmlui/handle/1866/16070)

**Conferences**:  
* La fin de l'autorité ? Pour une philosophie politique du web, Conférence à l'ENSSIB (Lyon), écouter en ligne le [ podcast de la conférence](http://www.enssib.fr/bibliotheque-numerique/ecouter/66991-la-fin-de-l-autorite-pour-une-philosophie-politique-du-web).
* Autorités numériques et post-vérité, Conférence prononcée à la Chaire LexUm en information juridique, le 11 avril 2017.

**See also**: [_Projet Profil_](http://ecrituresnumeriques.ca/fr/2016/4/7/Projet-Profil).
