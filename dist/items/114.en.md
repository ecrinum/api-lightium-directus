---
title: "<span>Michael E. Sinatra, « Representing Leigh Hunt’s Autobiography », in Veronica Alfano, Andrew Stauffer, (éds.). <i>Virtual Victorians : Networks, Connections, Technologies</i>, éds. Veronica Alfano et Andrew Stauffer, New York, Palgrave Macmillan, 2016, p. 107‑119.</span>"
short: That study attempted to elaborate the problematic of [Leigh Hunt's] position within the London literary and political scene between the years 1805 and1828, the contributions he made to British literature and journalism, and his public standing at the end of the romantic period. Since Hunt's life is obviously too complex to be rendered fully in any single study, the idea was not to attempt an exhaustive history, but rather to present a starting point for further inquiry into Hunt's career as a writer and public figure under the reign of Queen Victoria. [...]
cleanstring: spanMichael-E-Sinatra-Representing-Leigh-Hunts-Autobiography-inVeronica-Alfano-Andrew-Stauffer-eds-iVirtual-Victorians-Networks-Connections-Technologiesi-eds-Veronica-Alfano-et-Andrew-Stauffer-New-York-Palgrave-Macmillan-twozeroonesix-ponezerosevenoneoneninespan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13537
urlTitle: ""
categories:
  - 54
  - 42
  - 24

---
    
<!-- pas de contenu sur cette page -->
