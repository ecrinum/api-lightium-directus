---
title: "<span>Michael E. Sinatra, « Gender, Authorship and Male Domination : Mary Shelley’s Limited Freedom in « Frankenstein » and « The Last Man » », in Michael E. Sinatra, (éd.). <i>Mary Shelley’s Fictions: From Frankenstein to Falkner</i>, éd. Michael E. Sinatra, New York, Palgrave Macmillan, 2000, p. 95‑108.</span>"
short: Ever since Ellen Moer's « Literary Women » (1976), « Frankenstein » has been recognized as a novel in which issues about authorship are intimately bound up with those of gender. The work has frequently been related to the circumstance of Shelley's combining the biological role of mother with the social role of author. [...]
cleanstring: spanMichael-E-Sinatra-Gender-Authorship-and-Male-Domination-Mary-Shelleys-Limited-Freedom-in-Frankenstein-and-The-Last-Man-inMichael-E-Sinatra-ed-iMary-Shelleys-Fictions-From-Frankenstein-to-Falkneri-ed-Michael-E-Sinatra-New-York-Palgrave-Macmillan-twozerozerozero-pninefiveonezeroeightspan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13564
urlTitle: ""
categories:
  - 54
  - 42
  - 24
  - 24

---
    
<!-- pas de contenu sur cette page -->
