---
title: <span>Fabrice Marcoux, « Le livrel et le format ePub », in Marcello Vitali-Rosati, Michael E. Sinatra, (éds.). <i>Pratiques de l’édition numérique</i>, éds. Marcello Vitali-Rosati et Michael E. Sinatra, Montréal, Presses de l’Université de Montréal, 2014, (« Parcours numérique »), p. 177‑189.</span>
short: Il ne serait pas possible de tracer un panorama complet des enjeux de l’édition numérique sans parler des formats qui tentent de reproduire le mode typique de circulation des contenus papier — le livre — en l’adaptant au support numérique. C’est ce que l’on appelle « livre électronique » ou « livrel » (eBook en anglais). Il ne faut pas confondre le livre électronique avec la liseuse qui est le support de lecture. Le livre électronique est un fichier, formaté selon des standards déterminés. Bien évidemment, ces formats ne sont pas stables et changent très rapidement. Aujourd’hui, le standard ouvert de référence est l’ePub. Ce chapitre essaie d’en donner une description.
cleanstring: spanFabrice-Marcoux-Le-livrel-et-le-format-ePub-inMarcello-Vitali-Rosati-Michael-E-Sinatra-eds-iPratiques-de-ledition-numeriquei-eds-Marcello-Vitali-Rosati-et-Michael-E-Sinatra-Montreal-Presses-de-lUniversite-de-Montreal-twozeroonefor-Parcours-numerique-ponesevensevenoneeightninespan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13095
urlTitle: ""
categories:
  - 54
  - 42
  - 47
  - 22
  - 24
  - 5
  - 35

---
    
<!-- pas de contenu sur cette page -->
