---
title: "<span>Marcello Vitali-Rosati, « L’aura de la technique : photographie et restauration », in <i>No play. Images de la mémoire disséminée</i>, Paris-Alberobello, Éditions chiavediSvolta, 2007, p. 168‑189.</span>"
short: La photographie, le rêve de pouvoir reproduire en série une image, la perte de l'idée d'orginal. Tous les tirages que je peux donner à mes proches sont authentiques. La copie n'existe par; la pellicule l'empêche. [...]
cleanstring: spanMarcello-Vitali-Rosati-Laura-de-la-technique-photographie-et-restauration-iniNo-play-Images-de-la-memoire-dissemineei-Paris-Alberobello-Editions-chiavediSvolta-twozerozeroseven-ponesixeightoneeightninespan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13083
urlTitle: ""
categories:
  - 54
  - 42
  - 22
  - 7

---
    
<!-- pas de contenu sur cette page -->
