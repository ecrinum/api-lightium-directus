---
title: <span>Marcello Vitali-Rosati, « Digital Paratext. Editorialization and the very death of the author », in <i>Examining Paratextual Theory and its Applications in Digital Culture</i>, IGI Global, Hershey, Nadine Desrochers and Daniel Apollon, 2014, p. 110‑127.</span>
short: As shown by different scholars, the idea of “author” is not absolute or necessary. On the contrary, it came to life as an answer to the very practical needs of an emerging print technology in search of an economic model of its own. In this context, and according to the criticism of the notion of “author” made during the 1960–70s (in particular by Barthes and Foucault), it would only be natural to consider the idea of the author being dead as a global claim accepted by all scholars. Yet this is not the case, because, as Rose suggests, the idea of “author” and the derived notion of copyright are still too important in our culture to be abandoned. But why such an attachment to the idea of “author”? The hypothesis on which this chapter is based is that the theory of the death of the author—developed in texts such as What is an Author? by Michel Foucault and The Death of the Author by Roland Barthes—did not provide the conditions for a shift towards a world without authors because of its inherent lack of concrete editorial practices different from the existing ones. In recent years, the birth and diffusion of the Web have allowed the concrete development of a different way of interpreting the authorial function, thanks to new editorial practices—which will be named “editorialization devices” in this chapter. Thus, what was inconceivable for Rose in 1993 is possible today because of the emergence of digital technology—and in particular, the Web.
cleanstring: spanMarcello-Vitali-Rosati-Digital-Paratext-Editorialization-and-the-very-death-of-the-author-iniExamining-Paratextual-Theory-and-its-Applications-in-Digital-Culturei-IGI-Global-Hershey-Nadine-Desrochers-and-Daniel-Apollon-twozeroonefor-poneonezeroonetwosevenspan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/11392
urlTitle: ""
categories:
  - 54
  - 42
  - 22
  - 11
  - 14
  - 4
  - 19

---
    
<!-- pas de contenu sur cette page -->
