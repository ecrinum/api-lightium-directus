---
title: <span>Servanne Monjour, « Les virtualités du sténopé dans « Le Retour imaginaire » d’Atiq Rahimi », in Jean-Pierre Montier, (éd.). <i>Transactions photolittéraires</i>, éd. Jean-Pierre Montier, Rennes, Presses universitaires de Rennes, 2015, (« Interférences »), p. 359‑371.</span>
short: "Depuis quelque temps déjà, le bruit court que nous serions entrés dans « l'ère du virtuel», sans que l'on sache très bien ce qu'une telle expression signifie - et qui, d'ailleurs, cache une grande confusion avec l'avènement des nouvelles technologies numériques. Si, incontestablement, l'outil numérique aura marqué le tournant du XXIe siècle, la révolution souvent promise se traduit par davantage de permanences que de ruptures: c'est ainsi que, dans le champ de la photographie, on n'en finit plus d'attendre la disparition définitive de l'argentique, dont la mort est sans cesse reprogrammée. [...]"
cleanstring: spanServanne-Monjour-Les-virtualites-du-stenope-dans-Le-Retour-imaginaire-dAtiq-Rahimi-inJean-Pierre-Montier-ed-iTransactions-photolitterairesi-ed-Jean-Pierre-Montier-Rennes-Presses-universitaires-de-Rennes-twozeroonefive-Interferences-pthreefiveninethreesevenonespan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13828
urlTitle: ""
categories:
  - 54
  - 42
  - 39
  - 12
  - 7
  - 27

---
    
<!-- pas de contenu sur cette page -->
