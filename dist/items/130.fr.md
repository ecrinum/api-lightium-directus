---
title: <span>Marcello Vitali-Rosati, « Réflexions pour une resémantisation du concept de virtuel », in <i>Pourquoi des théories ?</i>, Besançon, Solitaires intempestifs, 2009, (« Expériences philosophiques »), p. 31‑55.</span>
short: Le discours qui sera proposé dans ces pages relève d'une idée particulière de ce que signifie « théorie ». Ma façon d'aborder la notion de « virtuel » n'est pas neutre; je commencerai par une précision sur le titre de ce texte et, en particulier, sur le mot « resémantiser », sur lequel se fonde mon approche théorique. [...]
cleanstring: spanMarcello-Vitali-Rosati-Reflexions-pour-une-resemantisation-du-concept-de-virtuel-iniPourquoi-des-theoriesi-Besancon-Solitaires-intempestifs-twozerozeronine-Experiences-philosophiques-pthreeonefivefivespan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13215
urlTitle: ""
categories:
  - 54
  - 42
  - 22
  - 44
  - 18
  - 15
  - 45
  - 27

---
    
<!-- pas de contenu sur cette page -->
