---
title: <span>Emmanuel Château-Dutier, « Administrer les Arts en Conseil. L’enjeu de la collégialité au Conseil des bâtiments civils (1795-1848) », in Jean-Michel Leniaud, François Monnier, (éds.). <i>La collégialité et les dysfonctionnements dans la décision administrative</i>, éds. Jean-Michel Leniaud et François Monnier, Paris, Ecole pratique des hautes études, 2011.</span>
short: ""
cleanstring: spanEmmanuel-Chateau-Dutier-Administrer-les-Arts-en-Conseil-Lenjeu-de-la-collegialite-au-Conseil-des-batiments-civils-onesevenninefive-oneeightforeight-inJean-Michel-Leniaud-Francois-Monnier-eds-iLa-collegialite-et-les-dysfonctionnements-dans-la-decision-administrativei-eds-Jean-Michel-Leniaud-et-Francois-Monnier-Paris-Ecole-pratique-des-hautes-etudes-twozerooneonespan
lang: EN
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 54
  - 42
  - 23

---
    
<!-- pas de contenu sur cette page -->
