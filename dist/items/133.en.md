---
title: "<span>Emmanuel Château-Dutier, « L’examen des plans d’alignement, une mission du Conseil des bâtiments civils », in Géraldine Rideau, Gilles Bienvenu, (éds.). <i>Autour de la ville de Napoléon : colloque de La Roche-sur-Yon</i>, éds. Géraldine Rideau et Gilles Bienvenu, Rennes, Presses universitaires de Rennes, 2006, (« Art et société »), p. 89‑99.</span>"
short: ""
cleanstring: spanEmmanuel-Chateau-Dutier-Lexamen-des-plans-dalignement-une-mission-du-Conseil-des-batiments-civils-inGeraldine-Rideau-Gilles-Bienvenu-eds-iAutour-de-la-ville-de-Napoleon-colloque-de-La-Roche-sur-Yoni-eds-Geraldine-Rideau-et-Gilles-Bienvenu-Rennes-Presses-universitaires-de-Rennes-twozerozerosix-Art-et-societe-peightninenineninespan
lang: EN
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 54
  - 42
  - 23

---
    
<!-- pas de contenu sur cette page -->
