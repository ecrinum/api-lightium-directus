---
title: "<span>Marcello Vitali-Rosati, <i>Corps et virtuel : itinéraires à partir de Merleau-Ponty</i>, Paris, L’Harmattan, 2009, 265 p.</span>"
short: "Voici une réflexion sur le rapport entre le concept de \"virtuel\" et celui de \"corps\". Le virtuel, pensé dans son sens le plus banal, à savoir en rapport avec les nouvelles technologies, met en crise l'idée cartésienne de corps comme chose placée dans l'enceinte d'un espace défini avec des abscisses et des ordonnées. Cette recherche relève d'un triple enjeu : redéfinir le concept de corps, approfondir la notion de virtuel et rendre compte du rapport entre les deux termes."
cleanstring: spanMarcello-Vitali-Rosati-iCorps-et-virtuel-itineraires-a-partir-de-Merleau-Pontyi-Paris-LHarmattan-twozerozeronine-twosixfivepspan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/12969
urlTitle: ""
categories:
  - 15
  - 17
  - 22
  - 27
  - 40
  - 44
  - 54

---
    
![](http://vitalirosati.com/wp-content/uploads/2014/01/9782296103887r.jpg)
