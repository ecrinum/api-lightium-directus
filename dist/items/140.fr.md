---
title: <span>Marcello Vitali-Rosati, <i>Égarements. Amour, mort et identités numériques</i>, Paris, Hermann, 2014, 138 p., (« Cultures Numériques »).</span>
short: "Êtes-vous prêt à partir en voyage ? Une quête qui a pour but de répondre à une question qui d’universelle finit par sembler banale. Un voyage à la recherche de nos identités. Qui suis-je ? Ou plutôt: c’est quoi, moi ?\r

  \r

  \r

  Ici ni carte, ni plan. A l’écart des sentiers battus, il nous faudra naviguer jusqu’à l’égarement, à l’affût des traces que chacun laisse. L’égarement est la condition de possibilité de ce parcours. Sur ce chemin qui n’en est pas un, quelques étapes indispensables : l’amour, la mort. Comment contribuent-ils à produire notre identité ?\r

  \r

  \r

  De cet égarement en naîtra un second, en apparence bien plus dépaysant, celui qui nous mènera à parcourir l’espace virtuel engendré par nos existences numériques. C’est ici semble-t-il, entre profils, pseudonymes, avatars et traces numériques, que se joue aujourd’hui le jeu de l’identité."
cleanstring: spanMarcello-Vitali-Rosati-iEgarements-Amour-mort-et-identites-numeriquesi-Paris-Hermann-twozeroonefor-onethreeeightp-Cultures-Numeriquesspan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/12982
urlTitle: ""
categories:
  - 10
  - 18
  - 22
  - 27
  - 31
  - 40
  - 54

---
    
![](http://s3.editions-hermann.fr/278-fiche_produit/egarements.jpg)
