---
title: <span>Marcello Vitali-Rosati, <i>S’orienter dans le virtuel</i>, Paris, Hermann, 2012, 178 p.</span>
short: Fictif, artificiel, imaginaire, trompeur, immatériel, irréel, impalpable, invisible, mystérieux... Lorsque l’on pense au virtuel, une foule d’idées nous submerge. Concept aux valeurs sémantiques multiformes et aux différents usages, sa signification reste floue et son sens en perpétuel mouvement. Qu’est-ce « réellement » que le virtuel? Quel rapport entre le sens philosophique et son emploi dans le domaine du numérique? Quelles sont ses implications politiques ? Quelle conception de la réalité en découle? Ce livre se veut une cartographie offrant des repères stables pour s’orienter et naviguer à travers le concept de virtuel. L’effet d’un phare en plein brouillard…
cleanstring: spanMarcello-Vitali-Rosati-iSorienter-dans-le-virtueli-Paris-Hermann-twozeroonetwo-oneseveneightpspan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/12983
urlTitle: ""
categories:
  - 54
  - 40
  - 22
  - 18
  - 13
  - 7
  - 27

---
    
<!-- pas de contenu sur cette page -->
