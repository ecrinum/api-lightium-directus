---
title: "<span>André-G. Bourassa, <i>Les nuits de la « Main » : cent ans de spectacles sur le boulevard Saint-Laurent (1891-1991)</i>, Montréal, VLB, 1993, 361 p., (« Études québécoises ; 30 »).</span>"
short: ""
cleanstring: spanAndre-G-Bourassa-iLes-nuits-de-la-Main-cent-ans-de-spectacles-sur-le-boulevard-Saint-Laurent-oneeightnineone-oneninenineonei-Montreal-VLB-onenineninethree-threesixonep-Etudes-quebecoises-threezerospan
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 54
  - 40
  - 37

---
    
<!-- pas de contenu sur cette page -->
