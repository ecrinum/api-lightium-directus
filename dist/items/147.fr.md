---
title: "<span>Jean-Marc Larrue, Giusy Pisano et France) Colloque de Cerisy juin 19-23 : Cerisy-la-Salle, <i>Les archives de la mise en scène : hypermédialités du théâtre</i>, Villeneuve-d’Ascq, Presses universitaires du Septentrion ; Cerisy-la-Salle, 2014, 422 p., (« Arts du spectacle. Images et sons »).</span>"
short: ""
cleanstring: spanJean-Marc-Larrue-Giusy-Pisano-et-France-Colloque-de-Cerisy-juin-onenine-twothree-Cerisy-la-Salle-iLes-archives-de-la-mise-en-scene-hypermedialites-du-theatrei-Villeneuve-dAscq-Presses-universitaires-du-Septentrion-Cerisy-la-Salle-twozeroonefor-fortwotwop-Arts-du-spectacle-Images-et-sonsspan
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 54
  - 40
  - 37

---
    
<!-- pas de contenu sur cette page -->
