---
title: <span>Jean-Marc Larrue, <i>Théâtre et intermédialité</i>, Villeneuve d’Ascq, Presses universitaires du Septentrion, 2015, 458 p., (« Arts du spectacle. Images et sons, 1564 »).</span>
short: "\"Théâtre et intermédialité propose une première application en français, à grande échelle et sur des objets variés – analyses de spectacles, modes de production, discours théoriques – des concepts intermédiaux, ou de concepts repris par l'intermédialité, à l’univers du théâtre. Si les études intermédiales, qui sont nées dans le sillage de la « révolution numérique », ont à peine trente ans, les processus qu’elles contribuent à mettre au jour remontent bien au-delà de cette dernière vague technologique majeure. Le théâtre en offre une très bonne illustration. Art deux fois millénaire, le théâtre est l’une des pratiques intermédiales les plus anciennes et les plus connues. L’intermédialité désigne à la fois un objet, une dynamique et une approche. Comme objet, elle concerne les relations complexes, foisonnantes, instables, polymorphes entre les médias. Cela touche autant des valeurs, des protocoles, des savoirs que des technologies qui passent ainsi, selon les modalités les plus diverses, d’un contexte médial à un autre. Comme dynamique, l’intermédialité est ce qui permet l’évolution, la création et le repositionnement continuel des médias, parfois aussi leur disparition : la dynamique intermédiale produit aussi des résidus (qu’on pense à la machine à écrire). Il découle de cela la nécessité d’une approche originale susceptible de mieux comprendre cet objet et cette dynamique. Les dix-neuf articles de cet ouvrage, en même temps qu’ils explorent le théâtre selon une perspective intermédiale, montrent bien la diversité des phénomènes intermédiaux et des approches qu’on peut en avoir au théâtre comme dans d’autres pratiques.\"-- Page 4 de la couverture."
cleanstring: spanJean-Marc-Larrue-iTheatre-et-intermedialitei-Villeneuve-dAscq-Presses-universitaires-du-Septentrion-twozeroonefive-forfiveeightp-Arts-du-spectacle-Images-et-sons-onefivesixforspan
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 54
  - 40
  - 33

---
    
<!-- pas de contenu sur cette page -->
