---
title: "<span>Jean-Marc Larrue, <i>L’institution littéraire et l'activité théâtrale : le cas de Montréal, 1880-1914</i>, Montréal, Département d’études françaises, Centre de documentation des études québécoises, Université de Montréal, 1988, 46 p., (« Rapports de recherche (Université de Montréal. Centre de documentation des études québécoises) 4 »;).</span>"
short: ""
cleanstring: spanJean-Marc-Larrue-iLinstitution-litteraire-et-lactivite-theatrale-le-cas-de-Montreal-oneeighteightzero-onenineonefori-Montreal-Departement-detudes-francaises-Centre-de-documentation-des-etudes-quebecoises-Universite-de-Montreal-onenineeighteight-forsixp-Rapports-de-recherche-Universite-de-Montreal-Centre-de-documentation-des-etudes-quebecoises-forspan
lang: EN
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 54
  - 40
  - 37

---
    
<!-- pas de contenu sur cette page -->
