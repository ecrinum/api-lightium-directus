---
title: "<span>Marcello Vitali-Rosati, <i>Navigations: Sur les traces d’Eugen...</i>, 1 edition, publie.net, 2014, 388 p.</span>"
short: Ce n'est pas seulement une expérimentation littéraire, c'est aussi et d'abord un grand texte, une formidable mise en abîme, forgée dans la chaleur italienne (sable, café, barbecue et cigare) et sous la neige montréalaise (flocons suspendus dans l'air, défiant la gravité), une fenêtre ouverte sur le souvenir d'un étudiant en philosophie, né à Florence, amoureux de H., ami de Peppe le poète, qui traverse les frontières sans bataille — c’est volontairement qu’il laisse des minuscules à tout ce qui touche de près ou de loin aux nationalités —, parti vivre à Pise, puis à Paris, puis à Montréal, et qui un jour rencontra Eugen, celui qui devint « personne itinérante » comme on dit au Québec, qui avait quitté la Roumanie dans le but d'atteindre le Canada en se cachant dans le container d'un cargo, qui dissimula ses papiers dans une gare à Belgrade pour échapper à la police, perdit son identité mais pas la volonté farouche de parvenir à ses fins, Eugen qui était fou comme un personnage de Kusturica, et qui compta jour après jour les kilomètres qui le séparaient de son rêve en buvant de la mauvaise bière volée chez Lidl. Réussira-t-il ?
cleanstring: spanMarcello-Vitali-Rosati-iNavigations-Sur-les-traces-dEugeni-one-edition-publienet-twozeroonefor-threeeighteightpspan
lang: FR
caption: ""
image: ""
url: navigations.vitalirosati.com
urlTitle: ""
categories:
  - 54
  - 40
  - 22
  - 27

---
    
<!-- pas de contenu sur cette page -->
