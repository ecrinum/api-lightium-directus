---
title: "<span>« Introduction », in Jean-Marc Larrue, Giusy Pisano, (éds.). <i>Les archives de la mise en scène: hypermédialités du théâtre</i>, éds. Jean-Marc Larrue et Giusy Pisano, Villeneuve-d’Ascq, Presses universitaires du Septentrion, 2014, (« Arts du spectacle. Images et sons »), p. 9‑21.</span>"
short: Le concept de « liveness » - ce qui est « live » - recouvre traditionnellement les notions de direct, d'immédiatet, de présence et de coprésence physiques dans le même espace (de l'émetteur et du récepteur), de vivant. Mais pour Auslander, le « live » peut aussi intégrer le « reproduit », le différé, donc le médiatisé, et il donne de cela l'exemple de la télévision - on pourrait ajouter celui de la radio - qui mise, comme le théâtre et contrairement au cinéma, sur son lien immédiat, en temps réel, avec ses auditeurs-spectateurs. [...]
cleanstring: spanIntroduction-inJean-Marc-Larrue-Giusy-Pisano-eds-iLes-archives-de-la-mise-en-scene-hypermedialites-du-theatrei-eds-Jean-Marc-Larrue-et-Giusy-Pisano-Villeneuve-dAscq-Presses-universitaires-du-Septentrion-twozeroonefor-Arts-du-spectacle-Images-et-sons-pninetwoonespan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13221
urlTitle: ""
categories:
  - 54
  - 42
  - 37
  - 5
  - 33
  - 26
  - 12
  - 7

---
    
<!-- pas de contenu sur cette page -->
