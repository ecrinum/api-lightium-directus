---
title: "<span>« Du média à la médiation : les trente ans de la pensée intermédiale et la résistance théâtrale », in Jean-Marc Larrue, Giusy Pisano, (éds.). <i>Les archives de la mise en scène: hypermédialités du théâtre</i>, éds. Jean-Marc Larrue et Giusy Pisano, Villeneuve-d’Ascq, Presses universitaires du Septentrion, 2014, (« Arts du spectacle. Images et sons »), p. 29‑56.</span>"
short: Si les études intermédiales, qui sont nées dans le sillage de la « révolution numérique », ont à peine trente ans, les processus qu'elles contribuent à mettre au jour remontent bien au-delà de cette dernière vague technologique majeure, comme l'a clairement illustré « Remediation. Understanding New Media », l'ouvrage clé que Jay David Bolter et Richard Grusin publiaient (en version imprimée) en 2000. [...]
cleanstring: spanDu-media-a-la-mediation-les-trente-ans-de-la-pensee-intermediale-et-la-resistance-theatrale-inJean-Marc-Larrue-Giusy-Pisano-eds-iLes-archives-de-la-mise-en-scene-hypermedialites-du-theatrei-eds-Jean-Marc-Larrue-et-Giusy-Pisano-Villeneuve-dAscq-Presses-universitaires-du-Septentrion-twozeroonefor-Arts-du-spectacle-Images-et-sons-ptwoninefivesixspan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13227
urlTitle: ""
categories:
  - 54
  - 42
  - 37
  - 33
  - 26
  - 12
  - 4

---
    
<!-- pas de contenu sur cette page -->
