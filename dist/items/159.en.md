---
title: <span>« Introduction », in Jean-Marc Larrue, (éd.). <i>Théâtre et intermédialité</i>, éd. Jean-Marc Larrue, Villeneuve-d’Ascq, Presses universitaires du Septentrion, 2015, (« Arts du spectacle. Images et sons »), p. 13‑23.</span>
short: "On imagine mal aujourd'hui un « spectacle » théâtral qui ne recourrait pas aux technologies de reproduction du son, c'est-à-dire à des « projections » sonores. Ce qui est aujourd'hui la règle était pourtant l'exception il y a moins d'un demi-siècle. Cette anecdote soulève des questions qui sont au coeur de la réflexion intermédiale : la place de la technologie et sa « naturalisation », le rôle des dispositifs, l'agentivité des « usagers », les rapports entre médias et médiations, l'institutionnalisation des pratiques médiatiques, etc. [...]"
cleanstring: spanIntroduction-inJean-Marc-Larrue-ed-iTheatre-et-intermedialitei-ed-Jean-Marc-Larrue-Villeneuve-dAscq-Presses-universitaires-du-Septentrion-twozeroonefive-Arts-du-spectacle-Images-et-sons-ponethreetwothreespan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13220
urlTitle: ""
categories:
  - 54
  - 42
  - 37
  - 33
  - 12
  - 4

---
    
<!-- pas de contenu sur cette page -->
