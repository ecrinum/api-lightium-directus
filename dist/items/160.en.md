---
title: "Colloquium «Écrivains, personnages et profils : l’éditorialisation de l’auteur»"
short: "Colloque international\r

  <br>Les 24 et 25 mai à l’Université de Montréal\r

  <br>sous la direction de Bertrand Gervais, Servanne Monjour, Jean-François Thériault, Marcello Vitali-Rosati\r

  <br>Consulter le <a href=\"http://colloque2016.ecrituresnumeriques.ca\">Site Web du colloque</a>"
cleanstring: Colloquium-Ecrivains-personnages-et-profils-leditorialisation-de-lauteur
lang: EN
caption: ""
image: http://vitalirosati.net/chaire/img/colloqueecrivains.png
url: http://colloque2016.ecrituresnumeriques.ca
urlTitle: Website
categories: []

---
    
<!-- pas de contenu sur cette page -->
