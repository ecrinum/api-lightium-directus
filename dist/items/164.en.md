---
title: Projet Profil
short: Initially conceived for strictly practical ends, users' profiles have become little by little unexpected spaces of self-expression and writing. This project aims to understand this new profiling structure, in order to analyse the theoretical and aesthetic issues.
cleanstring: Projet-Profil
lang: EN
caption: ""
image: http://blog.sens-public.org/marcellovitalirosati/wp-content/uploads/sites/2/2015/09/anne2.png
url: ""
urlTitle: ""
categories:
  - 2
  - 5
  - 7
  - 9
  - 10
  - 11
  - 18
  - 19
  - 21
  - 29
  - 31
  - 32
  - 36
  - 38
  - 39
  - 44
  - 45
  - 53
  - 58

---
    
![](http://blog.sens-public.org/marcellovitalirosati/wp-content/uploads/sites/2/2015/09/anne2.png) 

The development of the participatory web, or Web 2.0, has led in recent years to a proliferation of so-called user profiles. Each platform requires the creation of a "profile": from social networks to online shopping platforms, dating sites, online games, forums, newspapers, etc. On the well-known social network Facebook, in which users are required to report to their "friends" by publishing their personal data (date of birth, place of residence, activities, employment, family situation, etc.), the Quebecois writer Anne Archet nevertheless fills the fields for this profile data in a disconcerting way: "employment: succubus assistant to Satan"; "Studied: Go girls, three Big Macs girls at Hamburger University", etc. Similarly, on their Twitter profile, the ficticious writer with Oulipian personality Pharaon Parka describes themself as "Lurker and puppet, firstly. Leaks remixer." In 2012, still, Victoria Welby posted an astonishing service offer on Kijiji, the Canadian classifieds site: in the occasion she defined herself as a "public writer" and proposed to write erotic emails and other licentious stories for $50 an hour. In these three cases, as in others, the matter is to clearly divert the tool and its functions. These practices exceed the framework of the conventional user profile, and they are not singular examples. They highlight a wider phenomenon of writers who transgressively and playfully appropriate tools and digital forms occupy functional spaces in order to transform them into artistic ones. But what are the forms, values, ​​and implications of such ownership? The central hypothesis of this project is: these _détournements_ of user profile functions and forms can be considered as literary practices that, by their playful creativity, subvert the current use of the profile and its social implications in order to articulate a political function.

 Starting from a French corpus, our project intends to verify this hypothesis with the triple objective of:  
 1) Demonstrating the poetic function of the profile in online writing practices. Because that literary studies have a major role to play as part of an interdisciplinary reflection on the digital, we want to explore the literaryness of certain online writing practices. 2) Entering this profile study into a comparative perspective that includes historical forms and artistic practices - visual (self-portrait, silhouette), and, above all, textual (autobiographical and autofictionnal), from the position of studying digital culture according to relationships of continuity rather than rupture.  
 3) Showing the political function of literature. Literature takes into account the debate on the issue of personal data and the separation of public and private on the web. If literary détournement makes use of an obviously playful function, then this reappropriation of a formatted device, one especially designed to track users, certainly has political implications.

 The profile project is at the heart of many of the Chair's activities, including:   
 \- The seminar [ Digital writing and editorialization ](http://numericalwriting.ca/en/Activities/Events/2016/1/19/Seminar- Writing-digital-and-editorialization --- cycle-twozeroonefive-twozeroonesix ), cycle 2015-2016, on" Self-Editorialism "  
 \- Conference [ "Writers, characters, profiles: the editorialization of the author" ](http://ecrituresnumeriques.ca/en/2016/1/19/Colloque-Ecrivains-personnages-et-profils-leditorialisation-de-lauteur), organized May 24 and 25 at the University of Montreal, in collaboration with Bertrand Gervais (UQAM, CRC in Digital Arts and Literatures). </ p>
