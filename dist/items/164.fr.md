---
title: Projet Profil
short: D’abord conçus à des fins purement pratiques, les profils d'usager sont peu à peu devenus des espaces d’expression et d’écriture de soi inattendus. Ce projet vise à comprendre cette nouvelle structure profilaire, pour en mesurer les enjeux théoriques et esthétiques.
cleanstring: Projet-Profil
lang: FR
caption: ""
image: http://blog.sens-public.org/marcellovitalirosati/wp-content/uploads/sites/2/2015/09/anne2.png
url: ""
urlTitle: ""
categories:
  - 2
  - 5
  - 7
  - 9
  - 10
  - 11
  - 18
  - 19
  - 21
  - 29
  - 31
  - 32
  - 36
  - 38
  - 39
  - 44
  - 45
  - 53
  - 58

---
    
![](http://blog.sens-public.org/marcellovitalirosati/wp-content/uploads/sites/2/2015/09/anne2.png) 

Le développement du web participatif, ou web 2.0, a donné lieu ces dernières années à une multiplication de ce que l’on appelle les profils d’usagers. Chaque plateforme demande en effet la création d’un « profil » : depuis les réseaux sociaux jusqu’aux plateformes d’achat en ligne, en passant par les sites de rencontres, les jeux en ligne, les forums, les journaux, etc. Sur le réseau social bien connu Facebook, dans lequel l’usager est tenu de se présenter à ses « amis » en publiant ses données personnelles (date de naissance, lieu de résidence, activités, emploi, situation familiale, etc.), l’écrivaine québécoise Anne Archet remplit pourtant les champs destinés à ces données de profil de façon déconcertante : « emploi : succube adjointe à Satan » ; « A étudié : Allez les filles, trois Big Macs les filles à Université du Hamburger »... De même, sur son profil Twitter, l’écrivain fictif à caractère oulipien Pharaon Parka se décrit comme « Lurker et pantin d’abord. Remixeur de fuites ». En 2012 encore, Victoria Welby publiait sur le site de petites annonces canadien Kijiji une offre de service pour le moins surprenante : se définissant pour l’occasion comme une « écrivaine publique », elle proposait d’écrire, pour cinquante dollars de l’heure, courriers érotiques et autres récits licencieux. Dans ces trois cas comme dans d’autres, composant un corpus de langue française, il s’agit clairement de détourner l’outil et ses fonctions. Si ces pratiques s’extraient du cadre des usages conventionnels du profil d’utilisateur, elles ne sauraient être isolées, et mettent en évidence un phénomène d’appropriation des outils et des formes numériques par des écrivains qui, selon une démarche à la fois transgressive et ludique, investissent des espaces fonctionnels pour les transformer en espaces artistiques. Mais quelles sont les formes, les valeurs et les implications d’une telle appropriation ? L’hypothèse centrale de ce projet est la suivante : ces détournements des fonctions et des formes du profil d’usager peuvent être considérés comme des pratiques littéraires qui, par leur créativité ludique, subvertissent l’usage courant du profil et ses implications sociales, pour assumer une fonction politique.

À partir d’un corpus francophone, notre projet entend vérifier cette hypothèse avec le triple objectif de :  
1) Démontrer la fonction poétique du profil dans les pratiques d’écriture en ligne. Convaincus que les études littéraires ont un rôle majeur à jouer dans le cadre d’une réflexion interdisciplinaire sur le fait numérique, nous souhaitons explorer la littérarité de certaines pratiques d’écriture en ligne.  
2) Mettre en perspective cette étude du profil avec des formes et des pratiques artistiques historiques, notamment visuelles (l’autoportrait, la silhouette) mais surtout textuelles (autobiographiques et autofictionnelles), prenant le parti d’étudier la culture numérique selon des rapports de continuité plutôt que de rupture.  
3) Montrer la fonction politique de la littérature. La littérature prend en effet position par rapport au débat mené sur la question des données personnelles, de la séparation public/privé sur le web. Si le détournement littéraire engage une fonction ludique évidente, cette réappropriation d’un dispositif formaté, notamment conçu pour tracer les usagers, a certainement des implications politiques.

Le projet profil est au coeur de nombreuses activités de la Chaire dont:  
\- Le séminaire [Écritures numériques et éditorialisation](http://ecrituresnumeriques.ca/fr/Activites/Evenements/2016/1/19/Seminaire-Ecritures-numeriques-et-editorialisation---cycle-twozeroonefive-twozeroonesix), cycle 2015-2016, qui porte sur "l'Éditorialisation du soi"  
\- Le colloque ["Écrivains, personnages, profils: l'éditorialisation de l'auteur"](http://ecrituresnumeriques.ca/fr/2016/1/19/Colloque-Ecrivains-personnages-et-profils-leditorialisation-de-lauteur), organisé les 24 et 25 mai à l'Univerisité de Montréal, en collaboration avec Bertrand Gervais (UQAM, CRC en Arts et littératures numériques).
