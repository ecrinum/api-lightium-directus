---
title: "<span>Thierry Crouzet, « Nous sommes tous éditorialistes sans le savoir », [En ligne : http://tcrouzet.com/2016/02/03/nous-sommes-tous-editorialistes-dans-le-savoir/?platform=hootsuite]. Consulté le3 février 2016.</span>"
short: Depuis longtemps Marcello Vitali Rosati est dans mon paysage numérique, sans que j’ai même gratté sa théorie de l’éditorialisation. Et puis nous allons nous croiser dans un mois à Lyon, un autre chercheur en littérature me parle de lui, même me demande comment je me positionne par rapport à ses travaux… alors je demande à Marcello par où commencer. Il me pointe vers What is editorialization ? Lire la suite →
cleanstring: spanThierry-Crouzet-Nous-sommes-tous-editorialistes-sans-le-savoir-En-ligne-httptcrouzetcomtwozeroonesixzerotwozerothreenous-sommes-tous-editorialistes-dans-le-savoirplatformhootsuite-Consulte-lethree-fevrier-twozeroonesixspan
lang: EN
caption: ""
image: ""
url: ""
urlTitle: ""
categories: []

---
    
Thierry Crouzet, « Nous sommes tous éditorialistes sans le savoir », \[En ligne : http://tcrouzet.com/2016/02/03/nous-sommes-tous-editorialistes-dans-le-savoir/?platform=hootsuite\]. Consulté le3 février 2016.  
<http://tcrouzet.com/2016/02/03/nous-sommes-tous-editorialistes-dans-le-savoir/?platform=hootsuite>  
https://www.zotero.org/groups/critures\_numriques/items/3538AQNI  
