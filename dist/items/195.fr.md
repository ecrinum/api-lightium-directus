---
title: "<span>Marcello Vitali-Rosati, « Les MOOC : coup de pub ou espace d’apprentissage ? », [En ligne : http://blog.sens-public.org/marcellovitalirosati/les-mooc-coup-de-pub-ou-espace-dapprentissage/]. Consulté le21 janvier 2016.</span>"
short: ""
cleanstring: spanMarcello-Vitali-Rosati-Les-MOOC-coup-de-pub-ou-espace-dapprentissage-En-ligne-httpblogsens-publicorgmarcellovitalirosatiles-mooc-coup-de-pub-ou-espace-dapprentissage-Consulte-letwoone-janvier-twozeroonesixspan
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 22

---
    
Marcello Vitali-Rosati, « Les MOOC : coup de pub ou espace d’apprentissage ? », \[En ligne : http://blog.sens-public.org/marcellovitalirosati/les-mooc-coup-de-pub-ou-espace-dapprentissage/\]. Consulté le21 janvier 2016.  
<http://blog.sens-public.org/marcellovitalirosati/les-mooc-coup-de-pub-ou-espace-dapprentissage/>  
https://www.zotero.org/groups/critures\_numriques/items/T6X534Q8  
