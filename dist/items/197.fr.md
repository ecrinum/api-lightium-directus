---
title: "<span>Marcello Vitali-Rosati, « La fin de la distinction entre réalité et fiction », [En ligne : http://blog.sens-public.org/marcellovitalirosati/la-fin-de-la-distinction-entre-realite-et-fiction/]. Consulté le21 janvier 2016.</span>"
short: ""
cleanstring: spanMarcello-Vitali-Rosati-La-fin-de-la-distinction-entre-realite-et-fiction-En-ligne-httpblogsens-publicorgmarcellovitalirosatila-fin-de-la-distinction-entre-realite-et-fiction-Consulte-letwoone-janvier-twozeroonesixspan
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 22

---
    
Marcello Vitali-Rosati, « La fin de la distinction entre réalité et fiction », \[En ligne : http://blog.sens-public.org/marcellovitalirosati/la-fin-de-la-distinction-entre-realite-et-fiction/\]. Consulté le21 janvier 2016.  
<http://blog.sens-public.org/marcellovitalirosati/la-fin-de-la-distinction-entre-realite-et-fiction/>  
https://www.zotero.org/groups/critures\_numriques/items/3S9ZZ5NQ  
