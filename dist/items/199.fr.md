---
title: "<span>Marcello Vitali-Rosati, « Le numérique, est-il un support ? », [En ligne : http://blog.sens-public.org/marcellovitalirosati/le-numerique-est-il-un-support/]. Consulté le21 janvier 2016.</span>"
short: ""
cleanstring: spanMarcello-Vitali-Rosati-Le-numerique-est-il-un-support-En-ligne-httpblogsens-publicorgmarcellovitalirosatile-numerique-est-il-un-support-Consulte-letwoone-janvier-twozeroonesixspan
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 22

---
    
Marcello Vitali-Rosati, « Le numérique, est-il un support ? », \[En ligne : http://blog.sens-public.org/marcellovitalirosati/le-numerique-est-il-un-support/\]. Consulté le21 janvier 2016.  
<http://blog.sens-public.org/marcellovitalirosati/le-numerique-est-il-un-support/>  
https://www.zotero.org/groups/critures\_numriques/items/J7P2RRA7  
