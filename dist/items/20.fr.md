---
title: "<span>Marcello Vitali-Rosati, « Paratexte numérique : la fin de la distinction entre réalité et fiction? », <i>Cahiers ReMix</i>, vol. 1 / 5, 2015.</span>"
short: "L’idée que je voudrais essayer d’explorer est la suivante: les éléments paratextuels ont une fonction de seuil entre le hors-texte et le texte; par ce biais, ils nous permettent aussi le passage entre le niveau extradiégétique et le niveau diégétique, et, finalement, dans le cas de la littérature, entre réalité et fiction. Si ce modèle est assez défini dans le cas de l’édition papier, l’espace numérique a tendance à le rendre de plus en plus flou. Dans le Web, tout est texte et/ou paratexte; le même élément textuel (une adresse URL, par exemple) peut servir pour déclarer un passage à la fiction ou pour nous faire acheter quelque chose sur un site de ventes en ligne, ou encore pour regarder la météo ou pour gérer notre compte en banque."
cleanstring: spanMarcello-Vitali-Rosati-Paratexte-numerique-la-fin-de-la-distinction-entre-realite-et-fiction-iCahiers-ReMixi-volonefive-twozeroonefivespan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/12922
urlTitle: ""
categories:
  - 54
  - 41
  - 22
  - 18
  - 14

---
    
<!-- pas de contenu sur cette page -->
