---
title: "<span>Marcello Vitali-Rosati, « In-between, entre-deux, actions, essences et interfaces », [En ligne : http://blog.sens-public.org/marcellovitalirosati/in-between-entre-deux-actions-essences-et-interfaces/]. Consulté le21 janvier 2016.</span>"
short: ""
cleanstring: spanMarcello-Vitali-Rosati-In-between-entre-deux-actions-essences-et-interfaces-En-ligne-httpblogsens-publicorgmarcellovitalirosatiin-between-entre-deux-actions-essences-et-interfaces-Consulte-letwoone-janvier-twozeroonesixspan
lang: EN
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 22

---
    
Marcello Vitali-Rosati, « In-between, entre-deux, actions, essences et interfaces », \[En ligne : http://blog.sens-public.org/marcellovitalirosati/in-between-entre-deux-actions-essences-et-interfaces/\]. Consulté le21 janvier 2016.  
<http://blog.sens-public.org/marcellovitalirosati/in-between-entre-deux-actions-essences-et-interfaces/>  
https://www.zotero.org/groups/critures\_numriques/items/V94KPEHW  
