---
title: "<span>Peppe Cavallari, « Après le dernier clic : que signifie mourir sur le web ? », <i>Sens Public</i>, janvier 2013.</span>"
short: L’expérience de la mort sur le web révèle un paradoxe évident, celui de sites ou de profils abandonnés par leurs ’propriétaires’, des pages qui deviennent alors des cadavres numériques, leur définitive inactivité équivalant à la mort. Il s’agit cependant d’une mort qui ne correspond pas à une disparition. Au contraire, nous sommes devant une mort temporaire, qui continue à être visible même si tout processus d’écriture, l’écriture polymorphe au fondement de notre existence numérique, s’est arrêté.
cleanstring: spanPeppe-Cavallari-Apres-le-dernier-clic-que-signifie-mourir-sur-le-web-iSens-Publici-janvier-twozeroonethreespan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13089
urlTitle: ""
categories:
  - 54
  - 41
  - 48
  - 11
  - 18
  - 13
  - 4
  - 10

---
    
<!-- pas de contenu sur cette page -->
