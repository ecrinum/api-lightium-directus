---
title: "<span>Marcello Vitali-Rosati, « Dérives du numérique et possibilités de résistance : quelles libertés ? », [En ligne : http://theconversation.com/derives-du-numerique-et-possibilites-de-resistance-quelles-libertes-50180]. Consulté le24 novembre 2015.</span>"
short: « Le » numérique n’est ni un bien ou ni un mal, mais les technologies produisent un espace social particulier, avec des valeurs spécifiques. Il faut donc en faire une critique. Pour s’en libérer.
cleanstring: spanMarcello-Vitali-Rosati-Derives-du-numerique-et-possibilites-de-resistance-quelles-libertes-En-ligne-httptheconversationcomderives-du-numerique-et-possibilites-de-resistance-quelles-libertes-fivezerooneeightzero-Consulte-letwofor-novembre-twozeroonefivespan
lang: EN
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 22

---
    
Marcello Vitali-Rosati, « Dérives du numérique et possibilités de résistance : quelles libertés ? », \[En ligne : http://theconversation.com/derives-du-numerique-et-possibilites-de-resistance-quelles-libertes-50180\]. Consulté le24 novembre 2015.  
<http://theconversation.com/derives-du-numerique-et-possibilites-de-resistance-quelles-libertes-50180>  
https://www.zotero.org/groups/critures\_numriques/items/INVVP5P4  
