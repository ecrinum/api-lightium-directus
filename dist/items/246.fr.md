---
title: "<span>Marcello Vitali-Rosati, « Penser les attaques de Paris, autour de la vidéo d’Anonymous », [En ligne : http://theconversation.com/penser-les-attaques-de-paris-autour-de-la-video-danonymous-51095]. Consulté le24 novembre 2015.</span>"
short: Notre façon de penser l’événement, d’en parler, fait partie de l’événement. Réflexion autour de cette « éditorialisation » et du rôle d’Anonymous dans cette appropriation/relecture collective.
cleanstring: spanMarcello-Vitali-Rosati-Penser-les-attaques-de-Paris-autour-de-la-video-dAnonymous-En-ligne-httptheconversationcompenser-les-attaques-de-paris-autour-de-la-video-danonymous-fiveonezeroninefive-Consulte-letwofor-novembre-twozeroonefivespan
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 22
  - 16
  - 31

---
    
Marcello Vitali-Rosati, « Penser les attaques de Paris, autour de la vidéo d’Anonymous », \[En ligne : http://theconversation.com/penser-les-attaques-de-paris-autour-de-la-video-danonymous-51095\]. Consulté le24 novembre 2015.  
<http://theconversation.com/penser-les-attaques-de-paris-autour-de-la-video-danonymous-51095>  
https://www.zotero.org/groups/critures\_numriques/items/DQNCSF9N  
