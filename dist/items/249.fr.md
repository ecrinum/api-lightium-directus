---
title: "<span>Marcello Vitali Rosati, « Culture numérique – Penser les attaques de Paris: la vidéo d’Anonymous », [En ligne : http://blog.sens-public.org/marcellovitalirosati/penser-les-attaques-de-paris-la-video-danonymous/]. Consulté le20 janvier 2016.</span>"
short: ""
cleanstring: spanMarcello-Vitali-Rosati-Culture-numerique---Penser-les-attaques-de-Paris-la-video-dAnonymous-En-ligne-httpblogsens-publicorgmarcellovitalirosatipenser-les-attaques-de-paris-la-video-danonymous-Consulte-letwozero-janvier-twozeroonesixspan
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 21
  - 19

---
    
Marcello Vitali Rosati, « Culture numérique – Penser les attaques de Paris: la vidéo d’Anonymous », \[En ligne : http://blog.sens-public.org/marcellovitalirosati/penser-les-attaques-de-paris-la-video-danonymous/\]. Consulté le20 janvier 2016.  
<http://blog.sens-public.org/marcellovitalirosati/penser-les-attaques-de-paris-la-video-danonymous/>  
https://www.zotero.org/groups/critures\_numriques/items/PX99IPNH  
