---
title: "<span>Marcello Vitali Rosati, « Digital Architectures: The Web, Editorialization, and Metaontology », [En ligne : http://blog.sens-public.org/marcellovitalirosati/digital-architectures-the-web-editorialization-and-metaontology/]. Consulté le20 janvier 2016.</span>"
short: |-
  This paper will present and try to demonstrate three main theses:

      Digital space is actual space, the space in which we live. A space is a set of relationships between objects; in our contemporary society, space is a hybridization of connected and non-connected objects that are structured by writing.

      In digital space, writing occupies a fundamental position. Writing is the essential material of digital space. The web, which is an important part of digital space, is comprised of writing: everything on the web is written; even images and videos are code. Writing is the actual material of digital space.

      Digital space is best interpreted and understood using a performative paradigm. Digital space is not a representation of reality; it is, however, a particular way of producing and organizing reality.

  Two main notions will be proposed to illustrate these theses: the notion of editorialization and the notion of metaontology.
cleanstring: spanMarcello-Vitali-Rosati-Digital-Architectures-The-Web-Editorialization-and-Metaontology-En-ligne-httpblogsens-publicorgmarcellovitalirosatidigital-architectures-the-web-editorialization-and-metaontology-Consulte-letwozero-janvier-twozeroonesixspan
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 17
  - 19

---
    
Marcello Vitali Rosati, « Digital Architectures: The Web, Editorialization, and Metaontology », \[En ligne : http://blog.sens-public.org/marcellovitalirosati/digital-architectures-the-web-editorialization-and-metaontology/\]. Consulté le20 janvier 2016.  
<http://blog.sens-public.org/marcellovitalirosati/digital-architectures-the-web-editorialization-and-metaontology/>  
https://www.zotero.org/groups/critures\_numriques/items/TBGB8CRX  
