---
title: <span>Fabien Deglise, « Internet - Naviguer... et perdre son innocence », <i>Le Devoir</i>, 14 décembre 2013.</span>
short: Est-ce la fin du temps de l’innocence ? En s’installant dans les univers...
cleanstring: spanFabien-Deglise-Internet---Naviguer-et-perdre-son-innocence-iLe-Devoiri-onefor-decembre-twozeroonethreespan
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 31

---
    
Fabien Deglise, « Internet - Naviguer... et perdre son innocence », _Le Devoir_, 14 décembre 2013.  
<http://www.ledevoir.com/societe/science-et-technologie/395223/internet-naviguer-et-perdre-son-innocence>  
https://www.zotero.org/groups/critures\_numriques/items/DCWKQBA8  
