---
title: <span>Émilie Folie-Boivin, « Imaginer le roman de demain », <i>Le Devoir</i>, 3 octobre 2015.</span>
short: On voit encore très peu de livres « vraiment » électroniques qui intègrent à un bon texte...
cleanstring: spanEmilie-Folie-Boivin-Imaginer-le-roman-de-demain-iLe-Devoiri-three-octobre-twozeroonefivespan
lang: EN
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 9
  - 7

---
    
Émilie Folie-Boivin, « Imaginer le roman de demain », _Le Devoir_, 3 octobre 2015.  
<http://www.ledevoir.com/culture/livres/451541/imaginer-le-roman-de-demain>  
https://www.zotero.org/groups/critures\_numriques/items/5QUJXFRD  
