---
title: <span>Fabien Deglise, « Unis contre la surveillance numérique de masse », <i>Le Devoir</i>.</span>
short: Le citoyen numérique se fait quotidiennement et technologiquement espionner, mais, à...
cleanstring: spanFabien-Deglise-Unis-contre-la-surveillance-numerique-de-masse-iLe-Devoirispan
lang: EN
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 31

---
    
Fabien Deglise, « Unis contre la surveillance numérique de masse », _Le Devoir_.  
<http://www.ledevoir.com/societe/actualites-en-societe/399636/unis-contre-la-surveillance-numerique-de-masse>  
https://www.zotero.org/groups/critures\_numriques/items/T5WFTRBI  
