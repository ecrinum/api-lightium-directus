---
title: <span>Nathalie, « Un Québec «made in» Paris », <i>La Presse</i>, 23 janvier 2014.</span>
short: "Le français est le fondement de notre identité. C'est ce qui nous distingue et nous définit, affirme Pauline Marois. J'aurais une petite question pour la première ministre: de quel français parle-t-elle au juste? Du français du Québec ou du français de France?"
cleanstring: spanNathalie-Un-Quebec-made-in-Paris-iLa-Pressei-twothree-janvier-twozerooneforspan
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories: []

---
    
Nathalie, « Un Québec «made in» Paris », _La Presse_, 23 janvier 2014.  
<http://www.lapresse.ca/debats/chroniques/nathalie-petrowski/201401/23/01-4731558-un-quebec-made-in-paris.php>  
https://www.zotero.org/groups/critures\_numriques/items/KCEFJXPX  
