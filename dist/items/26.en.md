---
title: <span>Peppe Cavallari, « Toujours en ligne sur Facebook, ou l’utopie d'une absence », <i>Sens Public</i>, octobre 2012.</span>
short: "Aujourd'hui, Facebook est vraiment ennuyeux. Il n'y a rien, rien de rien, rien du tout. Une amie voyage seule en Écosse et publie les photos de ses repas : hier elle a pris du bacon au [...]"
cleanstring: spanPeppe-Cavallari-Toujours-en-ligne-sur-Facebook-ou-lutopie-dune-absence-iSens-Publici-octobre-twozeroonetwospan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13093
urlTitle: ""
categories:
  - 54
  - 41
  - 48
  - 29
  - 13
  - 31
  - 4
  - 10

---
    
<!-- pas de contenu sur cette page -->
