---
title: <span>Stéphane Baillargeon, « Quand le virtuel ébranle le réel », <i>Le Devoir</i>, 8 novembre 2014.</span>
short: Le virtuel ébranle tout le réel. Dans une scène récente de la drolatique série La théorie du K.O....
cleanstring: spanStephane-Baillargeon-Quand-le-virtuel-ebranle-le-reel-iLe-Devoiri-eight-novembre-twozerooneforspan
lang: EN
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 18
  - 27

---
    
Stéphane Baillargeon, « Quand le virtuel ébranle le réel », _Le Devoir_, 8 novembre 2014.  
<http://www.ledevoir.com/societe/medias/423198/quand-le-virtuel-ebranle-le-reel>  
https://www.zotero.org/groups/critures\_numriques/items/8XIRBGR6  
