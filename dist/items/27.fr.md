---
title: <span>Peppe Cavallari, « Une réflexion philosophique inédite sur le web. Une lecture de « L’être et l’écran. Comment le numérique change la perception » de Stéphane Vial », <i>Sens Public</i>, janvier 2014.</span>
short: "[« L'être et l'écran » de Stéphane Vial] ne se limite pas à revendiquer le droit des philosophes, un droit désormais reconnu, à s'occuper de web, d'applications, d’algorithmes et d'interfaces : il va bien au-delà de cette constatation pour encadrer l'ensemble des instruments techniques qui engendrent le web dans la pertinence d'une analyse philosophique, voire phénoménologique, qui les prend en compte en tant qu'instruments « phénoménotechniques », instruments qui « font le monde et nous le donne » et déterminent « la qualité de notre expérience d'exister »."
cleanstring: spanPeppe-Cavallari-Une-reflexion-philosophique-inedite-sur-le-web-Une-lecture-de-Letre-et-lecran-Comment-le-numerique-change-la-perception-de-Stephane-Vial-iSens-Publici-janvier-twozerooneforspan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13094
urlTitle: ""
categories:
  - 54
  - 41
  - 48
  - 5
  - 35

---
    
<!-- pas de contenu sur cette page -->
