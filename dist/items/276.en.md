---
title: "<span>Servanne Monjour, <i>La littérature à l’ère photographique : mutations, novations, enjeux : de l’argentique au numérique</i>, Montréal, Université de Montréal et Université Rennes 2, 2016, 477 p.</span>"
short: |-
  Désormais, nous sommes tous photographes. Nos téléphones intelligents nous permettent de capter, de modifier et de partager nos clichés sur les réseaux en moins d’une minute, tant et si bien que l’image photographique est devenue une nouvelle forme de langage. Réciproquement, serions-nous également tous écrivains ? Il existe en effet une véritable légitimité historique à penser que la notion d'écrivain, comme celle de photographe, s'étend le long d'un paradigme allant de la « simple » possession d'une aptitude technique jusqu'à la gloire des plus fortes figures de la vie culturelle collective. Cette thèse vise à déterminer comment se constitue une nouvelle mythologie de l’image photographique à l’ère du numérique, comprenant aussi bien la réévaluation du médium argentique vieillissant que l’intégration d’un imaginaire propre à ces technologies dont nous n’avons pas encore achevé de mesurer l’impact culturel sur nos sociétés. À cet égard, la perspective littéraire est riche d’enseignements en termes culturels, esthétiques ou même ontologiques, puisque la littérature, en sa qualité de relais du fait photographique depuis près de deux siècles, a pleinement participé à son invention : c’est là du moins l’hypothèse de la photolittérature. En cette période de transition technologique majeure, il nous revient de cerner les nouvelles inventions littéraires de la photographie, pour comprendre aussi bien les enjeux contemporains du fait photographique que ceux de la littérature.
  Nowadays, we are all photographers. Our smart phones allow us to take, edit and share our snapshots on social media in less than a minute, to the extent that the photographic image has become a new form of language. Reciprocally, have we all become writers as well? There truly is historical legitimacy in seeing the notion of the writer, like that of the photographer, as spanning a paradigmal spectrum, running from “simple” possession of technical aptitude, to the glory of the loftiest figures in our collective cultural life. This thesis aims to determine how the new mythology around the photographic image takes shape in the digital age, while also re-evaluating the aging medium of film, as well as integrating a newly imagined sphere of ideas surrounding these new technologies, for which we have yet to measure the cultural impact on our societies. In this respect, a literary perspective is rich in cultural and even ontological lessons, since literature has interacted with photography for nearly two centuries, and thus contributed to its invention : this is at least the central hypothesis of photoliterature. In this period of major technological transition, we must therefore identify photography’s new literary inventions, so that we can better understand the contemporary issues surrounding both the worlds of photography and literature.
cleanstring: spanServanne-Monjour-iLa-litterature-a-lere-photographique-mutations-novations-enjeux-de-largentique-au-numeriquei-Montreal-Universite-de-Montreal-et-Universite-Rennes-two-twozeroonesix-forsevensevenpspan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13614
urlTitle: ""
categories:
  - 54
  - 40
  - 39
  - 21
  - 18
  - 33
  - 16
  - 12

---
    
<!-- pas de contenu sur cette page -->
