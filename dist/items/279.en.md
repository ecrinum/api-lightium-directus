---
title: "Profils d'usagers et production de l'identité: au delà de l'opposition homme-machine - Conférence de Servanne Monjour au colloque \"Posthumain et subjectivités numériques\" de Cerisy-la-salle (juin 2016)"
short: ""
cleanstring: Profils-dusagers-et-production-de-lidentite-au-dela-de-lopposition-homme-machine---Conference-de-Servanne-Monjour-au-colloque-Posthumain-et-subjectivites-numeriques-de-Cerisy-la-salle-juin-twozeroonesix
lang: EN
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 10
  - 39
  - 43

---
    
Cette communication a été donnée dans le cadre du colloque intitulé Posthumain et subjectivités numériques qui s'est tenu au Centre Culturel International de Cerisy du 23 au 30 juin 2016, sous la direction de Sylvie BAUER, Claire LARSONNEUR, Hélène MACHINAL et Arnaud REGNAULD.

Disponible sur le site de [ France Culture Plus ](http://www.franceculture.fr/conferences/maison-de-la-recherche-en-sciences-humaines/cerisy-posthumain-et-subjectivites?xtmc=Servanne%20Monjour&xtnp=1&xtcr=1) et de [ La Forge numérique (Université de Caen Basse-Normandie)](http://www.unicaen.fr/recherche/mrsh/forge/4059)

_Résumé du colloque_

La question du posthumain s'invite depuis quelque temps dans la recherche universitaire, que celle-ci se concentre sur les sciences humaines ou sur les sciences dites dures, mais aussi dans le domaine de l'art. Le développement de l'informatique, du numérique et des biotechnologies invite à (re)penser l'humain et les idéaux humanistes, lorsque, dès le début d'un questionnement sur ce qu'humain signifie, le corps biologique s'augmente d'une conscience, puis d'un rapport croissant à la machine et à la technologie. En d'autres termes, s'il s'agit de cerner le devenir de l'humain en tant que corps lié à une conscience, il s'agit également de considérer la question du sujet non comme singularité isolée, mais comme forme inachevée et hybride, en constante métamorphose. Le développement du numérique amène alors à se poser la question de cette nouvelle subjectivité en rapport étroit avec les réseaux qui se donnent comme autant de prothèses quotidiennes (montres connectées, internet, clavier d'ordinateur, téléphone "intelligent"). Alors que le sujet laisse des traces dans l'univers numérique et qu'il est lui-même traçable, on peut s'interroger sur son autonomie, sa capacité à s'extraire, tant physiquement que virtuellement, des réseaux qui jalonnent et construisent le monde contemporain. On peut se demander quelle subjectivité se construit lorsque l'homme de chair et d'os se double d'avatars dans le monde virtuel et pourtant bien réel du numérique...

_Résumé de la communication_

Le développement du web participatif a donné lieu ces dernières années à une multiplication de ce que l’on appelle les profils d’usagers. Chaque plateforme demande en effet la création d’un "profil": depuis les réseaux sociaux jusqu’aux plateformes d’achat en ligne, en passant par les sites de rencontres, les jeux en ligne, les forums, les journaux, etc. D’abord conçus à des fins purement pratiques, ces profils sont aussi devenus des espaces d’expression et d’écriture de soi inattendus, notamment investis par les artistes et les écrivains qui en exploitent les contraintes et les potentialités pour construire leur identité et leur auctorialité. Dans certains cas, le profil d’usager devient même la seule incarnation de l’auteur, lequel n’existe qu’à travers la médiation du réseau social. En 2015, le romancier indien Perumal Murugan organisait ainsi son "suicide d’écrivain" sur Facebook: "Perumal Murugan est mort. Comme il n’est pas Dieu, il ne va pas ressusciter. \[...\] À partir de maintenant, il va simplement vivre comme l’enseignant qu’il a toujours été. Laissez-le tranquille". Dans cette communication, j’analyserai comment le profil tend à s’imposer comme une instance indépendante, encourageant à dépasser le clivage homme-machine et ses dérivés (numérique/non-numérique; avatar/personne; virtuel/réel). À terme, le profil permet d’abandonner le paradigme représentatif et ouvre la voie à une nouvelle forme de production identitaire à part entière.
