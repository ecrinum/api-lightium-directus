---
title: The end of authority? Toward a political philosophy for Web
short: "Enssib - École Nationale Supérieure of Technology and Library Sciences\r

  <br>17-21, Boulevard du 11 Novembre 1918\r

  <br>Villeurbanne\r

  <br>France\r

  <br>5pm-7pm, Tuesday 18th October 2016\r

  </h3>"
cleanstring: The-end-of-authority-Toward-a-political-philosophy-for-Web
lang: EN
caption: ""
image: http://www.enssib.fr/sites/www/files/logo-enssib_0.png
url: ""
urlTitle: ""
categories:
  - 3
  - 8
  - 11
  - 19
  - 22

---
    
Conference of _Marcello Vitali-Rosati_, Adjoint Professor of Literature and Digital Culture at the Department of French language Literatures of _University of Montréal_   
  
The authority model we knew is deeply connected to a specific conception of what an author is: the one that has developed in the printed edition fiels starting from 18th century, The emergency of this model corresponds to the emergency of the Nation-States : the state's authority is deeply linked to the writer-author's one. The changes determined by the digital give us the impression that this model is valid no longer. We state very often that the Internet is an authority emancipative space. "All the world" can speak, there are no more strong powers, there is no more authority : collectivity has the last word, there are no more authors. Yet, a more attentive analysis can show us that this is very far from true. The Internet is not a deconstructed and non-regulated space, but is merely characterized by different structures. In the frame of these emerging structures, new authority production devices take place. Identifying and studying them is essential to understand the political issues of the digital environment. This intervention propose an approach in order to analyse these new devices.  
  
To listen at the conference recording (french only): <http://www.enssib.fr/bibliotheque-numerique/ecouter/66991-la-fin-de-l-autorite-pour-une-philosophie-politique-du-web>
