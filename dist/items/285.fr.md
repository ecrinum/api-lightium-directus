---
title: Pour une approche philosophico-pratique de l’édition savante à l’ère de Google
short: "Enssib - École Nationale Supérieure\r

  des Sciences de l'Information et des Bibliothèques\r

  <br>17-21, Boulevard du 11 Novembre 1918\r

  <br>Villeurbanne\r

  <br>France\r

  <br>10h-12h, Jeudi, 20 octobre 2016\r

  </h3>"
cleanstring: Pour-une-approche-philosophico-pratique-de-ledition-savante-a-lere-de-Google
lang: FR
caption: ""
image: http://www.enssib.fr/sites/www/files/logo-enssib_0.png
url: ""
urlTitle: ""
categories:
  - 3
  - 8
  - 22
  - 35

---
    
Comment l'édition savante s’adapte-t-elle au numérique? Est-ce possible de concilier la philosophie utilitariste d'outils grand public comme Google avec les exigences spécifiques de la publication savante? Ces questions sont au coeur de la réflexion à la fois théorique et pratique menée par la _Chaire de recherche du Canada sur les écritures numériques_. Dans son intervention, Vitali-Rosati illustrera son approche et les résultats préliminaires de ses travaux, en présentant trois projets concrets: le CMS Lightium, l'éditeur de texte Stylo et le projet de traduction collaborative de l'Anthologie Palatine.  
  
Pour écouter l'enregistrement de la conférence : <http://www.enssib.fr/bibliotheque-numerique/index-des-auteurs?selecAuteur=Vitali-Rosati%2C%20Marcello#haut>
