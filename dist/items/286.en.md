---
title: Theory of editorialization
short: "Enssib - École Nationale Supérieure of Technology and Library Sciences\r

  <br>17-21, Boulevard du 11 Novembre 1918\r

  <br>Villeurbanne\r

  <br>France\r

  <br>2pm-6pm, Thursday 20th October 2016\r

  </h3>"
cleanstring: Theory-of-editorialization
lang: EN
caption: ""
image: http://www.enssib.fr/sites/www/files/logo-enssib_0.png
url: ""
urlTitle: ""
categories:
  - 3
  - 8
  - 19
  - 22

---
    
Marcello Vitali-Rosati will present the results of eight years of reflection on the concept of editorialization, conducted in the framework of the international seminary _Écritures numériques et éditorialisation_, organized since 2008 in cooperation with Nicolas Sauret.   
He defines "editorialization" as the dynamics aggregate producing the digital espace. These dynamics can be understood as the interaction between individual and collective action and a digital environment. Starting from this definition, he will analyse characteristics and particularities of editorialization, pointing out specifically the differences between editorialization and content publishing.  
  
To listen at the conference audio (french only) : <http://www.enssib.fr/bibliotheque-numerique/index-des-auteurs?selecAuteur=Vitali-Rosati%2C%20Marcello#haut>
