---
title: "<span>Nicolas Sauret et Ariane Mayer, « L’autorité dans Anarchy. Les constructions de l’autorité et de l’auctorialité dans un dispositif de production littéraire collaborative : le cas de l’expérience transmédia Anarchy.fr », <i>Quaderni</i>, 2016.</span>"
short: ""
cleanstring: spanNicolas-Sauret-et-Ariane-Mayer-Lautorite-dans-Anarchy-Les-constructions-de-lautorite-et-de-lauctorialite-dans-un-dispositif-de-production-litteraire-collaborative-le-cas-de-lexperience-transmedia-Anarchyfr-iQuadernii-twozeroonesixspan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/16070
urlTitle: ""
categories:
  - 54
  - 41
  - 52
  - 11
  - 35
  - 13

---
    
<!-- pas de contenu sur cette page -->
