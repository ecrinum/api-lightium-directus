---
title: "<span>Marcello Vitali-Rosati, « Digital Architectures: the Web, Editorialization and Metaontology », <i>Azimuth. Philosophical Coordinates in Modern and Contemporary Age</i>, vol. 4 / 7, 2016, p. 95‑111.</span>"
short: ""
cleanstring: spanMarcello-Vitali-Rosati-Digital-Architectures-the-Web-Editorialization-and-Metaontology-iAzimuth-Philosophical-Coordinates-in-Modern-and-Contemporary-Agei-volforseven-twozeroonesix-pninefiveoneoneonespan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/16067
urlTitle: ""
categories:
  - 54
  - 41
  - 22
  - 25
  - 18
  - 17
  - 4
  - 19

---
    
<!-- pas de contenu sur cette page -->
