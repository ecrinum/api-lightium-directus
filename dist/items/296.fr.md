---
title: Conférence de Bruno Bachimont et présentation du DESS en Édition Numérique
short: "Université de Montréal - Pavillon Lionel-Groulx\r

  <br> Salle C-3061\r

  <br> 16h30 - 16 novembre 2016\r

  <br>Conférence enregistrée</h3>"
cleanstring: Conference-de-Bruno-Bachimont-et-presentation-du-DESS-en-Edition-Numerique
lang: FR
caption: ""
image: http://i.imgur.com/wjCZvFJ.png
url: ""
urlTitle: ""
categories:
  - 3
  - 5
  - 6
  - 8
  - 11
  - 19
  - 25
  - 35

---
    
![](http://i.imgur.com/wjCZvFJ.png)   
  
Bruno Bachimont, professeur à l'_Université de technologie de Compiègne_ donnera une conférence intitulée "De l'épistémologie de la mesure à celle de la donnée : un nouveau nominalisme pour les sciences de la culture".  
  
Après la conférence de Bachimont, aura lieu la présentation du [diplôme d’études supérieures spécialisées (DESS) en édition numérique](http://nouvelles.umontreal.ca/article/2016/10/21/l-edition-numerique-entre-a-l-universite/?utm%5Fsource=UdeMNouvelles&utm%5Fcampaign=74bc6e9548-La%5Fquotidienne%5F21%5F10%5F2016&utm%5Fmedium=email&utm%5Fterm=0%5F5cf28dd13d-74bc6e9548-283176325) de l'Université de Montréal.  
Réservation obligatoire avant le 11 novembre : [carrefour@fas.umontreal.ca ](mailto:carrefour@fas.umontreal.ca)   
  
  
En collaboration avec le **CRIHN - Centre de Recherche Interuniversitaire sur les humanités numeriques**.  
  
L'enregistrement de la conférence est disponible sur le compte Archive.org de la [Chaire de recherche](https://archive.org/details/epistemologie-mesure-donnee-nominalisme).
