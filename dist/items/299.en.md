---
title: "<span>Marcello Vitali-Rosati, « « Littérature numérique » : changements et continuité », <i>TicArtToc</i>, 2016, p. 32‑35.</span>"
short: Les technologies numériques changent-elles vraiment la littérature? Peut-on parler d'une littérature numérique? Y a-t-il opposition entre la littérature papier et la littérature numérique? D'une part, on pourrait dire qu'il n'existe aucune opposition entre ces deux formes de littérature cas, dans les faits, il n'y a que des pratiques qui s'inscrivent toujours dans une continuité. D'autre part, on pourrait affirmer qu'au contraire il y a des différences fondamentales dans les pratiques d'écritures, dans les modèles de diffusion et de réception, dans les formats, dans les supports, etc. Cet article explore ces deux perspectives.
cleanstring: spanMarcello-Vitali-Rosati-Litterature-numerique-changements-et-continuite-iTicArtToci-twozeroonesix-pthreetwothreefivespan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/16063
urlTitle: ""
categories:
  - 54
  - 41
  - 22
  - 36
  - 30
  - 4

---
    
<!-- pas de contenu sur cette page -->
