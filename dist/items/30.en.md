---
title: <span>George Brown, Gerd Hauck et Jean-Marc Larrue, « “Mettre en scène” », <i>Intermédialités</i>, 2008, p. 9‑12.</span>
short: Ce numéro d’Intermédialités marque un nouveau pas dans la pénétration de l’approche intermédiale dans le champ des études théâtrales. Bien qu’on relève l’influence grandissante de la pensée intermédiale chez des chercheurs et théoriciens du théâtre au cours des quinze dernières années, on note une réticence du monde du théâtre à adopter cette autre et nouvelle façon de percevoir et de concevoir sa pratique. Ce n’est qu’en 2006 que l’intermédialité fait une première incursion majeure et s’affiche dans ce terrain a priori peu hospitalier grâce à l’ouvrage Intermediality in Theatre and Performance, publié sous la direction de Freda Chapple et Chiel Kattenbelt. [...]
cleanstring: spanGeorge-Brown-Gerd-Hauck-et-Jean-Marc-Larrue-Mettre-en-scene-iIntermedialitesi-twozerozeroeight-pnineonetwospan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13098
urlTitle: ""
categories:
  - 54
  - 41
  - 37
  - 20
  - 12

---
    
<!-- pas de contenu sur cette page -->
