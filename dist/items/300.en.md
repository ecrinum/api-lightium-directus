---
title: Symposium of the 'Literature and digital culture' seminar
short: "<h2>\r

  Tuesday, 6th december 2016\r

  <br>\r

  8.30 am - 5.30 pm\r

  <br>\r

  Hall C3145 - Pavillon Lionel Groulx\r

  <br>\r

  University of Montréal\r

  </h2>"
cleanstring: Symposium-of-the-Literature-and-digital-culture-seminar
lang: EN
caption: ""
image: http://www.larousse.fr/encyclopedie/data/images/1012378-Raphaël_lÉcole_dAthènes.jpg
url: ""
urlTitle: ""
categories:
  - 3
  - 52
  - 58
  - 60
  - 64

---
    
![](http://www.larousse.fr/encyclopedie/data/images/1012378-Raphaël_lÉcole_dAthènes.jpg)   
  
In the frame of the **"Literature and digital culture"** seminar for PhD and master students of the _University of Montréal_'s _French language literatures_, given by Mr. _Vitali-Rosati_, students will attend to a study day dedicated to the presentation of their final works, focusing on the relation between reality and imaginary.  
  
Every student will present a 20 minutes speech on his subject, followed by a collective discussion.  
  
### Program

* 8.30 am: Marie-France Baveye
* 9 am: Stéphanie Proulx
* 9.30 am: Roxanne Julien - _« Entre réalité, imaginaire et représentation de soi. Boumeries, bande dessinée numérique autobiographique »_
* 10 am: Pause
* 10.30 am: Laurent de Maisonneuve - « Réalité virtuelle, réalité augmentée et Pokémon GO. De Jean Valjean à Pikachu »
* 11 am: Laurence Nolet
* 11.30 am: Enrico Agostini Marchese - « La littérature numérique à la dérive. De lignes, d’écriture et d'espace »
* 12 pm: Pause
* 1 pm: Filip Dukanic - « La mimesis dans Les métopes du Parthénon »
* 1.30 pm: Nicolas Sauret - « Modéliser le réel, production ou représentation ? »
* 2 pm: Mélissa Golebiewski - « Treading water in a sea of seeming ». Vidéo, mimesis et effets de direct dans 2666 (Julien Gosselin / Roberto Bolaño))
* 2.30 pm: Marilyne Lamer - « Pratiquer l’espace: Rue Saint-Urbain »
* 3 pm: Pause
* 3.15 pm: Maude Veilleux
* 3.45 pm: Cédric Trahan - « L’imaginaire du profil facebook »
* 4.15 pm: David Beaudoin
  
  
**Free participation - the symposium will be in french only**   
  
The symposium will be live-transmitted via the [**"YouTube"**](https://www.youtube.com/channel/UC5LIw0dopbSSgqI2zdIi84w?spfreload=10) channel of the Research Chair
