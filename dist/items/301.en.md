---
title: Second session of the seminar Écritures Numériques et Éditorialisation - Hervé Le Crosnier and Guylaine Beaudry
short: "<h2>University of Montréal\r

  <br>Pavillon Lionel-Groulx - Hall C-2059\r

  <br>11.30 am - 1.30 pm - 15 décembre 2016\r

  <br>Recorded session</h2>"
cleanstring: Second-session-of-the-seminar-Ecritures-Numeriques-et-Editorialisation---Herve-Le-Crosnier-and-Guylaine-Beaudry
lang: EN
caption: ""
image: http://seminaire.sens-public.org/local/cache-vignettes/L230xH147/arton63-4873e.png
url: ""
urlTitle: ""
categories:
  - 3
  - 5
  - 6
  - 7
  - 8
  - 11
  - 13
  - 19
  - 25
  - 35

---
    
### From edition to editorialisation. Being publisher in the digital environment)
  
  
![](http://seminaire.sens-public.org/local/cache-vignettes/L230xH147/arton63-4873e.png)   
  
How has digital environment changed the publisher work? Which is the place of the editorialisation work in the publishing profession? To what extent is this work the object of outsourcing and delegation - ahead (knowledge engineers, ontologies construction...) and afterwards (search engine, hosts, broadcaster, annotators, and readers)?  
  
Speakers:  
  
**Hervé Le Crosnier**, multimedia publisher at _C&F_ publishing, teaching researcher at _University of Caen_, former library curator ;  
**Guylaine Beaudry**, _Concordia Library_ head curator, _Érudit_ former executive chief and Concordia new digital press creator.  
  
Session recording available on the Archive.org [Chair account](https://archive.org/details/edito17-seance2-edition-a-editorialisation).
