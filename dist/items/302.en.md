---
title: A glass behind the screen
short: "<h2>January, 19 - June, 8.\r

  <br>\r

  7 pm, NUMA - Paris\r

  <br>\r

  All sessions will be live-broadcasted</h2>"
cleanstring: A-glass-behind-the-screen
lang: EN
caption: ""
image: http://uvde.fr/wp-content/uploads/2014/12/uvde_plateau_media.png
url: ""
urlTitle: ""
categories:
  - 3
  - 7
  - 13
  - 25
  - 26
  - 27
  - 48

---
    
![](http://uvde.fr/wp-content/uploads/2014/12/uvde_plateau_media.png)   
  
[_Un verre derrière l’écran_](http://uvde.fr) is a digital culture seminar aiming to investigate, from a social and human sciences point of view, the new technologies' issues in contemporary society and culture. It is an opportunity to debate on digital-oriented theories, methods and practices. The purpose is to critically exchange and discuss on these themes in order to let the public opinion be more aware of the digital culture.  
  
These meetings want themselves to be a bridge between human sciences and digital players. The meetings are free and open to everybody and they gather together academics coming form professional and media domains. Every session is live broadcasted. _Un verre derrière l'écran_ is as well present on [**Facebook**](https://www.facebook.com/unverrederrierelecran/) and [**Twitter**](https://twitter.com/uvde%5F).  
  
  
Sessions schedule:  
  
19th January - [**Anne Guenand**](http://www.ergoia.estia.fr/ergoia-2014/articles/anne-guenand-universite-technologique-de-compiegne.htm) : [**« Expérience utilisateur : les gestes et émotions dans l’interface »**](http://uvde.fr/seance/experience-utilisateur-gestes-emotions-linterface/) ;  
  
23rd Mars - [**Christiane Licoppe**](https://fr.wikipedia.org/wiki/Christian%5FLicoppe) :[ « **Des "Smart Grids" au "Quantified Self" : technologies réflexives et gouvernement par les traces** »](http://uvde.fr/seance/smart-grids-quantified-self-technologies-reflexives-gouvernement-traces/) ;  
  
27th April - [**Ianis Lallemand**](http://diip.ensadlab.fr/fr/etudiants-chercheurs/article/ianis-lallemand) : « Design génératif : le code au service de l’art » ;  
  
18th May - [**Louise Merzeau**](http://merzeau.net) : « Mémoire et numérique : entre collecte et oubli, enjeux sociétaux et professionnels » ;  
  
8th June - [**Fanny Georges**](http://fannygeorges.free.fr) : « Représentation de soi et identités numériques post-mortem ».  
  
  
Seminar's organizer : [**Hétic**](http://www.hetic.net) and [**Numa**](https://paris.numa.co), with the partnership of [**Canada Research Chair on Digital textualities**](http://ecrituresnumeriques.ca/en/), the [**CRIHN - Intercollegiate Research Center on Digital Humanities**](http://www.crihn.org) and the [**University of Montréal**](http://www.umontreal.ca)
