---
title: Third session of the seminar Écritures Numériques et Éditorialisation - From library to web, and the other way round
short: "<h2>University of Montréal\r

  <br>Pavillon Lionel-Groulx - Hall C-8041\r

  <br>11.30pm - 1.30pm - 19 janvier 2017\r

  <br>Recorded session</h2>"
cleanstring: Third-session-of-the-seminar-Ecritures-Numeriques-et-Editorialisation---From-library-to-web-and-the-other-way-round
lang: EN
caption: ""
image: http://seminaire.sens-public.org/local/cache-vignettes/L230xH147/arton64-d1e62.png
url: ""
urlTitle: ""
categories:
  - 3

---
    
### From library to web, and the other way round
  
  
![](http://seminaire.sens-public.org/local/cache-vignettes/L230xH147/arton64-d1e62.png)   
  
Libraries play a privileged role in the landscape shaped by editorialization. If content access and circulation are his core mission, library as institution has to change itself in order to integrate the appropriation challenge. Furthermore, it has to give itself physical and digital devices capable of meeting existing recent practices and supporting new ones. We would focus the reflection on this balance between device and use, necessary for all _place of knowledge_.  
  
Intervenants :  
  
**Montréal : _Catherine Bernier_ from the _Bibliothèque des lettres et sciences humaines_ of the_University of Montréal_ ;** 
Paris : _Benoit Epron_ from the _ENSSIB_.  
  
The session video is available on the Archive.org [Chair account](https://archive.org/details/Edito17seance3-biblio-au-web-et-reciproquement).
