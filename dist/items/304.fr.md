---
title: Troisième séance séminaire Écritures Numériques et Éditorialisation - De la bibliothèque au web et réciproquement
short: "<h2>Université de Montréal\r

  <br>Pavillon Lionel-Groulx - Salle C-8041\r

  <br>11h30 - 13h30 - 19 janvier 2017\r

  <br>Séance enregistrée</h2>"
cleanstring: Troisieme-seance-seminaire-Ecritures-Numeriques-et-Editorialisation---De-la-bibliotheque-au-web-et-reciproquement
lang: FR
caption: ""
image: http://seminaire.sens-public.org/local/cache-vignettes/L230xH147/arton64-d1e62.png
url: ""
urlTitle: ""
categories:
  - 3

---
    
### De la bibliothèque au web et réciproquement
  
  
![](http://seminaire.sens-public.org/local/cache-vignettes/L230xH147/arton64-d1e62.png)   
  
Les bibliothèques jouent un rôle privilégié dans le paysage dessiné par l’éditorialisation. Si l'accès et la circulation des contenus demeurent au cœur de sa mission, la bibliothèque en tant qu’institution doit également se transformer pour intégrer la problématique de l’appropriation. Elle se doit ainsi d’investir des dispositifs physiques et numériques susceptibles d’accueillir les pratiques récentes et existantes tout en en favorisant de nouvelles. Nous porterons la réflexion sur cet équilibre entre dispositif et usage nécessaire à tout _lieu de savoir_.  
  
Intervenants :  
  
**Montréal : _Catherine Bernier_ de la _Bibliothèque des lettres et sciences humaines_ de l'_Université de Montréal_ ;** 
Paris : _Benoit Epron_ de l'_ENSSIB_.  
  
L'enregistrement de la séance est disponible sur le compte Archive.org de la [Chaire de recherche](https://archive.org/details/Edito17seance3-biblio-au-web-et-reciproquement).
