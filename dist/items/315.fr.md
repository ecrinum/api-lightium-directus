---
title: <span>Servanne Monjour, Marcello Vitali-Rosati et Gérard Wormser, « Le fait littéraire au temps du numérique. Pour une ontologie de l’imaginaire », <i>Sens Public</i>, décembre 2016.</span>
short: "Souvent conçues comme le vecteur de changements majeurs, les pratiques numériques constituent une occasion de mieux comprendre le fait littéraire en faisant apparaître de manière plus explicite que jamais des aspects ontologiques qui, en tant que tels, ont une valeur atemporelle. En particulier, et c’est l’objet de cet article, le fait numérique donne l’occasion de réinvestir une problématique qui parcourt l’ensemble de la réflexion sur le statut de la littérature depuis Platon et Aristote : celle du rapport entre littérature et réalité, dont Sartre et Derrida avaient déjà œuvré à déconstruire l’opposition au XXe siècle. Selon nous, le numérique souligne l’absence de séparation entre symbolique et non-symbolique, nous empêchant de penser une rupture entre imaginaire et réel. Pour rendre compte de cette structure, nous nous appuierons sur le concept d’éditorialisation, qui vient désigner l’ensemble des dispositifs permettant la production de contenus dans l’espace numérique en tenant compte de la fusion entre espace numérique et espace non numérique. À travers des exemples littéraires – Traque Traces de Cécile Portier et Laisse venir d’Anne Savelli et Pierre Ménard – nous démontrerons comment la littérature participe aujourd’hui à l’éditorialisation du monde, enterrant ainsi définitivement le dualisme imaginaire-réel pour lui préférer une structure anamorphique."
cleanstring: spanServanne-Monjour-Marcello-Vitali-Rosati-et-Gerard-Wormser-Le-fait-litteraire-au-temps-du-numerique-Pour-une-ontologie-de-limaginaire-iSens-Publici-decembre-twozeroonesixspan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/16362
urlTitle: ""
categories:
  - 54
  - 41
  - 39
  - 22
  - 25
  - 18
  - 4
  - 19

---
    
<!-- pas de contenu sur cette page -->
