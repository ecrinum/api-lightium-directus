---
title: "<span>Chloé Savoie-Bernard et Jean-François Thériault, « Ouvrir le livre et voir l’écran : pratiques littéraires et pratiques numériques », <i>Sens Public</i>, décembre 2016.</span>"
short: "Ce dossier s’intéresse à la manière dont s’entrelacent littérature et numérique dans la littérature contemporaine. Puisque le numérique teinte l’ensemble des pratiques humaines, la littérature fait partie des lieux qu’elle investit. Ainsi, la question autour de laquelle s’articulent les textes de ce dossier est donc la suivante : comment pratiques littéraires et pratiques numériques peuvent-elles dialoguer aujourd’hui, et que peut-on dire des interactions qui découlent de ces échanges ? Il ne s’agit pas d’affirmer que toute la production littéraire contemporaine développe un discours sur le numérique, mais bien que les nouvelles possibilités de lecture et d’analyse qui nous sont offertes méritent d’être analysées et mises à l’épreuve."
cleanstring: spanChloe-Savoie-Bernard-et-Jean-Francois-Theriault-Ouvrir-le-livre-et-voir-lecran-pratiques-litteraires-et-pratiques-numeriques-iSens-Publici-decembre-twozeroonesixspan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/16360
urlTitle: ""
categories:
  - 54
  - 41
  - 50
  - 4
  - 19

---
    
<!-- pas de contenu sur cette page -->
