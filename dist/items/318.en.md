---
title: Conference of Pierre Bayard
short: "<h2>Université du Québec à Montréal,\r

  <br>Management Science Pavilion, Local R-M120\r

  <br>Tuesday, 25th April 2017, 6:30 pm - 8:30 pm</h2>"
cleanstring: Conference-of-Pierre-Bayard
lang: EN
caption: ""
image: http://i.imgur.com/P8d7PfC.png
url: ""
urlTitle: ""
categories:
  - 3

---
    
![](http://i.imgur.com/P8d7PfC.png)

### _We are not told everthing_  
Advocacy for police criticism

  
**Pierre Bayard** is professor of French Literature at the **University Of Paris VIII** and **psychoanalyst**. He is known for his essays which revisit the paradoxes, the pitfalls of literature, its unsuspected possibles and to rely on rash methods of reading to restore truth hidden under the layers of fiction. His works, accessible and playful, reach a wide audience. The scope of its study objects belongs to both the genre of the **traditional detective novel** (Conan Doyle, Christie) and**classics of literature** (Shakespeare, Balzac, Laclos, Proust, etc.). Pierre Bayard chose to discover how the writers imitated their successors, how the characters influence the reality, or how universe of fiction open up worlds to invest. The variety of subjects he reflected, the **unusual angle** by which he approached them made his approach unique, for many, **_suspicious_**, and in this, particularly conducive to open the discussion and lead the debates.  
  
The conference is free and open to the public.  
  
Organized by   **Cassie Berard**  and   **Thomas Carrier-Lafleur** , in collaboration with the _Fond de recherche Société et culture du Québec_,**Figura**, **UQÀM**, the _Social Sciences and Humanities Research Council Of Canada_ and the **Canada Research Chair on Digital Textualities.**   
  
For additional information, you may e-mail[figura@uqam.ca](mailto:figura@uqam.ca),   
 phone **514 987 3000**, ext. **2153**, or visit[Figura's website](http://figura.uqam.ca/). 
