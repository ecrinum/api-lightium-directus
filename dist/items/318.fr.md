---
title: Conférence de Pierre Bayard
short: "<h2>Université du Québec à Montréal,\r

  <br>Pavillon des sciences de la gestion, Salle R-M120\r

  <br>Mardi 25 avril 2017, 18h30-20h30"
cleanstring: Conference-de-Pierre-Bayard
lang: FR
caption: ""
image: http://i.imgur.com/P8d7PfC.png
url: ""
urlTitle: ""
categories:
  - 3

---
    
![](http://i.imgur.com/P8d7PfC.png)

### _On ne nous dit pas tout_  
Plaidoyer pour la critique policière

**Pierre Bayard** est professeur de littérature française à l'**Université de Paris VIII** et **psychanalyste**. Il est reconnu pour ses essais qui revisitent les paradoxes, les écueils de la littérature, ses possibles insoupçonnés et pour s'appuyer sur des méthodes de lectures téméraires pour **rétablir la vérité cachée sous les couches de la fiction**. Ses travaux, accessibles et ludiques, rejoignent un vaste public. L’étendue de ses objets d’étude appartiennent tant au genre du **roman policier traditionnel** (Conan Doyle, Christie) qu’aux **classiques de la littérature** (Shakespeare, Balzac, Laclos, Proust, etc.). Pierre Bayard choisit de découvrir comment les écrivains ont imité leurs successeurs, comment les personnages influencent le réel, ou comment les univers de fiction ouvrent des mondes à investir. La variété des sujets auxquels il réfléchit, l’**angle inusité** par lequel il les aborde rendent sa démarche unique, pour plusieurs **_suspicieuse_**, et en cela, particulièrement propice à ouvrir la discussion et à entraîner les débats.  
  
La conférence est gratuite et ouverte au public.  
  
Organisée par **Cassie Bérard** et **Thomas Carrier-Lafleur**, en collaboration avec le _Fond de recherche Société et culture du Québec_, **Figura**, **l'UQÀM**, le _Conseil de recherche en sciences humaines du Canada_ et la **Chaire de recherche du Canada sur les écritures numériques.**   
  
Pour toute information additionnelle, vous pouvez communiquer par courriel à [figura@uqam.ca](mailto:figura@uqam.ca),   
par téléphone au **514 987 3000**, poste **2153**, ou visiter le site de [Figura](http://figura.uqam.ca/). 
