---
title: "Appel à contributions : L’ontologie du numérique. Entre mimésis et réalité."
short: "<h2>Dossier Sens Public\r

  <br>\r

  Sous la direction de Servanne Monjour, Marcello Vitali-Rosati, Matteo Treleani\r

  <br>\r

  Date limite : 1er Juillet 2017</h2>"
cleanstring: Appel-a-contributions--Lontologie-du-numerique-Entre-mimesis-et-realite
lang: FR
caption: ""
image: http://www.laboiteverte.fr/wp-content/uploads/2012/06/illustration-3d-10.jpg
url: ""
urlTitle: ""
categories:
  - 3

---
    
![](http://www.laboiteverte.fr/wp-content/uploads/2012/06/illustration-3d-10.jpg)
  
  
Dans sa longue notice (auto)biographique publiée sur _Le Tiers livre_, François Bon prédit en ces termes la fin de sa vie et l'aboutissement de son œuvre :  
  
Évolution progressive et définitive du site Tiers Livre en arborescence d’oeuvre transmedia et préparation d’un verre sphérique inaltérable et indestructible incluant la totalité de cette oeuvre unique.  
Déclare dans son dernier billet de blog : “J’aurais pu faire ma vie autrement, mais je n’y avais pas pensé avant”. Cependant, la révélation que l’auteur habitait depuis de nombreuses années dans son site Internet provoque un certain émoi et beaucoup de sensation et d’interrogation dans le monde numérique et littéraire.   
  
Non sans humour, l'écrivain souligne la fusion qui s’opère aujourd’hui entre les espaces numérique et non numérique, ou du moins le brouillage constant des frontières entre ce qui relève traditionnellement du « réel » et de l'« imaginaire ». En vérité, ce brouillage n’a rien d’inédit, aussi dirons-nous que le numérique permet de réinvestir certaines problématiques ontologiques qui ont traversé l’histoire de la pensée – en y ajoutant au passage ses propres paradoxes.  
  
D’un côté en effet, la notion de représentation a été largement utilisée pour analyser l’effet de nos écrans numériques, bien que l’on puisse regretter l’aspect restrictif d’une telle approche qui, essentiellement concentrée sur la dimension visuelle des médias numériques, occulte tout ce qui se trouve du côté des pratiques – l’analyse du concept d’interface, proposée par Alexander Galloway permet d’ailleurs d’y remédier (Galloway, 2012). D’un autre côté, le terme « réalité » (augmentée ou virtuelle) n’a cessé d’être convoqué afin de définir le statut des mondes numériques – l’adjectif « virtuel » ayant alors pour fonction d’affirmer une progressive perte de la matérialité du rapport avec l'espace dit réel (Serres 1994, Koepsell 2000, Virilio 1996). Aujourd’hui enfin, de plus en plus de chercheurs s’accordent à dire que nous vivons dans un espace hybride (Beaude 2012, Vitali-Rosati, 2012, Floridi 2014), où les distinctions entre réel et numérique n’ont plus de sens…  
  
Dans ce contexte, les narrations transmédia s’emploient elles aussi à repousser les frontières entre mondes fictionnels et monde(s) réel(s), en s’appuyant notamment l’engagement des spectateurs (Jenkins, 2008). Les produits en réalité augmentée mélangent désormais la vision du monde qui nous entoure avec des éléments ludiques ou issus de la fiction. Le statut de ces nouvelles narrations est complexe : comment qualifier les tweets de Clara Beaudoux dans son _Madeleine project_, ou ceux de Guillaume Vissac dans _Accident de personne_ ? Comment décrire le projet tentaculaire qui se construit depuis près de 20 ans autour du Général Instin, investissant l'espace web autant que l'espace urbain ? S'agit-il d'écriture documentaire, journalistique ou fictive ? Cette question est-elle encore seulement pertinente ? Quel est le statut de produits comme le jeu _Pokemon Go_ ou les _Street view trek_ proposés par Google ?  
  
Si le brouillage des frontières ontologiques est devenu un caractère constitutif du numérique, il n’en soulève pas moins de nombreuses questions : peut-on véritablement déclarer que les notions de représentation, de réel, ou de virtuel sont définitivement périmées ? Ou faudrait-il, au contraire, réaffirmer leur intérêt et leur pertinence, du moins d’un point de vue heuristique ? Peut-on parler d’une problématique « ontologique » dans la culture numérique ou s'agit-il d’une querelle de mots ?  
  
Ce dossier se conçoit comme un champ d'exploration de ces problématiques, dans une perspective résolument interdisciplinaire, accueillant tout autant la philosophie, l’esthétique, les études littéraires, la sémiologie, la sociologie ou les sciences de l’information et de la communication. Des arts numériques à la littérature hypermédiatique, en passant par les web documentaires et les jeux vidéo, de nombreux domaines permettent en effet d’investiguer ces dichotomies apparemment périlleuses entre représentation et réalité, réel et imaginaire, fiction et documentaire… Parmi les sujets traités, pourront notamment figurer (à titre indicatif) :
* le rapport entre espace numérique et espace non numérique
* les récits de soi
* les créations en réalité virtuelle
* la réalité augmentée
* les interactions entre jeux vidéos et monde réel
* le rôle documentaire des produits numériques (web documentaires, web-séries…)
* les récits transmédia et l’engagement des publics
* l'emploi du web sémantique ou des objets à des fins créatives
* l'actualité du concept de mimésis
* les enjeux du concept de vérité à l'époque du numérique
  
Les textes, compris entre 35 000 à 60 000 signes (illustrations bienvenues), doivent être adressés à la rédaction de [Sens Public](mailto:redaction@sens-public.org).  
  
**CALENDRIER** :  
1er juillet : remise des textes   
31 août : avis d'acceptation  
1er octobre : publication du dossier  
  
**BIBLIOGRAPHIE** :  
  
Aristote, _Poétique_, Paris, Le Livre de Poche, 1990.  
Auerbach, Erich, _Mimésis : la représentation de la réalité dans la littérature occidentale_, Paris, Gallimard, 1977.  
Beaude, Boris, _Internet. Changer l’espace, changer la société_, Limoges, FYP éditions, 2012\.   
Bolter, Jay-David & Richard Grusin, _Remediation. Understanding New Media_, Cambridge, Mass., MIT Press, 1999.  
Bon, François, _Après le livre_, Paris, Seuil, 2011.  
Bunia, Remigius, « Diegesis and Representation: Beyond the Fictional World, on the Margins of Story and Narrative », _Poetics Today_ 31, no 4 (1 décembre 2010), p. 679‑720.  
Cassou-Nogues, Pierre, _Mon zombie et moi. La philosophie comme fiction_, Paris, Seuil, 2010.  
Floridi, Luciano, _The 4th revolution: how the infosphere is reshaping human reality_, New York, Oxford, Oxford University Press, 2014.  
Fourmentraux, Jean-Paul (dir.), _Digital Stories. Art, design et culture transmédia_, Paris, Hermann, 2016.  
Galloway, Alexander R., _The Interface Effect_. Cambridge,UK ; Malden MA, Polity, 2012.  
Jenkins, Henry, _Convergence Culture: Where Old and New Media Collide_, New York, NYU Press, 2008.  
Koepsell, David R., _The Ontology of Cyberspace: Philosophy, Law, and the Future of Intellectual Property_, Chicago, Il, Open Court Publishing, 2003.  
Larsonneur, Claire, Arnaud Regnauld, Pierre Cassou-Nogues, Sara Touiza, _Le sujet digital_, Dijon, Presses du réel, 2015.  
Lavocat, François, _Fait et fiction: pour une frontière_, Paris, Seuil, 2016.  
Lévy, Pierre, _Qu’est-ce que le virtuel ?_, Paris, La découverte, 1998.  
Manovich, Lev, _The Language of New Media_, Cambridge, Mass., MIT Press, 2001.  
Monjour, Servanne, Marcello Vitali-Rosati et Gérard Wormser, « Le fait littéraire au temps du numérique. Pour une ontologie de l’imaginaire », _Sens Public_, décembre 2016.  
Orlando, Francesco, _Les objets désuets dans l’imagination littéraire_, Paris, Classiques Garnier, 2013.  
Platon, _La République_, Paris, Flammarion, 2002.  
Rodionoff, Anolga, _Les territoires saisis par le virtuel_, Rennes, PUR, 2012.  
Ruffel, Lionel, _Brouhaha, les mondes du contemporain_, Lagrasse, Verdier, 2016.  
Serres, Michel, _Atlas. le Grand livre du mois_, Paris, Flammarion, 1994\.   
Sartre, Jean-Paul, _L’imaginaire_, Paris, Gallimard, 1940.  
Vitali-Rosati, Marcello, « What Is Editorialization ? » _Sens Public_, 4 janvier 2016.  
Virilio, Paul, _Cybermonde, la politique du pire_, Paris, Textuel, 2010.  
Vial, Stéphane, _L’être et l’écran_, Paris, PUF, 2013.
