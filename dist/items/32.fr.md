---
title: <span>Marcello Vitali-Rosati, « Une éthique appliquée? Considérations pour une éthique du numérique », <i>Éthique publique</i>, vol. 14 / 2, 2012, p. 13‑32.</span>
short: Lorsque l’on s’interroge sur l’éthique du numérique, deux approches sont possibles. La première consiste à la considérer comme une éthique appliquée, une branche de l’éthique générale. Les principes éthiques généraux nous donneraient la capacité de discerner le bien et le mal ; les appliquer au numérique nous permettrait ainsi d’expliciter, à partir de ces principes, des normes de comportements dans ce domaine particulier. Mais cette approche ne prend pas en compte le fait que les principes éthiques puissent découler des conditions concrètes de leurs applications. Nous pouvons donc considérer une seconde approche, qui consisterait à partir de l’analyse du domaine du numérique pour fonder sur ses caractéristiques la réflexion morale. Par conséquent, l’éthique du numérique ne serait pas une éthique appliquée, mais une éthique première. Cet article tente de poser les bases pour une réflexion sur l’éthique du numérique qui prendrait en considération les changements de culture et de valeurs engendrés par les nouvelles technologies.
cleanstring: spanMarcello-Vitali-Rosati-Une-ethique-appliquee-Considerations-pour-une-ethique-du-numerique-iEthique-publiquei-volonefortwo-twozeroonetwo-ponethreethreetwospan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13100
urlTitle: ""
categories:
  - 54
  - 41
  - 22
  - 25
  - 15
  - 31
  - 27

---
    
<!-- pas de contenu sur cette page -->
