---
title: Quatrième séance séminaire Écritures Numériques et Éditorialisation - Faire métier de journaliste dans l’environnement numérique
short: "<h2>Université de Montréal\r

  <br>Salle B4310, Bâtiment 3200 Jean Brillant\r

  <br><b><i>Horaire spécial</i></b> : 12h30 - 14h30 - 16 mars 2017\r

  <br>Séance enregistrée</h2>"
cleanstring: Quatrieme-seance-seminaire-Ecritures-Numeriques-et-Editorialisation---Faire-metier-de-journaliste-dans-lenvironnement-numerique
lang: FR
caption: ""
image: http://seminaire.sens-public.org/local/cache-vignettes/L230xH147/arton65-2ecb6.png
url: ""
urlTitle: ""
categories:
  - 3
  - 5
  - 7
  - 19

---
    
### Faire métier de journaliste dans l’environnement numérique
  
  
![](http://seminaire.sens-public.org/local/cache-vignettes/L230xH147/arton65-2ecb6.png)   
  
Depuis les premières publications en ligne des journaux à la fin des années 90, les médias se sont adaptés à la nouvelle donne médiatique. Leur arrivée sur Internet et sur le web est marquée par plusieurs évolutions majeures, comme les nouvelles formes d’écritures (hypertexte, vidéo, webdocumentaire, microblogging) accompagnant les pratiques émergentes de consommation de l’information. Plus significatifs, des phénomènes tels que le référencement des journaux par Google, leur éditorialisation par Google News et à partir des années 2010, leur circulation massive de plus en plus fragmentée via les réseaux sociaux ou, plus récemment, leur intégration et leur personnalisation dans les newsfeed de Facebook ont défini un nouvel environnement redistribuant les rapports de pouvoir et d’autorité dont ils bénéficiaient. La controverse actuelle autour des fake-news et des sites de fact-checking est à ce titre révélatrice d’une crise de l’éditorialisation, que cette séance se propose d’interroger en examinant les stratégies mises en œuvre par les différents acteurs pour négocier cette transition.  
  
Intervenants :  
  
Montréal : _**Juliette De Maeyer**_, professeure adjointe au département de Communication de l’_Université de Montréal_ ;  
  
Paris : _**Denis Teyssou**_, _Agence France-Presse_.  
  
La Chaire de recherche utilise comme système d'archivage de ses médias [Internet Archive](https://archive.org). L’IA met gratuitement ses collections à la disposition des chercheurs, historiens et universitaires. Le robot d'indexation utilisé par l'IA est un logiciel libre, ainsi que son logiciel de numérisation de livres.  
  
Si ne visualisez pas le lecteur vidéo, l'enregistrement de la conférence est disponible sur le compte Archive.org de la [Chaire de recherche](https://archive.org/details/faire-metier-journaliste-numerique).
