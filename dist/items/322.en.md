---
title: Conference of Marcello Vitali-Rosati
short: "<h2>University of Montreal\r

  <br>Carrefour des arts et des sciences, Local C-2059\r

  <br>Tuesday, 11th April 2017, 4.30 pm\r

  <br>Registered session</h2>"
cleanstring: Conference-of-Marcello-Vitali-Rosati
lang: EN
caption: ""
image: http://www.chairelexum.ca/files/sites/21/2017/02/ChaireLexUM_Rosati_11-avril_web-475x734.jpg
url: ""
urlTitle: ""
categories:
  - 3
  - 22

---
    
![](http://www.chairelexum.ca/files/sites/21/2017/02/ChaireLexUM_Rosati_11-avril_web-475x734.jpg)

### Digital Authorities and Post-Truth

  
At the time of **« post-truth »** and of **« alternative facts »**, we can – and we _must_ – ask ourselves : What is authoritative in digital space ? What are the devices that give weight to an assertion or to information? It has often been argued that the Internet is a place of emancipation of the authority. « Everybody » can talk, there is no strong powers, there is no more authority, the community has the last word, there is no authors anymore. However, a closer analysis shows that this is far from true. The Internet is not a destructured and unregulated place, it is simply characterized by _different_ structures. Within the framework of these structures emerge new devices of production of authority. Identifying and studying these mechanisms is essential to understand the political stakes of the digital environment and in particular to try to identify "public" spaces where **truth regimes** are acceptable for democratic societies.  
  
The conference, presented by **Marcello Vitali-Rosati**, _Digital Textualities Canada Research Chair_'s holder, is an invitation from the **LexUM Chair on Legal Information**. It is free, and open to everyone.  
  
For additional information, you may visit[LexUM Chair on Legal Information's website.](http://www.chairelexum.ca/evenements/conferences/autorites-numeriques-et-post-verite/)   
  
The session is now available on the [Cyberjustice Lab's YouTube channel](https://www.youtube.com/watch?v=kPQ5a8XNWjA).
