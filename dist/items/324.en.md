---
title: <span>Élisabeth Routhier et Jean-François Thériault, « Performance auctoriale et dispositif littéraire. Autour de Pourquoi Bologne d’Alain Farah », <i>Fabula. Colloques en ligne</i>, février 2017.</span>
short: Dans une salle de classe de l’Université McGill, à Montréal, un jeune professeur en costume donne une séance de cours. Une cigarette électronique K808 Turbo Voluptueuse est posée sur son bureau pendant qu’il parle tantôt de littérature, tantôt des rapports conflictuels qu’il entretient avec sa mère. Ce professeur s’appelle Alain Farah et il est aussi écrivain. Il a publié un recueil de poésie, Quelque chose se détache du port, avant de faire paraitre son premier roman, Matamore no 29, suivi quelque temps plus tard de Pourquoi Bologne. [...]
cleanstring: spanElisabeth-Routhier-et-Jean-Francois-Theriault-Performance-auctoriale-et-dispositif-litteraire-Autour-de-Pourquoi-Bologne-dAlain-Farah-iFabula-Colloques-en-lignei-fevrier-twozeroonesevenspan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/18983
urlTitle: ""
categories:
  - 54
  - 41
  - 46
  - 50
  - 11
  - 16

---
    
<!-- pas de contenu sur cette page -->
