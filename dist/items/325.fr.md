---
title: <span>Enrico Agostini Marchese, « Les structures spatiales de l’éditorialisation. Terre et mer de Carl Schmitt et l’espace numérique », <i>Sens Public</i>, mars 2017.</span>
short: Comment penser l’espace à l’ère du numérique ? En nous inscrivant dans le sillage du « tournant spatial », nous nous proposons de conjuguer la théorie de l’éditorialisation et la réflexion sur l’espace à l’aide d’une confrontation avec l’ouvrage Terre et mer du théoricien de la géopolitique Carl Schmitt.
cleanstring: spanEnrico-Agostini-Marchese-Les-structures-spatiales-de-leditorialisation-Terre-et-mer-de-Carl-Schmitt-et-lespace-numerique-iSens-Publici-mars-twozeroonesevenspan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/18301
urlTitle: ""
categories:
  - 54
  - 41
  - 58
  - 25
  - 18
  - 19

---
    
<!-- pas de contenu sur cette page -->
