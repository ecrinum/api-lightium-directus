---
title: Round Table - The Encyclopedic Project in the Digital Era
short: "<h2>University of Montréal\r

  <br>Pavillon Lionel-Groulx, Local C-3061\r

  <br>Tuesday 6th April 2017, 4pm-5:30pm\r

  <br>Recorded session</h2>"
cleanstring: Round-Table---The-Encyclopedic-Project-in-the-Digital-Era
lang: EN
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 3

---
    
Organized by the **CRIHN**, _Canada Research Chair in Film and Media Studies_ and **Canada Research Chair in Digital Textualities** as a part of the _Technes_ project, this round table will raise the question of the renewal of the encyclopedic project as a form of systematisation and circulation of knowledge.  
How to reconsider the editorial and epistemological model of the Encyclopedia in the digital era?  
  
**Moderator** : Servanne Monjour  
  
**Speakers** : 
* Rémy Besson
* Benoît Melançon
* Emmanuel Château-Dutier
  
  
The CRC uses [Internet Archive](https://archive.org) as media archiving system. IA freely offers his collections to researchers and academics The IA indexation robot is a free software, as well as his books scanning software.  
  
If you can not display the video player, the seminar session recording is available on the Archive.org [Chair account](https://archive.org/details/TableRondeLeProjetEncyclopediqueALepoqueDuNumerique).
