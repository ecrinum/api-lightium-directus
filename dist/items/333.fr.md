---
title: Conférence de Jill Didur
short: "<h2>Université de Montréal\r

  <br>3150, rue Jean-Brillant, local C-8111\r

  <br>Vendredi 21 avril 2017, 11h-12h30\r

  </h2>\r\n"
cleanstring: Conference-de-Jill-Didur
lang: FR
caption: ""
image: http://www.crihn.org/files/sites/33/2015/07/didurjill.jpg
url: ""
urlTitle: ""
categories:
  - 3

---
    
![](http://www.crihn.org/files/sites/33/2015/07/didurjill.jpg)   
  
**Jill Didur** présentera une conférence intitulée « **Beyond Anti-Conquest: Unearthing the Botanical Archive with Locative Media** »:  
  
«Cette présentation étudie le rôle productif que les médias locatifs peuvent jouer pour déterrer les complexités de l'archive coloniale associée aux jardins botaniques. À travers une discussion sur l'histoire et le design des jardins alpins, les débouchés critiques des applications des médias locatifs, la structure pédagogique et les objectifs de mon application mobile expérimentale, _le Jardin alpin autrement_, je réfléchis comment les plateformes mobiles des médias peuvent être utilisées pour remettre en question la nostalgie du colonialisme qui influence souvent inconsciemment la façon dont les visiteurs sont encouragés à expérimenter les jardins botaniques.»   
  
Dr. Jill Didur est professeure et ancienne directrice du Département d'Anglais à l'**Université Concordia**, Montréal. Elle est co-éditrice du _Global Ecologies and the Environmental Humanities: Postcolonial Approaches_ (Routledge 2015), l'auteure de _Unsettling Partition: Literature, Gender, Memory_ (UTP 2006), et la créatrice de [Alpine Garden MisGuide / Jardin alpine autrement](https://itunes.apple.com/ca/app/alpine-garden-misguide/id991874716?mt=8) (iTunes Apple Store 2015).
