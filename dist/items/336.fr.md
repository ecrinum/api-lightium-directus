---
title: Colloque international <br> « L'invention littéraire des médias »
short: "<h2>Cinémathèque québécoise, 335, Boulevard de Maisonneuve Est, Montréal \r

  <br>Jeudi 27 et vendredi 28 avril 2017\r

  </h2>"
cleanstring: Colloque-international-br--Linvention-litteraire-des-medias-
lang: FR
caption: ""
image: http://i.imgur.com/zUJpLsv.png
url: http://ilm.ecrituresnumeriques.ca/fr/
urlTitle: "Site web du colloque "
categories:
  - 3
  - 7
  - 22
  - 39

---
    
![](http://i.imgur.com/zUJpLsv.png)
  
  
Les médias existeraient-ils sans la littérature? Pourrait-on en effet parler de télévision, de photographie ou de cinéma sans que ces dispositifs aient aussi été construits et institutionnalisés dans notre imaginaire collectif par la littérature et son discours ? Lorsque **Jules Verne**, dans _Les Frères Kip_ (1902), fait apparaître les visages des meurtriers Flig Balt et de Vin Mod sur une photographie du cadavre du Capitaine Gibson, dont la rétine a imprimé l'image de sa mort (comme si l’œil fonctionnait à la manière d'un appareil photographique!), il participe pleinement à la construction de l'imaginaire collectif de la photographie. Cet imaginaire du médium est certes largement fantasmé, mais il va influencer pour longtemps les pratiques photographiques et leur réception, à commencer par le pouvoir de _révélation_ de l'image et par sa prétendue valeur testimoniale. Ainsi, il apparaît clairement que les médias ne sont pas seulement des réalités technologiques, mais aussi - et peut-être d'abord - des constructions discursives. Or dans un contexte de bouleversements technologiques et culturels majeurs initiés par le numérique, il devient essentiel de mieux comprendre le rôle et l'influence que joue la littérature dans la construction et l'institutionnalisation des médias.   
  
C'est là l'objectif du colloque international «L'invention littéraire des médias», né de la collaboration entre la **Chaire de recherche du Canada en écritures numériques** et la **Chaire de recherche du Canada en études cinématographiques et médiatiques**, dont les travaux s'inscrivent au carrefour des médias anciens et modernes. Ce colloque réunira une vingtaine de chercheurs internationaux dont les travaux questionnent ce mode particulier de l'invention des médias qu'est la littérature.   
  
Organisateurs : **Thomas Carrier-Lafleur**, **André Gaudreault**, **Servanne Monjour** et **Marcello Vitali-Rosati**.
