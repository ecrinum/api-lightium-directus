---
title: Colloque ÉCRiDiL « Écrire, éditer, lire à l’ère numérique » 2018
short: "<h2>Montreal \r

  <br>Monday April 30<sup>th</sup>, 2018 and tuesday May 1<sup>st</sup>, 2018</h2>"
cleanstring: Colloque-ECRiDiL-Ecrire-editer-lire-a-lere-numerique-twozerooneeight
lang: EN
caption: ""
image: https://c2.staticflickr.com/2/1310/860471805_d4b919dc56_z.jpg?zz=1
url: ""
urlTitle: ""
categories:
  - 3
  - 22
  - 24

---
    
<!-- pas de contenu sur cette page -->
