---
title: Colloque ÉCRiDiL « Écrire, éditer, lire à l’ère numérique » 2018
short: "<h2>Montréal\r

  <br />Usine C, 1345 avenue Lalonde \r

  <br>Lundi 30 avril 2018 et mardi 1<sup>er</sup> mai 2018</h2>"
cleanstring: Colloque-ECRiDiL-Ecrire-editer-lire-a-lere-numerique-twozerooneeight
lang: FR
caption: ""
image: https://c2.staticflickr.com/2/1310/860471805_d4b919dc56_z.jpg?zz=1
url: ""
urlTitle: ""
categories:
  - 3
  - 22
  - 24

---
    
![](https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F42522179%2F249496737807%2F1%2Foriginal.jpg?w=800&rect=0%2C2%2C2214%2C1107&s=937470914caa00af4e779d3dcd202f8d)

### Le livre, défi de design : l’intersection numérique de la création et de l’édition

  
### Deuxième édition du Colloque ÉCRiDiL « Écrire, éditer, lire à l’ère numérique »

Le colloque ECRiDiL étudie sans les séparer les trois usages fondamentaux de la chaîne du livre (ÉCRire, éDIter, Lire) à partir de la culture de l’innovation par le design, dans l’esprit de l’innovation sociale et numérique.  

### Inscription gratuite

Réservez votre place !

[Développé par Eventbrite](https://www.eventbrite.fr/)
  
  
### Programme

#### – 30 avril –

* 8h30 – Accueil
* 8h45 – Mot de bienvenue
* 9h00-10h20 – Séance {A} « Matérialités du design du livre numérique »
   * Marc Jahjah et Clémence Jacquot, « Interroger la matérialité numérique d’une œuvre "hybride" : l’énonciation éditoriale de Poreuse »
   * Oriane Deseilligny, « Entre livre-objet et objet livre : représentations et circulations de la matérialité du livre en régime numérique »
   * Julien Drochon, « Plasticités plurielles des lectures numériques »
   * Période de questions
* 10h20-10h40 – Pause
* 10h40-12h00 – Séance {B} « Interactivité, arts et design éditorial »
   * Nolwenn Tréhondart et Alexandra Saemmer, « Le design éditorial des livres d'art numériques, au prisme de leurs pratiques de conception et de réception »
   * Françoise Paquienséguy, « Représenter la création artistique : les catalogues d'exposition numérique, comment aborder l’œuvre et son auteur ? »
   * Période de questions
   * Table ronde {C} « Hybridation des modèles éditoriaux et graphiques des publications d’art sur supports numériques »
* 12h00-13h15 – Lunch
* 13h15-14h30 – Séance {D} « Édition scientifique : enjeux »
   * Anthony Masure, « Design et humanités numériques, vers une convergence du livre et du web ? »
   * Jean-Louis Soubret, « Design thinking et édition - point d'avancement »
   * Lucile Haute et Julie Blanc, « Design des publications scientifiques multisupports »
   * Période de questions
* 14h30-14h50 – Pause
* 14h50-16h10 – Séance {E} « Édition scientifique : études de cas » \[SÉANCE PARALLÈLE\]

#### – 1er mai –

* 8h45 – Accueil
* 9h00-10h20 – Séance {H} « Fondements repensés »
   * Emmanuël Souchier, « Éléments pour une épistémologie du design en contexte numérique »
   * Franck Cormerais, « Retour sur la "Lecture Appropriative" dans la dynamique des humanités digitales »
   * Olivier Charbonneau, « Encoder les communs de la lecture »
   * Période de questions
* 10h20-10h40 – Pause
* 10h40-12h15 – Séance {I} « Interactions, poétiques en contexte numérique »
   * Arthur Perret, « La typographie du livre numérique, entre fluidité et frictions »
   * Arnaud Laborderie, « Du livre enrichi au livre augmenté : les enjeux d’une clôture numérique »
   * Période de questions
   * Elsa Tadier, « Corps, livre et design. Quand le numérique invite à revisiter la place du corps dans les dispositifs de lecture »
   * Dominique Raymond, « Potentiel2\. Numérique et contrainte pour Savage (Nathalie), Dickner et Fortier (Révolutions) »
   * Période de questions
* 12h15-13h30 – Lunch
* 13h30-14h30 – Stands {J} \[SÉANCE PARALLÈLE\]
   * Jimmy Gagné, « Présentation Album jeunesse animé et narré »
   * Nicolas Tilly, « Impression postnumérique et remix éditorial »
   * Gisèle Henniges, « Co-création auteur et designer : ergonomie de l’œuvre augmentée et expérience utilisateur »
   * Marcello Vitali-Rosati, Arthur Juchereau, Servanne Monjour et Nicolas Sauret, « Stylo : un éditeur sémantique WYSYWYM pour l'édition savante de demain »
* 13h30-14h30 – Atelier d'idéation collective {K} \[SÉANCE PARALLÈLE\]
* 14h30-14h50 – Pause
* 14h50-16h00 – Séance {L} « Interfaces, écritures et design éditorial »
   * Christine Develotte et Mabrouka El Hachani, « L’alliance du papier et de la tablette numérique : les spécificités d’une nouvelle écriture de fiction jeunesse »
   * Florence Rio, Bertrand Meslier et Eric Kergosien, « Le "KIT de rédaction" pour livres interactifs numériques ou la reconfiguration de l’acte d’écriture »
   * Nathalie Lacelle, Marie-Christine Beaudry et Prune Lieutier, « Projet de soutien au développement de démarches d’édition numérique jeunesse au Québec »
   * Période de questions
* 16h00-17h00 – Table ronde {M} « Imaginer des structures d’accompagnement éditorial en contexte numérique »
* Martine Clouzot, « "Living Book" et Enluminures médiévales. Résultats d'expérimentation sur la création, l'édition collaboratives et la diffusion d'un "livre vivant" »
* Enrico Agostini-Marchese, Elsa Bouchard, Arthur Juchereau, Nicolas Sauret et Marcello Vitali-Rosati, « Une plateforme pour le livre anthologique à l'époque numérique. Le cas de l'Anthologie Palatine »
* Benoît Epron et Catherine Muller, « L'abécédaire des mondes lettrés, un outil d'écriture collaborative savante »
* Période de questions
* 14h50-16h10 – Séance {F} « Techniques de la fabrication et de la diffusion des livres en environnement numérique » \[SÉANCE PARALLÈLE\]
* Cécile Meynard et Elisabeth Greslou, « Du manuscrit au numérique : une édition multiformes et multisupports de Stendhal »
* Antoine Fauchié, « Git comme nouvel ingrédient des chaînes de publication ? »
* Période de questions
* Fabrice Marcoux, « La co-édition grâce à Booktype au B7 : mise à l’épreuve du livre numérique »
* Jean-Michel Gascuel, « Le livre de demain sera connecté et transmedia ou ne sera pas »
* Période de questions
* 16h10-17h00 – Table ronde {G} « Publier la recherche »
  
  
### Comité d’organisation

**René Audet** (CRILCQ, Université Laval)  
**Renée Bourassa** (CRIalt, Université Laval)  
**Oriane Deseilligny** (GRIPIC, Université de Paris-13)  
**Bertrand Gervais** (CRC arts et littératures numériques, UQAM)  
**Michael Sinatra** (CRIHN, Université de Montréal)  
**Stéphane Vial** (PROJEKT, Université de Nîmes)  
**Josée Vincent** (GRÉLQ, Université de Sherbrooke)  
**Marcello Vitali-Rosati** (CRC écritures numériques, Université de Montréal)  
  
Pour plus d'informations, consultez le [site](http://ecridil.ex-situ.info/) du colloque.
