---
title: The Publishing Sphere
short: <h2>Tuesday 13<sup>th</sup> June 2017 (10.30 am - 10 pm) and Friday 14<sup>th</sup> June 2017 (9:45 am - 10 pm)</h2>
cleanstring: The-Publishing-Sphere
lang: EN
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 3
  - 22

---
    
### Ecosystems of Contemporary Literatures
  
  
The realm of writing has never been so extensive; nor has the idea of publication ever been so plural. Not a day passes without a great percentage of humanity publishing one or numerous texts: on a blog, a social media network, or elsewhere. A two-day program of performative live publishing will celebrate this emerging sphere with talks and conversations, readings, sound performances, interventions, and more. Radio Brouhaha, launched at the event, will livestream \[voicerepublic.com\] the final evening, capturing sounds of ambiance and infrastructure.

The traditional idea of the solitary author in direct contact with his editor, and speaking in absentia to an anonymous public is obsolete. In recent years an abundance of literary practices – performances, public readings, sound and visual work and new public spaces– have emerged, forming a vibrant artistic and political “publishing sphere.” If it is true that the imaginary of modern literature is constitutive of the fantasy of a “good” public sphere of democracy then we must find what kind of societies are emerging from the publishing sphere we are faced with today. 

At The Publishing Sphere scholars, writers, artists, and representatives of initiatives will investigate the different locations of contemporary literatures between an abstract sphere and a material space. They explore what constitutes a literary work beyond the materiality of the book, expose other forms of publishing besides texts, and survey the agents and players who inhabit the field. Alternating between round table discussions, performative miniature elements, workshop reach-outs, and literary readings they will scrutinize their pre-reflections, research notes, image materials, literary references, to create a publishing sphere of their own.

Chair's holder **Marcello Vitali-Rosati** will take place at a roundtable with Miriam Rasch between 16:40 and 17:20 on June 13th and will offer a presentation called "Editorialization: Writing and Producing Space". 

_The Publishing Sphere_ is supported by **Haus der Kulturen der Welt**, **Université Paris Lumières**, the **Institut Universitaire de France** and the **Alexander von Humboldt Foundation** via the professorship of Irene Albers at Peter Szondi-Institut für Allgemeine und Vergleichende Literaturwissenschaft of the Freie Universität Berlin.
