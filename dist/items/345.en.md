---
title: <span>Marcello Vitali-Rosati et Servanne Monjour, « Littérature et production de l’espace à l’ère numérique. L’éditorialisation de la Transcanadienne. Du spatial turn à Google maps », <i>@nalyses</i>, vol. 12 / 3, août 2017, p. 198‑229.</span>
short: "À l’heure où l’espace que nous habitons est de plus en plus façonné par les outils numériques, pouvons-nous le façonner en l’éditorialisant ? La littérature peut-elle constituer un outil de production de l’espace ? Peut-elle nous permettre de nous réapproprier les lieux et les territoires en apparence dépossédés de toute valeur littéraire par les géants de l'information ? Pour le savoir, l’équipe de la Chaire de recherche du Canada sur les Écritures numériques a mis en place en 2015 un projet de recherche-action le long de l'autoroute transcanadienne. Cette route mythique qui traverse le Canada d’un océan à l’autre a en effet donné lieu à une large série de productions médiatiques : des images, des vidéos, des cartes, des textes d’histoire, des données numériques, mais aussi des récits littéraires. C’est ainsi que des infrastructures comme l’autoroute, les motels, se mêlent au discours et à l’imaginaire pour construire l'espace. Afin d’étudier cet espace hybride, nous avons entrepris de sillonner nous-mêmes l’autoroute transcanadienne dans un voyage qui nous a menés de Montréal à Calgary. Nous avons rendu compte en temps réel de notre road-trip sur différents réseaux sociaux, de manière à comprendre comment la littérature participe à la production de l'espace à l'ère du numérique en proposant différentes stratégies d'éditorialisation."
cleanstring: spanMarcello-Vitali-Rosati-et-Servanne-Monjour-Litterature-et-production-de-lespace-a-lere-numerique-Leditorialisation-de-la-Transcanadienne-Du-spatial-turn-a-Google-maps-inalysesi-volonetwothree-aout-twozerooneseven-ponenineeighttwotwoninespan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/19177
urlTitle: ""
categories:
  - 54
  - 41
  - 22
  - 39
  - 25
  - 18
  - 13
  - 12
  - 4
  - 19

---
    
<!-- pas de contenu sur cette page -->
