---
title: "<span>Thomas Carrier-Lafleur, « Le mystère Littérature : la vocation « marginale » chez Foucault, Proust et Cendrars », <i>Cygne noir. Revue d’exploration sémiotique</i>, 2015.</span>"
short: Le présent article sera divisé en deux temps, qui communiquent dans la mesure où ils développent le même problème à partir de deux points de vue connexes. Premièrement, il sera question de la pensée littéraire que déploie l’entreprise archéologique du « premier » Michel Foucault (soit du milieu des années 1950 jusqu’à la publication de Les mots et les choses en 1966). L’intérêt de cette pensée réside en sa tentative de rendre simultanée autant une archéologie du savoir qu’une archéologie littéraire, dans la mesure où les œuvres de la littérature, plutôt que de reproduire « l’état des signes », viennent y ajouter un incommensurable mystère. On trouve ainsi chez Foucault l’idée que la littérature à l’ère de la modernité représente un mystérieux appel du « dehors » – c’est-à-dire de l’être pur du langage, lieu aussi paradoxal qu’insituable –, que l’écrivain devra instaurer au sein de sa propre existence. Dans un deuxième temps, seront analysées à titre d’illustrations critiques les entreprises littéraires contraires mais complémentaires de Proust et de Cendrars, en ce qu’elles permettront de mettre en relief ce « mystère » dont la littérature moderne est devenue le signe.
cleanstring: spanThomas-Carrier-Lafleur-Le-mystere-Litterature-la-vocation-marginale-chez-Foucault-Proust-et-Cendrars-iCygne-noir-Revue-dexploration-semiotiquei-twozeroonefivespan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/18986
urlTitle: ""
categories:
  - 54
  - 41
  - 66

---
    
<!-- pas de contenu sur cette page -->
