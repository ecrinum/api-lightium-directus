---
title: "<span>Thomas Carrier-Lafleur, « Le roman d’apprentissage à l’ère de la modernité : Foucault, Pirandello, Aragon et le spectacle du siècle », <i>@nalyses</i>, vol. 10 / 1, 2015, p. 240‑274.</span>"
short: Au carrefour des discours philosophiques, historiques et fictionnels sur la modernité, cet article entend arpenter la question – essentiellement littéraire – du regard de la contemporanéité, telle que posée par l’entreprise archéologique de Michel Foucault et récemment reprise par Giorgio Agamben. Après une démonstration qui soulèvera directement les diverses problématiques de cette question, seront étudiées leurs remédiations romanesques, grâce à une analyse comparée des romans d’apprentissage de la posture contemporaine que sont On tourne (1915) de Luigi Pirandello et Anicet ou le Panorama, roman (1921) de Louis Aragon. On verra en quoi la réflexion formelle sur la littérature en tant que question posée à la modernité est inséparable d’une réflexion théorique et historique sur ses conditions et ses effets.
cleanstring: spanThomas-Carrier-Lafleur-Le-roman-dapprentissage-a-lere-de-la-modernite-Foucault-Pirandello-Aragon-et-le-spectacle-du-siecle-inalysesi-volonezeroone-twozeroonefive-ptwoforzerotwosevenforspan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/18984
urlTitle: ""
categories:
  - 54
  - 41
  - 66

---
    
<!-- pas de contenu sur cette page -->
