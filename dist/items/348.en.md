---
title: <span>Thomas Carrier-Lafleur, « Virtualités de Blaise Cendrars. La référence cinématographique à l’œuvre dans l’écriture du reportage », <i>Trans-. Revue de littérature générale et comparée</i>, octobre 2014.</span>
short: S’intéressant essentiellement à sa production romanesque et à son activité journalistique, le présent article aborde la question de la référence cinématographique dans la vie et l’œuvre de Blaise Cendrars. À partir de Baudelaire et du « Peintre de la vie moderne », il sera d’abord question de la nature de l’artiste et de la figure du reporter à l’ère de la modernité des images mécaniques. Cette réflexion permettra ensuite de juger de la postérité du personnage baudelairien chez Cendrars, avec Dan Yack, son roman le plus énigmatique. Finalement, il sera montré en quoi le grand reportage de 1936 Hollywood, la Mecque du cinéma peut être lu comme un métareportage qui synthétise tous ces enjeux. C’est dans ce texte que Cendrars livre sa vision la plus audacieuse de l’art et de l’industrie cinématographiques, et que cette vision peut également servir de clé de lecture générale pour l’hétérogénéité de son œuvre littéraire.
cleanstring: spanThomas-Carrier-Lafleur-Virtualites-de-Blaise-Cendrars-La-reference-cinematographique-a-luvre-dans-lecriture-du-reportage-iTrans--Revue-de-litterature-generale-et-compareei-octobre-twozerooneforspan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/18981
urlTitle: ""
categories:
  - 11
  - 41
  - 54
  - 66

---
    
<!-- pas de contenu sur cette page -->
