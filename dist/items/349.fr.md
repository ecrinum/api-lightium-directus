---
title: <span>Thomas Carrier-Lafleur, « “Le regard de la science”. Retour sur la métaphore cinématographique dans le quatrième chapitre de L’Évolution créatrice », <i>Implications philosophiques</i>, décembre 2013.</span>
short: "Le présent article entend défendre l’hypothèse selon laquelle, pour bien comprendre les rapports entre la philosophie de Bergson et la science, il est nécessaire de penser à nouveaux frais la métaphore du cinématographe qui rythme et qui structure le quatrième et dernier chapitre de L’Évolution créatrice, « Le mécanisme cinématographique de la pensée et l’illusion mécanistique. Coup d’œil sur l’histoire des systèmes : le devenir réel et le faux évolutionnisme. »"
cleanstring: spanThomas-Carrier-Lafleur-Le-regard-de-la-science-Retour-sur-la-metaphore-cinematographique-dans-le-quatrieme-chapitre-de-LEvolution-creatrice-iImplications-philosophiquesi-decembre-twozeroonethreespan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/18980
urlTitle: ""
categories:
  - 41
  - 54
  - 66

---
    
<!-- pas de contenu sur cette page -->
