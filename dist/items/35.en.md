---
title: <span>Servanne Monjour, « La victoire du lobby gallinacé, ou les enjeux de la révolution numérique dans l’oeuvre photofictionnelle de Joan Fontcuberta », <i>Aletria</i>, vol. 24 / 2, 2014, p. 221‑230.</span>
short: "Face à l’émergence de la culture numérique, le marché de l’image subit depuis plusieurs années une série de mutations : obsolescence de l’argentique, difficultés des services photos de la presse écrite. Quel écho donnent les artistes, photographes ou écrivains, à cet état de crise avant tout économique du fait photographique? Assisterait-on vraiment à la mort du photographique? Cet article propose de mesurer l’impact de la révolution numérique à travers le prisme des photofictions de Joan Fontcuberta. Rejetant aussi bien les interprétations téléologiques qu’apocalyptiques de cette transition technologique, Joan Fontcuberta prend acte d’une désindexation du fait photographique, et signe une oeuvre résolument photolittéraire."
cleanstring: spanServanne-Monjour-La-victoire-du-lobby-gallinace-ou-les-enjeux-de-la-revolution-numerique-dans-loeuvre-photofictionnelle-de-Joan-Fontcuberta-iAletriai-voltwofortwo-twozeroonefor-ptwotwoonetwothreezerospan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13109
urlTitle: ""
categories:
  - 54
  - 41
  - 39
  - 12
  - 7

---
    
<!-- pas de contenu sur cette page -->
