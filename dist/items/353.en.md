---
title: <span>Thomas Carrier-Lafleur et Michaël Di Vita, « Laboratoire fictionnel en vue d’une philosophie romanesque de la nature. Ressouvenir proustien et reprise kierkegaardienne », <i>Klesis. Revue philosophique</i>, 2013, p. 51‑85.</span>
short: "Comment penser une philosophie de la nature en la rapportant à un individu existant ? Voilà le problème de ce texte. Mais d’abord, qu’est-ce qu’une philosophie de la nature ? Prenons l’exemple du jeune Schelling, celui de l’Introduction à l’Esquisse d’un système de philosophie de la nature : la philosophie de la nature s’y présente comme une «science nécessaire du système du savoir», une «physique, mais une physique spéculative» dont l’objet n’est pas tant l’organisation des corps tels que nous les percevons que les principes a priori que ces corps supposent."
cleanstring: spanThomas-Carrier-Lafleur-et-Michael-Di-Vita-Laboratoire-fictionnel-en-vue-dune-philosophie-romanesque-de-la-nature-Ressouvenir-proustien-et-reprise-kierkegaardienne-iKlesis-Revue-philosophiquei-twozeroonethree-pfiveoneeightfivespan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/18976
urlTitle: ""
categories:
  - 41
  - 54
  - 66

---
    
<!-- pas de contenu sur cette page -->
