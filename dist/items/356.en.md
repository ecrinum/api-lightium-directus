---
title: <span>Élisabeth Routhier, « Performativité et éthique de la remédiation dans Dora Bruder, de Patrick Modiano », <i>Itinéraires</i>, avril 2017.</span>
short: La disparition est-elle un événement ? Une fin ? Une figure ? Chez l’écrivain français Patrick Modiano, il semble qu’elle prenne plutôt la forme d’une invitation, d’un point de départ pour une écriture-quête se faufilant dans les mailles de la médiation. Le lien inextricable entre la disparition et la médiation est effectivement à la base de cette analyse de Dora Bruder, roman dont l’ouverture est particulièrement riche pour penser le geste d’écriture d’un point de vue intermédial. Après avoir présenté Dora Bruder comme l’espace d’une écriture-quête performative qui répond à un désir d’immédiateté, j’exposerai certains dispositifs traduisant une éthique de la remédiation. La question de la mémoire, inévitable, traverse à la fois l’écriture de Modiano et la mienne. Une mémoire protéiforme et dynamisée par l’auteur-narrateur.
cleanstring: spanElisabeth-Routhier-Performativite-et-ethique-de-la-remediation-dans-Dora-Bruder-de-Patrick-Modiano-iItinerairesi-avril-twozeroonesevenspan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/18973
urlTitle: ""
categories:
  - 54
  - 41
  - 46
  - 33
  - 12
  - 4
  - 7

---
    
<!-- pas de contenu sur cette page -->
