---
title: <span>Thomas Carrier-Lafleur, « Introspection créatrice et comédie humaine. Proust, Balzac et Bergson », <i>@nalyses</i>, vol. 6 / 3, 2011, p. 235‑277.</span>
short: En analysant la place que prend Honoré de Balzac dans l’oeuvre proustienne, cet article souhaite établir une comparaison stylistique entre l’auteur de la Comédie humaine et celui d’À la recherche du temps perdu. Le roman de Marcel Proust est riche des enseignements de l’entreprise balzacienne, ce qui ne veut pas dire qu’il ne tentera pas de la dépasser, au contraire. À l’aide du philosophe Henri Bergson, particulièrement avec son ouvrage Le Rire, sera ainsi expliquée la différence esthétique, voire poétique, entre les écrits de Balzac et ceux de Proust, le second reprenant le grand projet réaliste du premier pour le réfracter dans l’introspection créatrice de son héros-narrateur, ce qui fait de la Recherche une comédie humaine intérieure.
cleanstring: spanThomas-Carrier-Lafleur-Introspection-creatrice-et-comedie-humaine-Proust-Balzac-et-Bergson-inalysesi-volsixthree-twozerooneone-ptwothreefivetwosevensevenspan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/18972
urlTitle: ""
categories:
  - 54
  - 41
  - 66

---
    
<!-- pas de contenu sur cette page -->
