---
title: <span>Thomas Carrier-Lafleur, « Imaginaire médiatique et dynamique du regard dans l’œuvre proustienne », <i>@nalyses</i>, vol. 6 / 2, 2011, p. 233‑269.</span>
short: "Cet article propose d’analyser deux aspects majeurs, et pourtant méconnus, d’À la recherche du temps perdu : d’une part, celui d’« imaginaire médiatique », d’autre part, celui de « dynamique du regard ». Tous deux sont propres au XIXe siècle français, espace-temps d’inventions majeures pour notre modernité culturelle et artistique. Le texte proustien, un pied dans le XIEe siècle et l’autre dans le XXe, apparaît ainsi comme un catalyseur et comme un passeur. Le « temps retrouvé » de la Recherche, c’est aussi celui d’un XIXe siècle rendu sensible par le roman, médiatisé par l’œuvre. Le déploiement et la floraison de ces deux thématiques (la première questionnant la problématique de la mondanité et l’autre celle de l’imaginaire de l’œil et de la vision) seront relevés de façon générale dans la Recherche, puis on proposera deux études de cas ― sur le journal et sur la photographie ― qui viendront les illustrer."
cleanstring: spanThomas-Carrier-Lafleur-Imaginaire-mediatique-et-dynamique-du-regard-dans-luvre-proustienne-inalysesi-volsixtwo-twozerooneone-ptwothreethreetwosixninespan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/18971
urlTitle: ""
categories:
  - 54
  - 41
  - 66

---
    
<!-- pas de contenu sur cette page -->
