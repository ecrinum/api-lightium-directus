---
title: <span>Thomas Carrier-Lafleur, « Le rôle des déceptions dans À la recherche du temps perdu de Marcel Proust. Autofiction, crise du sujet et montage identitaire », <i>Études littéraires</i>, vol. 42 / 2, 2011, p. 139‑159.</span>
short: À la recherche du temps perdu raconte l’apprentissage du narrateur proustien, futur homme de lettres. Un parcours ponctué par d’innombrables déceptions pousse le héros vers le nihilisme. Ce sera ainsi jusqu’à la fin du Temps retrouvé. Cet enjeu des déceptions proustiennes sera d’abord étudié à travers le prisme de la formation littéraire du personnage, puis sous le signe de l’autofiction. C’est que les déceptions font tomber le narrateur dans une crise du sujet de laquelle il ne pourra sortir qu’en attribuant une nouvelle fonction de l’oeuvre littéraire, fonction qui sera nommée, quelques décennies plus tard, « autofiction ». Ce que l’on pourrait alors appeler « l’autofiction proustienne » pousse également l’auteur à user d’un autre concept que nous désignons par le terme de « montage identitaire », suivant une expression de Sophie-Jan Arrien et Jean-Pierre Sirois-Trahan, le « montage des identités », et à partir de la récente étude d’Anne Henry, La tentation de Marcel Proust.
cleanstring: spanThomas-Carrier-Lafleur-Le-role-des-deceptions-dans-A-la-recherche-du-temps-perdu-de-Marcel-Proust-Autofiction-crise-du-sujet-et-montage-identitaire-iEtudes-litterairesi-volfortwotwo-twozerooneone-ponethreenineonefiveninespan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/18970
urlTitle: ""
categories:
  - 54
  - 41
  - 66

---
    
<!-- pas de contenu sur cette page -->
