---
title: <span>Thomas Carrier-Lafleur, « Regards de la modernité. Simulacre et négativité dans Les Carnets de Malte Laurids Brigge et On tourne », <i>@nalyses</i>, vol. 11 / 3, septembre 2016, p. 163‑182.</span>
short: "Cette étude entend proposer une lecture comparative de deux romans européens, chronologiquement et thématiquement voisins, que l’on associe au mouvement général du modernisme littéraire : Les Carnets de Malte Laurids Brigge (1910) de Rainer Maria Rilke et On tourne (1915) de Luigi Pirandello. Il s’agira essentiellement de montrer en quoi ces textes donnent à voir le monde moderne à travers une série d’images marginales, qui s’agencent et se modulent alors que les personnages du récit progressent vers leur inévitable fin. Plus particulièrement, en portant notre attention sur les représentations de la vie parisienne chez Rilke et du fonctionnement d’un grand studio de cinéma chez Pirandello, nous verrons comment ces romans tentent d’instaurer une critique interne du progrès et de ses avatars."
cleanstring: spanThomas-Carrier-Lafleur-Regards-de-la-modernite-Simulacre-et-negativite-dans-Les-Carnets-de-Malte-Laurids-Brigge-et-On-tourne-inalysesi-voloneonethree-septembre-twozeroonesix-ponesixthreeoneeighttwospan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/19176
urlTitle: ""
categories:
  - 54
  - 41
  - 66

---
    
<!-- pas de contenu sur cette page -->
