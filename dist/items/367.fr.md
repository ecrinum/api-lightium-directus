---
title: <span>Thomas Carrier-Lafleur, « Opening Night et la remédiation théâtrale. « Film-balade » ou film « tragique » ? », <i>Études littéraires</i>, vol. 45 / 3, 2014, p. 43‑63.</span>
short: S’inspirant à la fois de la critique deleuzienne du « nouvel Hollywood » et de la dialectique métaphysique de l’apollinien et du dionysiaque telle que pensée par Nietzsche dans La Naissance de la tragédie, nous souhaitons interroger ici quelques-uns des modes sur lesquels le théâtre habite le cinéma. Par suite, afin de déplacer légèrement l’angle sous lequel on envisage habituellement l’adaptation et pour profiter ainsi d’un changement de point de vue, nous préfèrerons à la problématique du théâtre filmé le concept de « filmer le théâtre ». Nous explorerons celui-ci à partir d’une analyse du film Opening Night de John Cassavetes (1977), où la crise d’une actrice qui se trouve entre deux âges constitue, pour le réalisateur, et donc pour le septième art, une occasion de produire de la nouveauté théâtrale.
cleanstring: spanThomas-Carrier-Lafleur-Opening-Night-et-la-remediation-theatrale-Film-balade-ou-film-tragique-iEtudes-litterairesi-volforfivethree-twozeroonefor-pforthreesixthreespan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/19173
urlTitle: ""
categories:
  - 54
  - 41
  - 66
  - 33
  - 7

---
    
<!-- pas de contenu sur cette page -->
