---
title: <span>Thomas Carrier-Lafleur et Guillaume Lavoie, « La réflexion médiatique dans Kid Sentiment de Jacques Godbout », <i>Nouvelles vues. Revue sur les pratiques et les théories du cinéma au Québec</i>, 2015.</span>
short: Réalisé en 1967 et sorti en 1968, Kid Sentiment propose une réflexion dont le sujet est au moins double. D’un côté, il s’agit d’enquêter sur la « jeunesse d’aujourd’hui », grâce à la participation de deux membres du groupe rock Les Sinners, François Guy et Louis Parizeau, accompagnés de leurs deux amies de cœur, Andrée Cousineau et Michèle Mercure, quatre jeunes gens qui pour l’occasion incarnent des adolescents très « à gogo » en pleine découverte de la carte de Tendre. Par un maillage très serré de modes de communications, dont le présent article proposera l’analyse structurale, Godbout explore les différentes situations et thématiques constitutives de l’imaginaire de cette jeunesse « gogo » ou « yéyé ». D’un autre côté, ce treizième film sert de laboratoire réflexif pour le réalisateur, dans la mesure où à la question symbolique « qu’est-ce que la jeunesse en 1967-68? » s’ajoute une réflexion formelle sur le statut du cinéma direct, dont Kid Sentiment représente une manifestation tardive, et en cela maniériste.
cleanstring: spanThomas-Carrier-Lafleur-et-Guillaume-Lavoie-La-reflexion-mediatique-dans-Kid-Sentiment-de-Jacques-Godbout-iNouvelles-vues-Revue-sur-les-pratiques-et-les-theories-du-cinema-au-Quebeci-twozeroonefivespan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/18998
urlTitle: ""
categories:
  - 54
  - 41
  - 66
  - 7

---
    
<!-- pas de contenu sur cette page -->
