---
title: <span>Thomas Carrier-Lafleur et David Bélanger, « Le café ou le néant. Enquête sur l’absurde dans le café romanesque français des années 1930-1950 », <i>Implications philosophiques</i>, juillet 2015.</span>
short: "À travers la thématique de la représentation littéraire du concept philosophique de l’absurde, ou, pour le dire autrement, le thème de la minceur de l’existence, le présent article souhaite faire la lumière sur un sujet majeur du roman moderne de la première moitié du vingtième siècle : celui des cafés. Comme nous l’apprend Roquentin dans La Nausée ou Sartre dans L’Être et le néant, le café est un lieu où l’existence se remarque, se pèse et s’étudie. C’est ainsi que nous nous intéresserons à l’opposition entre le café, lieu en apparence fixe et routinier, et la rue, pleine d’agitation et de renversements. C’est grâce à ce jeu entre le repos et la vitesse propre à la dialectique du café et de la rue que le roman est en mesure de réfléchir l’existence moderne et, par conséquent, la sienne. Cette enquête sera menée à travers la lecture de deux romans qui ont été choisis pour leur complémentarité temporelle et thématique : d’une part, Le Chiendent (1933) de Raymond Queneau et, d’autre part, Les Gommes (1953) d’Alain Robbe-Grillet."
cleanstring: spanThomas-Carrier-Lafleur-et-David-Belanger-Le-cafe-ou-le-neant-Enquete-sur-labsurde-dans-le-cafe-romanesque-francais-des-annees-oneninethreezero-oneninefivezero-iImplications-philosophiquesi-juillet-twozeroonefivespan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/18997
urlTitle: ""
categories:
  - 54
  - 41
  - 66

---
    
<!-- pas de contenu sur cette page -->
