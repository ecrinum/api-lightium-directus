---
title: <span>Marcello Vitali-Rosati, « Le train est l’ancêtre d’Internet », <i>Institut national audiovisuel</i>, 2012.</span>
short: "Nous sommes fascinés par le train et le cinéma, fascination provoquée parce tous les deux donnent l’impression du mouvement réel, un mouvement technique qu’on est capable de gérer : lorsqu’on regarde un film, on peut toujours faire un arrêt sur image. L’approche philosophique nous permet aussi de relier Internet dans ce mouvement qui va du train au cinéma. Le Web est un flux de données, ses contenus ne sont jamais stables à l’opposé des contenus des autres médias. Et ce qui nous passionne dans le numérique et fait la force d’Internet, c’est qu’il nous donne l’illusion du réel et que nous pouvons facilement le gérer. Cela pose le problème de la mission de créer des archives du Web, qui peut sembler irréaliste tant le matériel concerné est vaste et non structuré : ne serait-ce pas trahir ce mouvement perpétuel ? Ou alors, on peut considérer que le Web est une trahison du continu du réel et les archives la structure la plus appropriée pour appréhender ce nouveau média…"
cleanstring: spanMarcello-Vitali-Rosati-Le-train-est-lancetre-dInternet-iInstitut-national-audiovisueli-twozeroonetwospan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/12974
urlTitle: ""
categories:
  - 54
  - 41
  - 22
  - 5
  - 26
  - 12
  - 7
  - 27

---
    
<!-- pas de contenu sur cette page -->
