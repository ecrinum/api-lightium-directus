---
title: <span>Thomas Carrier-Lafleur, « La Littérature en marge du cinéma ou la négation cinématographique », in Jean Cléder, Frank Wagner, (éds.). <i>Le cinéma de la littérature</i>, éds. Jean Cléder et Frank Wagner, Lormont, Éditions nouvelles Cécile Defaut, 2017, p. 93‑117.</span>
short: ""
cleanstring: spanThomas-Carrier-Lafleur-La-Litterature-en-marge-du-cinema-ou-la-negation-cinematographique-inJean-Cleder-Frank-Wagner-eds-iLe-cinema-de-la-litteraturei-eds-Jean-Cleder-et-Frank-Wagner-Lormont-Editions-nouvelles-Cecile-Defaut-twozerooneseven-pninethreeoneonesevenspan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/19178
urlTitle: ""
categories:
  - 54
  - 42
  - 66

---
    
<!-- pas de contenu sur cette page -->
