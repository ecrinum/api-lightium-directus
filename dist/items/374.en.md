---
title: <span>Servanne Monjour, « L’écrivain de profil(s)... Facebook. Réflexion autour d'une photographie de profil de Victoria Welby », in David Martens. , Jean-Pierre Montier. , Anne Reverseau. <i>L’écrivain vu par la photographie. Formes, usages, enjeux</i>, Rennes, Presses universitaires de Rennes, 2017, p. 219‑228.</span>
short: Opérant la distinction entre l’auteur littéraire et la fonction auteur à laquelle peut désormais prétendre n’importe quel usager de l’outil numérique, Étienne Candel et Gustavo Gomez-Mejia suggèrent qu’« au-delà de la compétence technique de l’auteur, la valeur littéraire relèverait en particulier des connotations attribuées au prestige d’une technologie ou d’une marque à laquelle il associe son nom. Dans ce cadre, l’œuvre littéraire apparaîtrait comme indissociable de la strate des discours tenus à son sujet comme production technique[ref.] ». En d’autres termes, afin d’affirmer la littérarité de son travail d’écriture numérique, l’auteur aurait tout intérêt à s’associer explicitement à la célèbre marque à la pomme plutôt qu’à une autre enseigne de moindre prestige. Car « ce n’est pas tant “lire” qui compte que “lire sur iPhone”, ni tant “écrire” que “écrire sous Java”2 ». Devant un tel constat, sans doute aussi regrettable que problématique (comment évaluer, notamment, ce critère de notoriété? Quelle en serait la pérennité?), notre attention sera immédiatement attirée par une annonce originale...
cleanstring: spanServanne-Monjour-Lecrivain-de-profils-Facebook-Reflexion-autour-dune-photographie-de-profil-de-Victoria-Welby-inDavid-Martens--Jean-Pierre-Montier--Anne-Reverseau-iLecrivain-vu-par-la-photographie-Formes-usages-enjeuxi-Rennes-Presses-universitaires-de-Rennes-twozerooneseven-ptwooneninetwotwoeightspan
lang: EN
caption: ""
image: ""
url: http://hdl.handle.net/1866/18302
urlTitle: ""
categories:
  - 54
  - 42
  - 39
  - 18
  - 10
  - 19

---
    
<!-- pas de contenu sur cette page -->
