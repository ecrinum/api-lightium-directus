---
title: <span>Marcello Vitali-Rosati, « La virtualité d’Internet. Une tentative d’éclaircissement terminologique », <i>Sens Public</i>, avril 2009.</span>
short: Depuis plusieurs années le mot « virtuel » est utilisé pour caractériser des pratiques quotidiennes liées à l’emploi des nouvelles technologies et en particulier d’internet. Mais qu’est-ce qui est virtuel en internet ? Pour répondre à cette question il faut d’abord remonter aux racines philosophiques de ce mot et essayer d’éclaircir sa signification. C’est la tentative proposée dans cette article.
cleanstring: spanMarcello-Vitali-Rosati-La-virtualite-dInternet-Une-tentative-declaircissement-terminologique-iSens-Publici-avril-twozerozeroninespan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/12975
urlTitle: ""
categories:
  - 54
  - 41
  - 22
  - 18
  - 7
  - 27

---
    
<!-- pas de contenu sur cette page -->
