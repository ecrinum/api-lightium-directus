---
title: Écritures numériques et éditorialisation - Cycle 2017-2018
short: <h4>Environnement, milieu, espace et architecture</h4>
cleanstring: Ecritures-numeriques-et-editorialisation---Cycle-twozerooneseven-twozerooneeight
lang: FR
caption: ""
image: http://seminaire.sens-public.org/local/cache-vignettes/L150xH197/rubon14-1bdf6.png
url: ""
urlTitle: ""
categories:
  - 3
  - 19
  - 22
  - 52

---
    
[ Inscription au séminaire « Ecritures numériques et éditorialisation » (cycle 2017-18)](http://seminaire.sens-public.org/spip.php?breve16)   

Rendez-vous : salle C-1017-11 (Lionel Groulx) 
  
  
Les précédents cycles du séminaire se sont attachés à articuler la notion d’éditorialisation avec différentes pratiques de l’édition numérique, en questionnant tant la production et la diffusion des savoirs (archive, bibliothèque, édition savante) que le journalisme ou les grandes plateformes du web.  
En éclairant de manière récurrente les liens entre espace, environnement et éditorialisation, ces séances sont venues interroger les travaux récents de définition et de conceptualisation de l’éditorialisation. Pour Marcello Vitali-Rosati, « l’éditorialisation désigne l’ensemble des dynamiques qui produisent et structurent l’espace numérique. Ces dynamiques sont les interactions des actions individuelles et collectives avec un environnement numérique particulier. ». Dans cette définition, l’éditorialisation participe à la structuration de l’espace numérique et dès lors aux conditions même de la construction de connaissances, rejoignant en quelque sorte ce que Jeffrey Schnapp a conceptualisé avec le _knowledge design_.  
Louise Merzeau a montré de son côté le lien entre les dispositifs d’éditorialisation et l’arrangement de milieux transitionnels, qu’elle associe à un environnement-support, et au milieu transmédiatique.

« Outre qu’il réintroduit du temps sédimenté dans le geste collaboratif, cet « étoilement applicatif » met en évidence la structure transmédiatique du milieu dans lequel les formes de la participation sont désormais appelées à s’exercer. \[...\] Entre média et boîte à outils, cet « environnement-support » (Zacklad, 2013) marque l’avènement de pratiques d’écriture-lecture spécifiquement numériques, où nous voyons la marque d’une culture digitale en train de se constituer. »

En l’articulant avec les notions de milieu, d’espace, de territoire, d’écosystème, nous chercherons à déterminer en quoi l’éditorialisation permet de mieux comprendre cette « culture digitale ».

Notre exploration théorique investira ainsi deux champs d’étude :

1. environnement, écosystème, milieu
2. territoire, espace, architecture

Nous souhaitons proposer une formule en ateliers, invitant les participants au séminaire à un travail proactif d’annotation collaborative autour d’un corpus de textes récents. Il s’agira de mettre ces textes en perspective avec la théorie de l’éditorialisation et d’inscrire celle-ci dans une pensée du milieu et de l’espace « comme une perspective possible pour une refondation du collectif à l’ère des réseaux ».  
Nous nous emploierons en particulier à éclairer l’œuvre que nous a laissé Louise Merzeau et à poursuivre ainsi la contribution toujours lumineuse et inspirante de notre collègue. Chaque champ sera clôturé par une séance de conférence.

### Calendrier

Les séances se dérouleront en duplex au CNAM de 17h30 à 19h30 et à l’Université de Montréal de 11h30 à 13h30, aux dates suivantes :

### Champ environnement, écosystème, milieu

**13 novembre 2017** : atelier 1  
Organisateurs : _Nicolas Sauret_ & _Gérard Wormser_   
  
  
**4 décembre 2017** : atelier 2  
Organisateurs : _Servanne Monjour_ & _Matteo Treleani_   
  
  
**23 janvier 2018** : atelier 3  
Organisateurs : _Chloé Girard_ & _Jérôme Valluy_   
  
  
**13 mars 2018** : conférence de _Thierry Bardini_  
**Pour une médiologie critique : Prémisses d’une archéologie médiatique du temps réel**  
  
  
Cette contribution propose d’articuler une médiologie critique autour de trois concepts centraux : mode/modalité/modulation, récursivité, et cause formelle. Cependant, le sens de « critique » mobilisé ici ne renvoie pas exclusivement à celui associé aux travaux fondateurs de l’école de Francfort. En fait, il s’agit plutôt de référer le sens que la thermodynamique donne à ce terme : pour celle-ci, un état critique correspond à une transition de phase de deuxième ordre, où, au point critique, on ne peut distinguer les deux phases du corps affecté par la transition de phase (Bardini 2014). L’introduction de ce vocabulaire hérité de la physique suit les intuitions de Gilbert Simondon (1958, 2005), qu’il s’agira ici de mettre à jour.

Ceci consiste à réévaluer et à compléter son modèle ontogénétique à la lueur de phénomènes contemporains dont il ne pouvait certes pas avoir conscience. Il s’agit en particulier de se demander dans quelle mesure l’écologie médiatique contemporaine nécessite sa mise à jour par l’introduction de deux modes d’existence distincts, mais de plus en plus miscibles, pour qualifier les formes de vie (individuations vitales, psychiques et collectives) : les modes d’existence analogues et numériques. L’état critique de l’écologie médiatique contemporaine qui me préoccupe particulièrement, c’est ce moment où, justement, les phases ou modalités analogiques et numériques de nos existences se mêlent et se confondent en une seule réalité. Alors que nous pensons encore la réalité comme susceptible d’être « virtuelle » ou « augmentée » par les moyens technologiques d’une panoplie de media informatisés, il me semble crucial de nous interroger sur l’espace-temps particulier et sur les singuliers entrelacs de présence et d’absence, que ces appareils médiatiques rendent possibles.

### Champ territoire, espace, architecture

**10 avril 2018** : atelier 4  
Organisateurs : _Emmanuel Château-Dutier_ & _Enrico Agostini Marchese_   
  
**15 mai 2018** : atelier 5  
Organisateurs : _Marcello Vitali-Rosati_ & _Évelyne Broudoux_   
  
**5 juin 2018** : conférence _Lionel Ruffel_ 

### Protocole des ateliers

Les ateliers s’organisent autour de la lecture de un ou deux textes de références (ou enregistrement vidéo de communication). L’œuvre de Louise Merzeau fera l’objet d’une attention particulière pour être intégrée au corpus d’étude.  
Les participants seront invités à annoter les textes en amont de la séance à l’aide de l’outil d’annotation en ligne Hypothes.is. L’objectif de ces annotations sera d’identifier les éléments clés du texte en lien avec la problématique et d’initier un débat parmi les participants.  
Les responsables de séance auront la charge de synthétiser les débats pour proposer en début de séance des pistes de discussion.  
Un document en ligne sera également à disposition des participants pour une prise de note collaborative pendant et en amont de la séance.  
Les ateliers donneront lieu à la production de différents textes : minutes des prises de paroles, synthèses des discussions, notices de notions théoriques. Ces textes sur wiki, préparés par les responsables de séance, sont destinés à être librement amendés et enrichis par les participants.

**Organisateurs** : Nicolas Sauret, Marcello Vitali-Rosati, Marta Severo, Evelyne Broudoux  
  
Le séminaire est organisé conjointement par la Chaire de Recherche du Canada sur les écritures numériques et par le Dicen-IDF (thématique « Éditorialisation, Documentarisation, Traçabilité »).
