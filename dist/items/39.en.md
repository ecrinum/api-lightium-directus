---
title: "<span>Marcello Vitali-Rosati, « L’ambiguïté politique d'Internet. Lecture de « La démocratie Internet : Promesses et limites » de Dominique Cardon », <i>Sens Public</i>, juin 2011.</span>"
short: Dans son dernier livre, Dominique Cardon analyse de quelle manière Internet change les structures de la démocratie participative. Internet ne favorise pas une vision politique plutôt qu’une autre, mais bien plutôt bouleverse les formes mêmes du politique. En clarifiant plusieurs aspects de l’Internet et en articulant une série complexe et hétérogène de pratiques nouvelles, l’auteur prend ainsi en compte la valeur procédurale des pratiques liées à Internet pour comprendre comment les usages ont déterminé des nouvelles formes de participation.
cleanstring: spanMarcello-Vitali-Rosati-Lambiguite-politique-dInternet-Lecture-de-La-democratie-Internet-Promesses-et-limites-de-Dominique-Cardon-iSens-Publici-juin-twozerooneonespan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/12976
urlTitle: ""
categories:
  - 54
  - 41
  - 22
  - 5
  - 29
  - 25
  - 19

---
    
<!-- pas de contenu sur cette page -->
