---
title: " International Journal of Photoliterature first release"
short: The international journal of photoliterature is launched in 2017 with a first issue dedicated to the range of photoliterary poetics elements
cleanstring: -International-Journal-of-Photoliterature-first-release
lang: EN
caption: ""
image: http://phlit.org/press/wp-content/uploads/2016/09/talbot1-2.jpeg
url: ""
urlTitle: ""
categories:
  - 3
  - 22
  - 39

---
    
<!-- pas de contenu sur cette page -->
