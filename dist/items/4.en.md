---
title: "Colloquium "
short: "Colloque international\r

  <br>Les 24 et 25 mai à l’Université de Montréal\r

  <br>sous la direction de Bertrand Gervais, Servanne Monjour, Jean-François Thériault, Marcello Vitali-Rosati\r

  <br>Consulter le <a href=\"http://colloque2016.ecrituresnumeriques.ca\">Site Web du colloque</a>"
cleanstring: Colloquium-
lang: EN
caption: ""
image: http://vitalirosati.net/chaire/img/colloqueecrivains.png
url: http://colloque2016.ecrituresnumeriques.ca
urlTitle: Website
categories:
  - 3
  - 5
  - 9
  - 10
  - 11
  - 12
  - 16
  - 18
  - 19
  - 21
  - 22
  - 29
  - 33
  - 36
  - 38
  - 39
  - 50

---
    
Depuis les années 1990, les écrivains ont progressivement envahi la toile, investissant les blogues et les réseaux sociaux, expérimentant des formes hypermédiatiques inédites. Des communautés en ligne se sont formées, de même que des coopératives d’écrivains, opérant une reconfiguration évidente des rapports entre les instances auctoriale, lectrice et éditoriale. Sous l’effet de ces nouvelles pratiques, que l’on peut désigner comme des formes d’éditorialisation, la tentation est grande de constater un affaiblissement de la figure auctoriale au profit de la multiplication des oeuvres collectives, lesquelles ont par ailleurs remis en question le rôle institutionnel des maisons d’édition. Il est cependant possible d’observer, simultanément, l’émergence de pratiques d’écriture en ligne inédites où la figure auctoriale se met en scène, jouant des tensions entre l'auteur, l’écrivain, le personnage d’écrivain et la personne elle-même. Ces pratiques s’observent bien évidemment sur des sites d’auteurs, sur des blogues consacrés à la publication littéraire, mais aussi dans des espaces réputés laisser peu de place à l’expression littéraire, et qui font pourtant preuve d’un potentiel poétique insoupçonné. Ainsi, des plateformes impliquant des usages très formatés, telles que Facebook ou Twitter, sont détournées par des écrivains pour raconter une histoire ou façonner un personnage autofictif, accueillant ainsi de nouveaux types de récits de soi.

En d’autres termes, on pourrait dire que désormais l'auteur s'éditorialise, tant la production de la figure auctoriale s’est muée en un ensemble de pratiques d'éditorialisation de l'écrivain. En effet, de même que l’écrivain produit ses personnages, l'écrivain numérique (qu'il diffuse un contenu littéraire, documentaire, etc.) produit son auteur, ou se produit en tant qu'auteur. Cette forme inédite de production identitaire permet (ou pas) de marquer la distance entre les différentes instances (personne, écrivain, auteur), afin justement de faire exister – dans le sens étymologique de ex-ire, “sortir” - l’auteur.

Ce colloque a pour objectif d’examiner les différentes stratégies d’éditorialisation de l’auteur et d’en relever les principaux enjeux : de quelle manière se construit et se manifeste l’identité numérique de l’écrivain ? Quels sont les enjeux reliés à l’émergence de nouvelles figures auctoriales ? Comment ces pratiques d’écriture de soi en ligne renégocient-elles les rapports entre réalité et fiction ? Comment se renégocient les rapports entre les différentes instances de production du texte (éditeur, mais aussi lecteur) ? Peut-on identifier de nouvelles formes d’autorité et de validation des contenus ? Quels discours les écrivains produisent-ils sur ces mutations et, de fait, comment réagit l’institution littéraire à cet écrivain numérique qui perturbe la chaîne du livre?

Programme prochainement disponible
