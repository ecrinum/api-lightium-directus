---
title: "Conférence de Tim Fulford : Picturing the Picturesque Tour; or, Making a Digital Edition of an Early 19th-century MS"
short: "<h2>5 février\r

  <br>11h, salle C-8111\r

  <br>Pavillon Lionel-Groulx, Université de Montréal</h2>"
cleanstring: Conference-de-Tim-Fulford--Picturing-the-Picturesque-Tour-or-Making-a-Digital-Edition-of-an-Early-onenineth-century-MS
lang: FR
caption: ""
image: https://i.imgur.com/rbmj7aX.jpg
url: ""
urlTitle: ""
categories:
  - 3

---
    
Tim Fulford présentera la façon dont il a développé une édition numérique d'un des premiers exemples de ce qu'on appelle aujourd'hui la littérature de voyage - le journal du voyage, le cahiers de dessins et l'album faits par un des premiers participants du premier voyage organisé en Grande-Bretagne. _Banks of Wye_ de Robert Bloomfield est une collection multi-média de textes, privés et publics, qui nous dit beaucoup à l'égard du rapport esthétique entre Ms et culture de l'imprimé et la naissante industrie du tourisme en Grande-Bretagne au XIXe siècle. Fulford nous fera faire un tour virtuel de l'édition et nous guidera à travers quelques-uns des problèmes qu'il a résolus dans sa création.  
  
La recherche de Fulford se situe dans le domaine de la littérature de l'ère romantique et dans le contexte du colonialisme, de l'exploration, de la science, du paysage, du pitoresque, de la religion. Il a publié nombreux articles et livres autour de ces question, en s'intéressant d'écrivains comme William Wordsworth, S. T. Coleridge, Robert Bloomfield, Mary Robinson, William Cowper, Jane Austen et John Clare. Fulford est en train de préparer une édition critique de Robert Southey et d'Humphry Davy. Sa prochaine monographie portera sur une étude de la tarde poétique des poètes des lacs.  
  
