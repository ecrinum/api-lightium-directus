---
title: <span>Enrico Agostini Marchese, « La littérature à la dérive numérique. De lignes, d’écriture et d’espaces », <i>Sens Public</i>, décembre 2017.</span>
short: "L’opposition millénaire entre réel et imaginaire, inaugurée par Platon, est-elle encore valable à l’époque du numérique ? Nous entendons questionner le positionnement platonicien en nous appuyant paradoxalement sur l’art le moins réel d’après Platon lui-même : la littérature. Nous montrerons, à travers un bref parcours historique de la filiation qui de la flânerie baudelairienne en passant par la pratique situationniste de la dérive, mène jusqu’à la littérature numérique contemporaine portant sur l’espace, comment cette typologie de littérature, avec ses stratégies d’écriture, ses poétiques et ses pratiques, peut déjouer cette opposition gravée dans notre pensée."
cleanstring: spanEnrico-Agostini-Marchese-La-litterature-a-la-derive-numerique-De-lignes-decriture-et-despaces-iSens-Publici-decembre-twozeroonesevenspan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/19744
urlTitle: ""
categories:
  - 54
  - 41
  - 58
  - 25
  - 18
  - 4

---
    
<!-- pas de contenu sur cette page -->
