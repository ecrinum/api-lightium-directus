---
title: "Marcello Vitali-Rosati lecture : Theory of editorialization. Philosophical reflection on digital technlogies' issues"
short: "<h2>20th March 2018\r

  <br>10.30 am\r

  <br>University of Rouen</h2>"
cleanstring: Marcello-Vitali-Rosati-lecture--Theory-of-editorialization-Philosophical-reflection-on-digital-technlogies-issues
lang: EN
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 3
  - 22

---
    
Within the study day "Les Humanités numériques, une question éditoriale?", Marcello Vitali-Rosati will talk about the theory of editorialization as well as his research on the topic
