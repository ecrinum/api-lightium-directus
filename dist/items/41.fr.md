---
title: <span>Marcello Vitali-Rosati, « Une philosophie du numérique. Lecture de « Pour un humanisme numérique » de Milad Doueihi », <i>Sens Public</i>, novembre 2011.</span>
short: Ce que nous propose Milad Doueihi dans son dernier livre Pour un humanisme numérique n’est pas une simple analyse des changements apportés par les nouvelles technologies. C’est une philosophie à part entière, dont les implications théoriques ne sont pas circonscrites au seul domaine des technologies de l’information, mais engendrent une véritable vision du monde. Ces pages veulent rendre compte de cet apparat théorique et mettre en évidence ses principaux enjeux.
cleanstring: spanMarcello-Vitali-Rosati-Une-philosophie-du-numerique-Lecture-de-Pour-un-humanisme-numerique-de-Milad-Doueihi-iSens-Publici-novembre-twozerooneonespan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/12996
urlTitle: ""
categories:
  - 54
  - 41
  - 22
  - 25
  - 26
  - 27

---
    
<!-- pas de contenu sur cette page -->
