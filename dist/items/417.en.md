---
title: "<span>Marcello Vitali-Rosati, <i>On Editorialization: Structuring Space and Authority in the Digital Age</i>, Amsterdam, Institute of Network Cultures, 2018, 114 p., (« Theory on demand », 26).</span>"
short: "In On Editorialization: Structuring Space and Authority in the Digital Age Marcello Vitali-Rosati examines how authority changes in the digital era. Authority seems to have vanished in the age of the web, since the spatial relationships that authority depends on are thought to have levelled out: there are no limits or boundaries, no hierarchies or organized structures anymore. Vitali-Rosati claims the opposite to be the case: digital space is well-structured and material and has specific forms of authority. Editorialization is one key process that organizes this space and thus brings into being digital authority. Investigating this process of editorialization, Vitali-Rosati reveals how politics can be reconceived in the digital age."
cleanstring: spanMarcello-Vitali-Rosati-iOn-Editorialization-Structuring-Space-and-Authority-in-the-Digital-Agei-Amsterdam-Institute-of-Network-Cultures-twozerooneeight-oneoneforp-Theory-on-demand-twosixspan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/19868
urlTitle: ""
categories:
  - 54
  - 40
  - 22
  - 25
  - 31
  - 19

---
    
<!-- pas de contenu sur cette page -->
