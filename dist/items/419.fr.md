---
title: "Séance de travail des membres du CRIHN : Darren Wershler"
short: "<h2>16 mars 2018\r

  <br />13h, Salle C-8076\r

  <br />Université de Montréal</h2>"
cleanstring: Seance-de-travail-des-membres-du-CRIHN--Darren-Wershler
lang: FR
caption: ""
image: http://www.crihn.org/files/sites/33/2015/07/darren-wershler-225x225.jpg
url: ""
urlTitle: ""
categories:
  - 3

---
    
![](http://www.crihn.org/files/sites/33/2015/07/darren-wershler-225x225.jpg)
  
  
Notre troisième séance de travail des membres du CRIHN pour 2017-2018 sera consacrée au projet de Darren Wershler: « **Residual Media Depot : Between Media Archaeology and Cultural Technique** ».

Darren Wershler est titulaire de la Chaire de recherche de l'Université Concorda (Tier 2) en Médias et Littérature contemporaine. Son travail porte sur les relations entre art d'avant-garde et écriture, et sur les études en communication et l'histoire des médias. Il a été aussi professeur des départements d'anglais et de communication. Il a aussi travaillé comme écrivain, éditeur et designer de livre papier et nouveaux médias. Par conséquent, il a développé une approche interdisciplinaire dans la recherche et l'enseignement. Il s'est engagé pour que les universités continuent de chercher nouvelles façons pour tisser des liens entre personnes, organisations et choses en dehors d'elles-mêmes.
  
  
