---
title: "Marcello Vitali-Rosati presentation : authority in the digital era"
short: "<h2>24th-25th March 2018\r

  <br />10am, Palais des congrès et de la culture du Mans\r

  <br />Le mans</h2>"
cleanstring: Marcello-Vitali-Rosati-presentation--authority-in-the-digital-era
lang: EN
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 3
  - 22
  - 48

---
    
Marcello Vitali-Rosati will participate to the symposium "Le numérique et nous, organized by the University du Mans, with a presentation on the notion of authority, and its modifications in the digital era

The next day, Peppe Cavallari will give a presentation on screens and digital culture, titled "La fenêtre, la main et l’écran : notes pour une théorie de la vision numérique"
