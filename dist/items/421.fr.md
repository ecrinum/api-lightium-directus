---
title: Présentations de Marcello Vitali-Rosati et Peppe Cavallari
short: "<h2>24-25 mars 2018\r

  <br />10h, Palais des congrès et de la culture du Mans\r

  <br />Le mans</h2>"
cleanstring: Presentations-de-Marcello-Vitali-Rosati-et-Peppe-Cavallari
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 3
  - 22
  - 48

---
    
Marcello Vitali-Rosati interviendra dans le colloque "Le numérique et nous", organisé par l'Université du Mans, avec une présentation sur la notion d'autorité et ses changements à l'ère du numérique.

Peppe Cavallari, quant à lui, présentera le lendemain avec sa communication "La fenêtre, la main et l’écran : notes pour une théorie de la vision numérique".

Regardez [ici](http://carrefoursdelapensee.univ-lemans.fr/fr/carrefours-2018/videos.html) les présentations de Marcello Vitali-Rosati et Peppe Cavallari.
