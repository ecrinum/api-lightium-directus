---
title: Marcello Vitali-Rosati's On Editorialization presentation
short: "<h2>3rd May 2018\r

  <br />4.45 pm, CRIHN\r

  <br />Local C-8132, Pavillon Lionel-Groulx\r

  <br />University of Montréal</h2>"
cleanstring: Marcello-Vitali-Rosatis-On-Editorialization-presentation
lang: EN
caption: ""
image: https://i.imgur.com/TsTCgdN.png
url: ""
urlTitle: ""
categories:
  - 3
  - 22
  - 24

---
    
![](https://i.imgur.com/TsTCgdN.png)
  
  
Thursday 3rd May, at 4.45 pm, the CRIHN will held the presentation of the latest book by Marcello Vitali-Rosati, On Editorialization: Structuring Space and Authority in the Digital Age, available on open acces here : <http://networkcultures.org/blog/publication/tod-26-on-editorialization-structuring-space-and-authority-in-the-digital-age/>.

The presentation will be also the occasion to inaugurate the new CRIHN's local at the University of Montreal, pavillon Lionel-Groulx.

Please confirm your presence via Eventbrite.

### Inscription gratuite

[Développé par Eventbrite](https://www.eventbrite.fr/)
