---
title: Lancement du livre de Marcello Vitali-Rosati, On Editorialization
short: "<h2>3 mai 2018\r

  <br />16h45, CRIHN\r

  <br />Local C-8132, Pavillon Lionel-Groulx\r

  <br />Université de Montréal</h2>"
cleanstring: Lancement-du-livre-de-Marcello-Vitali-Rosati-On-Editorialization
lang: FR
caption: ""
image: https://i.imgur.com/TsTCgdN.png
url: ""
urlTitle: ""
categories:
  - 3
  - 22
  - 24

---
    
![](https://i.imgur.com/TsTCgdN.png)
  
  
Jeudi 3 mai prochain à 16h45, aura lieu le lancement du dernier livre de Marcello Vitali-Rosati, On Editorialization: Structuring Space and Authority in the Digital Age, consultable en libre accès à l'adresse suivante : <http://networkcultures.org/blog/publication/tod-26-on-editorialization-structuring-space-and-authority-in-the-digital-age/>.

Le lancement sera également l'occasion d'inaugurer les nouveaux locaux du CRIHN à l'Université de Montréal, pavillon Lionel-Groulx.

Une confirmation de présence via Eventbrite est demandée.

### Inscription gratuite

[Développé par Eventbrite](https://www.eventbrite.fr/)
