---
title: L'édition à l'ère du numérique
short: <h2>Parution du dernier livre de Marcello Vitali-Rosati et Benoît Epron</h2>
cleanstring: Ledition-a-lere-du-numerique
lang: FR
caption: ""
image: http://extranet.editis.com/it-yonixweb/IMAGES/DEC/P3/9782707199355.jpg
url: ""
urlTitle: ""
categories:
  - 3
  - 4
  - 5
  - 6
  - 8
  - 22

---
    
![](http://extranet.editis.com/it-yonixweb/IMAGES/DEC/P3/9782707199355.jpg)
  
  
Publié chez la prestigieuse collection « Repère » de la maison d'édition _La Découverte_, le livre écrit par le titulaire de la Chaire, Marcello Vitali-Rosati, avec Benoît Epron, professeur de l'ENSSIB se propose de dresse un état de l'art du monde de l'édition à l'époque du numérique.

Le numérique est en train de remodeler l’ensemble du processus de production du savoir, de validation des contenus et de diffusion des connaissances. En cause : l’émergence de nouveaux outils et de nouvelles pratiques d’écriture et de lecture, mais aussi un changement plus global que l’on pourrait qualifier de culturel.

Les éditeurs ont posé en termes tantôt apocalyptiques tantôt technophiles un grand nombre de questions, notamment sur l’avenir du livre, les modes d’accès à la connaissance, la légitimation des contenus en ligne et les droits d’auteur. Cet ouvrage propose un état des lieux de l’impact effectif des mutations technologiques sur l’édition, à partir de trois fonctions principales des instances éditoriales : la production des contenus, leur circulation et leur légitimation.

Cet ouvrage combine une approche académique de compréhension des modèles, une observation empirique des pratiques et usages et une analyse des logiques stratégiques déployées dans ce secteur.

Pour plus d'informations ainsi que pour lire un extrait de l'ouvrage, rendez-vous sur le site de la Découverte: <http://www.collectionreperes.com/catalogue/index-L%5F%5F%5Fdition%5F%5F%5F%5Fl%5F%5F%5Fre%5Fdu%5Fnum%5F%5Frique-9782707199355.html>
