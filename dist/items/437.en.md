---
title: <span>Thomas Carrier-Lafleur, André Gaudreault, Servanne Monjour[et al.], « L’invention littéraire des médias », <i>Sens Public</i>, avril 2018.</span>
short: Les médias existeraient-ils sans la littérature ? Pourrait-on parler de « télévision », de « photographie », de « cinéma » ou du « numérique » sans que ces dispositifs aient aussi été construits, institutionnalisés et même parfois déconstruits dans l’imaginaire collectif par la littérature et son discours ? À l’heure où le numérique semble encore s’inventer, le présent dossier vise à souligner le rôle du fait littéraire dans la construction de nos médias. En même temps, l’hybridation médiatique de notre contemporanéité numérique rend nécessaire une réflexion sur la capacité des médias à se réinventer réciproquement, renouvelant chaque fois l’ordre du discours et la fonction de la littérature. En raison de sa capacité à témoigner de l’hétérogénéité de notre univers médiatique, la littérature offre un terrain privilégié – où tout reste encore à faire – pour mener une telle recherche.
cleanstring: spanThomas-Carrier-Lafleur-Andre-Gaudreault-Servanne-Monjouret-al-Linvention-litteraire-des-medias-iSens-Publici-avril-twozerooneeightspan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/19962
urlTitle: ""
categories:
  - 54
  - 41
  - 66
  - 39
  - 22
  - 33

---
    
<!-- pas de contenu sur cette page -->
