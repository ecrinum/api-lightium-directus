---
title: Intervention de Marcello Vitali-Rosati et Michael Sinatra dans la journée d'étude OpenCon
short: "<h2>Vendredi 19 octobre, 13h45\r

  <br />Carrefour des Arts et des Sciences, Salle C-3061\r

  <br />Université de Montréal</h2>"
cleanstring: Intervention-de-Marcello-Vitali-Rosati-et-Michael-Sinatra-dans-la-journee-detude-OpenCon
lang: FR
caption: ""
image: https://crihn.openum.ca/files/sites/33/2018/10/opencon2018.jpg
url: ""
urlTitle: ""
categories:
  - 3
  - 22
  - 24

---
    
![](https://crihn.openum.ca/files/sites/33/2018/10/opencon2018.jpg)

## OpenCon Satellite Montréal 2018

SPARC, organisme dédié à la promotion du libre accès, est à l’origine d’un mouvement qui s’adresse aux professionnel.le.s de tous les milieux s’intéressant à l’Open Access, l’Open Science, l’Open Education et l’Open Data. En plus d’une réunion annuelle internationale ([http://www.opencon2018.org](http://www.opencon2018.org/)), il soutient l’organisation d’événements satellites pour en faire la promotion à une échelle locale.

C’est dans cette perspective que l’EBSI, en collaboration avec le Centre de recherche interuniversitaire sur les humanités numériques (CRIHN) et les Bibliothèques de l’Université de Montréal, désire vous inviter à cette première édition de OpenCon Satellite Montréal.

La journée sera divisée en quatre périodes : trois panels d’intervenant.e.s portant sur le Libre Accès, la Libre Éducation et les Données libres, suivis d’un atelier de nature interactive.

* 10h00 – Mot de bienvenue
* 10h15-11h35 – **Session 1\. Ressources éducatives libres**  
   * Pascale Blanc (Vitrine technologie-éducation)  
   * Isabelle Laplante (ÉDUQ)  
   * Robert Gérin-Lajoie (EDUlib – UdeM)  
   * Simon Villeneuve (Cégep de Chicoutimi)  
   * Hugh McGuire (Librivox)
* 11h35-11h50 – Pause café
* 11h50-13h – **Session 2\. Données ouvertes**  
   * Patrick Lozeau (Laboratoire d’innovation urbaine de la Ville de Montréal)  
   * Marina Gallet (Wikidata – Cinémathèque québécoise)  
   * Diane Sauvé (Bibliothèques de l’Université de Montréal)  
   * Mathieu Gauthier-Pilote (Fondation Lionel-Groulx)
* 13h-13h45 – Dîner libre
* 13h45-15h – **Session 3\. Libre accès**  
   * Florence Piron (Éditions science et bien commun, Université Laval)  
   * Émilie Paquin (Érudit)  
   * Geoffrey R. Little (Presses de l’Université Concordia)  
   * Michael E. Sinatra et Marcello Vitali Rosati (Collection Parcours numériques, Centre de recherche interuniversitaire sur les humanités numériques (CRIHN), Université de Montréal)  
   * Émilie et Jean-Marie Tremblay (Les Classiques des sciences sociales)
* 15h-15h15 – Pause café
* 15h15-17h00 – **Atelier**
* 17h00 – Mot de la fin

\*Les interventions d’une durée de 10 minutes seront suivies par une discussion de 30 minutes.

**Réservation (gratuite) [en ligne](https://www.eventbrite.com/e/billets-opencon-satellite-montreal-2018-50889535972)** 
