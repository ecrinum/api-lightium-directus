---
title: Lancement du livre de Servanne Monjour, Mythologies postphotographiques
short: "<h2>Mardi 27 novembre, 17h30\r

  <br />Libraire du square-Outremont\r

  <br />1061, avenue Bernard, Montréal"
cleanstring: Lancement-du-livre-de-Servanne-Monjour-Mythologies-postphotographiques
lang: FR
caption: ""
image: https://i.imgur.com/ol4FBD0.png
url: ""
urlTitle: ""
categories:
  - 3

---
    
![](https://i.imgur.com/ol4FBD0.png)
