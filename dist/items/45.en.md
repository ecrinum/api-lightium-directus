---
title: <span>Mark Bieber, Teresa Dobson, Lindsay Doll[et al.], « Drilling for Papers in INKE », <i>Scholarly and Research Communication</i>, vol. 3 / 1, mars 2012.</span>
short: In this article, we discuss the first year research plan for the INKE interface design team, which focuses on a prototype for chaining. Interpretable as a subclass of Unsworths scholarly primitive of discovering, chaining is the process of beginning with an exemplary article, then finding the articles that it cites, the articles they cite, and so on until the reader begins to get a feel for the terrain. The chaining strategy is of particular utility for scholars working in new areas, either through doing background work for interdisciplinary interests or else by pursuing a subtopic in a domain that generates a paper storm of publications every year. In our prototype project, we plan to produce a system that accepts a seed article, tunnels through a number of levels of citation, and generates a summary report listing the most frequent authors and articles. One of the innovative features of this prototype is its use of the experimental oil and water interface effect, which uses text animation to provide the user with a sense of the underlying process.
cleanstring: spanMark-Bieber-Teresa-Dobson-Lindsay-Dollet-al-Drilling-for-Papers-in-INKE-iScholarly-and-Research-Communicationi-volthreeone-mars-twozeroonetwospan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13000
urlTitle: ""
categories:
  - 54
  - 41
  - 24
  - 5

---
    
<!-- pas de contenu sur cette page -->
