---
title: <span>Benoît Epron et Marcello Vitali-Rosati, <i>L’édition à l’ère numérique</i>, Paris, La Découverte, 2018, 128 p., (« Repères »).</span>
short: |-
  Le numérique est en train de remodeler l’ensemble du processus de production du savoir, de validation des contenus et de diffusion des connaissances. En cause : l’émergence de nouveaux outils et de nouvelles pratiques d’écriture et de lecture, mais aussi un changement plus global que l’on pourrait qualifier de culturel.
  Les éditeurs ont posé en termes tantôt apocalyptiques tantôt technophiles un grand nombre de questions, notamment sur l’avenir du livre, les modes d’accès à la connaissance, la légitimation des contenus en ligne et les droits d’auteur. Cet ouvrage propose un état des lieux de l’impact effectif des mutations technologiques sur l’édition, à partir de trois fonctions principales des instances éditoriales : la production des contenus, leur circulation et leur légitimation.
  Cet ouvrage combine une approche académique de compréhension des modèles, une observation empirique des pratiques et usages et une analyse des logiques stratégiques déployées dans ce secteur.
cleanstring: spanBenoit-Epron-et-Marcello-Vitali-Rosati-iLedition-a-lere-numeriquei-Paris-La-Decouverte-twozerooneeight-onetwoeightp-Reperesspan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/20642
urlTitle: ""
categories:
  - 54
  - 40
  - 22
  - 28
  - 35
  - 4
  - 19

---
    
<!-- pas de contenu sur cette page -->
