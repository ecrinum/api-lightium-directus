---
title: <span>Thomas Carrier-Lafleur, « Comment parler de ce que l’on aime ? Poétique de l’indirect et origine des médias chez le dernier Roland Barthes », <i>MuseMedusa</i>, 2018.</span>
short: "À partir d’un des derniers articles rédigés par Roland Barthes, « On échoue toujours à parler de ce qu’on aime », le présent article s’intéresse à la résonance du mythe de Dibutade dans l’œuvre tardive de l’écrivain. Ce que cherche le dernier Barthes, ce sont les mots justes pour parler de ce qu’il aime le plus, et qui maintenant n’est plus : il veut parler de sa mère et trouver une façon de dire son chagrin. Mais les mots sont toujours les mots des autres. Pour retrouver une certaine vérité de l’expression, Barthes sera ainsi amené à explorer des formes et des médias autres : photographie, roman, haïku. Cette poétique de l’indirect est le seul moyen de parler de ce que l’on aime."
cleanstring: spanThomas-Carrier-Lafleur-Comment-parler-de-ce-que-lon-aime-Poetique-de-lindirect-et-origine-des-medias-chez-le-dernier-Roland-Barthes-iMuseMedusai-twozerooneeightspan
lang: EN
caption: ""
image: ""
url: http://musemedusa.com/dossier_6/carrier-lafleur/
urlTitle: ""
categories:
  - 54
  - 41
  - 66

---
    
<!-- pas de contenu sur cette page -->
