---
title: <span>Margot Mellet, « Des signatures à soi. Marguerite par Marguerite », <i>MuseMedusa</i>, 2018.</span>
short: |-
  En plus d’une production littéraire prolifique variée (poèmes, recueil de nouvelles…), Marguerite de Navarre a également tenu une correspondance abondante avec son entourage, avec les politiques ou intellectuels de son temps dont la correspondance avec Guillaume Briçonnet, alors évêque de Meaux. Cette correspondance entre deux figures intellectuelles, politiques et évangéliques de l’histoire de France s’étend de juin 1521 à novembre 1524. Il s’agit d’une correspondance « littéraire » fondée sur un enjeu spirituel primordial : il s’agit de renouer avec Dieu. Communément considérée comme la source majeure de l’innutrition spirituelle de Marguerite, la Correspondance entretenue avec Guillaume Briçonnet est davantage lue et présentée dans un rapport historique, plutôt que dans une dimension pleinement littéraire et stylistique.
  Dès la première lecture de la Correspondance, le lecteur peut déjà déceler une qualité et un soin apportés aux signatures par Marguerite d’Angoulême attestant d’une franchise épistolaire, d’un talent littéraire et d’un sentiment particulier au contexte de la lettre signée. Les signatures épistolaires ne sont pas de simples modalités de politesse ou des conventions silencieuses entre les destinataires, mais de véritables signes poétiques de soi pour Marguerite de Navarre.
cleanstring: spanMargot-Mellet-Des-signatures-a-soi-Marguerite-par-Marguerite-iMuseMedusai-twozerooneeightspan
lang: FR
caption: ""
image: ""
url: http://musemedusa.com/dossier_6/mellet/
urlTitle: ""
categories:
  - 54
  - 41
  - 70

---
    
<!-- pas de contenu sur cette page -->
