---
title: <span>Marcello Vitali-Rosati, « Mais où est passé le réel ? Profils, représentations et métaontologie », <i>MuseMedusa</i>, 2018.</span>
short: Mais où est passé le réel ? C’est une question apparemment éternelle qui, dans un contexte contemporain marqué par les médiations numériques (réseaux sociaux, dispositifs de réalité virtuelle, réalité augmentée, etc.), retrouve aujourd’hui un sens et une valeur singuliers. Le mythe de Dibutade, en tant que mythe d’origine d’une forme particulière de représentation, celle du profil, me semble particulièrement adapté pour poser la question du réel aujourd’hui à partir notamment des expériences d’« écriture profilaire » que l’on voit se multiplier sur le web. Sera proposée dans cet article une interprétation de l’écriture de profil avec une approche philosophique que l’auteur appelle « métaontologie ». L’ambition de la métaontologie est de repenser le statut du réel après la déconstruction de ce concept qui a été opérée à la fin du XXe siècle dans ce mouvement de pensée, hétérogène et multiple, auquel on fait référence sous l’étiquette – par ailleurs très problématique – de « post-structuralisme ».
cleanstring: spanMarcello-Vitali-Rosati-Mais-ou-est-passe-le-reel-Profils-representations-et-metaontologie-iMuseMedusai-twozerooneeightspan
lang: FR
caption: ""
image: ""
url: http://musemedusa.com/dossier_6/vitali-rosati/
urlTitle: ""
categories:
  - 54
  - 41
  - 22
  - 18
  - 17
  - 16
  - 10

---
    
<!-- pas de contenu sur cette page -->
