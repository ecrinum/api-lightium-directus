---
title: "<span>Michael E. Sinatra, « Promoting Open Access and Innovations: From Synergies to Le Centre de recherche interuniversitaire sur les humanités numériques », <i>Scholarly and Research Communication</i>, vol. 6 / 4, 2015.</span>"
short: This article discusses the relationship between digital humanities and disciplinary boundaries in the last decade, primarily in the context of the national project Synergies. It offers first an overview of Synergies as a concrete example of the way technological change impacts the very notion of disciplines by trying to create a platform that was interdisciplinary by nature, then discusses the creation of a new Digital Humanities centre in Québec—Le Centre de recherche interuniversitaire sur les humanités numériques – and the ways it was conceived as encompassing a range of disciplinary approach.
cleanstring: spanMichael-E-Sinatra-Promoting-Open-Access-and-Innovations-From-Synergies-to-Le-Centre-de-recherche-interuniversitaire-sur-les-humanites-numeriques-iScholarly-and-Research-Communicationi-volsixfor-twozeroonefivespan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13002
urlTitle: ""
categories:
  - 54
  - 41
  - 24
  - 28

---
    
<!-- pas de contenu sur cette page -->
