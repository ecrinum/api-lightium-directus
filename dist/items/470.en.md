---
title: "<span>Michael Nardone, « We Are the Amp: On the Human Microphone », in <i>Public Poetics. Critical Issues in Canadian Poetry and Poetics</i>, Waterloo, Wilfrid Laurier University Press, 2015, p. 289‑311.</span>"
short: Poetic forms emerge out of public contexts of language, as response, as confrontation. The emergent contexts of forms more traditionally situated within poetic practice have been explored and described widely—for example, the metrical devices of Ancient Greek verse as mnemonic aids for the oral circulation of information across space, and the sestina’s repetitive structure that allowed one to showcase both craft and improvisation during feasts or gatherings.
cleanstring: spanMichael-Nardone-We-Are-the-Amp-On-the-Human-Microphone-iniPublic-Poetics-Critical-Issues-in-Canadian-Poetry-and-Poeticsi-Waterloo-Wilfrid-Laurier-University-Press-twozeroonefive-ptwoeightninethreeoneonespan
lang: EN
caption: ""
image: ""
url: ""
urlTitle: ""
categories:
  - 54
  - 42

---
    
<!-- pas de contenu sur cette page -->
