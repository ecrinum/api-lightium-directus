---
title: "Participation de la chaire au congrès international DH 2019 "
short: "<h2>TivoliVredenburg\r

  <br>Utrecht, Pays Bas\r

  <br>10-12 juillet 2019\r

  <h2>"
cleanstring: Participation-de-la-chaire-au-congres-international-DH-twozeroonenine-
lang: FR
caption: ""
image: https://dh2019.adho.org/wp-content/uploads/2018/12/LOGO_dh2019_small.jpg
url: https://dh2019.adho.org/
urlTitle: https://dh2019.adho.org/
categories:
  - 3
  - 4
  - 5
  - 6
  - 8
  - 10
  - 11
  - 13
  - 19
  - 22
  - 24
  - 28
  - 29
  - 35
  - 58
  - 70

---
    
![](https://dh2019.adho.org/wp-content/uploads/2018/12/LOGO_dh2019_small.jpg)

La Chaire de recherche du Canada sur les écritures numériques participera au congrès mondial des Humanités Numériques avec plusieurs membres, présentant différents projets et recherches. Consultez le [site internet du colloque](https://dh2019.adho.org/) pour plusieurs informations.
