---
title: Internship offer at the CRC on digital textualities
short: "Deadline: September 18, 2019\r

  <br /> Start date: May 2020"
cleanstring: Internship-offer-at-the-CRC-on-digital-textualities
lang: EN
caption: ""
image: ""
url: https://globalink.mitacs.ca/#/student/application/welcome
urlTitle: ""
categories:
  - 3

---
    
The Canada Research Chair on Digital Textualities, led by Dr. Marcello Vitali-Rosati at the University of Montreal, is currently looking for 6 interns interested in working, as early as the month of  MAY 2020 </ strong>, on two digital humanities projects: the first one is about [ collaborative digital edition of the Palatine Anthology ](http://ecrituresnumeriques.ca/en/2016/1/19/Greek-Anthology), the second on [ the theoretical and practical issues of the digital transition of scholarly journals ](http://ecrituresnumeriques.ca/en/2018/3/22/Journal-twozero). The selected students will receive a research grant of CAD 6,000 covering travel and accommodation costs as well as direct research expenses. 

Candidates interested in these positions can submit their applications via the MITACS platform (NPO dedicated to research and innovation development and support) at [ https://globalink.mitacs.ca/#/student/application/welcome ](https://globalink.mitacs.ca/#/student/application/welcome). They will be able to find the available projects - project description, supervision, prerequisites and any other information - in the "Projects" tab, indicating the name of the professor by Marcello Vitali-Rosati. 

 For more information on the CRC on Digital Textualities, its team and its work, please visit its website at: [ http: //ecrituresnumeriques.ca/en/](http://ecrituresnumeriques.ca/en/). If you have any questions, you can contact the team by email: [crc.ecrituresnumeriques@gmail.com](mailto:crc.ecrituresnumeriques@gmail.com). 
