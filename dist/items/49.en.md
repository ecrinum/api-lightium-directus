---
title: "<span>Marcello Vitali-Rosati, « Les revues littéraires en ligne : entre éditorialisation et réseaux d’intelligences », <i>Études françaises</i>, vol. 50 / 3, 2014, p. 83.</span>"
short: Cet article propose un état des lieux sur les revues littéraires numériques. Cette tâche pourrait sembler facile si l’on considère que ces expériences existent depuis très peu de temps. Les premières revues en ligne apparaissent, en effet, au début des années 1990. Pourtant, la question est beaucoup plus complexe que ce que l’on pourrait penser, et cela, pour une série de raisons qui seront analysées dans cet article. Il n’est tout d’abord pas évident de s’entendre sur ce que l’on définit par l’expression « revue littéraire numérique ». D’une part car on fait référence, avec le mot « numérique », à une série d’expériences et de pratiques hétérogènes et différentes qui peuvent difficilement être regroupées ensemble. D’autre part parce que ce qu’on appelle désormais la « révolution numérique » a déterminé des changements importants quant au sens des contenus, de leur production, de leur validation et de leur distribution et a par conséquent fortement affecté la signification du mot « revue » lui-même. Il faudra ainsi prendre séparément en considération une série de phénomènes différents et essayer de rendre compte de pratiques hétérogènes qui se chevauchent et empiètent l’une sur l’autre. L’article proposera d’abord une analyse des enjeux de la numérisation des revues, à savoir le processus de transposition des revues papier au format électronique. Il s’attaquera ensuite aux expériences des revues numériques dès leur création pour comprendre s’il y a une différence, et laquelle, entre les premières et les secondes. Pour finir, on tentera de comprendre en quoi le numérique en tant que phénomène culturel — et en particulier les changements de diffusion et de circulation des contenus ainsi que les différentes formes de ce que l’on appelle désormais « éditorialisation » — a transformé l’idée même de revue et donné lieu à des pratiques et à des expériences complexes et hybrides dont la place dans le panorama culturel est difficile à saisir.
cleanstring: spanMarcello-Vitali-Rosati-Les-revues-litteraires-en-ligne-entre-editorialisation-et-reseaux-dintelligences-iEtudes-francaisesi-volfivezerothree-twozeroonefor-peightthreespan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/11379
urlTitle: ""
categories:
  - 54
  - 41
  - 22
  - 5
  - 6
  - 13
  - 19

---
    
<!-- pas de contenu sur cette page -->
