---
title: <span>Marcello Vitali-Rosati, « Écrire et écrit. Le journal comme atelier d’entraînement de la pensée », <i>Pratiques de formation</i>, 2013, p. 89‑95.</span>
short: |-
  Le journal correspond à un atelier d'entraînement de la pensée où il devient possible de réécrire l'écrit. C'est-à-dire, c'est un lieu de flânerie, où l'auteur produit sa pensée et crée en toute liberté. Ce recueil d'idées déposées dans ce journal est l'essence de ce qui deviendra ensuite l'article, la thèse, le livre, où la forme et le contenu seront contraints par les règles orthographiques, grammaticales et syntaxiques. Le journal rend possible cette réflexivité immédiate et est garante de l'émergence de l'idée avant sa transformation et sa théorisation.

  The diaries is a training workshop of the mind where it is possible to rewrite the writing. That is to say, it is a place to scroll, where the author creates and produces his thoughts freely. This collection of ideas submitted in this diary is the essence of what later become the diary's thesis, the book, where the form and content will be constrained by the rules of spelling, grammar and syntax. Diaries makes this immediate reflexivity possible and guarantees the emergence of the idea before its transformation and theorization.
cleanstring: spanMarcello-Vitali-Rosati-Ecrire-et-ecrit-Le-journal-comme-atelier-dentrainement-de-la-pensee-iPratiques-de-formationi-twozeroonethree-peightnineninefivespan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/12978
urlTitle: ""
categories:
  - 54
  - 41
  - 22
  - 11
  - 4

---
    
<!-- pas de contenu sur cette page -->
