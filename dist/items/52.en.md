---
title: <span>Marcello Vitali-Rosati, « CLASSER CALCULER », <i>Sens-Public</i>, mai 2012.</span>
short: "Un rêve, plus ou moins explicite, hante nos esprits depuis plusieurs millénaires. On le retrouve ci et là dans les listes égyptiennes, dans les catalogues aristotéliciens, dans le règles mnémotechniques des néoplatoniciens florentins de la renaissance, dans les constructions mathématiques de Leibniz, dans les affirmations des grands noms du web : le monde est constitué d’une masse énorme d’informations, dont la connaissance et l’exploitation permettrait la maîtrise quasi-totale. Il serait alors possible de tout savoir, de tout prévoir, de tout faire. Mais deux limites, proprement humaines, empêchent la détention et l’exploitation de cette globalité d’informations : l’accessibilité et la calculabilité."
cleanstring: spanMarcello-Vitali-Rosati-CLASSER-CALCULER-iSens-Publici-mai-twozeroonetwospan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/12979
urlTitle: ""
categories:
  - 54
  - 41
  - 22
  - 5
  - 25
  - 19

---
    
<!-- pas de contenu sur cette page -->
