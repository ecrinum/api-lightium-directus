---
title: <span>Marcello Vitali-Rosati, « Auteur ou acteur du web ? », <i>Implications philosophiques</i>, juillet 2012.</span>
short: |-
  Le web est un espace d’action. Cette affirmation, qui sera la thèse fondamentale de ces pages, pose tout de suite une série de questions. En premier lieu, quelles sont les actions sur le web ? Ensuite, qui est l’acteur ? Parle-t-on d’acteurs ou d’auteurs ?
  Je ne pourrais prétendre donner ici une réponse à ces questions ; l’ambition de cet article se limite à en illustrer les enjeux pour approfondir la compréhension de notre monde numérique.
cleanstring: spanMarcello-Vitali-Rosati-Auteur-ou-acteur-du-web-iImplications-philosophiquesi-juillet-twozeroonetwospan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/12980
urlTitle: ""
categories:
  - 54
  - 41
  - 22
  - 11
  - 6
  - 4
  - 19

---
    
<!-- pas de contenu sur cette page -->
