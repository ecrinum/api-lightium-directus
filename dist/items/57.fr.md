---
title: "<span>Michael E. Sinatra, « Introducing « Critical Essays » : Leigh Hunt and Theatrical Criticism in the Early Nineteenth Century », <i>Keats-Shelley Journal</i>, vol. 50, 2001, p. 100‑123.</span>"
short: The years 1801 to 1808 saw the emergence of Leigh Hunt as a public figure on the London literary scene, first with the publication of his collection of poetry, «Juvenilia», and then with his work as theater critic for «The News» between 1805 and 1807. [...]
cleanstring: spanMichael-E-Sinatra-Introducing-Critical-Essays-Leigh-Hunt-and-Theatrical-Criticism-in-the-Early-Nineteenth-Century-iKeats-Shelley-Journali-volfivezero-twozerozeroone-ponezerozeroonetwothreespan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13516
urlTitle: ""
categories:
  - 54
  - 41
  - 24

---
    
<!-- pas de contenu sur cette page -->
