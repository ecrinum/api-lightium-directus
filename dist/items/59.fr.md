---
title: <span>Michael E. Sinatra, « Readings of homosexuality in Mary Shelley’s « Frankenstein » and four film adaptations », <i>Gothic Studies</i>, vol. 7 / 2, 2005, p. 185‑202.</span>
short: "This essay proposes to read one more time the issue of homosexuality in Mary Shelley's first novel, Frankenstein. In order to offer a new angle on the homosexual component of Victor Frankenstein's relationship with his creature when next teaching this most canonical Romantic novel, this essay considers Shelley's work alongside four film adaptations: James Whale's 1931 Frankenstein, Whale's 1935 The Bride of Frankenstein, Richard O'Brien's 1975 The Rocky Horror Picture Show, and Kenneth Branagh's 1994 Mary Shelley's Frankenstein. [...]"
cleanstring: spanMichael-E-Sinatra-Readings-of-homosexuality-in-Mary-Shelleys-Frankenstein-and-four-film-adaptations-iGothic-Studiesi-volseventwo-twozerozerofive-poneeightfivetwozerotwospan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13519
urlTitle: ""
categories:
  - 54
  - 41
  - 24
  - 32

---
    
<!-- pas de contenu sur cette page -->
