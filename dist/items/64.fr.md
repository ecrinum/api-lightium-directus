---
title: "<span>Jean-Marc Larrue, « “Theatre in French Canada: Laying the Foundations 1606-1867” », <i>Jeu</i>, 1985, p. 202‑204.</span>"
short: "« Theatre in French Canada: Laying the Foundations 1606-1867 » nous est présenté comme «a history of theatre», mais c'est en réalité une histoire du théâtre écrit que nous propose l'auteur. Doucette, en effet, concentre son étude sur les oeuvres écrites au Canada (par des Canadiens ou par des étrangers, résidants et immigrants) dont le texte, original ou réédité, est encore disponible. [...]"
cleanstring: spanJean-Marc-Larrue-Theatre-in-French-Canada-Laying-the-Foundations-onesixzerosix-oneeightsixseven-iJeui-onenineeightfive-ptwozerotwotwozeroforspan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13184
urlTitle: ""
categories:
  - 54
  - 41
  - 37

---
    
<!-- pas de contenu sur cette page -->
