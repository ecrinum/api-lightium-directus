---
title: "<span>Jean-Marc Larrue, « Les Créations Scéniques de Louis-Honoré Fréchette: Juin 1880 », <i>Theatre Research in Canada / Recherches théâtrales au Canada</i>, vol. 7 / 2, 1986.</span>"
short: |-
  Description détaillée de la mise en scène de deuxpièces de L.-H. Fréchette représentées à Montréal en 1880. L'auteur souligne les qualités spectaculaires de ces pièces, visiblement influencéespar les techniques deproduction new-yorkais de l'époque.
  A detailed description of the staging of two plays by L.H. Fréchette in Montréal in 1880. The author empbasizes the spectacular quality of these productions, visibly influenced by technical methods then current in New York.
cleanstring: spanJean-Marc-Larrue-Les-Creations-Sceniques-de-Louis-Honore-Frechette-Juin-oneeighteightzero-iTheatre-Research-in-Canada--Recherches-theatrales-au-Canadai-volseventwo-onenineeightsixspan
lang: FR
caption: ""
image: ""
url: https://journals.lib.unb.ca/index.php/TRIC/article/view/7391/8450
urlTitle: ""
categories:
  - 54
  - 41
  - 37

---
    
<!-- pas de contenu sur cette page -->
