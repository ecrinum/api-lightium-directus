---
title: <span>Jean-Marc Larrue, « Le splendide délire d’un soliloque à vingt voix », <i>Jeu</i>, 1986, p. 98‑99.</span>
short: Quelque part dans un désert, Michaela, une cantatrice italienne, est ficelée sur une voie ferrée où vont bientôt se télescoper deux trains lancés à pleine vitesse. Les train sont expérimentaux et militaires, évidemment. On présume que le premier, Santa Claus, vient de l'Ouest puisqu'il est anglophone et que l'autre, Staline, vient de l'Est. Staline parle russe, comme il se doit. Les deux convois roulent avec la même conviction aveugle et suicidaire. [...]
cleanstring: spanJean-Marc-Larrue-Le-splendide-delire-dun-soliloque-a-vingt-voix-iJeui-onenineeightsix-pnineeightnineninespan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13186
urlTitle: ""
categories:
  - 54
  - 41
  - 37

---
    
<!-- pas de contenu sur cette page -->
