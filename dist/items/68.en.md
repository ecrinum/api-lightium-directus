---
title: <span>Jean-Marc Larrue, « Les véritables débuts de la revue québécoise », <i>L’Annuaire théâtral</i>, 1987, p. 39‑70.</span>
short: |-
  L'étonnante prospérité du théâtre d'expression française à Montréal au début du XXe sicle tient à divers facteurs que nous avons tenté de cerner dans une étude à paraître. Tant dans le domaine de l'interprétation et de la scénographie que dans celui de la critique et de la réception critique, l'activité théâtrale connaît alors à Montréal l'une
  des plus glorieuses et des plus dynamiques périodes de son histoire. [...]
cleanstring: spanJean-Marc-Larrue-Les-veritables-debuts-de-la-revue-quebecoise-iLAnnuaire-theatrali-onenineeightseven-pthreeninesevenzerospan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13187
urlTitle: ""
categories:
  - 54
  - 41
  - 37

---
    
<!-- pas de contenu sur cette page -->
