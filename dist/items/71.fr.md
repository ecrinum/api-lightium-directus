---
title: <span>Jean-Marc Larrue, « Compte rendu. “Le Burlesque québécois et américain” de Chantal Hébert », <i>Revue d’histoire de l'Amérique française</i>, vol. 43 / 2, 1989, p. 267‑268.</span>
short: En 1981, Chantal Hébert consacrait un premier ouvrage au théâtre burlesque du Québec. Il s'agissait d'une version modifiée de son mémoire de maîtrise. Elle y révélait l'apport et l'importance du burlesque dans l'histoire théâtrale locale. Dans Le Burlesque québécois et américain, que viennent de publier les Presses de l'Université Laval et qui est sa thèse de doctorat, Chantai Hébert nous entraîne dans la dramaturgie burlesque en nous proposant une analyse comparée d'oeuvres québécoises et américaines caractéristiques de ce genre populaire. [...]
cleanstring: spanJean-Marc-Larrue-Compte-rendu-Le-Burlesque-quebecois-et-americain-de-Chantal-Hebert-iRevue-dhistoire-de-lAmerique-francaisei-volforthreetwo-onenineeightnine-ptwosixseventwosixeightspan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13192
urlTitle: ""
categories:
  - 54
  - 41
  - 37

---
    
<!-- pas de contenu sur cette page -->
