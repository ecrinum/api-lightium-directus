---
title: <span>Jean-Marc Larrue, « Le théâtre expérimental et la fin de l’unique », <i>Jeu</i>, 1989, p. 64‑72.</span>
short: Bien sûr le théâtre est toujours expérimental, même le théâtre le plus conventionnel, le plus bourgeois, le plus reproducteur. Il est toujours expérimental car il y a toujours, au théâtre comme dans tous les autres arts de création ou de reprise, une forme minimale d'expérimentation. Il ne faut donc pas entendre expérimental au sens strict du dictionnaire, il ne faut surtout pas séparer l'épithète de son substantif. Si tout le théâtre est, à divers degrés, expérimental, ce qu'il est convenu d'appeler le «théâtre expérimental» — pour éviter toute confusion, il vaudrait mieux écrire «Théâtre Expérimental» — recouvre une pratique relativement circonscrite dans le temps. [...]
cleanstring: spanJean-Marc-Larrue-Le-theatre-experimental-et-la-fin-de-lunique-iJeui-onenineeightnine-psixforseventwospan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13193
urlTitle: ""
categories:
  - 54
  - 41
  - 37

---
    
<!-- pas de contenu sur cette page -->
