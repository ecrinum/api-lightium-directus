---
title: <span>Jean-Marc Larrue, « Compte rendu. « Veilleurs de nuit. Saison théâtrale 1988-1989 » de David Gilbert », <i>L’Annuaire théâtral</i>, 1990, p. 103‑106.</span>
short: La revue les Herbes rouges a donné naissance à la maison d'édition Les Herbes rouges, il y a une dizaine d'années, et a créé une collection «Théâtre» que dirige Gilbert David. La collection compte surtout des oeuvres dramatiques et a déjà recruté des dramaturges d'importance (Claude Poissant, Normand Canac-Marquis). La maison d'édition, à ce qu'on dit, va accueillir de plus en plus de textes critiques et théoriques sur le théâtre. Nous ne pouvons que nous en réjouir et souhaiter que l'entreprise réussisse. [...]
cleanstring: spanJean-Marc-Larrue-Compte-rendu-Veilleurs-de-nuit-Saison-theatrale-onenineeighteight-onenineeightnine-de-David-Gilbert-iLAnnuaire-theatrali-onenineninezero-ponezerothreeonezerosixspan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13195
urlTitle: ""
categories:
  - 54
  - 41
  - 37

---
    
<!-- pas de contenu sur cette page -->
