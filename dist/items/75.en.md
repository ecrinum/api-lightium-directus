---
title: <span>Jean-Marc Larrue, « Contre », <i>Jeu</i>, 1990, p. 141‑144.</span>
short: Pas facile de monter une pièce de Heiner Müller, surtout si, comme le fait Gilles Maheu, on choisit parmi ses textes les plus courts et les moins dramatiques. Il y a trois ans, Maheu montait le bref Hamlet-Machine ; cette fois encore, l'oeuvre fait à peine quinze pages. À la lecture, même en paressant, on boucle le cycle de Médée en vingt minutes. Bien court pour un spectacle au bout du monde! [...]
cleanstring: spanJean-Marc-Larrue-Contre-iJeui-onenineninezero-poneforoneoneforforspan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13197
urlTitle: ""
categories:
  - 54
  - 41
  - 37

---
    
<!-- pas de contenu sur cette page -->
