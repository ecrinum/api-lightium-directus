---
title: <span>Jean-Marc Larrue, « De l’expérience collective à la découverte des cycles », <i>L’Annuaire théâtral</i>, 1990, p. 9‑30.</span>
short: C'est en avril 1980 que Jacques Lessard rassemble autour de lui quelques diplômés du Conservatoire d'art dramatique de Québec et jette les bases de ce qui allait devenir l'une des plus remarquables entreprises théâtrales du Québec des années 80. Le Théâtre Repère, tout comme le groupe Carbone 14, le Nouveau Théâtre expérimental, la troupe Omnibus, le Théâtre expérimental des femmes et le groupe La Veillée, s'inscrit dans ce que, faute d'appellation officielle, on pourrait qualifier de courant postmoderne. [...]
cleanstring: spanJean-Marc-Larrue-De-lexperience-collective-a-la-decouverte-des-cycles-iLAnnuaire-theatrali-onenineninezero-pninethreezerospan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13198
urlTitle: ""
categories:
  - 54
  - 41
  - 37

---
    
<!-- pas de contenu sur cette page -->
