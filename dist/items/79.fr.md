---
title: "<span>André-Gilles Bourassa et Jean-Marc Larrue, « Le Monument National (1893-1923) : trente ans de théâtre dans la salle Ludger-Duvernay », <i>L’Annuaire théâtral</i>, 1991, p. 69‑100.</span>"
short: En 1993, le Monument National aura cent ans. À l'aube de ce centenaire, qui correspond sensiblement à celui du théâtre professionnel francophone local, et alors que le Monument subit une importante cure de rajeunissement, l'occasion est belle de rappeler les faits saillants de cette institution à la carrière tourmentée. [...]
cleanstring: spanAndre-Gilles-Bourassa-et-Jean-Marc-Larrue-Le-Monument-National-oneeightninethree-oneninetwothree-trente-ans-de-theatre-dans-la-salle-Ludger-Duvernay-iLAnnuaire-theatrali-oneninenineone-psixnineonezerozerospan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13202
urlTitle: ""
categories:
  - 54
  - 41
  - 37

---
    
<!-- pas de contenu sur cette page -->
