---
title: Technes
short: The CRC on Digital Textualities is associated with the Technès project in order to rethink cinema and its techniques, both historical and digital.
cleanstring: Technes
lang: EN
caption: ""
image: http://grafics.ca/wp-content/uploads/2015/07/Techne%CC%80s.jpg
url: ""
urlTitle: ""
categories:
  - 2
  - 4
  - 8
  - 12
  - 19
  - 22
  - 23
  - 24
  - 33

---
    
The entry of cinema in the third millenium has been produced under the sign of a technological paradigmatic shift. The "digital revolution" has provoked profound changes: the film is on the edge of disappearing, the cinemas open themselves to the live-projection of operas, and films are more and more frequently viewed on what one might call "nomadic devices". The boundaries between média are now more and more diffuse and the "exploded cinema" of today approaches more and more the universes of television, videos, Web and other digital platforms.

Further information on the [GRAFICS website](http://grafics.ca/projets/).
