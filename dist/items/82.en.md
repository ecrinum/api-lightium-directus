---
title: <span>Jean-Marc Larrue, « La scénographie professionnelle au Québec (1870-1990) ou la quête historique d’un pouvoir et d'une reconnaissance », <i>L’Annuaire théâtral</i>, 1992, p. 103‑136.</span>
short: Jamais les scénographes du Québec — appelons-les ainsi par commodité — n'ont autant fait parler d'eux! Ils sont partout et s'affirment avec toute la vigueur de ceux qui sont convaincus du bien fondé de leur cause et qui réclament enfin leur dû. Qui pourrait, qui oserait les en blâmer? D'expositions en colloques, d'émissions radiophoniques en numéros de revues spécialisées, ils imposent, individuellement ou par le biais de leur Association (des professionnels des arts de la scène du Québec), une nouvelle donne au monde du théâtre québécois dont ils constituent désormais l'une des forces majeures. [...]
cleanstring: spanJean-Marc-Larrue-La-scenographie-professionnelle-au-Quebec-oneeightsevenzero-onenineninezero-ou-la-quete-historique-dun-pouvoir-et-dune-reconnaissance-iLAnnuaire-theatrali-onenineninetwo-ponezerothreeonethreesixspan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13205
urlTitle: ""
categories:
  - 54
  - 41
  - 37

---
    
<!-- pas de contenu sur cette page -->
