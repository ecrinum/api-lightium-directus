---
title: <span>Jean-Marc Larrue, « Sur les scènes montréalaises », <i>Cap-aux-Diamants</i>, 1993, p. 32‑37.</span>
short: |-
  Si le théâtre québécois est né avec Michel Tremblay et s'est émancipé avec Robert
  Lepage, il y a longtemps que les foules se pressent dans les nombreuses salles de la
  métropole pour assister à des spectacles dramatiques. Le théâtre anglophone a
  aussi connu ses heures de gloire, notamment dans la communauté yiddishophone
  de la «Lower Main».
cleanstring: spanJean-Marc-Larrue-Sur-les-scenes-montrealaises-iCap-aux-Diamantsi-onenineninethree-pthreetwothreesevenspan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13196
urlTitle: ""
categories:
  - 54
  - 41
  - 37

---
    
<!-- pas de contenu sur cette page -->
