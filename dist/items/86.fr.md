---
title: <span>Jean-Marc Larrue, « Le cinéma des premiers temps à Montréal et l’institution du théâtre », <i>Cinémas</i>, vol. 6 / 1, 1995, p. 119‑131.</span>
short: Cet article propose un tableau précis des liens étroits qui unirent le cinéma des premiers temps à l’institution théâtrale montréalaise existante. À cette époque, le cinéma joue un rôle central dans les stratégies d’émergence du théâtre francophone qui s’éloigne alors du théâtre anglophone, son rival issu de la ville de New York, tout autant qu’il joue un rôle d’attrait dans un contexte de vive concurrence. Non seulement cela donne-t-il lieu à des scènes mixtes (théâtrales et cinématographiques), mais également à une interprétation plus profonde de deux arts qui aboutit aux premières manifestations d’un cinéma-théâtre québécois.
cleanstring: spanJean-Marc-Larrue-Le-cinema-des-premiers-temps-a-Montreal-et-linstitution-du-theatre-iCinemasi-volsixone-onenineninefive-poneonenineonethreeonespan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13207
urlTitle: ""
categories:
  - 54
  - 41
  - 37

---
    
<!-- pas de contenu sur cette page -->
