---
title: <span>Jean-Marc Larrue, « Les paradoxes du lieu », <i>L’Annuaire théâtral</i>, 1995, p. 9‑16.</span>
short: Si le lieu a toujours occupé une place capitale dans la pratique théâtrale, l'intérêt des chercheurs à son endroit s'est manifesté bien tardivement. Les premières réflexions marquantes sur le sujet datent de la Renaissance italienne. [...]
cleanstring: spanJean-Marc-Larrue-Les-paradoxes-du-lieu-iLAnnuaire-theatrali-onenineninefive-pnineonesixspan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13208
urlTitle: ""
categories:
  - 54
  - 41
  - 37

---
    
<!-- pas de contenu sur cette page -->
