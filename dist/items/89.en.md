---
title: "<span>Jean-Marc Larrue, « Le burlesque québécois : l’avant-garde version « peuple » », <i>Jeu</i>, 2002, p. 87‑98.</span>"
short: Tout le monde a entendu parler du burlesque, mais cette pratique reste le parent pauvre du champ théâtral. D'abord, parce qu'il a été longtemps discrédité, au Québec comme ailleurs. Mais aussi parce qu'on a peine à le définir et à le distinguer d'autres pratiques nord-américaines de la même époque, tels que les spectacles de variétés, le vaudeville américain et les revues. [...]
cleanstring: spanJean-Marc-Larrue-Le-burlesque-quebecois-lavant-garde-version-peuple-iJeui-twozerozerotwo-peightsevennineeightspan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13210
urlTitle: ""
categories:
  - 54
  - 41
  - 37

---
    
<!-- pas de contenu sur cette page -->
