---
title: Anthologie Palatine
short: "Dans la foulée des avancées récentes réalisées dans les « digital classics », la CRC sur les écritures numériques est en train de mettre sur pied une plateforme web consacrée à l’Anthologie Palatine. "
cleanstring: Anthologie-Palatine
lang: FR
caption: ""
image: http://blog.sens-public.org/marcellovitalirosati/wp-content/uploads/sites/2/2017/03/9.png
url: ""
urlTitle: ""
categories:
  - 2
  - 4
  - 8
  - 13
  - 19
  - 22
  - 26
  - 35
  - 38
  - 39
  - 49
  - 52
  - 58

---
    
![](http://blog.sens-public.org/marcellovitalirosati/wp-content/uploads/sites/2/2017/03/9.png)   
  
L’équipe de la CRC sur les écritures numériques, en collaboration avec Elsa Bouchard, a entrepris la création d’une plateforme web consacrée à l’Anthologie Palatine, un recueil byzantin d’épigrammes grecques dont l’ancêtre remonte à l’époque hellénistique (323-30 av. J.-C).  
Nous avons créé une base de données ouverte, interrogeable via une API qui permet de :
1. Transcrire le manuscrit
2. Proposer des traductions
3. Aligner les traductions
4. Transcrire les scholies
5. Mettre en relation les épigrammes entre elles
  
Ce projet de plateforme entend répondre à trois objectifs :
1. **Euristique** : mettre en évidence les relations intertextuelles entre les épigrammes. Permettre aux hellénistes de travailler avec le contenu de l’AP de façon non linéaire et d’évoluer à travers le corpus suivant des marqueurs codés (auteur, thèmes et autres mots-clés) directement inspirés des annotations marginales du manuscrit de l’AP.
2. **Édition et traduction** : donner accès simultanément au texte original grec (images du manuscrit à l’appui), à une nouvelle traduction française et une traduction inédite des commentaires marginaux du manuscrit de l’AP (scholies, intertitres, etc.). Cette traduction reflétera l’état des recherches récentes sur le contexte culturel dans lequel ces poèmes ont été composés.
3. **Technique** : concevoir une plateforme mutualisable pour la valorisation et la diffusion des textes anciens. Identification des exigences et des caractéristiques pour la création d’une plateforme destinée à l’édition critique.
  
Le manuscrit de l’Anthologie (le _Codex Palatinus 23_) a été retrouvé en 1606 par Claude Saumaise à Heidelberg. Ces poèmes ont dès lors exercé une influence majeure sur la littérature de la Renaissance jusqu’à aujourd’hui. Le manuscrit est aujourd’hui conservé à la bibliothèque de l’université d’Heidelberg, où il a été numérisé et rendu disponible dans la collection numérique de la bibliothèque. L’Anthologie telle que nous la possédons aujourd’hui résulte de compilations successives, à chaque fois modifiées, additionnées et réarrangées par les compilateurs. Elle dérive ultimement d’une collection constituée vers 100 av. J.-C. par le poète Méléagre de Gadara. Cette collection, intitulée La Couronne, fut arrangée par Méléagre non pas de façon aléatoire mais bien suivant certains principes organisationnels – au premier chef les thèmes littéraires présents dans les épigrammes – qui ont été mis en lumière par des recherches récentes.  
  
La retraduction et la publication en ligne de l’anthologie nous apparaît essentielle à double titre. D’une part, le projet donnera une nouvelle visibilité et une meilleure accessibilité à une œuvre dont la traduction est aujourd’hui obsolète. D’autre part, il permet d’envisager de façon inédite des problématiques contemporaines propres aux humanités numériques. Selon Milad Doueihi en effet, la notion d’anthologie est au cœur des considérations de notre époque, dite « numérique ». Milad Doueihi développe cette idée dans son ouvrage Pour un humanisme numérique, dans lequel il note que la pratique anthologique, par son nécessaire processus de sélection qui se veut exhaustif, représentatif d’une totalité, « instaure une grille conceptuelle façonnée par la dynamique de la réception et par le savoir spécifique lié aux textes circulant sur le réseau et aux autorités qui leur sont associées ». Or, à notre sens, le manuscrit de l’AP répond précisément à cette dynamique de réception. Bien que par la force du support, les textes se trouvent classés les uns à la suite des autres, les notes et commentaires laissés en marge par les lecteurs rappellent que la conception d’une anthologie repose aussi sur un système sémantique interne, un dialogue entre les textes qui s’étend au-delà de leur simple proximité et qui les relie à même leur contenu. L’étude des collections d’épigrammes révèle l’existence d’un réseau de motifs et de figures représentant un univers culturel complexe et riche de liens intertextuels. La prise en compte de ces renvois textuels est fondamentale pour comprendre cet univers culturel et les dispositifs numériques sont particulièrement adaptés pour mettre en évidence des connexions autrement difficiles à repérer. C’est sur ces réflexions que prend appui notre projet, dont l’objectif est de produire, à l’aide d’un classement non linéaire, des parcours de lecture qui ne soient plus régis par la matérialité du support, mais par des liens sémantiques développés par les textes eux-mêmes.  
Le projet proposé ici concerne une sélection des livres 4, 5 et 7 de l’Anthologie Palatine. Cette sélection initiale reposera sur un critère chronologique : les épigrammes retenues seront celles attribuables à Méléagre et aux auteurs dont il s’est servi, autrement dit les textes remontant au plus tard à la période hellénistique. Une telle sélection permettra un examen précis du travail fait par le complilateur de la Couronne.  
  
Compte tenu de ce contexte, la philosophie de la plateforme repose sur les trois critères suivants :
* _Mise en valeur du projet anthologique_. La plateforme doit permettre de valoriser la construction raisonnée propre à l’anthologie, telle que les anciens l’ont autrefois pensée. Il sera par exemple possible d’attribuer des balises à chaque élément textuel pour que sa signification soit comprise par la machine. Par exemple, nous pourrons baliser le mot « Gadara » avec une étiquette précisant qu’il s’agit d’une ville, et qui indique ses coordonnées géographiques. Cela permettra de visualiser sur une carte le lieu situé dans l’épigramme étudiée. Ce procédé peut être appliqué aux noms propres, aux thématiques traitées ainsi qu’aux formes linguistiques et métriques. L’objectif est simple : chaque balise devient à son tour une liste de lecture, permettant de circuler dans l’anthologie, par lieu, date, forme métrique, nom etc.
* _Interopérabilité et standardisation_. Les textes et les contenus seront décrits en utilisant des systèmes standards de balisage (_TEI_) et mis à disposition sur une base de données ouverte de façon à être exploitable par l’ensemble de la communauté de chercheurs et par un nombre élevé de plateformes.
* _Libre accès_. Enfin, notre projet s’inscrit à double titre dans une politique de libre accès. D’une part, notre travail de transcription et de traduction sera accessible gratuitement, de façon à faciliter le travail des chercheurs, mais aussi à faire connaître l’anthologie auprès du grand public. D’autre part, le code de notre plateforme sera en open-source, de façon à faciliter la réalisation de projets similaires au nôtre, et de mutualiser les expertises techniques.
  
  
[Site officiel](http://anthologiegrecque.org/fr%5FFR/#!index.md) du projet de la CRC sur l'Anthologie Palatine.  
  
[Plateforme](https://anthologia.ecrituresnumeriques.ca/home) d'édition collaborative de l'Anthologie Palatine.  
  
[Billet de blog](http://blog.sens-public.org/marcellovitalirosati/pour-une-edition-participative-de-lanthologie-palatine/) de Marcello Vitali-Rosati sur l'Anthologie Palatine.  
  
Article consacré à notre projet sur le blogue de recherche [Modéliser et virtualiser](https://movi.hypotheses.org/237)   
  
[Projet _Hetic_ de visualisation de quelques épigrammes de l'Anthologie Palatine](http://anthologie.valentin-crochemore.fr/#!/accueil).  
  
Projet développé grâce au soutien du CRSH - Conseil de recherches en sciences humaines
