---
title: <span>Jean-Marc Larrue, « Hervé Guay L’éveil culturel – Théâtre et presse à Montréal, 1898-1914 », <i>Theatre Research in Canada / Recherches théâtrales au Canada</i>, vol. 34 / 1, 2013, p. 139‑142.</span>
short: Issu d’une thèse de doctorat soutenue en 2005, l’essai L’éveil culturel—Théâtre et presse à Montréal, 1898-1914 que Hervé Guay a publié aux Presses de l’Université de Montréal en 2010, se lit presque comme un roman, ce qui indique que cette mutation de la thèse vers l’essai, souvent hasardeuse, a ici parfaitement réussi. On apprécie la précision de l’information foisonnante et la langue élégante et fluide dans laquelle elle est livrée. L’étude de Guay porte sur la critique théâtrale à une période qui est souvent considérée comme un premier âge d’or du théâtre à Montréal, qu’il soit anglophone ou francophone (ou même yiddishophone). [...]
cleanstring: spanJean-Marc-Larrue-Herve-Guay-Leveil-culturel---Theatre-et-presse-a-Montreal-oneeightnineeight-onenineonefor-iTheatre-Research-in-Canada--Recherches-theatrales-au-Canadai-volthreeforone-twozeroonethree-ponethreenineonefortwospan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13211
urlTitle: ""
categories:
  - 54
  - 41
  - 37

---
    
<!-- pas de contenu sur cette page -->
