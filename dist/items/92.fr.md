---
title: <span>Marcello Vitali-Rosati, « Interpretazione in una logica n-dimensionale di Metafisica 4,4 », <i>Teoria</i>, vol. 23 / 2, février 2003, p. 89‑96.</span>
short: Il Principio di non contraddizione (PNC) così come tradizionalmente inteso sembra minare alla base la possibilità di un'alterità « assoluta », di un'alterità, cioè che non sia in nessun modo riconducibile ad un'ipseità che la conosca. [...]
cleanstring: spanMarcello-Vitali-Rosati-Interpretazione-in-una-logica-n-dimensionale-di-Metafisica-forfor-iTeoriai-voltwothreetwo-fevrier-twozerozerothree-peightnineninesixspan
lang: FR
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13213
urlTitle: ""
categories:
  - 54
  - 41
  - 22
  - 17
  - 15

---
    
<!-- pas de contenu sur cette page -->
