---
title: <span>Teresa Dobson, Michael E. Sinatra, Stan Ruecker[et al.], « Citation Rhetoric Examined », <i>Proceedings of the 2010 Digital Humanities Conference</i>, 2010, p. 7‑10.</span>
short: In his influential monograph «The Rhetoric of Citation Systems», Connors (1999) elaborates on the principle that scholars working with different forms of citation find themselves thinking differently, since the citation format has natural consequences in the way it interacts with the material in the practice of the writer.
cleanstring: spanTeresa-Dobson-Michael-E-Sinatra-Stan-Rueckeret-al-Citation-Rhetoric-Examined-iProceedings-of-the-twozeroonezero-Digital-Humanities-Conferencei-twozeroonezero-psevenonezerospan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13217
urlTitle: ""
categories:
  - 54
  - 41
  - 24
  - 5

---
    
<!-- pas de contenu sur cette page -->
