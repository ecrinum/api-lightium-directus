---
title: <span>Michael E. Sinatra, « Shelley’s Editing Process in the Preface to Epipsychidion », <i>The Keats-Shelley Review</i>, vol. 11 / 1, 1997, p. 167‑181.</span>
short: |-
  Prefaces are often disregarded by readers who, more often than not, start
  without taking time to peruse them first. Sir Walter Scott knew this
  perfectly well, and he wrote about it, very wittily, in 'A PostScript Which
  Should Have Been a Preface', the last chapter of his novel Waverley
  written in 1814: 'most novel readers, as my own conscience reminds me,
  are apt to be guilty of the sin of omission respecting the same matter of
  prefaces' . Scott refers to novel readers but poetry readers are also 'guilty
  of the sin of omission', maybe even more so in so far as they may wish,
  understandably enough, to read only poetry and not a prose introduction.
  Many critics include prefaces in their analysis, but most of the time only as
  a means of interpreting the work they precede. Thus critics limit the role of
  prefaces simply to introductory materials and exclude any other potential
  interpretation. It is sometimes forgotten that the very presence or absence
  of a preface is already pregnant with meaning. [...]
cleanstring: spanMichael-E-Sinatra-Shelleys-Editing-Process-in-the-Preface-to-Epipsychidion-iThe-Keats-Shelley-Reviewi-voloneoneone-oneninenineseven-ponesixsevenoneeightonespan
lang: EN
caption: ""
image: ""
url: https://papyrus.bib.umontreal.ca/xmlui/handle/1866/13514
urlTitle: ""
categories:
  - 54
  - 41
  - 24

---
    
<!-- pas de contenu sur cette page -->
