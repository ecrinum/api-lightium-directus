---
title: Stylo
short: "<h2>Redesigning the digital editing workflow for scholarly journals in the social sciences</h2>\r\n"
cleanstring: Stylo
lang: EN
caption: ""
image: http://vitalirosati.net/slides/img/wt2.png
url: ""
urlTitle: ""
categories: null

---
    
**Use Stylo online at [stylo.ecrituresnumeriques.ca](https://stylo.ecrituresnumeriques.ca/)**

Documentation at [stylo-doc.ecrituresnumeriques.ca](https://stylo-doc.ecrituresnumeriques.ca/)

### Context

The philosophy of STYLO builds on the simple fact that writers' skills are semantical rather than graphical, whereas authors are often asked to deliver graphical layout rather than enriched text.

When submitting a text, authors transcribe bibliographical references, footnotes, titles, keywords with simple graphical markup - emphasized, underlined, italic, indentation, quotation marks, etc. This graphical markup is neither specific nor explicit, resulting in a low level of scientificity.

Further, in the editorial process, this graphically formated text must be reinterpreted by the editors, converting graphical markup into a semantical one. This two-way process leads to a serious loss of time for editors and a problematic loss of meaning for the academic community.

![meaning](https://i.imgur.com/qwS2rvh.png) 

### Project

STYLO addresses these issues with an ambition to change the content production workflow of the social science academic journals.

Think of STYLO as a semantic writing tool that aims at improving the scholarly edition and publication process by putting the production of semantic markup into the hands of the academic writers themselves.

It is designed as a WYSYWYM text editor ("what you see is what you mean") providing a simple environment dedicated to writing with intra-textual semantic markup possibilities. The interface of the editor adapts to each step of the workflow - from writing to editing and authoring - in order to offer to the users, whether it be the author, the editor, or the distributor, different sets of metadatas.

Stylo assists the markup process thanks to on-the-fly alignment with online authorities (LOC, Rameau, Wikidata, Orcid, etc.).

In order to avoid the frequent data breaches during the editing process, Stylo offers a continuous workflow from writing to diffusion, assuring that semantic information given by the author will indeed be published.

### Workflow & Outputs

Stylo can provide multiple outputs depending on the needs :

1. XML (Erudit, JATS, TEI, etc.) for diffusion and harversting platforms
2. HTML5 for direct publication on CMS
3. PDF&EPUB with programmable stylesheets

![](https://i.imgur.com/ssUfJFR.png)
