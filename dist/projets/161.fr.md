---
title: Réécrire la Transcanadienne à l'ère numérique
short: Comment penser l’espace à l’époque du numérique ? Ce projet a pour objectif d’explorer cette question en proposant des pistes de réflexion à la fois théoriques et pratiques. L'espace mythique de la Transcanadienne constituera tout autant le terrain d’étude que le terrain d’exploration de notre recherche.
cleanstring: Reecrire-la-Transcanadienne-a-lere-numerique
lang: FR
caption: ""
image: http://www.infrastructure.gc.ca/images/Edmonton-742.jpg
url: ""
urlTitle: ""
categories: null

---
    
![](http://www.infrastructure.gc.ca/images/Edmonton-742.jpg) 

La transcanadienne, cette route mythique qui traverse le Canada de la côte ouest à la côte est, a donné lieu à une série de productions médiatiques : des récits littéraires, des images, des vidéos, des cartes, des textes d’histoire, des données numériques (cartes numériques, fiche Wikipédia, etc.). En mai 2016, notre laboratoire de recherche (le Theolinum, laboratoire de la Chaire de recherche du Canada sur les écritures numériques) va traverser le Canada en suivant la transcanadienne, afin d’ajouter à l’ensemble de ces médiations notre propre expérience de voyage. La question que nous voulons poser est la suivante : quelle est la nature de cet espace ? Que représente-t-il et comment le pensons-nous ?

En effet, aujourd’hui, un premier réflexe consiste à chercher toutes nos informations sur le web, qui regroupe des renseignements historiques, des images, des récits, mais aussi des cartes. Ainsi, cet espace au demeurant relativement vide sur une carte traditionnelle, lorsqu’il est expérimenté à la fois physiquement par le voyage et médiatement par le web, se « remplit » très vite d’une série de valeurs, de symboles, de références cinématographiques et littéraires, de récits historiques et patriotiques. La question qui nous anime est alors la suivante : serait-il possible, pour emprunter l’idée de Perec qui tenta autrefois l’expérience à Paris, d’épuiser cet espace à la fois géographique et imaginaire de la Transcanadienne ? Sur un plan davantage théorique, que signifierait cet épuisement ? Procéderait-il en effet d’une forme d’hyper-représentation – un compte rendu dans les moindres détails de ce qui se trouve factuellement dans cet espace - ou bien au contraire d’une activité de production du réel ?

Pour répondre à ces questions, nous proposons de révéler l’espace de la transcanadienne en le questionnant à plusieurs niveaux. D’une part, nous en ferons l’expérience par le biais d’un voyage en voiture de Montréal à Calgary. Nous documenterons et nous raconterons notre traversée à l’aide d’une série d’outils numériques (écriture en ligne, photos, vidéos).

L’hypothèse que nous souhaitons démontrer est la suivante : le numérique constitue autant un outil de représentation que de production de l’espace. Il nous semble indéniable, en effet, que notre regard et notre expérience d’un territoire tel que celui de la Transcanadienne – de même que notre façon de l’organiser en l’écrivant – sont profondément structurés par les outils numériques et leurs agencements : les différentes sources, les cartes interactives, et l’ensemble des informations, données et documents que nous trouvons sur le web. En d’autres termes, notre façon de penser cet espace et de l’envisager passe par la médiation (souvent inconsciente) des outils qui le cartographient, le décrivent et le racontent en le remplissant d’images, de récits et d’informations.

 Consulter les travaux réalisés par l'équipe pendant la traversée :  
 Le [ Carnet de voyage ](http://blog.sens-public.org/marcellovitalirosati/transcan16-j-1/) de Marcello Vitali-Rosati  
 Le [ Storify ](https://storify.com/ENumeriques/epuisement-de-la-transcanadienne) de Marie-Christine Corbeil  
 Les [Postcards From Google Street view](http://transcanproject.tumblr.com) de Servanne Monjour  
 La [carte augmentée](https://umap.openstreetmap.fr/en/map/epuisement-de-la-transcanadienne%5F86272#6/48.655/-82.090) sur Open Street Map  
 Les [photographies](https://www.flickr.com/photos/142996315@N08/collections/) d'Erwan Geffroy  
 Notre [ présentation](http://blog.sens-public.org/marcellovitalirosati/transcan16-j-5/) enregistrée à Calgary
