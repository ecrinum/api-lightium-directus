---
title: Collection Parcours numériques
short: "La collection <a href=\"http://parcoursnumeriques-pum.ca\">Parcours numériques</a> a pour objectif de développer une réflexion théorique approfondie et savante sur le monde numérique, en produisant des textes de référence en français qui peuvent enrichir le débat et servir de guide dans les pratiques, tout en expérimentant de nouvelles formes d’éditorialisation.\r

  \ "
cleanstring: Collection-Parcours-numeriques
lang: FR
caption: ""
image: https://crihn.files.wordpress.com/2013/10/parcoursnumeriques.png
url: ""
urlTitle: ""
categories: null

---
    
![](http://www.parcoursnumeriques-pum.ca/local/cache-vignettes/L173xH260/pn-pratiques_ed-couv_seul-1d41c.jpg) 

La collection « Parcours numériques », publiée aux Presses de l’Université de Montréal, a pour objectif de développer une réflexion théorique approfondie et savante sur le monde numérique. Elle publie des textes de référence en français qui viennent enrichir le débat théorique sur les nouvelles technologies, tout en expérimentant au niveau pratique de nouvelles formes d’éditorialisation. Ainsi, tous les textes de la collection sont publiés à la fois en formats papier, numérique et numérique augmenté. l’édition augmentée étant disponible en accès libre sur le web. L’édition numérique augmentée est en accès libre sur la plateforme de la collection, les versions papier et numérique homothétique (ePub et pdf) sont payantes.

Directeurs de la collection : Michael E. Sinatra et Marcello Vitali-Rosati  
Éditeur : Les Presses de l’Université de Montréal (PUM)

Déjà disponibles :  
[Tous artistes !](http://parcoursnumeriques-pum.ca/tousartistes) de Sophie Limare, Annick Girard et Anaïs Guilet, 2017.  
[Expérimenter les humanités numériques](http://parcoursnumeriques-pum.ca/experimenterleshumanitesnumeriques) Sous la direction de Étienne Cavalié, Frédéric Clavert, Olivier Legendre et Dana Martin, 2017.  
[Adophobie](http://parcoursnumeriques-pum.ca/adophobie) de Jocelyn Lachance, 2016.  
[Les MOOCs](http://parcoursnumeriques-pum.ca/lesmoocs) de Pablo Achard, 2016.  
[Surveiller et sourire](http://www.parcoursnumeriques-pum.ca/surveilleretsourire) de Sophie Limare, 2015.  
[Virus, parasites et ordinateurs](http://www.parcoursnumeriques-pum.ca/virusparasitesetordinateurs) d'Ollivier Dyens, 2015.  
[Mémoires audiovisuelles](http://www.parcoursnumeriques-pum.ca/memoiresaudiovisuelles) de Matteo Treleani, 2014.  
[Âme et Ipad](http://www.parcoursnumeriques-pum.ca/ameetipad) de Maurizio Ferraris, 2014.  
[Pratiques de l'édition numérique](http://www.parcoursnumeriques-pum.ca/pratiques) sous la direction de Marcello Vitali-Rosati et Michael Sinatra, 2014.

Accéder au [site de la collection Parcours numériques](http://parcoursnumeriques-pum.ca)
