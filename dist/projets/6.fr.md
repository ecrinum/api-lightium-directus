---
title: Lightium, un CMS super simple
short: Lightium est un CMS simple d'utilisation, conçu pour présenter et valoriser le travail des chercheurs.
cleanstring: Lightium-un-CMS-super-simple
lang: FR
caption: ""
image: ""
url: ""
urlTitle: ""
categories: null

---
    
Le site que vous consultez actuellement a été conçu avec Lightium !  
Vous pouvez vous aussi installer Lightium, disponible sur le [Github de la Chaire](https://github.com/EcrituresNumeriques).  
Pour plus d'informations, consulter [le site du CMS Lightium](http://lightium.org/fr/).
