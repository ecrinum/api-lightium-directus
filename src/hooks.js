/** @type {import('@sveltejs/kit').Handle} */
export async function handle({ event, resolve }) {
  const { params } = event;
  const { lang } = params;

  /**
   * Établir l’attribut `lang` du HTML.
   * Cette fonction suppose que la chaîne `%lang%` figure dans le HTML de la page.
   */
  const response = await resolve(event, {
    transformPageChunk: ({ html }) => html.replace('%lang%', lang ? lang : ''),
  });

  return response;
}
