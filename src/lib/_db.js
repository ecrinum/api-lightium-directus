import sqlite3 from 'sqlite3';
import { open } from 'sqlite';

const dbPath = 'db.sqlite3'; // path relative to root

export async function openDB() {
  return await open({
    filename: dbPath,
    driver: sqlite3.Database
  });
}
