import { createDirectus, authentication, rest } from '@directus/sdk';
/** @type {String} */
import { PUBLIC_DIRECTUS_URL } from '$env/static/public';

// warn if no directus url is set, this can ease debugging early
if (!PUBLIC_DIRECTUS_URL) {
  console.warn('Variable d’environnement "PUBLIC_DIRECTUS_URL" non trouvée, est-ce que le fichier `.env` a été créé et rempli?');
}

/** @type {import('@directus/sdk').Directus} */
export const directus = createDirectus(PUBLIC_DIRECTUS_URL).with(authentication()).with(rest());
