import { openDB } from '$lib/_db';

export async function getIndex(idProp, tableName, itemProps, params) {

  const { id, language } = params;
  let item = {};
  const db = await openDB();
  const item_query =
      `
    SELECT
      ${itemProps.join(', ')}
    FROM ${tableName}
    WHERE id_cat = ${id}
    `;

  await db.each(item_query, [], (err, row) => {

    if (err) {
      throw new Error('Erreur @ db each', err);
    }

    let _item = {};

    _item[idProp] = id

    itemProps.forEach(prop => {
      _item[prop] = Buffer(row[prop] || []).toString('utf8');
    });

    // expects lang prop
    let _language = _item.lang.toLowerCase();

    item[_language] = _item;
  });

  return {
    body: JSON.stringify(item[language]),
    headers: {
      'content-type': 'application/json'
    }
  }
}
