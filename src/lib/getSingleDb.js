import { openDB } from '$lib/_db';

export async function getSingle(idProp, tableName, itemProps, params) {

  const { id, language } = params;
  let item = {};
  const db = await openDB();
  const item_query =
      `
    SELECT
      ${itemProps.join(', ')}
    FROM ${tableName}
    WHERE ${idProp} = ${id}
    `;

  await db.each(item_query, [], (err, row) => {

    if (err) {
      throw new Error('Erreur @ db each', err);
    }

    let _item = {};

    // make int
    _item[idProp] = Number(id);

    // deal w/ detabase read buffer
    itemProps.forEach(prop => {
      _item[prop] = Buffer(row[prop] || []).toString('utf8');
    });

    // expects lang prop
    let _language = _item.lang.toLowerCase();

    item[_language] = _item;
  });

  return item[language];
}
