export const statuts = [
  {
    translations: [{
      "languages_code": "fr-CA",
      "title": "Professeur titulaire"
    },
      {
        "languages_code": "en-CA",
        "title": "Full professor"
      }]
  },
  {
    translations: [{
      "languages_code": "fr-CA",
      "title": "Chercheur associé"
    },
      {
        "languages_code": "en-CA",
        "title": "Fellow Researcher"
      }]
  },
  {
    translations: [{
      "languages_code": "fr-CA",
      "title": "Professeure associée"
    },
      {
        "languages_code": "en-CA",
        "title": "Fellow Researcher"
      },
      {
        "languages_code": "en-CA",
        "title": "Candidat au doctorat"
      },
      {
        "languages_code": "en-CA",
        "title": "PhD Candidate"
      }]
  },
  {
    translations: [{
      "languages_code": "fr-CA",
      "title": "Candidate au doctorat"
    },
      {
        "languages_code": "en-CA",
        "title": "PhD Candidate"
      }]
  },
  {
    translations: [{
      "languages_code": "fr-CA",
      "title": "Étudiant à la maîtrise"
    },
      {
        "languages_code": "en-CA",
        "title": "Master’s Studient"
      }]
  },
  {
    translations: [{
      "languages_code": "fr-CA",
      "title": "Professeur adjoint"
    },
      {
        "languages_code": "en-CA",
        "title": "Assistant Teacher"
      }]
  },
  {
    translations: [{
      "languages_code": "fr-CA",
      "title": "Professeure adjointe"
    },
      {
        "languages_code": "en-CA",
        "title": "Assistant professor"
      }]
  },
  {
    translations: [{
      "languages_code": "fr-CA",
      "title": "Professeur agrégé"
    },
      {
        "languages_code": "en-CA",
        "title": "Associate Professor"
      }]
  }, {
    translations: [
      {
        "languages_code": "fr-CA",
        "title": "Diplômé à la maîtrise"
      },
      {
        "languages_code": "en-CA",
        "title": "Master’s Degree"
      }
    ]
  },
  {
    translations: [{
      "languages_code": "fr-CA",
      "title": "Diplômée à la maîtrise"
    },
      {
        "languages_code": "en-CA",
        "title": "Master’s Degree"
      }]
  },
  {
    translations: [{
      "languages_code": "fr-CA",
      "title": "Post-doctorant"
    },
      {
        "languages_code": "en-CA",
        "title": "Postdoctoral Fellow"
      }]
  },
  {
    translations: [{
      "languages_code": "fr-CA",
      "title": "Post-doctorante"
    },
      {
        "languages_code": "en-CA",
        "title": "Postdoctoral Fellow"
      }]
  },
  {
    translations: [{
      "languages_code": "fr-CA",
      "title": "Docteur"
    },
      {
        "languages_code": "en-CA",
        "title": "Doctor"
      }]
  },
  {
    translations: [{
      "languages_code": "fr-CA",
      "title": "Docteure"
    },
      {
        "languages_code": "en-CA",
        "title": "Doctor"
      }]
  },
  {
    translations: [{
      "languages_code": "fr-CA",
      "title": "Artiste en résidence"
    },
      {
        "languages_code": "en-CA",
        "title": "Resident Artist"
      }]
  },
  {
    translations: [{
      "languages_code": "fr-CA",
      "title": "Chercheur invité"
    },
      {
        "languages_code": "en-CA",
        "title": "Visiting Researcher"
      }]
  },
  {
    translations: [{
      "languages_code": "fr-CA",
      "title": "Chercheure invitée"
    },
      {
        "languages_code": "en-CA",
        "title": "Visiting Researcher"
      }]
  }
];