import { Directus } from '@directus/sdk';
import { PUBLIC_DIRECTUS_URL } from '$env/static/public';
const directus = new Directus(PUBLIC_DIRECTUS_URL);

/**
 * Helper function to get categories in plateforme from their legacy ID
 * @param legacyIds
 * @returns {Promise<*[]>}
 */
export async function queryCategories(legacyIds) {
  let result = [];
  try {
    const collection = await directus.items('categories');
    const query = await collection.readByQuery({
      filter: {
        legacy_cat_id: {
          _in: legacyIds.map(id => String(id))
        }
      }
    });
    result = query.data;
  } catch (e) {
    console.error(`Error fetching categories from Directus`, e);
  }
  return result;
}
