import { Directus } from '@directus/sdk';
import { PUBLIC_DIRECTUS_URL } from '$env/static/public';
const directus = new Directus(PUBLIC_DIRECTUS_URL);

/**
 * Helper function to get concepts in plateforme from their legacy ID
 * @param legacyIds
 * @returns {Promise<*[]>}
 */
export async function queryConcepts(legacyIds) {
  let result = [];
  try {
    const collection = await directus.items('concepts');
    const query = await collection.readByQuery({
      filter: {
        legacy_subcat_id: {
          _in: legacyIds.map(id => String(id))
        }
      }
    });
    result = query.data;
  } catch (e) {
    console.error(`Error fetching concepts from Directus`, e);
  }
  return result;
}
