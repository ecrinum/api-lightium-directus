import { Directus } from '@directus/sdk';
import { PUBLIC_DIRECTUS_URL } from '$env/static/public';
const directus = new Directus(PUBLIC_DIRECTUS_URL);

/**
 * Helper function to get personnes in plateforme from their legacy ID
 * @param legacyIds
 * @returns {Promise<*[]>}
 */
export async function queryPersonnes(legacyIds) {
  let result = [];
  try {
    const collection = await directus.items('personnes');
    const query = await collection.readByQuery({
      filter: {
        legacy_subcat_id: {
          _in: legacyIds.map(id => String(id))
        }
      }
    });
    result = query.data;
  } catch (e) {
    console.error(`Error fetching personnes from Directus`, e);
  }
  return result;
}
