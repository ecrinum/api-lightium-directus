import { json } from '@sveltejs/kit';
import { openDB } from '$lib/_db';

/**
 * Obtenir les catégories principales en format JSON
 * 
 * La réponse donnera une collection d'items, où chaque
 * item prendra la forme suivante:
 * { fr: <données FR>, en: <données EN> }
 * 
 * @returns {Response} Items au format JSON
 */
export async function GET() {
  const db = await openDB();

  // la propriété qui contient l'identifiant (ID)
  const idProp = 'id_cat';

  // La collection à retourner, dans laquelle on va ajouter
  // les items progressivement (un Map est plus pratique à
  // manipuler qu'un Array).
  // Chaque entrée du Map devrait être un objet avec une propriété
  // pour chaque langue:
  // { fr: <données FR>, en: <données EN> }
  const itemsMap = new Map();

  await db.each(`
    SELECT
      id_cat,lang,name,description,cleanstring
    FROM
      category_lang
    `, [], (err, row) => {

      if (err) {
        throw new Error('Erreur @ db each', err);
      }

      // On va construire l'item, mais une fois pour chaque langue.
      // item => { fr: ..., en: ... } où chaque prop de langue
      // correspond à la construction faite avec l'objet `_item`.
      const _item = {
        id_cat: row.id_cat,
        name: Buffer(row.name || '').toString('utf8'),
        lang: Buffer(row.lang || '').toString('utf8'),
        description: Buffer(row.description || '').toString('utf8'),
        cleanstring: Buffer(row.cleanstring || '').toString('utf8'),
      };

      // on s'attend à recevoir une propriété `lang`
      // qui identifie la langue de la rangée actuelle
      const _language = _item.lang.toLowerCase();

      // Rappel: la variable `_item` correspond à la variante
      // des données dans **une seule langue**.
      // On veut obtenir un objet qui consolide les données des
      // deux langues.
      if (itemsMap.has(_item[idProp])) {
        // Si une entrée existe déjà:
        // on consolide avec l'entrée qui existe déjà (1)
        // et on met à jour l'item avec la langue de l'itération actuelle (2),
        itemsMap.set(_item[idProp], {
          // 1)
          ...itemsMap.get(_item[idProp]),
          // 2)
          [_language]: _item,
        })
      } else {
        // l'item n'est pas encore entré, on l'ajoute en
        // l'identifiant par `idProp` et en spécifiant la langue
        itemsMap.set(_item[idProp], {
          [idProp]: _item[idProp],
          [_language]: _item
        });
      }
  });

  // maintenant que l'itération est terminée, on transforme les
  // valeurs du Map pour en faire un Array (tableau), qui peut
  // être traduit en JSON
  const items = Array.from(itemsMap.values());

  return json(items);
}
