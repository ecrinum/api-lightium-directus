import { getSingle } from '$lib/getSingleDb';
import { json } from '@sveltejs/kit';

export async function GET({ params }) {
  const single = await getSingle(
    'id_cat',
    'category_lang',
    [
      'lang',
      'name',
      'description',
      'cleanstring',
    ],
    params
  );

  console.log({ single })

  return json(single);
}
