import { json as json$1 } from '@sveltejs/kit';
import { directus } from '$lib/directus';
import slugify from '$lib/utils/slugify';

export async function POST({ request, url }) {
  const { host } = url;
  const { email, password } = await request.json();
  /** @type {array} raw exported categories from old sqlite db */
  let allDbCategories = [];
  /** @type {array} categories to PUT */
  let mangledCategories = [];

  console.log(`
    Début de l’importation
  `);

  // 0. plateforme auth
  try {
    await directus.login(email, password);
  } catch (e) {
    console.error(e);
    throw new Error("@migration task: Migrate this return statement (https://github.com/sveltejs/kit/discussions/5774#discussioncomment-3292701)");
    // Suggestion (check for correctness before using):
    // return json$1(e, {
    //   status: 401
    // });
    return {
      status: 401,
      body: e
    };
  }

  // 1. fetch cats
  try {
    const getCategories = await fetch(`http://${host}/categories.json`);
    allDbCategories = await getCategories.json();
  } catch (e) {
    console.error('Error fetching categories.json', e);
    throw new Error("@migration task: Migrate this return statement (https://github.com/sveltejs/kit/discussions/5774#discussioncomment-3292701)");
    // Suggestion (check for correctness before using):
    // return json$1(e, {
    //   status: 500
    // });
    return {
      status: 500,
      body: e
    }
  }
  console.log('Fetch categories OK...');

  let lastCatId;
  /** @type {object} default category model */
  let tmpCat = { translations: [] };
  // 2. Mangle category
  allDbCategories.map(async (categorie) => {
    if (categorie.id_cat !== lastCatId) {
      if (tmpCat.translations.length) {
        mangledCategories.push(tmpCat);
      }

      // set last ev id
      lastCatId = categorie.id_cat;

      // reset object
      tmpCat = {
        legacy_cat_id: categorie.id_cat,
        translations: [],
      };
    }

    // create translation
    tmpCat.translations.push({
      languages_code: categorie.lang === 'FR' ? 'fr-CA' : 'en-CA',
      title: categorie.name,
      slug: slugify(categorie.name),
      legacy_slug: categorie.cleanstring,
      description: categorie.short || null,
    });
  });

  // 3. PUT the categories
  const categories = await directus.items('categories');
  let chunkSize = 10;
  let chunksAt = 0;
  let problems = 0;
  let operations = [];

  while (chunksAt < mangledCategories.length) {
    try {
      const operation = await categories.createMany(mangledCategories.slice(chunksAt, chunksAt + chunkSize));
      console.log(`Batch operation ${chunksAt/chunkSize + 1}/${Math.ceil(mangledCategories.length / chunkSize)} done`);
      operations.push(operation);
      chunksAt += chunkSize;
    } catch (e) {
      problems++;
      console.error(`Error doing createMany`, e);

      if (problems > mangledCategories.length/chunkSize) {
        return json$1({
  message: 'Trop de problèmes',
  error: e,
}, {
          status: 500
        })
      }
    }
  }

  console.log(`
  ✅ Opérations terminées.
  `);
  return json$1({
  totalOperations: operations.length,
  problems,
  operations,
});
}
