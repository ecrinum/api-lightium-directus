import { json as json$1 } from '@sveltejs/kit';
import { directus } from '$lib/directus';

export async function POST({ request }) {
  const { email, password } = await request.json();
  let allStatuses = [];

  console.log(`
    Démarrage de la création des langues...
  `);

  // 0. plateforme auth
  try {
    await directus.login(email, password);
  } catch (e) {
    console.error(e);

    // cannot proceed
    throw new Error("@migration task: Migrate this return statement (https://github.com/sveltejs/kit/discussions/5774#discussioncomment-3292701)");
    // Suggestion (check for correctness before using):
    // return json$1(e, {
    //   status: 401
    // });
    return {
      status: 401,
      body: e
    };
  }

  // 1. créer les langues
  let erreurs = 0;
  let operation;
  const collection = await directus.items(`languages`);
  try {
    operation = await collection.createMany([
      {
        'code': 'fr-CA',
        'lang_id': 'fr',
        'name': 'Français'
      },
      {
        'code': 'en-CA',
        'lang_id': 'en',
        'name': 'Anglais'
      },
    ]);
  } catch (e) {
    console.error(`Erreur dans la création des langues`, e);
    erreurs++;
  }

  console.log(`
    Fin de la création des langues.
  `);

  return json$1({
  erreurs,
  operation,
});
}