import { json as json$1 } from '@sveltejs/kit';
import { directus } from '$lib/directus';
import { statuts } from '$lib/status_pour_personnes_translations.js'

export async function POST({ request }) {
  const { email, password } = await request.json();

  console.log(`
    Démarrage de la création des statuts...
  `);

  // 0. plateforme auth
  try {
    await directus.login(email, password);
  } catch (e) {
    console.error(e);

    // cannot proceed
    throw new Error("@migration task: Migrate this return statement (https://github.com/sveltejs/kit/discussions/5774#discussioncomment-3292701)");
    // Suggestion (check for correctness before using):
    // return json$1(e, {
    //   status: 401
    // });
    return {
      status: 401,
      body: e
    };
  }

  // 1. créer les statuts
  let erreurs = 0;
  let operation;
  const collection = await directus.items(`status_pour_personnes`);
  try {
    operation = await collection.createMany(statuts);
    console.log(`Création des statuts en batch…`)
  } catch (e) {
    console.error(`Erreur dans la création des statuts`, e);
    erreurs++;
  }

  console.log(`
    Fin de la création des statuts.
  `);

  return json$1({
  erreurs,
  operation,
});
}