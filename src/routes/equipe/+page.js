export async function load({ url, fetch }) {
  const { host, pathname } = url;
  const res = await fetch(`http://${host}/equipe.json`);
  const items = await res.json();

  if (res.ok) {
    return {
      items,
      pathname
    }
  }
}
