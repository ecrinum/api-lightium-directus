import { openDB } from '$lib/_db';

export async function GET({ params }) {
  // les paramètres dont on a besoin
  const { id, language } = params;

  // les colonnes de la table
  const itemProps = [
    'lang',
    'name',
    'short',
    'cleanstring',
    'description',
    'caption',
    'image',
  ];
  const id_equipe = 6;
  const idProp = 'id_subcat';
  const tableName = 'category_sub_lang';
  let item = {};
  const db = await openDB();
  const item_query =
      `
      SELECT
          category_sub.id_cat,
          ${itemProps.map(p => `${tableName}.${p}`).join(', ')}
      FROM category_sub
               INNER JOIN category_sub_lang ON category_sub.id_subcat = category_sub_lang.id_subcat
      WHERE category_sub.id_cat = ${id_equipe}
      AND category_sub.id_subcat = ${id}
    `;

  // on a besoin de sélectionner uniquement les  membres appartennat à
  // la sous-catégorie "équipe"

  await db.each(item_query, [], (err, row) => {

    if (err) {
      throw new Error('Erreur @ db each', err);
    }

    let _item = {};

    _item[idProp] = id;

    itemProps.forEach(prop => {
      _item[prop] = Buffer(row[prop] || []).toString('utf8');
    });

    // expects lang prop
    let _language = _item.lang.toLowerCase();

    item[_language] = _item;
  });

  return new Response(JSON.stringify(item[language]), {
    headers: {
      'content-type': 'application/json'
    }
  })
}
