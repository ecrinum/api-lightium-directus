import { json as json$1 } from '@sveltejs/kit';
import { writeFile } from 'fs';
import { NodeHtmlMarkdown } from 'node-html-markdown';
import YAML from 'yaml';

export async function GET({ host, params }) {
  const { id, language } = params;
  const get_item = await fetch(`http://${host}/equipe/${id}.${language}.json`);
  const item_data = await get_item.json();

  const front_matter = {
    name: item_data.name || '',
    short: item_data.short || '',
    cleanstring: item_data.cleanstring || '',
    lang: item_data.lang || '',
    caption: item_data.caption || '',
    description: item_data.description || '',
    image: item_data.image || '',
    categories: item_data.categories
  };
  // disable folding in yaml
  YAML.scalarOptions.str.fold.lineWidth = 0;
  const front_yaml = YAML.stringify(front_matter);
  const item_body = NodeHtmlMarkdown.translate(`${item_data.content}`) || '<!-- pas de contenu sur cette page -->';

  const composed_markdown = `---
${front_yaml}
---

`;

  writeFile(`dist/equipe/${id}-${item_data.cleanstring}.${language}.md`, composed_markdown, (err, data) => {
    if (err) {
      console.error(`Erreur en rédigeant ${id}.${language}.md`, err);
    }

  });

  throw new Error("@migration task: Migrate this return statement (https://github.com/sveltejs/kit/discussions/5774#discussioncomment-3292701)");
  // Suggestion (check for correctness before using):
  // return json$1(composed_markdown, {
  //   headers: {
  //     'content-type': 'text/plain; charset=utf-8'
  //   }
  // });
  return {
    body: composed_markdown,
    headers: {
      'content-type': 'text/plain; charset=utf-8'
    }
  }
}
