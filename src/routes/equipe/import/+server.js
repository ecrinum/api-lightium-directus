import { json as json } from '@sveltejs/kit';
import { directus } from '$lib/directus';
import { createItem } from '@directus/sdk';
import slugify from '$lib/utils/slugify';

/**
 * Importer les événements
 * 
 * Request body devrait être: Content-Type: application/json
 * avec les champs `email` et `password` remplis.
 * 
 * @returns {Response} réponse au format JSON
 */
export async function POST({ request, url }) {
  const { host } = url;
  // email et password doivent figurer dans le corps (body)
  // de la requête en format JSON
  const { email, password } = await request.json();

  /** @type {Array} exported items from old sqlite db */
  let allDbItems = [];

  console.log(`
    Début de l’importation
  `);

  // 0. plateforme auth
  try {
    await directus.login(email, password);
  } catch (e) {
    console.error(e);

    return json(e, {
      status: 401
    });
  }

  // 1. fetch items
  try {
    const getItems = await fetch(`http://${host}/equipe.json`);
    allDbItems = await getItems.json();
  } catch (e) {
    console.error('Error fetching "equipe.json"', e);
    return json(e, {
      status: 500
    });
  }

  // 2. Mangle items
  /** @type {Array<{translations: Array, legacy_item_id: Number}>} items to PUT */
  const mangledItems = allDbItems.map(item => {
    // soyons fous: extrayons l'adresse courriel qui figure souvent dans
    // le corps de la bio (champ description dans Lightium)
    // on récupère la valeur du courriel (s'il y a un résultat) avec:
    //   `.exec(string)[1]`
    // (la méthode `exec()` retourne un Array<string, string> avec le résultat du 
    // groupe de capture en 2e position, ou null si rien trouvé)
    const emailRegex = /<a href="mailto:(.+)"/;

    return {
      first_name: item['fr'].name.split(' ').slice(0, 1).join(' '),
      last_name: item['fr'].name.split(' ').slice(1).join(' '),
      slug: slugify(item['en'].name),
      email: emailRegex.exec(item['fr'].description) ?
        emailRegex.exec(item['fr'].description)[1] :
        null,
      is_alumnus: false,

      translations: [
        // fr
        {
          languages_code: item['fr'].lang.toLowerCase(),
          links: item['fr'].url ? JSON.stringify([
            { label: item['fr'].urlTitle, url: item['fr'].url }
          ]) : null,
          // attention, la taille du champ `description` est possiblement limité dans la BD
          description: item['fr'].short?.substring(0, 250) || '',
          // dans la BD Lightium, le champ 'description' contient le paragraphe biographique
          // ne pas confondre avec le champ 'description' dans Directus...
          content_html: item['fr'].description || '',
        },
        // en
        {
          languages_code: item['en'].lang.toLowerCase(),
          legacy_slug: item['en'].cleanstring,
          links: item['en'].url ? JSON.stringify([
            { label: item['en'].urlTitle, url: item['en'].url }
          ]) : null,
          // attention, la taille du champ `description` est possiblement limité dans la BD
          description: item['en'].short?.substring(0, 250) || '',
          // dans la BD Lightium, le champ 'description' contient le paragraphe biographique
          // ne pas confondre avec le champ 'description' dans Directus...
          content_html: item['en'].description || '',
        },
      ],

      // champs legacy
      legacy_slug: item['fr'].cleanstring,
      legacy_subcat_id: item.id_subcat,
      legacy_image: /^https?/.test(item['fr'].image) ? item['fr'].image : null,
    };
  });

  // 3. PUT the items
  /** 
   * Compteur pour le suivi de la boucle de création des items
   * @type {Number}
   */
  let itemCounter = 0;

  /**
   * La collection des opérations faites sur la BD - utile pour
   * inspecter à la fin
   * @type {Array<any>}
   */
  const problematicOperations = [];

  /**
   * La collection des opérations faites sur la BD - utile pour
   * inspecter à la fin
   * @type {Array<any>}
   */
  const operations = [];

  // boucle qui fait l'itération
  while (itemCounter < mangledItems.length) {
    try {
      // écriture d'un seul item dans Directus
      const operation = await directus.request(
        createItem('persons', mangledItems[itemCounter])
      );

      // opération réussie
      operations.push(operation);

      console.log(`✅ opération ${itemCounter + 1}/${mangledItems.length} réussie`);
    } catch (e) {
      // uh-oh
      problematicOperations.push(e)

      console.error(`❌ Erreur de création de l’item ${itemCounter + 1}/${mangledItems.length}`);

      // s'il y a 50% de problèmes, on arrête
      if (problematicOperations.length > mangledItems.length/2) {
        return json({
          message: 'Trop de problèmes',
          errors: problematicOperations,
        }, {
          status: 500
        });
      }
    } finally {
      // incrémentation du compteur pour la boucle
      itemCounter++;
    }
  }

  console.log(`
  Opérations terminées.
  `);

  return json({
    totalOperations: operations.length,
    totalProblems: problematicOperations.length,
    operations,
    problematicOperations,
  });
}
