import { openDB } from '$lib/_db';
import { json } from '@sveltejs/kit';

/**
 * Obtenir les événements en format JSON
 * 
 * La réponse donnera une collection d'items, où chaque
 * item prendra la forme suivante:
 * { fr: <données FR>, en: <données EN> }
 * 
 * @returns {Response} Items au format JSON
 */
export async function GET() {
  const db = await openDB();

  // quel ID est associé à un événement?
  const idProp = 'id_event';

  // les paramètres dont on a besoin
  // les colonnes de la table
  const itemProps = [
    String(idProp),
    'lang',
    'title',
    'short',
    'description',
    'location',
  ];
  const itemTableName = 'events_lang';
  const table2Name = `events`;
  const table2Props = [
    String(idProp),
    'time',
    'endTime',
    'Gkey',
  ];
  
  // La requête SQL dans laquelle on va effectuer 1 join.
  // 
  // Plus loin, nous allons faire les transformations nécessaires
  // pour pouvoir retourner le résultat en JSON (comme le 
  // traitement des buffers, imposés par la bibliothèque sqlite)
  const item_query =
    `
    SELECT
      ${itemProps.map(p => `${itemTableName}.${p}`).join(', ')},
      ${table2Props.map(p => `${table2Name}.${p}`).join(', ')}
    FROM
      ${itemTableName}
    INNER JOIN
      ${table2Name}
    ON
      ${table2Name}.${idProp} = ${itemTableName}.${idProp}
    `;

  // La collection à retourner, dans laquelle on va ajouter
  // les items progressivement (un Map est plus pratique à
  // manipuler qu'un Array).
  // Chaque entrée du Map devrait être un objet avec une propriété
  // pour chaque langue:
  // { fr: <données FR>, en: <données EN> }
  const itemsMap = new Map();

  // on a besoin de sélectionner uniquement les items appartenant à
  // la sous-catégorie "événements" (var `idEvenements`)
  await db.each(item_query, [], (err, row) => {

    if (err) {
      throw new Error('Erreur @ db each', err);
    }

    // On va construire l'item, mais une fois pour chaque langue.
    // item => { fr: ..., en: ... } où chaque prop de langue
    // correspond à la construction faite avec l'objet `_item`.
    let _item = {};
    
    // On doit faire l'itération conditionnellement, car on va recevoir
    // des `Buffer()` qu'il faut transformer en String.
    itemProps.forEach(prop => {
      // Le champ ID (`idProp`) quant à lui peut être copié tel quel
      if (prop === idProp) {
        _item[idProp] = row[prop];
      } else {
        _item[prop] = Buffer(row[prop] || []).toString('utf8');
      }
    });

    // Les champs suivants peuvent être copiés tels quels
    // (peu importe si on écrase ce qu'on a fait avec les 
    // buffers ci-dessus)
    _item['time'] = row['time'];
    _item['endTime'] = row['endTime'];
    _item['Gkey'] = Buffer(row['Gkey'] || []).toString('utf8');

    // on s'attend à recevoir une propriété `lang`
    // qui identifie la langue de la rangée actuelle
    const _language = _item.lang.toLowerCase();

    // Rappel: la variable `_item` correspond à la variante
    // des données dans **une seule langue**.
    // On veut obtenir un objet qui consolide les données des
    // deux langues.
    if (itemsMap.has(_item[idProp])) {
      // Si une entrée existe déjà:
      // on consolide avec l'entrée qui existe déjà (1)
      // et on met à jour l'item avec la langue de l'itération actuelle (2),
      itemsMap.set(_item[idProp], {
        // 1)
        ...itemsMap.get(_item[idProp]),
        // 2)
        [_language]: _item,
      })
    } else {
      // l'item n'est pas encore entré, on l'ajoute en
      // l'identifiant par `idProp` et en spécifiant la langue
      itemsMap.set(_item[idProp], {
        [idProp]: _item[idProp],
        [_language]: _item
      });
    }
  });

  // maintenant que l'itération est terminée, on transforme les
  // valeurs du Map pour en faire un Array (tableau), qui peut
  // être traduit en JSON
  const items = Array.from(itemsMap.values());

  return json(items);
}
