import { openDB } from '$lib/_db';
import { json } from '@sveltejs/kit';

export async function GET({ params }) {
  // les paramètres dont on a besoin
  const { id, language } = params;

  // les colonnes de la table
  const itemProps = [
    'lang',
    'title',
    'short',
    'description',
    'location',
  ];
  const idProp = 'id_event';
  const tableName = 'events_lang';
  let item = {};
  const db = await openDB();
  const item_query =
    `
    SELECT
      events.${idProp},events.time,events.endTime,
      ${itemProps.map(p => `${tableName}.${p}`).join(', ')}
    FROM events
      JOIN ${tableName} ON events.${idProp} = ${tableName}.${idProp}
    WHERE events.${idProp} = ${id}
    `;

  // on a besoin de sélectionner uniquement les  membres appartennat à
  // la sous-catégorie "équipe"

  await db.each(item_query, [], (err, row) => {

    if (err) {
      throw new Error('Erreur @ db each', err);
    }

    let _item = {};

    _item[idProp] = Number(id);

    itemProps.forEach(prop => {
      _item[prop] = Buffer(row[prop] || []).toString('utf8');
    });

    // expects lang prop
    let _language = _item.lang.toLowerCase();

    item[_language] = _item;
  });

  return json(item[language]);
}
