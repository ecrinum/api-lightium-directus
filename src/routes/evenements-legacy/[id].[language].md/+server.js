import { writeFile } from 'fs';
import { NodeHtmlMarkdown } from 'node-html-markdown';
import YAML from 'yaml';

export async function GET({ url, params }) {
  const { host } = url;
  const { id, language } = params;
  const get_item = await fetch(`http://${host}/evenements-legacy/${id}.${language}.json`);
  const item_data = await get_item.json();

  const front_matter = {
    lang: item_data.lang || '',
    title: item_data.title || '',
    short: item_data.short || '',
    description: item_data.description || '',
    time: item_data.time || '',
    location: item_data.location || '',
  };
  // disable folding in yaml
  YAML.scalarOptions.str.fold.lineWidth = 0;
  const front_yaml = YAML.stringify(front_matter);
  const item_body = NodeHtmlMarkdown.translate(`${item_data.content}`) || '<!-- pas de contenu sur cette page -->';

  const composed_markdown = `---
${front_yaml}
---
    
${item_body}
`;

  writeFile(`dist/evenements-legacy/${id}.${language}.md`, composed_markdown, (err, data) => {
    if (err) {
      console.error(`Erreur en rédigeant ${id}.${language}.md`, err);
    }

    console.log('Fini', data);
  });

  return new Response(composed_markdown, {
    headers: {
      'content-type': 'text/plain; charset=utf-8'
    }
  });
}
