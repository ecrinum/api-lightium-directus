import { json as json } from '@sveltejs/kit';
import { directus } from '$lib/directus';
import { createItem } from '@directus/sdk';
import slugify from '$lib/utils/slugify';

/**
 * Importer les événements
 * 
 * Request body devrait être: Content-Type: application/json
 * avec les champs `email` et `password` remplis.
 * 
 * @returns {Response} réponse au format JSON
 */
export async function POST({ request, url }) {
  const { host } = url;
  // email et password doivent figurer dans le corps (body)
  // de la requête en format JSON
  const { email, password } = await request.json();

  /** @type {Array} exported items from old sqlite db */
  let allDbItems = [];

  console.log(`
    Début de l’importation
  `);

  // 0. plateforme auth
  try {
    await directus.login(email, password);
  } catch (e) {
    console.error(e);

    return json(e, {
      status: 401
    });
  }

  // 1. fetch items
  try {
    const getItems = await fetch(`http://${host}/evenements-legacy.json`);
    allDbItems = await getItems.json();
  } catch (e) {
    console.error('Error fetching "evenements-legacy.json"', e);
    return json(e, {
      status: 500
    });
  }

  // 2. Mangle items
  /** @type {Array<{translations: Array, legacy_item_id: Number}>} items to PUT */
  const mangledItems = allDbItems.map(item => {
    // time is a UNIX timestamp
    // in JS, multiply the time by 1000 to use it with the `Date()` constructor
    const date_start = new Date(item['fr'].time * 1000);

    return {
      date_start: date_start ? date_start : null,
      // indiquer le statut 'published' (la valeur par défaut peut être 'draft')
      status: 'published',
      translations: [
        // fr
        {
          languages_code: item['fr'].lang.toLowerCase(),
          title: item['fr'].title,
          slug: slugify(item['fr'].title),
          legacy_slug: item['fr'].cleanstring,
          location: item['fr'].location,
          legacy_location: item['fr'].location,
          legacy_image: /^https?/.test(item['fr'].image) ? item['fr'].image : null,
          // attention, la taille du champ `description` est possiblement limité dans la BD
          description: item['fr'].short?.substring(0, 250) || '',
          content_html: item['fr'].description || '',
        },
        // en
        {
          languages_code: item['en'].lang.toLowerCase(),
          title: item['en'].title,
          slug: slugify(item['en'].title),
          legacy_slug: item['en'].cleanstring,
          location: item['en'].location,
          legacy_location: item['en'].location,
          legacy_image: /^https?/.test(item['en'].image) ? item['en'].image : null,
          // attention, la taille du champ `description` est possiblement limité dans la BD
          description: item['en'].short?.substring(0, 250) || '',
          content_html: item['en'].description || '',
        },
      ],
      legacy_time: item['fr'].time,
      legacy_event_id: item.id_event,
      legacy_gkey: item['fr'].Gkey,
    };
  });

  // 3. PUT the items
  /** 
   * Compteur pour le suivi de la boucle de création des items
   * @type {Number}
   */
  let itemCounter = 0;

  /**
   * La collection des opérations faites sur la BD - utile pour
   * inspecter à la fin
   * @type {Array<any>}
   */
  const problematicOperations = [];

  /**
   * La collection des opérations faites sur la BD - utile pour
   * inspecter à la fin
   * @type {Array<any>}
   */
  const operations = [];

  // boucle qui fait l'itération
  while (itemCounter < mangledItems.length) {
    try {
      // écriture d'un seul item dans Directus
      const operation = await directus.request(
        createItem('events', mangledItems[itemCounter])
      );

      // opération réussie
      operations.push(operation);

      console.log(`✅ opération ${itemCounter + 1}/${mangledItems.length} réussie`);
    } catch (e) {
      // uh-oh
      problematicOperations.push(e)

      console.error(`❌ Erreur de création de l’item ${itemCounter + 1}/${mangledItems.length}`);

      // s'il y a 50% de problèmes, on arrête
      if (problematicOperations.length > mangledItems.length/2) {
        return json({
          message: 'Trop de problèmes',
          errors: problematicOperations,
        }, {
          status: 500
        });
      }
    } finally {
      // incrémentation du compteur pour la boucle
      itemCounter++;
    }
  }

  console.log(`
  Opérations terminées.
  `);

  return json({
    totalOperations: operations.length,
    totalProblems: problematicOperations.length,
    operations,
    problematicOperations,
  });
}
