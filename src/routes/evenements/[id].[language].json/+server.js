import { getSingle } from '$lib/getSingleDb';
import { json } from '@sveltejs/kit';

export async function GET({ params }) {
  const single = await getSingle(
      'id_item',
      'item_lang',
      [
        'lang',
        'title',
        'short',
        'content',
        'cleanstring',
        'caption',
        'image'
      ],
      params
  );

  return json(single);
}
