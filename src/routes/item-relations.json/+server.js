import { json } from '@sveltejs/kit';
import { openDB } from '$lib/_db';

export async function GET() {
  const db = await openDB();

  /**
   * Carte ("Map") des items, triés par id_item
   * (une Map est plus facile à traiter qu'un Array)
   * 
   * @type {Map<Number, { id_item: Number, subcats: Array<Number> }>}
   */
  const itemsMap = new Map();

  const relationsTable = `item_assoc`;
  const relationsFields = [
    'id_item',
    'id_subcat',
  ];

  await db.each(`
    SELECT
      ${relationsFields.map(field => `${relationsTable}.${field}`).join(',')}
    FROM
      ${relationsTable}
    `, [], (err, row) => {

      if (err) {
        throw new Error('Erreur en sélection les "item_assoc"', err);
      }

      // Dans notre `itemsMap`, nous allons soit:
      //   A) mettre à jour l'entrée si elle existe;
      //   B) créer l'entrée si elle n'existe pas (identifiant: id_item).
      if (itemsMap.has(row.id_item)) {
        // A) une entrée existe pour cet item; nous allons l'altérer
        const currItem = itemsMap.get(row.id_item);

        itemsMap.set(currItem.id_item, {
          // 1) on récupère l'item actuel pour le consolider...
          id_item: currItem.id_item,
          // 2) on écrase les `subcats` en ajoutant celui de l'itération actuelle
          subcats: [...currItem.subcats, row.id_subcat],
        });
      } else {
        // B) une entrée n'existe pas encore dans le Map, nous allons l'ajouter
        itemsMap.set(row.id_item, {
          /** @type {Number} */
          id_item: row.id_item,
          /** @type {Array<Number>} */
          subcats: row.id_subcat ? [row.id_subcat] : [],
        });
      }
  });

  // à présent, itemsMap est bien rempli, nous pouvons
  // en faire un tableau (Array)
  const items = Array.from(itemsMap.values());

  // group by id_item
  // items.sort((a, b) => {
  //   return b.id_item - a.id_item
  // });

  return json(items);
}
