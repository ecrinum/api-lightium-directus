import { json } from '@sveltejs/kit';
import { openDB } from '$lib/_db';

/**
 * Obtenir les items en format JSON
 * 
 * La réponse donnera une collection d'items, où chaque
 * item prendra la forme suivante:
 * { fr: <données FR>, en: <données EN> }
 * 
 * @returns {Response} Items au format JSON
 */
export async function GET({ url }) {
  const { searchParams } = url;
  const db = await openDB();

  let limit = 900;
  const query_max = parseInt(searchParams.get('limit'), 10);
  if (query_max && query_max < 999) {
    limit = query_max;
  }

  // La collection à retourner, dans laquelle on va ajouter
  // les items progressivement (un Map est plus pratique à
  // manipuler qu'un Array).
  // Chaque entrée du Map devrait être un objet avec une clé
  // pour chaque langue:
  // { fr: <données FR>, en: <données EN> }
  const itemsMap = new Map();

  await db.each(`
    SELECT
      item.id_item,
      item.year,
      item.month,
      item.day,
      item.time,
      item.zoterokey,
      
      item_lang.id_item,
      item_lang.lang,
      item_lang.title,
      item_lang.content,
      item_lang.short,
      item_lang.cleanstring,
      item_lang.image,
      item_lang.caption,
      item_lang.url,
      item_lang.urltitle

    FROM
      item
    LEFT JOIN
      item_lang ON item.id_item = item_lang.id_item
    LIMIT
      ${limit}
    `, [], async (err, row) => {

    if (err) {
      throw new Error('Erreur @ db each', err);
    }

    // On va construire l'item, mais une fois pour chaque langue.
    // item => { fr: ..., en: ... } où chaque prop de langue
    // correspond à la construction faite avec l'objet `_item`.
    let _item = {
      id_item: row.id_item,
      time: row.time,
      year: row.year,
      month: row.month,
      day: row.day,
      title: Buffer(row.title).toString('utf8'),
      lang: Buffer(row.lang).toString('utf8'),
      content: Buffer(row.content || '').toString('utf8'),
      short: Buffer(row.short || '').toString('utf8'),
      cleanstring: Buffer(row.cleanstring || '').toString('utf8'),
      image: Buffer(row.image || '').toString('utf8'),
      url: Buffer(row.url || '').toString('utf8'),
      urlTitle: Buffer(row.urlTitle || '').toString('utf8'),
      zoterokey: Buffer(row.zoterokey || '').toString('utf8'),
    };
    
    // on s'attend à recevoir une propriété `lang`
    // qui identifie la langue de la rangée actuelle
    const _language = _item.lang.toLowerCase();

    // Rappel: la variable `_item` correspond à la variante
    // des données dans **une seule langue**.
    // On veut obtenir un objet qui consolide les données des
    // deux langues.
    if (itemsMap.has(_item['id_item'])) {
      // Si une entrée existe déjà:
      // on consolide avec l'entrée qui existe déjà (1)
      // et on met à jour l'item avec la langue de l'itération actuelle (2),
      itemsMap.set(_item['id_item'], {
        // 1)
        ...itemsMap.get(_item['id_item']),
        // 2)
        [_language]: _item,
      })
    } else {
      // l'item n'est pas encore entré, on l'ajoute en
      // l'identifiant par 'id_item' et en spécifiant la langue
      itemsMap.set(_item['id_item'], {
        id_item: _item['id_item'],
        [_language]: _item
      });
    }
  });

  // maintenant que l'itération est terminée, on transforme les
  // valeurs du Map pour en faire un Array (tableau), qui peut
  // être traduit en JSON
  const items = Array.from(itemsMap.values());

  return json(items);
}
