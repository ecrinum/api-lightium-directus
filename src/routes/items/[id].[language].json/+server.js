import { openDB } from '$lib/_db';
import { json } from '@sveltejs/kit';

export async function GET({ params }) {
  const { id, language } = params;
  const itemProps = [
    'id_item',
    'lang',
    'title',
    'content',
    'short',
    'cleanstring',
    'caption',
    'image',
    'url',
    'urlTitle'
  ];
  let item = {};
  const db = await openDB();
  const item_query =
    `
    SELECT
       ${itemProps.join(', ')}
    FROM item_lang
    WHERE id_item = ${id}
    `;

  const item_categories_query = `
  SELECT id_subcat
  FROM item_assoc
  WHERE item_assoc.id_item=${id}
  `;

  const categories = await db.all(item_categories_query);

  await db.each(item_query, [], (err, row) => {

    if (err) {
      throw new Error('Erreur @ db each', err);
    }

    let _item = {
      id_item: row.id_item,
      title: Buffer(row.title || []).toString('utf8'),
      short: Buffer(row.short || []).toString('utf8'),
      cleanstring: Buffer(row.cleanstring || []).toString('utf8'),
      caption: Buffer(row.caption || []).toString('utf8'),
      image: Buffer(row.image || []).toString('utf8'),
      url: Buffer(row.url || []).toString('utf8'),
      urlTitle: Buffer(row.urlTitle || []).toString('utf8'),
      lang: Buffer(row.lang || []).toString('utf8'),
      content: Buffer(row.content || []).toString('utf8'),
      categories: categories.map(s => s.id_subcat),
    }
    let _language = _item.lang.toLowerCase();

    item[_language] = _item;
  });

  return json(item[language]);
}
