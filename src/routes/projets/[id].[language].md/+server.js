import { json as json$1 } from '@sveltejs/kit';
import { writeFile } from 'fs';
import { NodeHtmlMarkdown } from 'node-html-markdown';
import YAML from 'yaml';

export async function GET({ url, params }) {
  const { host } = url;
  const { id, language } = params;
  const get_item = await fetch(`http://${host}/projets/${id}.${language}.json`);
  const item_data = await get_item.json();

  const front_matter = {
    title: item_data.title || '',
    short: item_data.short || '',
    cleanstring: item_data.cleanstring || '',
    lang: item_data.lang || '',
    caption: item_data.caption || '',
    image: item_data.image || '',
    url: item_data.url || '',
    urlTitle: item_data.urlTitle || '',
    categories: item_data.categories
  };
  // disable folding in yaml
  YAML.scalarOptions.str.fold.lineWidth = 0;
  const front_yaml = YAML.stringify(front_matter);
  const item_body = NodeHtmlMarkdown.translate(`${item_data.content}`) || '<!-- pas de contenu sur cette page -->';

  const composed_markdown = `---
${front_yaml}
---
    
${item_body}
`;

  writeFile(`dist/projets/${id}.${language}.md`, composed_markdown, (err, data) => {
    if (err) {
      console.error(`Erreur en rédigeant ${id}.${language}.md`, err);
    }

    console.log('Fini', data);
  });

  return new Response(composed_markdown, {
    headers: {
      'content-type': 'text/plain; charset=utf-8'
    }
  });
}
