import { json } from '@sveltejs/kit';
import { openDB } from '$lib/_db';

export async function GET() {

  const db = await openDB();

  let items = [];

  const idProjets = 2;
  const relationsTable = `item_assoc`;
  const relationsFields = [
    'id_item',
    'id_subcat',
  ];

  await db.each(`
    SELECT
      ${relationsFields.map(field => `${relationsTable}.${field}`).join(',')}
    FROM
      ${relationsTable}
    WHERE
      ${relationsTable}.${relationsFields[1]} = ${idProjets}
    `, [], (err, row) => {

    let item = {
      id_item: row.id_item,
      id_subcat: row.id_subcat,
    }
    items.push(item);
  });

  return json(items);
}
