import { json } from '@sveltejs/kit';
import { readItems, updateItem } from '@directus/sdk'
import { directus } from '$lib/directus';

/**
 * Create the relations on the `projects` table in directus
 * 
 * (POST since we need to work w/ credentials)
 * @returns {Response}
 */
export async function POST({ request, url }) {
  const { host } = url;

  // email et password doivent figurer dans le corps (body)
  // de la requête en format JSON
  const { email, password } = await request.json();

  /**
   * @type {Array<{ id_item: Number, subcats: Array<Number> }>}
   * Exported items from old sqlite db
   */
  let itemsWithRelations = [];

  /** @type {Array} Opérations de patchage réussies */
  const operations = [];

/** @type {Array} Opérations de patchage qui ont échoué */
  const problematicOperations = [];

  let itemsCounter = 1;
  
  console.log(`
    Démarrage de la création de relations...
  `);

  // 0. plateforme auth
  try {
    await directus.login(email, password);
  } catch (e) {
    console.error(e);

    // cannot proceed
    return json(e, {
      status: 401
    });
  }

  // 1. fetch relations
  try {
    const getRelations = await fetch(`http://${host}/item-relations.json`);
    itemsWithRelations = await getRelations.json();
  } catch (e) {
    console.error(`Error fetching relations`, e);

    // cannot continue
    return json(e, {
      status: 404
    });
  }

  // 3. for each item in the relations dictionary, patch
  // the matching item in Directus (through the
  // `legacy_subcat_id` and `legacy_item_id` fields)
  for (const item of itemsWithRelations) {
    const itemResult = await directus.request(
      readItems('publications', {
        filter: {
          legacy_item_id: {
            '_eq': String(item.id_item)
          }
        },
        limit: -1
      })
    );

    if (!itemResult.length) {
      // no results -- item is likely not a project
    } else {
      // take first item returned
      const itemToPatch = itemResult[0];
      // extract its ID
      const itemId = itemToPatch.id;

      // the following block of requests get the "subcategories"
      // (axis, concepts, persons...) which have a matching `legacy_subcat_id`
      // of course, a `subcat_id` could refer to a person or a concept;
      // we'll just use the `_in` operator to check in
      const axisResult = await directus.request(
        readItems('axis', {
          filter: {
            legacy_subcat_id: {
              _in: item.subcats.map(i => String(i))
            }
          }
        })
      );

      const conceptsResult = await directus.request(
        readItems('concepts', {
          filter: {
            legacy_subcat_id: {
              _in: item.subcats.map(i => String(i))
            }
          }
        })
      );

      const researchFieldsResult = await directus.request(
        readItems('research_fields', {
          filter: {
            legacy_subcat_id: {
              _in: item.subcats.map(i => String(i))
            }
          }
        })
      );

      const researchObjectsResult = await directus.request(
        readItems('research_objects', {
          filter: {
            legacy_subcat_id: {
              _in: item.subcats.map(i => String(i))
            }
          }
        })
      );

      const personsResult = await directus.request(
        readItems('persons', {
          filter: {
            legacy_subcat_id: {
              _in: item.subcats.map(i => String(i))
            }
          }
        })
      );

      // patch the item using a partial object
      // relations can take an array of primary keys
      // (which are the IDs in Directus)
      try {
        const operation = await directus.request(
          updateItem('publications', itemId, {
            // specific ID must be explicitely named for relations
            // for there is a junction table with id, <item>_id, <relation-item>_id
            // e.g. persons_id, axis_id, etc.

            persons_relations: personsResult.map(i => {
              return {
                persons_id: Number(i.id)
              }
            }),
            axis_relations: axisResult.map(i => {
              return {
                axis_id: Number(i.id)
              }
            }),
            concepts_relations: conceptsResult.map(i => {
              return {
                concepts_id: Number(i.id)
              }
            }),
            research_fields_relations: researchFieldsResult.map(i => {
              return {
                research_fields_id: Number(i.id)
              }
            }),
            research_objects_relations: researchObjectsResult.map(i => {
              return {
                research_objects_id: Number(i.id)
              }
            }),
          })
        );

        console.log(`✅ Opération ${itemsCounter}/${itemsWithRelations.length} réussie`);
        
        operations.push(operation);
      } catch (e) {
        console.error(`❌ [${e?.response?.status}] Error patching item`, itemId);

        problematicOperations.push(e);
      }
    }

    // incrémenter compteur (qui aide à suivre les opérations)
    itemsCounter++;
  }

  console.log(`
  Opérations terminées.
  `);

  return json({
    totalSuccess: operations.length,
    totalProblems: problematicOperations.length,
    operations,
    problematicOperations
  });
}
