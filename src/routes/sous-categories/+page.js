export async function load({ url, fetch }) {
  const res = await fetch(`sous-categories.json`);
  const items = await res.json();
  const { pathname } = url;

  if (res.ok) {
    return {
      items,
      pathname
    }
  }
}
