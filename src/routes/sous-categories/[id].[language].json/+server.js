import { getSingle } from '$lib/getSingleDb';
import { json } from '@sveltejs/kit';

export async function GET({ params }) {
  const single = await getSingle(
      'id_subcat',
      'category_sub_lang',
      [
        'lang',
        'name',
        'short',
        'image',
        'description',
        'caption',
        'cleanstring',
      ],
      params
  );

  return json(single);
}
