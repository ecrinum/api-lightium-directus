import { json as json$1 } from '@sveltejs/kit';
import { Directus } from '@directus/sdk';
import { PUBLIC_DIRECTUS_URL } from '$env/static/public';
const directus = new Directus(PUBLIC_DIRECTUS_URL);

/**
 * POST since we need to work w/ credentials
 * @param request
 * @param url
 * @returns {Promise<{body, status: number}|{body: {completedOperations: string, warnings: number, message: string, problems: number}}>}
 * @constructor
 */
export async function POST({ request, url }) {
  const { host } = url;
  const { email, password } = await request.json();
  let legacyRelations = [];
  let allOnlineConcepts = [];

  // 0. plateforme auth
  try {
    await directus.login(email, password);
  } catch (e) {
    console.error(e);

    // cannot proceed
    throw new Error("@migration task: Migrate this return statement (https://github.com/sveltejs/kit/discussions/5774#discussioncomment-3292701)");
    // Suggestion (check for correctness before using):
    // return json$1(e, {
    //   status: 401
    // });
    return {
      status: 401,
      body: e,
    };
  }

  // 1. fetch relations
  try {
    const getRelations = await fetch(`http://${host}/subcat-relations.json`);
    legacyRelations = await getRelations.json();
  } catch (e) {
    console.error(`Error fetching local relations`, e);

    // cannot continue
    throw new Error("@migration task: Migrate this return statement (https://github.com/sveltejs/kit/discussions/5774#discussioncomment-3292701)");
    // Suggestion (check for correctness before using):
    // return json$1(e, {
    //   status: 404
    // });
    return {
      status: 404,
      body: e,
    };
  }

  // 3. get categories from Directus API
  try {
    const collection = await directus.items('concepts');
    const query = await collection.readByQuery({
      limit: -1
    });

    allOnlineConcepts = query.data
  } catch (e) {
    console.error(`Error fetching concepts from Directus`, e);

    throw new Error("@migration task: Migrate this return statement (https://github.com/sveltejs/kit/discussions/5774#discussioncomment-3292701)");
    // Suggestion (check for correctness before using):
    // return json$1(e, {
    //   status: 500
    // });
    return {
      status: 500,
      body: e,
    }
  }

  // 4. iterate and update
  // iterator to keep track
  let i = 0;
  let warnings = 0;
  let problems = 0;
  const collection = await directus.items(`concepts`);
  const categoryCollection = await directus.items(`categories`);
  for (const concept of allOnlineConcepts) {
    const { id, legacy_subcat_id } = concept;
    let matchingCategory;

    const matchingLegacyRelation = legacyRelations.find(relation => {
      return String(relation.id_subcat) === legacy_subcat_id;
    });

    if (!matchingLegacyRelation) {
      console.info('↩️ No category found; skipping.', concept);
      continue;
    }
    // now what’s the category on Directus?
    try {
      const queryCategory = await categoryCollection.readByQuery({
        filter: {
          legacy_cat_id: matchingLegacyRelation.id_cat
        },
        limit: 1 // should be sufficient since unique
      });
      if (queryCategory.data?.length) {
        matchingCategory = queryCategory.data[0]; // grab first (and only) result}
        console.log(`✅ trouvé:`, matchingCategory.id);
      } else {
        console.warn(`No matching category found upstream using legacy id_cat; skipping ${i+1}/${allOnlineConcepts.length}`, matchingLegacyRelation);
        i++;
        warnings++;
        continue;
      }
    }
     catch (e) {
      console.error(`Error reading categories on Directus`, e);
    }
    try {
      const operation = await collection.updateOne(id, {
        category: parseInt(matchingCategory.id, 10)
      });
    } catch (e) {
      p++;
      console.error(`❗️ Error updating subcategory with its category`, e);
    }
    console.info(`Opération ${i + 1}/${allOnlineConcepts.length} terminée avec succès.`)

    // increment counter for checks
    i++;
  }

  return json$1({
  message: 'Terminé.',
  completedOperations: `${i}/${allOnlineConcepts.length}`,
  warnings,
  problems,
})
}
