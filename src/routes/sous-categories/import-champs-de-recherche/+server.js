import { json as json } from '@sveltejs/kit';
import { directus } from '$lib/directus';
import { createItem } from '@directus/sdk';
import slugify from '$lib/utils/slugify';

/**
 * Importer les événements
 * 
 * Request body devrait être: Content-Type: application/json
 * avec les champs `email` et `password` remplis.
 * 
 * @returns {Response} réponse au format JSON
 */
export async function POST({ request, url }) {
  const { host } = url;
  // email et password doivent figurer dans le corps (body)
  // de la requête en format JSON
  const { email, password } = await request.json();

  /** @type {Array} exported items from old sqlite db */
  let allDbItems = [];

  console.log(`
    Début de l’importation
  `);

  // 0. plateforme auth
  try {
    await directus.login(email, password);
  } catch (e) {
    console.error(e);

    return json(e, {
      status: 401
    });
  }

  // 1. fetch items
  try {
    const getItems = await fetch(`http://${host}/sous-categories/champs-de-recherche.json`);
    allDbItems = await getItems.json();
  } catch (e) {
    console.error('Error fetching "sous-categories/champs-de-recherche.json"', e);
    return json(e, {
      status: 500
    });
  }

  // 2. Mangle items
  /** @type {Array<{translations: Array, legacy_item_id: Number}>} items to PUT */
  const mangledItems = allDbItems.map(item => {

    return {
      translations: [
        // fr
        {
          languages_code: item['fr'].lang.toLowerCase(),
          title: item['fr'].name,
          slug: slugify(item['fr'].name),
          legacy_slug: item['fr'].cleanstring,
          legacy_image: /^https?/.test(item['fr'].image) ? item['fr'].image : null,
          // attention, la taille du champ `description` est possiblement limité dans la BD
          description: item['fr'].short?.substring(0, 250) || '',
          content_html: item['fr'].description || '',
        },
        // en
        {
          languages_code: item['en'].lang.toLowerCase(),
          title: item['en'].name,
          slug: slugify(item['en'].name),
          legacy_slug: item['en'].cleanstring,
          legacy_image: /^https?/.test(item['en'].image) ? item['en'].image : null,
          // attention, la taille du champ `description` est possiblement limité dans la BD
          description: item['en'].short?.substring(0, 250) || '',
          content_html: item['en'].description || '',
        },
      ],
      legacy_subcat_id: item.id_subcat,
    };
  });

  // 3. PUT the items
  /** 
   * Compteur pour le suivi de la boucle de création des items
   * @type {Number}
   */
  let itemCounter = 0;

  /**
   * La collection des opérations faites sur la BD - utile pour
   * inspecter à la fin
   * @type {Array<any>}
   */
  const problematicOperations = [];

  /**
   * La collection des opérations faites sur la BD - utile pour
   * inspecter à la fin
   * @type {Array<any>}
   */
  const operations = [];

  // boucle qui fait l'itération
  while (itemCounter < mangledItems.length) {
    try {
      // écriture d'un seul item dans Directus
      const operation = await directus.request(
        // nom de collection à remplacer
        createItem('research_fields', mangledItems[itemCounter])
      );

      // opération réussie
      operations.push(operation);

      console.log(`✅ opération ${itemCounter + 1}/${mangledItems.length} réussie`);
    } catch (e) {
      // uh-oh
      problematicOperations.push(e)

      console.error(`❌ Erreur de création de l’item ${itemCounter + 1}/${mangledItems.length}`);

      // s'il y a 50% de problèmes, on arrête
      if (problematicOperations.length > mangledItems.length/2) {
        return json({
          message: 'Trop de problèmes',
          errors: problematicOperations,
        }, {
          status: 500
        });
      }
    } finally {
      // incrémentation du compteur pour la boucle
      itemCounter++;
    }
  }

  console.log(`
  Opérations terminées.
  `);

  return json({
    totalOperations: operations.length,
    totalProblems: problematicOperations.length,
    operations,
    problematicOperations,
  });
}
