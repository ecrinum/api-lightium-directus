import { openDB } from '$lib/_db';
import { json } from '@sveltejs/kit';

/**
 * Ce bloc reproduit la même logique que pour `/sous-categories.json`,
 * mais en filtrant avec la variable cat_id (définie au début).
 * 
 * @returns {Response} Items formattés dans une collection JSON
 */
export async function GET() {
  const db = await openDB();
  
  // on se sert de cet identifiant dans la clause WHERE
  // de la requête SQL pour filtrer les données
  // dans la BD, l'ID de catégorie est 4
  const cat_id = 4;

  // La collection à retourner, dans laquelle on va ajouter
  // les items progressivement (un Map est plus pratique à
  // manipuler qu'un Array).
  // Chaque entrée du Map devrait être un objet avec une clé
  // pour chaque langue:
  // { fr: <données FR>, en: <données EN> }
  const itemsMap = new Map();

  const primaryTable = `category_sub_lang`;
  const primaryField = `id_subcat`;
  const primaryFields = [
    'id_subcat',
    'lang',
    'name',
    'description',
    'cleanstring',
    'image'
  ];
  const secondaryTable = `category_sub`;
  const secondaryField = `id_cat`;
  const secondaryFields = [
    'id_cat',
    'id_subcat',
  ];

  await db.each(`
    SELECT
      ${primaryFields.map(field => `${primaryTable}.${field}`).join(',')},
      ${secondaryFields.map(field => `${secondaryTable}.${field}`).join(',')}
    FROM
      ${primaryTable}
    INNER JOIN
      ${secondaryTable}
    ON  
      ${primaryTable}.${primaryField} = ${secondaryTable}.${primaryField}
    WHERE
      ${secondaryTable}.${secondaryField} = ${cat_id}
    `, [], (err, row) => {

    const _item = {
      id_cat: row.id_cat,
      id_subcat: row.id_subcat,
      name: Buffer(row.name).toString('utf8'),
      lang: Buffer(row.lang).toString('utf8'),
      image: Buffer(row.image).toString('utf8'),
      description: Buffer(row.description).toString('utf8'),
      cleanstring: Buffer(row.cleanstring || []).toString('utf8'),
    };

    // on s'attend à recevoir une propriété `lang`
    // qui identifie la langue de la rangée actuelle
    const _language = _item.lang.toLowerCase();

    // Rappel: la variable `_item` correspond à la variante
    // des données dans **une seule langue**.
    // On veut obtenir un objet qui consolide les données des
    // deux langues.
    if (itemsMap.has(_item['id_subcat'])) {
      // Si une entrée existe déjà:
      // on consolide avec l'entrée qui existe déjà (1)
      // et on met à jour l'item avec la langue de l'itération actuelle (2),
      itemsMap.set(_item['id_subcat'], {
        // 1)
        ...itemsMap.get(_item['id_subcat']),
        // 2)
        [_language]: _item,
      })
    } else {
      // l'item n'est pas encore entré, on l'ajoute en
      // l'identifiant par 'id_subcat' et en spécifiant la langue
      itemsMap.set(_item['id_subcat'], {
        id_cat: _item['id_cat'],
        id_subcat: _item['id_subcat'],
        [_language]: _item
      });
    }
  });

  // maintenant que l'itération est terminée, on transforme les
  // valeurs du Map pour en faire un Array (tableau), qui peut
  // être traduit en JSON
  const items = Array.from(itemsMap.values());

  return json(items);
}
