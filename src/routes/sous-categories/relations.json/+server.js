import { json } from '@sveltejs/kit';
import { openDB } from '$lib/_db';

export async function GET() {

  const db = await openDB();

  let items = [];

  const relationsTable = `category_sub`;
  const relationsFields = [
    'id_cat',
    'id_subcat',
  ];
  const excludeId = 6; // équipe ID, à exclure

  await db.each(`
    SELECT
      ${relationsFields.map(field => `${relationsTable}.${field}`).join(',')}
    FROM
      ${relationsTable}
    WHERE
      ${relationsTable}.${relationsFields[0]} != ${excludeId}
    `, [], (err, row) => {

    let item = {
      id_cat: row.id_cat,
      id_subcat: row.id_subcat,
    }
    items.push(item);
  });

  return json(items);
}
