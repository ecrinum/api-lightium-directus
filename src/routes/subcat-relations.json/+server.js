import { json } from '@sveltejs/kit';
import { openDB } from '$lib/_db';

export async function GET() {
  const db = await openDB();

  let subcategories = [];

  const relationsTable = `category_sub`;
  const relationsFields = [
    'id_cat',
    'id_subcat',
  ];

  await db.each(`
    SELECT
      ${relationsFields.map(field => `${relationsTable}.${field}`).join(',')}
    FROM
      ${relationsTable}
    `, [], (err, row) => {

    let item = {
      id_cat: row.id_cat,
      id_subcat: row.id_subcat,
    }
    subcategories.push(item);
  });

  // group by id_cat
  subcategories.sort((a, b) => {
    return a.id_cat - b.id_cat
  });

  return json(subcategories);
}
